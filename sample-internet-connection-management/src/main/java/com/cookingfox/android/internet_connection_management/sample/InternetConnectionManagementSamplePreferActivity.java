package com.cookingfox.android.internet_connection_management.sample;

import android.os.Bundle;
import android.preference.PreferenceActivity;

import com.cookingfox.android.prefer.impl.prefer.AndroidPreferProvider;
import com.cookingfox.android.prefer_fragment.impl.PreferFragment;

/**
 * Preference activity using auto-generated fragment from Prefer library.
 */
public class InternetConnectionManagementSamplePreferActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PreferFragment fragment = PreferFragment.create(AndroidPreferProvider.getDefault(this));

        getFragmentManager()
                .beginTransaction()
                .replace(android.R.id.content, fragment)
                .commit();
    }

}
