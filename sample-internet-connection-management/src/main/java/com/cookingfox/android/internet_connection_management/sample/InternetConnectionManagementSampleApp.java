package com.cookingfox.android.internet_connection_management.sample;

import com.cookingfox.android.core_component.impl.android.application.AndroidComponentApplication;
import com.cookingfox.android.internet_connection_management.component.InternetConnectionManagementComponent;

public class InternetConnectionManagementSampleApp extends AndroidComponentApplication {

    @Override
    protected void onApplicationInitialized() {
        getComponentManager().loadComponent(InternetConnectionManagementComponent.class);
    }

}
