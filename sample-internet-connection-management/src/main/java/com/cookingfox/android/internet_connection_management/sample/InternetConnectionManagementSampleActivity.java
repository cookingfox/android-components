package com.cookingfox.android.internet_connection_management.sample;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.cookingfox.android.core_component.impl.android.activity.ComponentsAwareAppCompatActivity;
import com.cookingfox.android.internet_connection_management.domain.state.InternetConnectionManagementState;
import com.cookingfox.android.internet_connection_management.domain.state.InternetConnectionManagementStateObserver;
import com.cookingfox.chefling.api.CheflingContainer;
import com.cookingfox.lapasse.api.state.observer.StateChanged;
import com.cookingfox.logging.Logger;

import rx.android.schedulers.AndroidSchedulers;

public class InternetConnectionManagementSampleActivity extends ComponentsAwareAppCompatActivity {

    private TextView connection_ok;
    private TextView last_ping_status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(view -> showSettings());

        connection_ok = (TextView) findViewById(R.id.connection_ok);
        last_ping_status = (TextView) findViewById(R.id.last_ping_status);
    }

    @Override
    public void onComponentsLoaded(CheflingContainer container) {
        InternetConnectionManagementStateObserver stateObserver =
                container.getInstance(InternetConnectionManagementStateObserver.class);

        InternetConnectionManagementState state = stateObserver.getCurrentState();

        setConnectionOkText(state.isConnectionOk());
        setLastPingStatusText(state.lastPingResponse().statusCode());

        stateObserver.observeStateChanges()
                .map(StateChanged::getState)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(changedState -> {
                    Logger.info(changedState.lastPingResponse());
                    setConnectionOkText(changedState.isConnectionOk());
                    setLastPingStatusText(changedState.lastPingResponse().statusCode());
                });
    }

    private void setLastPingStatusText(int statusCode) {
        last_ping_status.setText(String.format(getString(R.string.last_ping_status), statusCode));
    }

    private void setConnectionOkText(boolean connectionOk) {
        connection_ok.setText(String.format(getString(R.string.connection_ok), connectionOk));
    }

    private void showSettings() {
        startActivity(new Intent(this, InternetConnectionManagementSamplePreferActivity.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            showSettings();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
