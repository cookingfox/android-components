package com.cookingfox.android.network_adapter_management.android.driver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;

import com.cookingfox.android.core_component.api.android.context.AppContext;
import com.cookingfox.android.network_adapter_management.domain.driver.WifiNetworkDriver;
import com.cookingfox.android.network_adapter_management.domain.vo.ConnectivityChangeInfo;
import com.cookingfox.android.network_adapter_management.domain.vo.WifiAdapterInfo;
import com.cookingfox.android.network_adapter_management.domain.vo.WifiConnectionInfo;
import com.cookingfox.logging.Logger;

import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Implementation of {@link WifiNetworkDriver} that uses the Android system services.
 */
public class AndroidWifiNetworkDriver implements WifiNetworkDriver {

    /**
     * Intent action of connectivity change.
     */
    private final static String INTENT_ACTION = ConnectivityManager.CONNECTIVITY_ACTION;

    //----------------------------------------------------------------------------------------------
    // PROPERTIES
    //----------------------------------------------------------------------------------------------

    /**
     * Wrapper of Android application context.
     */
    private final AppContext appContext;

    /**
     * Android connectivity manager.
     */
    private final ConnectivityManager connectivityManager;

    /**
     * Rx Subject that is used to publish connectivity changes.
     */
    private final PublishSubject<ConnectivityChangeInfo> connectivityChangePublisher = PublishSubject.create();

    /**
     * Android wifi manager.
     */
    private final WifiManager wifiManager;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public AndroidWifiNetworkDriver(AppContext appContext,
                                    ConnectivityManager connectivityManager,
                                    WifiManager wifiManager) {
        this.appContext = appContext;
        this.connectivityManager = connectivityManager;
        this.wifiManager = wifiManager;
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public void disableWifiAdapter() {
        wifiManager.setWifiEnabled(false);
    }

    @Override
    public void enableWifiAdapter() {
        wifiManager.setWifiEnabled(true);
    }

    @Override
    public WifiAdapterInfo getCurrentWifiAdapterInfo() {
        return new WifiAdapterInfo.Builder()
                .isEnabled(isWifiAdapterEnabled())
                .build();
    }

    @Override
    public WifiConnectionInfo getCurrentWifiConnectionInfo() {
        NetworkInfo info = connectivityManager.getActiveNetworkInfo();

        boolean isAvailable = false;
        boolean isConnected = false;

        // is this wifi network info?
        if (info != null && info.getType() == ConnectivityManager.TYPE_WIFI) {
            isAvailable = info.isAvailable();
            isConnected = info.isConnected();
        } else {
            Logger.warn("No active network or other than wifi | Network info: %s", info);
        }

        return new WifiConnectionInfo.Builder()
                .isAvailable(isAvailable)
                .isConnected(isConnected)
                .build();
    }

    @Override
    public boolean isWifiAdapterEnabled() {
        return wifiManager.isWifiEnabled();
    }

    @Override
    public Observable<ConnectivityChangeInfo> observeConnectionChanges() {
        return connectivityChangePublisher;
    }

    @Override
    public void register() {
        appContext.registerReceiver(connectivityChangeReceiver, new IntentFilter(INTENT_ACTION));
    }

    @Override
    public void unregister() {
        appContext.unregisterReceiver(connectivityChangeReceiver);
    }

    //----------------------------------------------------------------------------------------------
    // PROTECTED METHODS
    //----------------------------------------------------------------------------------------------

    /**
     * Handle the connectivity change.
     *
     * @param intent The intent.
     */
    protected void handleConnectivityChange(Intent intent) {
        ConnectivityChangeInfo info = new ConnectivityChangeInfo.Builder()
                .extraInfo(intent.getStringExtra(ConnectivityManager.EXTRA_EXTRA_INFO))
                .build();

        // emit change
        connectivityChangePublisher.onNext(info);
    }

    /**
     * Connectivity change broadcast receiver.
     */
    protected final BroadcastReceiver connectivityChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent == null) {
                Logger.info("Intent was null");
            } else if (!INTENT_ACTION.equals(intent.getAction())) {
                Logger.info("Unexpected intent action: %s", intent.getAction());
            } else {
                handleConnectivityChange(intent);
            }
        }
    };

}
