package com.cookingfox.android.network_adapter_management.android.preferences;

import com.cookingfox.android.network_adapter_management.domain.preferences.NetworkAdapterManagementPreferences;
import com.cookingfox.android.prefer_rx.api.pref.RxPrefGroup;
import com.cookingfox.android.prefer_rx.api.pref.typed.BooleanRxPref;
import com.cookingfox.android.prefer_rx.impl.pref.AndroidRxPrefGroup;
import com.cookingfox.android.prefer_rx.impl.pref.typed.AndroidBooleanRxPref;
import com.cookingfox.android.prefer_rx.impl.prefer.AndroidRxPrefer;

/**
 * Android Prefer implementation of {@link NetworkAdapterManagementPreferences}.
 */
public class AndroidNetworkAdapterManagementPreferences
        implements NetworkAdapterManagementPreferences {

    private final AndroidRxPrefGroup<Key> prefGroup;
    private final AndroidBooleanRxPref<Key> autoEnableWifiAdapter;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public AndroidNetworkAdapterManagementPreferences(AndroidRxPrefer prefer) {
        // group
        prefGroup = prefer.addNewGroup(Key.class);
        prefGroup.setTitle("Network Adapter Management");
        prefGroup.setSummary("Manage network adapters");

        // auto enable wifi
        autoEnableWifiAdapter = prefGroup.addNewBoolean(Key.AutoEnableWifiAdapter, true);
        autoEnableWifiAdapter.setTitle("Auto-enable wifi adapter");
        autoEnableWifiAdapter.setSummary("Whether to automatically enable the wifi adapter");
    }

    //----------------------------------------------------------------------------------------------
    // GETTERS
    //----------------------------------------------------------------------------------------------

    @Override
    public BooleanRxPref<Key> autoEnableWifiAdapter() {
        return autoEnableWifiAdapter;
    }

    @Override
    public RxPrefGroup<Key> getPrefGroup() {
        return prefGroup;
    }

}
