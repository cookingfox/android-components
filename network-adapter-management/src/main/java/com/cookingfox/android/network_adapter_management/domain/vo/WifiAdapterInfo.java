package com.cookingfox.android.network_adapter_management.domain.vo;

import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;

import org.immutables.value.Value;

/**
 * Contains wifi adapter information.
 */
@DefaultValueStyle
@Value.Immutable
public interface WifiAdapterInfo {

    /**
     * @return Whether the wifi adapter is enabled.
     */
    boolean isEnabled();

    class Builder extends ImmutableWifiAdapterInfo.Builder {
    }

}
