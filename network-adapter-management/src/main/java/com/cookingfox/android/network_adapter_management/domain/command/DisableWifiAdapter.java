package com.cookingfox.android.network_adapter_management.domain.command;

import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;
import com.cookingfox.lapasse.api.command.Command;

import org.immutables.value.Value;

@DefaultValueStyle
@Value.Immutable
public interface DisableWifiAdapter extends Command {

    class Builder extends ImmutableDisableWifiAdapter.Builder {
    }

}
