package com.cookingfox.android.network_adapter_management.testing.domain.driver;

import com.cookingfox.android.network_adapter_management.domain.driver.WifiNetworkDriver;
import com.cookingfox.android.network_adapter_management.domain.vo.ConnectivityChangeInfo;
import com.cookingfox.android.network_adapter_management.domain.vo.WifiAdapterInfo;
import com.cookingfox.android.network_adapter_management.domain.vo.WifiConnectionInfo;

import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Mock implementation of {@link WifiNetworkDriver}.
 */
public class MockWifiNetworkDriver implements WifiNetworkDriver {

    public int called_disableWifiAdapter = 0;
    public int called_enableWifiAdapter = 0;
    public int called_register = 0;
    public int called_unregister = 0;
    public PublishSubject<ConnectivityChangeInfo> connectionChangeSubject = PublishSubject.create();
    public boolean mock_wifiConnectionIsOk = false;
    public boolean mock_wifiAdapterIsEnabled = false;

    @Override
    public void disableWifiAdapter() {
        mock_wifiAdapterIsEnabled = false;
        called_disableWifiAdapter++;
    }

    @Override
    public void enableWifiAdapter() {
        mock_wifiAdapterIsEnabled = true;
        called_enableWifiAdapter++;
    }

    @Override
    public WifiAdapterInfo getCurrentWifiAdapterInfo() {
        return new WifiAdapterInfo.Builder()
                .isEnabled(mock_wifiAdapterIsEnabled)
                .build();
    }

    @Override
    public WifiConnectionInfo getCurrentWifiConnectionInfo() {
        return new WifiConnectionInfo.Builder()
                .isAvailable(mock_wifiConnectionIsOk)
                .isConnected(mock_wifiConnectionIsOk)
                .build();
    }

    @Override
    public boolean isWifiAdapterEnabled() {
        return mock_wifiAdapterIsEnabled;
    }

    @Override
    public Observable<ConnectivityChangeInfo> observeConnectionChanges() {
        return connectionChangeSubject;
    }

    @Override
    public void register() {
        called_register++;
    }

    @Override
    public void unregister() {
        called_unregister++;
    }

}
