package com.cookingfox.android.network_adapter_management.domain.event;

import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;
import com.cookingfox.android.network_adapter_management.domain.vo.WifiAdapterInfo;

import org.immutables.value.Value;

@DefaultValueStyle
@Value.Immutable
public interface WifiAdapterInfoUpdated extends WifiInfoUpdated {

    WifiAdapterInfo info();

    class Builder extends ImmutableWifiAdapterInfoUpdated.Builder {
    }

}
