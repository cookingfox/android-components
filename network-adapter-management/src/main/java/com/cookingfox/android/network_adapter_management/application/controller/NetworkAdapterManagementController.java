package com.cookingfox.android.network_adapter_management.application.controller;

import com.cookingfox.android.network_adapter_management.domain.manager.WifiAdapterManager;
import com.cookingfox.chefling.api.CheflingLifecycle;

/**
 * Controls the network adapter managers.
 */
public class NetworkAdapterManagementController implements CheflingLifecycle {

    private final WifiAdapterManager wifiAdapterManager;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public NetworkAdapterManagementController(WifiAdapterManager wifiAdapterManager) {
        this.wifiAdapterManager = wifiAdapterManager;
    }

    //----------------------------------------------------------------------------------------------
    // LIFECYCLE
    //----------------------------------------------------------------------------------------------

    @Override
    public void initialize() {
        wifiAdapterManager.register();
    }

    @Override
    public void dispose() {
        wifiAdapterManager.unregister();
    }

}
