package com.cookingfox.android.network_adapter_management.domain.preferences;

import com.cookingfox.android.prefer_rx.api.pref.RxPrefs;
import com.cookingfox.android.prefer_rx.api.pref.typed.BooleanRxPref;

/**
 * Network adapter management preferences.
 */
public interface NetworkAdapterManagementPreferences
        extends RxPrefs<NetworkAdapterManagementPreferences.Key> {

    /**
     * Preference keys.
     */
    enum Key {
        AutoEnableWifiAdapter
    }

    /**
     * @return Whether the wifi adapter should be enabled on start.
     */
    BooleanRxPref<Key> autoEnableWifiAdapter();

}
