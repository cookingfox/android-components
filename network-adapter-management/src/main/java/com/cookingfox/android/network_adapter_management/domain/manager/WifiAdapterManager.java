package com.cookingfox.android.network_adapter_management.domain.manager;

import com.cookingfox.android.network_adapter_management.domain.command.DisableWifiAdapter;
import com.cookingfox.android.network_adapter_management.domain.command.EnableWifiAdapter;
import com.cookingfox.android.network_adapter_management.domain.command.RequestWifiDriverInfo;
import com.cookingfox.android.network_adapter_management.domain.driver.WifiNetworkDriver;
import com.cookingfox.android.network_adapter_management.domain.event.WifiAdapterInfoUpdated;
import com.cookingfox.android.network_adapter_management.domain.event.WifiConnectionInfoUpdated;
import com.cookingfox.android.network_adapter_management.domain.event.WifiInfoUpdated;
import com.cookingfox.android.network_adapter_management.domain.facade.NetworkAdapterManagementFacade;
import com.cookingfox.android.network_adapter_management.domain.preferences.NetworkAdapterManagementPreferences;
import com.cookingfox.android.network_adapter_management.domain.state.NetworkAdapterManagementState;
import com.cookingfox.android.network_adapter_management.domain.vo.ConnectivityChangeInfo;
import com.cookingfox.android.network_adapter_management.domain.vo.WifiAdapterInfo;
import com.cookingfox.lapasse.annotation.HandleCommand;
import com.cookingfox.lapasse.annotation.HandleEvent;
import com.cookingfox.lapasse.impl.helper.LaPasse;

import java.util.Collection;
import java.util.LinkedHashSet;

import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

/**
 * Provides access to the wifi adapter and monitors the wifi connection.
 */
public class WifiAdapterManager {

    private final NetworkAdapterManagementFacade facade;
    private final NetworkAdapterManagementPreferences preferences;
    private final CompositeSubscription subs = new CompositeSubscription();
    private final WifiNetworkDriver wifiNetworkDriver;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public WifiAdapterManager(NetworkAdapterManagementFacade facade,
                              NetworkAdapterManagementPreferences preferences,
                              WifiNetworkDriver wifiNetworkDriver) {
        this.facade = facade;
        this.preferences = preferences;
        this.wifiNetworkDriver = wifiNetworkDriver;
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    /**
     * Disable the wifi adapter.
     */
    public void disableWifiAdapter() {
        facade.handleCommand(new DisableWifiAdapter.Builder().build());
    }

    /**
     * Enable the wifi adapter.
     */
    public void enableWifiAdapter() {
        facade.handleCommand(new EnableWifiAdapter.Builder().build());
    }

    /**
     * Register the manager:
     * - Registers network driver.
     * - Requests the current wifi driver info.
     * - Subscribes to connectivity changes.
     * - Automatically enables wifi adapter if necessary.
     *
     * @see NetworkAdapterManagementPreferences#autoEnableWifiAdapter()
     */
    public void register() {
        LaPasse.mapHandlers(this, facade);

        // request current driver state
        facade.handleCommand(new RequestWifiDriverInfo.Builder().build());

        // observe wifi connection changes
        subs.add(wifiNetworkDriver.observeConnectionChanges().subscribe(onConnectivityChanged));

        // register driver
        wifiNetworkDriver.register();

        // auto enable wifi adapter if preferred
        if (preferences.autoEnableWifiAdapter().getValue()) {
            enableWifiAdapter();
        }
    }

    /**
     * Unregisters the manager.
     */
    public void unregister() {
        subs.unsubscribe();

        wifiNetworkDriver.unregister();
    }

    //----------------------------------------------------------------------------------------------
    // HANDLERS
    //----------------------------------------------------------------------------------------------

    /**
     * Disable the wifi adapter, or ignore if already disabled.
     *
     * @param state The current state.
     * @return Wifi adapter information updated event.
     */
    @HandleCommand(command = DisableWifiAdapter.class)
    WifiAdapterInfoUpdated disableWifiAdapter(NetworkAdapterManagementState state) {
        // already disabled: ignore
        if (!state.isWifiAdapterEnabled()) {
            return null;
        }

        wifiNetworkDriver.disableWifiAdapter();

        return createWifiAdapterInfoUpdatedEvent();
    }

    /**
     * Enable the wifi adapter, or ignore if already enabled.
     *
     * @param state The current state.
     * @return Wifi adapter information updated event.
     */
    @HandleCommand(command = EnableWifiAdapter.class)
    WifiAdapterInfoUpdated enableWifiAdapter(NetworkAdapterManagementState state) {
        // already enabled: ignore
        if (state.isWifiAdapterEnabled()) {
            return null;
        }

        wifiNetworkDriver.enableWifiAdapter();

        return createWifiAdapterInfoUpdatedEvent();
    }

    /**
     * Request the current {@link WifiNetworkDriver} information and use it to update the state.
     *
     * @return A collection of wifi info updated events.
     */
    @HandleCommand(command = RequestWifiDriverInfo.class)
    Collection<WifiInfoUpdated> requestWifiDriverInfo() {
        Collection<WifiInfoUpdated> infoUpdates = new LinkedHashSet<>();

        // get wifi adapter info
        infoUpdates.add(new WifiAdapterInfoUpdated.Builder()
                .info(wifiNetworkDriver.getCurrentWifiAdapterInfo())
                .build());

        // get wifi connection info
        infoUpdates.add(new WifiConnectionInfoUpdated.Builder()
                .info(wifiNetworkDriver.getCurrentWifiConnectionInfo())
                .build());

        return infoUpdates;
    }

    /**
     * Process a wifi info updated event and update the state accordingly.
     *
     * @param state The current state.
     * @param event The event to process.
     * @return Updated state object.
     */
    @HandleEvent
    NetworkAdapterManagementState onWifiInfoUpdated(NetworkAdapterManagementState state, WifiInfoUpdated event) {
        NetworkAdapterManagementState.Builder builder = new NetworkAdapterManagementState.Builder()
                .from(state);

        if (event instanceof WifiAdapterInfoUpdated) {
            builder.wifiAdapterInfo(((WifiAdapterInfoUpdated) event).info());
        } else if (event instanceof WifiConnectionInfoUpdated) {
            builder.wifiConnectionInfo(((WifiConnectionInfoUpdated) event).info());
        } else {
            throw new UnsupportedOperationException("Unsupported wifi info updated event: " + event);
        }

        return builder.build();
    }

    //----------------------------------------------------------------------------------------------
    // PROTECTED METHODS
    //----------------------------------------------------------------------------------------------

    /**
     * @return Uses the current driver info to create a wifi adapter info updated event.
     */
    protected WifiAdapterInfoUpdated createWifiAdapterInfoUpdatedEvent() {
        // use driver to request enabled state to get most accurate value
        WifiAdapterInfo wifiAdapterInfo = new WifiAdapterInfo.Builder()
                .isEnabled(wifiNetworkDriver.isWifiAdapterEnabled())
                .build();

        return new WifiAdapterInfoUpdated.Builder()
                .info(wifiAdapterInfo)
                .build();
    }

    /**
     * Subscriber for connectivity changes.
     */
    protected final Action1<ConnectivityChangeInfo> onConnectivityChanged = new Action1<ConnectivityChangeInfo>() {
        @Override
        public void call(ConnectivityChangeInfo info) {
            facade.handleCommand(new RequestWifiDriverInfo.Builder().build());
        }
    };

}
