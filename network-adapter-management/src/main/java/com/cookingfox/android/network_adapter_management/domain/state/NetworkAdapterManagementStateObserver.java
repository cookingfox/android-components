package com.cookingfox.android.network_adapter_management.domain.state;

import com.cookingfox.lapasse.api.state.observer.RxStateObserver;

/**
 * Concrete state observer interface for {@link NetworkAdapterManagementState}.
 */
public interface NetworkAdapterManagementStateObserver extends
        RxStateObserver<NetworkAdapterManagementState> {
}
