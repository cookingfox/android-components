package com.cookingfox.android.network_adapter_management.domain.command;

import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;
import com.cookingfox.lapasse.api.command.Command;

import org.immutables.value.Value;

/**
 * Request information about the current state of the wifi connection and adapter.
 */
@DefaultValueStyle
@Value.Immutable
public interface RequestWifiDriverInfo extends Command {

    class Builder extends ImmutableRequestWifiDriverInfo.Builder {
    }

}
