package com.cookingfox.android.network_adapter_management.domain.event;

import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;
import com.cookingfox.android.network_adapter_management.domain.vo.WifiConnectionInfo;

import org.immutables.value.Value;

@DefaultValueStyle
@Value.Immutable
public interface WifiConnectionInfoUpdated extends WifiInfoUpdated {

    WifiConnectionInfo info();

    class Builder extends ImmutableWifiConnectionInfoUpdated.Builder {
    }

}
