package com.cookingfox.android.network_adapter_management.component;

import com.cookingfox.android.core_component.api.component.Component;
import com.cookingfox.android.core_component.api.component.config.ComponentCheflingConfig;
import com.cookingfox.android.core_component.api.component.manager.ComponentManager;
import com.cookingfox.android.network_adapter_management.application.controller.NetworkAdapterManagementController;
import com.cookingfox.android.network_adapter_management.component.config.NetworkAdapterManagementCheflingConfig;
import com.cookingfox.android.network_adapter_management.domain.facade.NetworkAdapterManagementFacade;
import com.cookingfox.android.network_adapter_management.domain.preferences.NetworkAdapterManagementPreferences;
import com.cookingfox.android.prefer.api.pref.Prefs;
import com.cookingfox.chefling.api.CheflingContainer;
import com.cookingfox.lapasse.api.facade.Facade;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Network adapter component.
 */
public class NetworkAdapterManagementComponent implements Component {

    private final CheflingContainer container;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public NetworkAdapterManagementComponent(CheflingContainer container) {
        this.container = checkNotNull(container);
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public ComponentCheflingConfig getCheflingConfig() {
        return new NetworkAdapterManagementCheflingConfig();
    }

    @Override
    public Facade getLaPasseFacade() {
        return container.getInstance(NetworkAdapterManagementFacade.class);
    }

    @Override
    public Prefs getPrefs() {
        return container.getInstance(NetworkAdapterManagementPreferences.class);
    }

    @Override
    public void loadComponent(ComponentManager componentManager) {
        // no component dependencies
    }

    @Override
    public void registerComponent() {
        container.getInstance(NetworkAdapterManagementController.class);
    }

    @Override
    public void unregisterComponent() {
        // note: implicit de-registration
        container.removeInstanceAndMapping(NetworkAdapterManagementController.class);
    }

}
