package com.cookingfox.android.network_adapter_management.domain.vo;

import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;

import org.immutables.value.Value;

/**
 * Provides information about what changed in the network connectivity.
 */
@DefaultValueStyle
@Value.Immutable
public interface ConnectivityChangeInfo {

    /**
     * @return Extra info about what changed.
     */
    String extraInfo();

    class Builder extends ImmutableConnectivityChangeInfo.Builder {
    }

}
