package com.cookingfox.android.network_adapter_management.domain.state;

import com.cookingfox.android.network_adapter_management.domain.vo.WifiAdapterInfo;
import com.cookingfox.android.network_adapter_management.domain.vo.WifiConnectionInfo;
import com.cookingfox.lapasse.api.state.State;

import org.immutables.value.Value;

/**
 * Immutable state object for network adapter management.
 */
@Value.Immutable
public abstract class NetworkAdapterManagementState implements State {

    //----------------------------------------------------------------------------------------------
    // IMMUTABLE PROPERTIES
    //----------------------------------------------------------------------------------------------

    /**
     * @return Information about the wifi adapter.
     */
    public abstract WifiAdapterInfo wifiAdapterInfo();

    /**
     * @return Information about the wifi connection.
     */
    public abstract WifiConnectionInfo wifiConnectionInfo();

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    /**
     * @return Whether the wifi adapter is enabled.
     */
    public boolean isWifiAdapterEnabled() {
        return wifiAdapterInfo().isEnabled();
    }

    /**
     * @return Whether a connection to a wifi network is established.
     */
    public boolean isWifiConnected() {
        WifiConnectionInfo connection = wifiConnectionInfo();

        return isWifiAdapterEnabled() && connection.isAvailable() && connection.isConnected();
    }

    //----------------------------------------------------------------------------------------------
    // STATIC METHODS
    //----------------------------------------------------------------------------------------------

    /**
     * @return The initial state object for network adapter management.
     */
    public static NetworkAdapterManagementState createInitialState() {
        WifiAdapterInfo wifiAdapterInfo = new WifiAdapterInfo.Builder()
                .isEnabled(false)
                .build();

        WifiConnectionInfo wifiConnectionInfo = new WifiConnectionInfo.Builder()
                .isAvailable(false)
                .isConnected(false)
                .build();

        return new Builder()
                .wifiAdapterInfo(wifiAdapterInfo)
                .wifiConnectionInfo(wifiConnectionInfo)
                .build();
    }

    //----------------------------------------------------------------------------------------------
    // IMMUTABLE BUILDER
    //----------------------------------------------------------------------------------------------

    public static class Builder extends ImmutableNetworkAdapterManagementState.Builder {
    }

}
