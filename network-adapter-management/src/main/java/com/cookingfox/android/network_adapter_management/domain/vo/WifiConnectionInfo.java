package com.cookingfox.android.network_adapter_management.domain.vo;

import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;

import org.immutables.value.Value;

/**
 * Contains wifi connection information.
 */
@DefaultValueStyle
@Value.Immutable
public interface WifiConnectionInfo {

    /**
     * @return Whether a wifi connection is available.
     */
    boolean isAvailable();

    /**
     * @return Whether a wifi connection is established.
     */
    boolean isConnected();

    class Builder extends ImmutableWifiConnectionInfo.Builder {
    }
}
