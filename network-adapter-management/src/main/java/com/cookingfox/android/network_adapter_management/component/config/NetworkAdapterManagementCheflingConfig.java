package com.cookingfox.android.network_adapter_management.component.config;

import com.cookingfox.android.core_component.api.component.config.ComponentCheflingConfig;
import com.cookingfox.android.network_adapter_management.android.driver.AndroidWifiNetworkDriver;
import com.cookingfox.android.network_adapter_management.android.preferences.AndroidNetworkAdapterManagementPreferences;
import com.cookingfox.android.network_adapter_management.domain.driver.WifiNetworkDriver;
import com.cookingfox.android.network_adapter_management.domain.facade.NetworkAdapterManagementFacade;
import com.cookingfox.android.network_adapter_management.domain.preferences.NetworkAdapterManagementPreferences;
import com.cookingfox.android.network_adapter_management.domain.state.NetworkAdapterManagementState;
import com.cookingfox.android.network_adapter_management.domain.state.NetworkAdapterManagementStateObserver;
import com.cookingfox.android.network_adapter_management.testing.domain.driver.MockWifiNetworkDriver;
import com.cookingfox.chefling.api.CheflingBuilder;
import com.cookingfox.chefling.api.CheflingConfig;
import com.cookingfox.chefling.impl.Chefling;
import com.cookingfox.lapasse.impl.facade.LaPasseRxFacade;

/**
 * Chefling configuration for network adapter management component.
 */
public class NetworkAdapterManagementCheflingConfig implements ComponentCheflingConfig {

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public CheflingBuilder createCheflingBuilder() {
        return Chefling.createBuilder()
                .addConfig(domainConfig);
    }

    @Override
    public CheflingConfig createDeviceConfig() {
        return deviceConfig;
    }

    @Override
    public CheflingConfig createEmulatorConfig() {
        return deviceConfig;
    }

    @Override
    public CheflingConfig createTestConfig() {
        return testConfig;
    }

    //----------------------------------------------------------------------------------------------
    // DEVICE CONFIGURATION
    //----------------------------------------------------------------------------------------------

    public final CheflingConfig deviceConfig = container ->
            container.mapType(WifiNetworkDriver.class, AndroidWifiNetworkDriver.class);

    //----------------------------------------------------------------------------------------------
    // DOMAIN CONFIGURATION
    //----------------------------------------------------------------------------------------------

    public final CheflingConfig domainConfig = container -> {
        // maps factory for facade
        container.mapFactory(NetworkAdapterManagementFacade.class, c -> {
            NetworkAdapterManagementState initialState = NetworkAdapterManagementState.createInitialState();

            LaPasseRxFacade<NetworkAdapterManagementState> facade = new LaPasseRxFacade.Builder<>(initialState).build();

            return new NetworkAdapterManagementFacade(facade);
        });

        // maps state observer to facade
        container.mapType(NetworkAdapterManagementStateObserver.class, NetworkAdapterManagementFacade.class);

        // concrete preferences
        container.mapType(NetworkAdapterManagementPreferences.class, AndroidNetworkAdapterManagementPreferences.class);
    };

    //----------------------------------------------------------------------------------------------
    // TEST CONFIGURATION
    //----------------------------------------------------------------------------------------------

    public final CheflingConfig testConfig = container ->
            container.mapType(WifiNetworkDriver.class, MockWifiNetworkDriver.class);

}
