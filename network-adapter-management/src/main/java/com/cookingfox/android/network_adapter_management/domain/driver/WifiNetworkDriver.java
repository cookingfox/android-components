package com.cookingfox.android.network_adapter_management.domain.driver;

import com.cookingfox.android.network_adapter_management.domain.vo.ConnectivityChangeInfo;
import com.cookingfox.android.network_adapter_management.domain.vo.WifiAdapterInfo;
import com.cookingfox.android.network_adapter_management.domain.vo.WifiConnectionInfo;

import rx.Observable;

/**
 * The wifi adapter driver provides functionality for managing the wifi adapter and connection.
 */
public interface WifiNetworkDriver {

    /**
     * Disable the wifi adapter.
     */
    void disableWifiAdapter();

    /**
     * Enable the wifi adapter.
     */
    void enableWifiAdapter();

    /**
     * @return The current wifi adapter info.
     */
    WifiAdapterInfo getCurrentWifiAdapterInfo();

    /**
     * @return The current wifi connection info.
     */
    WifiConnectionInfo getCurrentWifiConnectionInfo();

    /**
     * @return Whether the wifi adapter is enabled.
     */
    boolean isWifiAdapterEnabled();

    /**
     * @return An observable of when the wifi connection info changes.
     */
    Observable<ConnectivityChangeInfo> observeConnectionChanges();

    /**
     * Register the driver.
     */
    void register();

    /**
     * Unregister the driver.
     */
    void unregister();

}
