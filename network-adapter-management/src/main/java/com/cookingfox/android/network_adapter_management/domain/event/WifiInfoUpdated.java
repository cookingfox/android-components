package com.cookingfox.android.network_adapter_management.domain.event;

import com.cookingfox.lapasse.api.event.Event;

/**
 * Base event interface for when wifi network info is updated.
 */
public interface WifiInfoUpdated extends Event {

}
