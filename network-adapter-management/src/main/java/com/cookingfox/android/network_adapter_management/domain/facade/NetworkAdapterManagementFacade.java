package com.cookingfox.android.network_adapter_management.domain.facade;

import com.cookingfox.android.network_adapter_management.domain.state.NetworkAdapterManagementState;
import com.cookingfox.android.network_adapter_management.domain.state.NetworkAdapterManagementStateObserver;
import com.cookingfox.lapasse.api.facade.Facade;
import com.cookingfox.lapasse.api.facade.RxFacade;
import com.cookingfox.lapasse.impl.facade.LaPasseRxFacadeDelegate;

/**
 * Concrete {@link Facade} implementation for the {@link NetworkAdapterManagementState}.
 */
public class NetworkAdapterManagementFacade
        extends LaPasseRxFacadeDelegate<NetworkAdapterManagementState>
        implements NetworkAdapterManagementStateObserver {

    public NetworkAdapterManagementFacade(RxFacade<NetworkAdapterManagementState> facade) {
        super(facade);
    }

}
