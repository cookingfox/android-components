package com.cookingfox.android.network_adapter_management.component;

import com.cookingfox.android.core_component.api.component.manager.ComponentManager;
import com.cookingfox.android.core_component.testing.component.manager.TestComponentManager;
import com.cookingfox.android.network_adapter_management.domain.facade.NetworkAdapterManagementFacade;
import com.cookingfox.android.network_adapter_management.domain.state.NetworkAdapterManagementStateObserver;
import com.cookingfox.chefling.api.CheflingContainer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Unit tests for {@link NetworkAdapterManagementComponent}.
 */
public class NetworkAdapterManagementComponentTest {

    //----------------------------------------------------------------------------------------------
    // TEST SETUP
    //----------------------------------------------------------------------------------------------

    private ComponentManager componentManager;
    private CheflingContainer container;

    @Before
    public void setUp() throws Exception {
        container = TestComponentManager.buildContainer();
        componentManager = container.getInstance(ComponentManager.class);
    }

    @After
    public void tearDown() throws Exception {
        componentManager.unloadAllComponents();
        container.disposeContainer();
    }

    //----------------------------------------------------------------------------------------------
    // TESTS: loadComponent
    //----------------------------------------------------------------------------------------------

    @Test
    public void loadComponent_should_not_throw() throws Exception {
        componentManager.loadComponent(NetworkAdapterManagementComponent.class);
    }

    @Test
    public void loadComponent_should_map_state_observer_correctly() throws Exception {
        componentManager.loadComponent(NetworkAdapterManagementComponent.class);

        NetworkAdapterManagementStateObserver stateObserver =
                container.getInstance(NetworkAdapterManagementStateObserver.class);

        assertTrue(stateObserver instanceof NetworkAdapterManagementFacade);
    }

}
