package com.cookingfox.android.network_adapter_management.domain.manager;

import com.cookingfox.android.core_component.testing.component.manager.TestComponentManager;
import com.cookingfox.android.network_adapter_management.component.NetworkAdapterManagementComponent;
import com.cookingfox.android.network_adapter_management.domain.event.WifiAdapterInfoUpdated;
import com.cookingfox.android.network_adapter_management.domain.event.WifiConnectionInfoUpdated;
import com.cookingfox.android.network_adapter_management.domain.facade.NetworkAdapterManagementFacade;
import com.cookingfox.android.network_adapter_management.domain.preferences.NetworkAdapterManagementPreferences;
import com.cookingfox.android.network_adapter_management.domain.state.NetworkAdapterManagementState;
import com.cookingfox.android.network_adapter_management.domain.vo.ConnectivityChangeInfo;
import com.cookingfox.android.network_adapter_management.testing.domain.driver.MockWifiNetworkDriver;
import com.cookingfox.chefling.api.CheflingContainer;
import com.cookingfox.lapasse.api.event.Event;
import com.cookingfox.lapasse.api.state.observer.OnStateChanged;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for {@link WifiAdapterManager}.
 */
public class WifiAdapterManagerTest {

    //----------------------------------------------------------------------------------------------
    // TEST SETUP
    //----------------------------------------------------------------------------------------------

    private CheflingContainer container;
    private NetworkAdapterManagementFacade facade;
    private MockWifiNetworkDriver mockWifiAdapterDriver;
    private NetworkAdapterManagementPreferences preferences;
    private Event stateChangedEvent;
    private WifiAdapterManager wifiAdapterManager;

    @Before
    public void setUp() throws Exception {
        container = TestComponentManager.buildComponent(NetworkAdapterManagementComponent.class);

        mockWifiAdapterDriver = container.getInstance(MockWifiNetworkDriver.class);
        preferences = container.getInstance(NetworkAdapterManagementPreferences.class);
        wifiAdapterManager = container.getInstance(WifiAdapterManager.class);
        facade = container.getInstance(NetworkAdapterManagementFacade.class);

        facade.addStateChangedListener(onStateChanged);
    }

    @After
    public void tearDown() throws Exception {
        facade.removeStateChangedListener(onStateChanged);
        wifiAdapterManager.unregister();
        stateChangedEvent = null;
        container.disposeContainer();
    }

    //----------------------------------------------------------------------------------------------
    // TESTS: register
    //----------------------------------------------------------------------------------------------

    @Test
    public void register_should_register_driver() throws Exception {
        wifiAdapterManager.register();

        assertTrue(mockWifiAdapterDriver.called_register == 1);
    }

    @Test
    public void register_should_populate_state_with_driver_state() throws Exception {
        assertFalse(facade.getCurrentState().isWifiAdapterEnabled());

        // don't auto enable
        preferences.autoEnableWifiAdapter().setValue(false);

        // set mock states
        mockWifiAdapterDriver.mock_wifiAdapterIsEnabled = true;
        mockWifiAdapterDriver.mock_wifiConnectionIsOk = true;

        wifiAdapterManager.register();

        assertTrue(facade.getCurrentState().isWifiAdapterEnabled());
        assertTrue(facade.getCurrentState().isWifiConnected());
    }

    @Test
    public void register_should_auto_enable_adapter_if_preference_true() throws Exception {
        preferences.autoEnableWifiAdapter().setValue(true);

        wifiAdapterManager.register();

        assertTrue(mockWifiAdapterDriver.called_enableWifiAdapter == 1);
        assertTrue(facade.getCurrentState().isWifiAdapterEnabled());
        assertTrue(stateChangedEvent instanceof WifiAdapterInfoUpdated);
    }

    @Test
    public void register_should_only_auto_enable_adapter_if_not_enabled() throws Exception {
        mockWifiAdapterDriver.mock_wifiAdapterIsEnabled = true;

        preferences.autoEnableWifiAdapter().setValue(true);

        wifiAdapterManager.register();

        assertTrue(mockWifiAdapterDriver.called_enableWifiAdapter == 0);
        assertTrue(facade.getCurrentState().isWifiAdapterEnabled());
    }

    //----------------------------------------------------------------------------------------------
    // TESTS: disableWifiAdapter
    //----------------------------------------------------------------------------------------------

    @Test
    public void disableWifiAdapter_should_disable_wifi_adapter() throws Exception {
        mockWifiAdapterDriver.mock_wifiAdapterIsEnabled = true;

        wifiAdapterManager.register();

        wifiAdapterManager.disableWifiAdapter();

        assertTrue(mockWifiAdapterDriver.called_disableWifiAdapter == 1);
        assertFalse(facade.getCurrentState().isWifiAdapterEnabled());
    }

    @Test
    public void disableWifiAdapter_should_only_disable_wifi_adapter_if_enabled() throws Exception {
        mockWifiAdapterDriver.mock_wifiAdapterIsEnabled = false;

        preferences.autoEnableWifiAdapter().setValue(false);

        wifiAdapterManager.register();

        wifiAdapterManager.disableWifiAdapter();

        assertTrue(mockWifiAdapterDriver.called_disableWifiAdapter == 0);
        assertFalse(facade.getCurrentState().isWifiAdapterEnabled());
    }

    //----------------------------------------------------------------------------------------------
    // TESTS: observeConnectionChanges
    //----------------------------------------------------------------------------------------------

    @Test
    public void observeWifiConnection_should_update_state_wifi_info() throws Exception {
        wifiAdapterManager.register();

        // mock connection status
        mockWifiAdapterDriver.mock_wifiConnectionIsOk = true;

        mockWifiAdapterDriver.connectionChangeSubject.onNext(new ConnectivityChangeInfo.Builder()
                .extraInfo("MOCK EXTRA INFO")
                .build());

        assertTrue(facade.getCurrentState().isWifiConnected());
        assertTrue(stateChangedEvent instanceof WifiConnectionInfoUpdated);
    }

    //----------------------------------------------------------------------------------------------
    // HELPERS
    //----------------------------------------------------------------------------------------------

    final OnStateChanged<NetworkAdapterManagementState> onStateChanged = new OnStateChanged<NetworkAdapterManagementState>() {
        @Override
        public void onStateChanged(NetworkAdapterManagementState state, Event event) {
            stateChangedEvent = event;
        }
    };

}
