package com.cookingfox.android.core_component.sample;

import android.app.Application;

import com.cookingfox.android.core_component.compatibility.chefling.CheflingContainerProvider;
import com.cookingfox.android.core_component.impl.android.application.AndroidComponentApplication;
import com.cookingfox.android.network_adapter_management.component.NetworkAdapterManagementComponent;
import com.cookingfox.android.prefer.impl.prefer.AndroidPreferProvider;
import com.cookingfox.logging.Logger;

import org.threeten.bp.ZonedDateTime;

/**
 * Example Android Application implementation that does not extend
 * {@link AndroidComponentApplication} but uses an anonymous implementation.
 */
public class CoreComponentSampleApp extends Application {

    //----------------------------------------------------------------------------------------------
    // LIFECYCLE
    //----------------------------------------------------------------------------------------------

    @Override
    public void onCreate() {
        super.onCreate();

        AndroidComponentApplication.create(componentApp);
    }

    //----------------------------------------------------------------------------------------------
    // OVERRIDDEN COMPONENT APP FOR CALLBACKS
    //----------------------------------------------------------------------------------------------

    final AndroidComponentApplication componentApp = new AndroidComponentApplication(this) {
        @Override
        protected void onApplicationInitialized() {
            Logger.info();

            validateApplicationInitialized();

            /**
             * Example usage of using the component manager to load a component.
             */
            getComponentManager().loadComponent(NetworkAdapterManagementComponent.class);

            /**
             * Example usage of using the Chefling container to retrieve an instance of a class.
             */
            getCheflingContainer().getInstance(ExampleController.class);
        }

        @Override
        protected void onApplicationDisposed() {
            Logger.info();

            validateApplicationDisposed();
        }
    };

    //----------------------------------------------------------------------------------------------
    // PROTECTED METHODS
    //----------------------------------------------------------------------------------------------

    /**
     * Validate that application elements have initialized.
     */
    protected void validateApplicationInitialized() {
        // android three ten
        try {
            // should not throw
            ZonedDateTime.now();
            Logger.info("Android ThreeTen is initialized");
        } catch (Exception e) {
            e.printStackTrace();
            Logger.error("Android ThreeTen is not initialized");
        }

        // prefer
        try {
            // should throw (default already set)
            AndroidPreferProvider.setDefault(AndroidPreferProvider.getDefault(this));
            Logger.error("Prefer is not initialized");
        } catch (Exception e) {
            Logger.info("Prefer is initialized");
        }

        // chefling container provider
        try {
            // should not throw (also validate all mappings)
            CheflingContainerProvider.getContainer().validateContainer();
            Logger.info("Chefling container provider is initialized");
        } catch (Exception e) {
            e.printStackTrace();
            Logger.error("Chefling container provider is not initialized");
        }
    }

    /**
     * Validate that application elements have been disposed.
     */
    protected void validateApplicationDisposed() {
        // note: android three ten can not be disposed

        // prefer
        try {
            // should throw
            AndroidPreferProvider.disposeDefault();
            Logger.error("Prefer is not disposed");
        } catch (Exception e) {
            // expected exception!
            Logger.info("Prefer is disposed");
        }

        // chefling container provider
        try {
            // should throw
            CheflingContainerProvider.getContainer();
            Logger.error("Chefling container provider is not disposed");
        } catch (Exception e) {
            // expected exception!
            Logger.info("Chefling container provider is disposed");
        }
    }

}
