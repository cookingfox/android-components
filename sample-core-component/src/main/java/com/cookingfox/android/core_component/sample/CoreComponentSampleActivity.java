package com.cookingfox.android.core_component.sample;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.cookingfox.android.core_component.api.android.context.Environment;
import com.cookingfox.android.core_component.impl.android.activity.ComponentsAwareAppCompatActivity;
import com.cookingfox.chefling.api.CheflingContainer;
import com.cookingfox.logging.Logger;

public class CoreComponentSampleActivity extends ComponentsAwareAppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public void onComponentsLoaded(CheflingContainer container) {
        Logger.info("Environment: %s", container.getInstance(Environment.class).getId());
    }

}
