package com.cookingfox.android.core_component.sample;

import com.cookingfox.chefling.api.CheflingLifecycle;
import com.cookingfox.logging.Logger;

/**
 * Example class which implements the Chefling lifecycle.
 */
public class ExampleController implements CheflingLifecycle {

    @Override
    public void initialize() {
        Logger.info();
    }

    @Override
    public void dispose() {
        Logger.info();
    }

}
