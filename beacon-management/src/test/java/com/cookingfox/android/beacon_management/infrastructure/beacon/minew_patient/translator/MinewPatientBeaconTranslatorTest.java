package com.cookingfox.android.beacon_management.infrastructure.beacon.minew_patient.translator;

import com.cookingfox.chefling.api.CheflingContainer;
import com.cookingfox.chefling.impl.Chefling;
import com.cookingfox.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static com.cookingfox.android.beacon_management.testing.beacon.TestAdPacketProvider.MINEW_BATTERY_LEVEL_PACKET_1;
import static com.cookingfox.android.beacon_management.testing.beacon.TestAdPacketProvider.MINEW_CONTROL_STATE_PACKET_1;
import static com.cookingfox.android.beacon_management.testing.beacon.TestAdPacketProvider.MINEW_PACKET_1;
import static com.cookingfox.android.beacon_management.testing.beacon.TestAdPacketProvider.MINEW_PRODUCT_CODE_PACKET_1;
import static com.cookingfox.android.beacon_management.testing.beacon.TestAdPacketProvider.SNF_PACKET_1;
import static com.cookingfox.android.beacon_management.testing.beacon.TestAdPacketProvider.SNF_PACKET_2;
import static com.cookingfox.android.beacon_management.testing.beacon.TestAdPacketProvider.SNF_PACKET_3;
import static com.cookingfox.android.beacon_management.testing.beacon.TestAdPacketProvider.UNKNOWN_PACKET_1;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public class MinewPatientBeaconTranslatorTest {

    private CheflingContainer container;
    private MinewPatientBeaconTranslator minewTranslator;

    //----------------------------------------------------------------------------------------------
    // SETUP
    //----------------------------------------------------------------------------------------------

    @Before
    public void setUp() throws Exception {
        Logger.initTest();

        container = Chefling.createBuilder().buildContainer();

        minewTranslator = container.getInstance(MinewPatientBeaconTranslator.class);
    }

    @After
    public void tearDown() throws Exception {
        container.disposeContainer();
    }

    //----------------------------------------------------------------------------------------------
    // MINEW PATIENT ADV_1
    //----------------------------------------------------------------------------------------------

    @Test
    public void should_be_of_minew_patient_beacon_type() throws Exception {
        assertTrue(minewTranslator.isOfBeaconType(MINEW_PACKET_1));
    }

    @Test
    public void should_return_minew_patient_beacon_battery_level() throws Exception {
        assertEquals(MINEW_BATTERY_LEVEL_PACKET_1, minewTranslator.getBatteryLevel(MINEW_PACKET_1));
    }

    @Test
    public void should_return_minew_patient_beacon_control_state() throws Exception {
        assertEquals(MINEW_CONTROL_STATE_PACKET_1, minewTranslator.getState(MINEW_PACKET_1));
    }

    @Test
    public void should_return_minew_patient_beacon_product_code() throws Exception {
        assertEquals(MINEW_PRODUCT_CODE_PACKET_1, minewTranslator.getProductCode(MINEW_PACKET_1));
    }

    @Test
    public void stick_n_find_1_should_not_be_of_type_minew_patient() throws Exception {
        assertFalse(minewTranslator.isOfBeaconType(SNF_PACKET_1));
    }

    @Test
    public void stick_n_find_2_should_not_be_of_type_minew_patient() throws Exception {
        assertFalse(minewTranslator.isOfBeaconType(SNF_PACKET_2));
    }

    @Test
    public void stick_n_find_3_should_not_be_of_type_minew_patient() throws Exception {
        assertFalse(minewTranslator.isOfBeaconType(SNF_PACKET_3));
    }

    @Test
    public void unknown_should_not_be_of_minew_patient_beacon_type() throws Exception {
        assertFalse(minewTranslator.isOfBeaconType(UNKNOWN_PACKET_1));
    }
}
