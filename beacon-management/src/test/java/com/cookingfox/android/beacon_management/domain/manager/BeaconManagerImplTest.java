package com.cookingfox.android.beacon_management.domain.manager;

import com.cookingfox.android.beacon_management.component.BeaconManagementComponent;
import com.cookingfox.android.beacon_management.domain.beacon.DetectedBeacon;
import com.cookingfox.android.beacon_management.domain.command.ProcessBluetoothScanResults;
import com.cookingfox.android.beacon_management.domain.event.DetectedBeaconsUpdated;
import com.cookingfox.android.beacon_management.domain.exception.NoProviderForBeaconTypeRegisteredException;
import com.cookingfox.android.beacon_management.domain.facade.BeaconManagementFacade;
import com.cookingfox.android.beacon_management.domain.state.BeaconManagementState;
import com.cookingfox.android.beacon_management.domain.state.BeaconManagementStateObserver;
import com.cookingfox.android.beacon_management.infrastructure.beacon.blue_maestro_button.provider.BlueMaestroButtonBeaconProvider;
import com.cookingfox.android.beacon_management.infrastructure.beacon.blue_maestro_button.vo.BlueMaestroButtonDetectedBeacon;
import com.cookingfox.android.beacon_management.infrastructure.beacon.ibeacon.provider.IBeaconBeaconProvider;
import com.cookingfox.android.beacon_management.infrastructure.beacon.minew_patient.connected.MinewPatientConnectedBeacon;
import com.cookingfox.android.beacon_management.infrastructure.beacon.minew_patient.provider.MinewPatientBeaconProvider;
import com.cookingfox.android.beacon_management.infrastructure.beacon.minew_patient.values.MinewPatientControlState;
import com.cookingfox.android.beacon_management.infrastructure.beacon.minew_patient.vo.MinewPatientDetectedBeacon;
import com.cookingfox.android.beacon_management.testing.beacon.TestAdPacketProvider;
import com.cookingfox.android.bluetooth_management.domain.vo.scanning.BluetoothScanResult;
import com.cookingfox.android.bluetooth_management.testing.driver.MockBluetoothScannerDriver;
import com.cookingfox.android.core_component.compatibility.lapasse.logger.CookingFoxLapasseLogger;
import com.cookingfox.android.core_component.testing.component.manager.TestComponentManager;
import com.cookingfox.chefling.api.CheflingContainer;
import com.cookingfox.lapasse.api.event.Event;
import com.cookingfox.lapasse.api.state.observer.StateChanged;
import com.cookingfox.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import rx.functions.Action1;
import rx.observers.TestSubscriber;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

public class BeaconManagerImplTest {

    private BeaconManagerImpl beaconManager;
    private CheflingContainer container;
    private BeaconManagementFacade facade;
    private MockBluetoothScannerDriver mockScannerDriver;
    private Event stateChangedEvent;

    @Before
    public void setUp() throws Exception {
        Logger.initTest();

        container = TestComponentManager.buildComponent(BeaconManagementComponent.class);

        facade = container.getInstance(BeaconManagementFacade.class);
        facade.addLogger(new CookingFoxLapasseLogger<>());

        mockScannerDriver = container.getInstance(MockBluetoothScannerDriver.class);

        // Setup the BeaconScanningManager
        beaconManager = container.getInstance(BeaconManagerImpl.class);

        container.getInstance(BeaconManagementStateObserver.class)
                .observeStateChanges()
                .subscribe(onStateChanged);
    }

    @After
    public void tearDown() throws Exception {
        container.disposeContainer();
        stateChangedEvent = null;
    }

    //----------------------------------------------------------------------------------------------
    // PROVIDER REGISTRATION TESTS
    //----------------------------------------------------------------------------------------------

    @Test
    public void duplicate_provider_registration_attempt_should_result_in_single_registration() throws Exception {
        beaconManager.registerProvider(MinewPatientBeaconProvider.class);
        beaconManager.registerProvider(MinewPatientBeaconProvider.class);
        assertEquals(1, beaconManager.beaconProviders.size());
    }

    @Test
    public void calling_register_should_add_registration_to_manager() throws IllegalStateException {
        beaconManager.registerProvider(IBeaconBeaconProvider.class);
        assertEquals(1, beaconManager.beaconProviders.size());
    }

    @Test
    public void calling_unregister_should_remove_registration_from_manager() throws IllegalStateException {
        beaconManager.registerProvider(IBeaconBeaconProvider.class);
        beaconManager.unregisterProvider(IBeaconBeaconProvider.class);
        assertEquals(0, beaconManager.beaconProviders.size());
    }

    @Test
    public void calling_unregister_once_on_manager_should_unregister_correct_type() throws IllegalStateException {
        beaconManager.registerProvider(IBeaconBeaconProvider.class);
        assertEquals(1, beaconManager.beaconProviders.size());

        beaconManager.registerProvider(MinewPatientBeaconProvider.class);
        assertEquals(2, beaconManager.beaconProviders.size());

        beaconManager.unregisterProvider(IBeaconBeaconProvider.class);
        assertEquals(1, beaconManager.beaconProviders.size());

        assertNotNull(beaconManager.beaconProviders.get(MinewPatientBeaconProvider.class));
    }

    @Test(expected = NullPointerException.class)
    public void register_should_throw_if_null() throws Exception {
        beaconManager.registerProvider(null);
    }

    @Test(expected = NullPointerException.class)
    public void unregister_should_throw_if_null() throws Exception {
        beaconManager.unregisterProvider(null);
    }

    @Test
    public void unregister_should_not_throw_on_unexisting_registration() throws Exception {
        beaconManager.registerProvider(IBeaconBeaconProvider.class);
        beaconManager.unregisterProvider(MinewPatientBeaconProvider.class);
    }

    //----------------------------------------------------------------------------------------------
    // DETECTION TESTS
    //----------------------------------------------------------------------------------------------

    @Test
    public void detected_beacons_should_update_on_scanresult_with_registered_ibeacon_provider() throws Exception {
        beaconManager.registerProvider(IBeaconBeaconProvider.class);

        BluetoothScanResult mockScanResult = mockScannerDriver
                .generateMockScanResult("A1-B2-C3-D4-E5-F6", -50, TestAdPacketProvider.IBEACON_PACKET_1);

        Map<String, BluetoothScanResult> mockResults = new LinkedHashMap<>();
        mockResults.put(mockScanResult.macAddress(), mockScanResult);

        facade.handleCommand(new ProcessBluetoothScanResults.Builder()
                .scanResults(mockResults)
                .build());

        assertTrue(stateChangedEvent instanceof DetectedBeaconsUpdated);
    }

    @Test
    public void observeBeaconType_should_filter_multiple_detected_beacons_of_specified_type() throws Exception {
        BluetoothScanResult result1 = mockScannerDriver.generateMockScanResult("A1-B2-C3-D4-E5-F6", -70, TestAdPacketProvider.MINEW_PACKET_1);
        BluetoothScanResult result2 = mockScannerDriver.generateMockScanResult("A2-E3-C4-D5-E6-F7", -60, TestAdPacketProvider.MINEW_PACKET_2);
        BluetoothScanResult result3 = mockScannerDriver.generateMockScanResult("A3-E4-C5-D6-E7-F8", -50, TestAdPacketProvider.MINEW_PACKET_3);
        BluetoothScanResult result4 = mockScannerDriver.generateMockScanResult("A4-F5-C6-D7-E8-F9", -40, TestAdPacketProvider.SNF_PACKET_1);
        BluetoothScanResult result5 = mockScannerDriver.generateMockScanResult("A5-F6-C7-D8-E9-F0", -30, TestAdPacketProvider.SNF_PACKET_2);
        BluetoothScanResult result6 = mockScannerDriver.generateMockScanResult("A6-E7-C8-D9-E0-F1", -20, TestAdPacketProvider.SNF_PACKET_3);
        BluetoothScanResult result7 = mockScannerDriver.generateMockScanResult("A7-E8-C9-D0-E1-F2", -10, TestAdPacketProvider.UNKNOWN_PACKET_1);
        BluetoothScanResult result8 = mockScannerDriver.generateMockScanResult("A8-E9-C0-D1-E2-F3", -80, TestAdPacketProvider.SNF_PACKET_4);

        List<BluetoothScanResult> mockResults = new LinkedList<>();
        mockResults.add(result1);
        mockResults.add(result2);
        mockResults.add(result3);
        mockResults.add(result4);
        mockResults.add(result5);
        mockResults.add(result6);
        mockResults.add(result7);
        mockResults.add(result8);

        beaconManager.registerProvider(MinewPatientBeaconProvider.class);

        TestSubscriber<List<MinewPatientDetectedBeacon>> testSubscriber = TestSubscriber.create();

        beaconManager.observeBeaconType(MinewPatientDetectedBeacon.class)
                .subscribe(testSubscriber);

        broadcastMockScanresults(mockResults);

        testSubscriber.assertNoErrors();
        testSubscriber.assertValueCount(1);

        List<MinewPatientDetectedBeacon> minewBeacons = testSubscriber.getOnNextEvents().get(0);

        // Assert we have detected 3 MINEW TAG beacons
        assertEquals(3, minewBeacons.size());
    }

    @Test
    public void observeBeaconType_should_filter_single_detected_beacon_of_specified_type() throws Exception {
        BluetoothScanResult scanResult = getMinewMockResult();

        MinewPatientDetectedBeacon expected = new MinewPatientDetectedBeacon.Builder()
                .batteryLevel(TestAdPacketProvider.MINEW_BATTERY_LEVEL_PACKET_1)
                .controlState(TestAdPacketProvider.MINEW_CONTROL_STATE_PACKET_1)
                .customIdentifier(TestAdPacketProvider.MINEW_CUSTOM_IDENTIFIER_PACKET_1)
                .productCode(TestAdPacketProvider.MINEW_PRODUCT_CODE_PACKET_1)
                .scanResult(scanResult)
                .build();

        List<MinewPatientDetectedBeacon> result = getSingleDetectedMinewBeacon(scanResult);

        assertEquals(Collections.singletonList(expected), result);
    }

    @Test
    public void should_throw_if_provider_is_not_registered() throws Exception {
        beaconManager.registerProvider(IBeaconBeaconProvider.class);

        TestSubscriber<List<MinewPatientDetectedBeacon>> testSubscriber = TestSubscriber.create();
        testSubscriber.assertNoErrors();
        beaconManager.observeBeaconType(MinewPatientDetectedBeacon.class).subscribe(testSubscriber);

        testSubscriber.assertError(NoProviderForBeaconTypeRegisteredException.class);
    }

    @Test
    public void should_not_throw_if_provider_is_registered() throws Exception {
        beaconManager.registerProvider(IBeaconBeaconProvider.class);
        beaconManager.registerProvider(MinewPatientBeaconProvider.class);

        TestSubscriber<List<MinewPatientDetectedBeacon>> testSubscriber = TestSubscriber.create();
        testSubscriber.assertNoErrors();
        beaconManager.observeBeaconType(MinewPatientDetectedBeacon.class).subscribe(testSubscriber);

        testSubscriber.assertNoErrors();
    }

    //----------------------------------------------------------------------------------------------
    // CONNECTION FLOW TESTS
    //----------------------------------------------------------------------------------------------

    @Test
    public void calling_connect_with_beacon_should_not_return_null() throws Exception {
        MinewPatientDetectedBeacon detectedMinewBeacon = getSingleDetectedMinewBeacon(getMinewMockResult()).get(0);

        assertTrue(beaconManager.connectWithBeacon(MinewPatientConnectedBeacon.class, detectedMinewBeacon) != null);
    }

    @Test
    public void connecting_beacon_type_should_return_connected_device_of_beacon_type() throws Exception {
        MinewPatientDetectedBeacon detectedBeacon = getSingleDetectedMinewBeacon(getMinewMockResult()).get(0);

        beaconManager.connectWithBeacon(MinewPatientConnectedBeacon.class, detectedBeacon)
                .subscribe(connectedMinewPatientBeacon -> {
                    assertTrue(connectedMinewPatientBeacon != null);
                });
    }

    @Test
    public void should_read_light_sensor_threshold() throws Exception {
        MinewPatientDetectedBeacon detectedBeacon = getSingleDetectedMinewBeacon(getMinewMockResult()).get(0);

        beaconManager.connectWithBeacon(MinewPatientConnectedBeacon.class, detectedBeacon)
                .subscribe(connectedMinewPatientBeacon -> {
                    connectedMinewPatientBeacon.readLightSensorThreshold().subscribe();
                });
    }

    @Test
    public void should_publish_junk_scan_results() throws Exception {
        String junkMacAddress = "A1:B2:C3:D4:E5:F6";

        BluetoothScanResult junkScanResult = mockScannerDriver
                .generateMockScanResult(junkMacAddress, -50, new byte[0]);

        Map<String, BluetoothScanResult> mockResults = new LinkedHashMap<>();
        mockResults.put(junkMacAddress, junkScanResult);

        facade.handleCommand(new ProcessBluetoothScanResults.Builder()
                .scanResults(mockResults)
                .build());

        assertTrue(stateChangedEvent instanceof DetectedBeaconsUpdated);

        Map<String, BluetoothScanResult> junkScanResults = facade.getCurrentState().junkScanResults();

        assertEquals(junkScanResult, junkScanResults.get(junkMacAddress));
        assertEquals(1, junkScanResults.size());
    }

    @Test
    public void should_publish_only_publish_unprocessable_results_to_junk() throws Exception {
        String junkMacAddress = "A1:B2:C3:D4:E5:F6";
        String minewPatientMacAddress = "00:00:00:00:00:01";
        String blueMaestroMacAddress = "00:00:00:00:00:02";

        BluetoothScanResult junkScanResult = mockScannerDriver
                .generateMockScanResult(junkMacAddress, -50, new byte[0]);

        BluetoothScanResult mockMinewScanResult = mockScannerDriver
                .generateMockScanResult(minewPatientMacAddress, -50, TestAdPacketProvider.MINEW_PACKET_1);

        BluetoothScanResult mockBlueMaestroScanResult = mockScannerDriver
                .generateMockScanResult(blueMaestroMacAddress, -50, TestAdPacketProvider.BLUE_MAESTRO_BUTTON_IDLE);

        Map<String, BluetoothScanResult> mockResults = new LinkedHashMap<>();
        mockResults.put(junkMacAddress, junkScanResult);
        mockResults.put(minewPatientMacAddress, mockMinewScanResult);
        mockResults.put(blueMaestroMacAddress, mockBlueMaestroScanResult);

        beaconManager.registerProvider(MinewPatientBeaconProvider.class);
        beaconManager.registerProvider(BlueMaestroButtonBeaconProvider.class);

        facade.handleCommand(new ProcessBluetoothScanResults.Builder()
                .scanResults(mockResults)
                .build());

        assertTrue(stateChangedEvent instanceof DetectedBeaconsUpdated);

        Map<String, BluetoothScanResult> junkScanResults = facade.getCurrentState().junkScanResults();

        assertEquals(junkScanResult, junkScanResults.get(junkMacAddress));
        assertEquals(1, junkScanResults.size());
    }

    @Test
    public void should_not_publish_processable_beacons_to_junk() throws Exception {
        String minewPatientMacAddress = "00:00:00:00:00:01";
        String blueMaestroMacAddress = "00:00:00:00:00:02";

        BluetoothScanResult mockMinewScanResult = mockScannerDriver
                .generateMockScanResult(minewPatientMacAddress, -50, TestAdPacketProvider.MINEW_PACKET_1);

        BluetoothScanResult mockBlueMaestroScanResult = mockScannerDriver
                .generateMockScanResult(blueMaestroMacAddress, -50, TestAdPacketProvider.BLUE_MAESTRO_BUTTON_IDLE);

        Map<String, BluetoothScanResult> mockResults = new LinkedHashMap<>();
        mockResults.put(minewPatientMacAddress, mockMinewScanResult);
        mockResults.put(blueMaestroMacAddress, mockBlueMaestroScanResult);

        beaconManager.registerProvider(MinewPatientBeaconProvider.class);
        beaconManager.registerProvider(BlueMaestroButtonBeaconProvider.class);

        facade.handleCommand(new ProcessBluetoothScanResults.Builder()
                .scanResults(mockResults)
                .build());

        assertTrue(stateChangedEvent instanceof DetectedBeaconsUpdated);

        Map<String, BluetoothScanResult> junkScanResults = facade.getCurrentState().junkScanResults();

        assertEquals(0, junkScanResults.size());
    }

    //----------------------------------------------------------------------------------------------
    // filterBeaconsByType
    //----------------------------------------------------------------------------------------------

    @Test
    public void filterBeaconsByType_should_only_pass_through_minew_beacons() throws Exception {
        BluetoothScanResult result1 = mockScannerDriver.generateMockScanResult("A1-B2-C3-D4-E5-F6", -70, TestAdPacketProvider.MINEW_PACKET_1);
        BluetoothScanResult result2 = mockScannerDriver.generateMockScanResult("A2-E3-C4-D5-E6-F7", -60, TestAdPacketProvider.MINEW_PACKET_2);
        BluetoothScanResult result3 = mockScannerDriver.generateMockScanResult("A3-E4-C5-D6-E7-F8", -50, TestAdPacketProvider.MINEW_PACKET_3);
        BluetoothScanResult result4 = mockScannerDriver.generateMockScanResult("A4-F5-C6-D7-E8-F9", -40, TestAdPacketProvider.SNF_PACKET_1);
        BluetoothScanResult result5 = mockScannerDriver.generateMockScanResult("A5-F6-C7-D8-E9-F0", -30, TestAdPacketProvider.SNF_PACKET_2);
        BluetoothScanResult result6 = mockScannerDriver.generateMockScanResult("A6-E7-C8-D9-E0-F1", -20, TestAdPacketProvider.SNF_PACKET_3);
        BluetoothScanResult result7 = mockScannerDriver.generateMockScanResult("A7-E8-C9-D0-E1-F2", -10, TestAdPacketProvider.UNKNOWN_PACKET_1);

        MinewPatientDetectedBeacon detected1 = generateMinewPatientDetectedBeacon(result1);
        MinewPatientDetectedBeacon detected2 = generateMinewPatientDetectedBeacon(result2);
        MinewPatientDetectedBeacon detected3 = generateMinewPatientDetectedBeacon(result3);

        BlueMaestroButtonDetectedBeacon detected4 = generateBlueMaestroDetectedBeacon(result4);
        BlueMaestroButtonDetectedBeacon detected5 = generateBlueMaestroDetectedBeacon(result5);
        BlueMaestroButtonDetectedBeacon detected6 = generateBlueMaestroDetectedBeacon(result6);

        DetectedBeacon detected7 = () -> result7;

        Collection<DetectedBeacon> detectedBeacons = Arrays.asList(detected1, detected2, detected3, detected4, detected5, detected6, detected7);

        List<MinewPatientDetectedBeacon> result = beaconManager.filterBeaconsByType(detectedBeacons, MinewPatientDetectedBeacon.class);

        assertEquals(3, result.size());
        assertTrue(detectedBeacons.contains(detected1));
        assertTrue(detectedBeacons.contains(detected2));
        assertTrue(detectedBeacons.contains(detected3));
        assertTrue(detectedBeacons.contains(detected4));
        assertTrue(detectedBeacons.contains(detected5));
        assertTrue(detectedBeacons.contains(detected6));
        assertTrue(detectedBeacons.contains(detected7));
    }

    //----------------------------------------------------------------------------------------------
    // filterDetectedBeacons
    //----------------------------------------------------------------------------------------------

    @Test
    public void filterDetectedBeacons_should_return_empty_map_on_no_registered_providers() throws Exception {
        BluetoothScanResult result1 = mockScannerDriver.generateMockScanResult("A1-B2-C3-D4-E5-F6", -70, TestAdPacketProvider.MINEW_PACKET_1);
        BluetoothScanResult result2 = mockScannerDriver.generateMockScanResult("A2-E3-C4-D5-E6-F7", -60, TestAdPacketProvider.MINEW_PACKET_2);
        BluetoothScanResult result3 = mockScannerDriver.generateMockScanResult("A3-E4-C5-D6-E7-F8", -50, TestAdPacketProvider.MINEW_PACKET_3);
        BluetoothScanResult result4 = mockScannerDriver.generateMockScanResult("A8-E9-C0-D1-E2-F3", -80, TestAdPacketProvider.SNF_PACKET_4);

        Map<String, BluetoothScanResult> scanResultMap = new LinkedHashMap<>();
        scanResultMap.put(result1.macAddress(), result1);
        scanResultMap.put(result2.macAddress(), result2);
        scanResultMap.put(result3.macAddress(), result3);
        scanResultMap.put(result4.macAddress(), result4);

        Map<String, DetectedBeacon> detectedBeacons = beaconManager.filterDetectedBeacons(scanResultMap);

        assertEquals(0, detectedBeacons.size());
    }

    @Test
    public void filterDetectedBeacons_should_return_two_valid_beacon_detects() throws Exception {
        BluetoothScanResult result1 = mockScannerDriver.generateMockScanResult("A1-B2-C3-D4-E5-F6", -70, TestAdPacketProvider.MINEW_PACKET_1);
        BluetoothScanResult result2 = mockScannerDriver.generateMockScanResult("A2-E3-C4-D5-E6-F7", -60, TestAdPacketProvider.MINEW_PACKET_2);
        BluetoothScanResult result3 = mockScannerDriver.generateMockScanResult("A4-F5-C6-D7-E8-F9", -40, TestAdPacketProvider.SNF_PACKET_1);
        BluetoothScanResult result4 = mockScannerDriver.generateMockScanResult("A5-F6-C7-D8-E9-F0", -30, TestAdPacketProvider.SNF_PACKET_2);
        BluetoothScanResult result5 = mockScannerDriver.generateMockScanResult("A6-E7-C8-D9-E0-F1", -20, TestAdPacketProvider.SNF_PACKET_3);
        BluetoothScanResult result6 = mockScannerDriver.generateMockScanResult("A7-E8-C9-D0-E1-F2", -10, TestAdPacketProvider.UNKNOWN_PACKET_1);
        BluetoothScanResult result7 = mockScannerDriver.generateMockScanResult("A8-E9-C0-D1-E2-F3", -80, TestAdPacketProvider.SNF_PACKET_4);

        Map<String, BluetoothScanResult> scanResultMap = new LinkedHashMap<>();
        scanResultMap.put(result1.macAddress(), result1);
        scanResultMap.put(result2.macAddress(), result2);
        scanResultMap.put(result3.macAddress(), result3);
        scanResultMap.put(result4.macAddress(), result4);
        scanResultMap.put(result5.macAddress(), result5);
        scanResultMap.put(result6.macAddress(), result6);
        scanResultMap.put(result7.macAddress(), result7);

        beaconManager.registerProvider(MinewPatientBeaconProvider.class);

        Map<String, DetectedBeacon> detectedBeacons = beaconManager.filterDetectedBeacons(scanResultMap);

        assertEquals(2, detectedBeacons.size());

        int validBeaconCount = 0;
        for (Map.Entry<String, DetectedBeacon> entry : detectedBeacons.entrySet()) {
            if (entry.getValue() instanceof MinewPatientDetectedBeacon) {
                validBeaconCount++;
            }
        }

        assertEquals(2, validBeaconCount);
    }

    @Test
    public void filterDetectedBeacons_should_return_two_valid_beacon_detects_after_unregistering_and_registering_providers() throws Exception {
        BluetoothScanResult result1 = mockScannerDriver.generateMockScanResult("A1-B2-C3-D4-E5-F6", -70, TestAdPacketProvider.MINEW_PACKET_1);
        BluetoothScanResult result2 = mockScannerDriver.generateMockScanResult("A2-E3-C4-D5-E6-F7", -60, TestAdPacketProvider.MINEW_PACKET_2);
        BluetoothScanResult result3 = mockScannerDriver.generateMockScanResult("A4-F5-C6-D7-E8-F9", -40, TestAdPacketProvider.SNF_PACKET_1);
        BluetoothScanResult result4 = mockScannerDriver.generateMockScanResult("A5-F6-C7-D8-E9-F0", -30, TestAdPacketProvider.SNF_PACKET_2);
        BluetoothScanResult result5 = mockScannerDriver.generateMockScanResult("A6-E7-C8-D9-E0-F1", -20, TestAdPacketProvider.SNF_PACKET_3);
        BluetoothScanResult result6 = mockScannerDriver.generateMockScanResult("A7-E8-C9-D0-E1-F2", -10, TestAdPacketProvider.UNKNOWN_PACKET_1);
        BluetoothScanResult result7 = mockScannerDriver.generateMockScanResult("A8-E9-C0-D1-E2-F3", -80, TestAdPacketProvider.SNF_PACKET_4);

        Map<String, BluetoothScanResult> scanResultMap = new LinkedHashMap<>();
        scanResultMap.put(result1.macAddress(), result1);
        scanResultMap.put(result2.macAddress(), result2);
        scanResultMap.put(result3.macAddress(), result3);
        scanResultMap.put(result4.macAddress(), result4);
        scanResultMap.put(result5.macAddress(), result5);
        scanResultMap.put(result6.macAddress(), result6);
        scanResultMap.put(result7.macAddress(), result7);

        beaconManager.registerProvider(MinewPatientBeaconProvider.class);

        // Add a unrelated provider to test handling of parallel providers
        beaconManager.registerProvider(BlueMaestroButtonBeaconProvider.class);

        Map<String, DetectedBeacon> detectedBeacons = beaconManager.filterDetectedBeacons(scanResultMap);

        beaconManager.unregisterProvider(BlueMaestroButtonBeaconProvider.class);
        beaconManager.unregisterProvider(MinewPatientBeaconProvider.class);
        beaconManager.registerProvider(MinewPatientBeaconProvider.class);

        assertEquals(2, detectedBeacons.size());

        detectedBeacons.clear();

        // Change the data to trigger beacons state update
        result1 = mockScannerDriver.generateMockScanResult("B1-B2-C3-D4-E5-F6", -56, TestAdPacketProvider.MINEW_PACKET_1);
        scanResultMap.put(result1.macAddress(), result1);

        detectedBeacons = beaconManager.filterDetectedBeacons(scanResultMap);

        assertEquals(3, detectedBeacons.size());
    }

    //----------------------------------------------------------------------------------------------
    // HELPER METHODS
    //----------------------------------------------------------------------------------------------

    private final Action1<StateChanged<BeaconManagementState>> onStateChanged =
            new Action1<StateChanged<BeaconManagementState>>() {
                @Override
                public void call(StateChanged<BeaconManagementState> stateChanged) {
                    stateChangedEvent = stateChanged.getEvent();
                }
            };

    private BlueMaestroButtonDetectedBeacon generateBlueMaestroDetectedBeacon(BluetoothScanResult result) {
        return new BlueMaestroButtonDetectedBeacon() {
            @Override
            public int batteryLevel() {
                return 0;
            }

            @Override
            public int pressedState() {
                return 0;
            }

            @Override
            public BluetoothScanResult scanResult() {
                return result;
            }
        };
    }

    private MinewPatientDetectedBeacon generateMinewPatientDetectedBeacon(BluetoothScanResult result) {
        return new MinewPatientDetectedBeacon() {
            @Override
            public int batteryLevel() {
                return 0;
            }

            @Override
            public MinewPatientControlState controlState() {
                return null;
            }

            @Override
            public int customIdentifier() {
                return 0;
            }

            @Override
            public int productCode() {
                return 0;
            }

            @Override
            public BluetoothScanResult scanResult() {
                return result;
            }
        };
    }

    private BluetoothScanResult getMinewMockResult() {
        return mockScannerDriver
                .generateMockScanResult("A1-B2-C3-D4-E5-F6", -50, TestAdPacketProvider.MINEW_PACKET_1);
    }

    private List<MinewPatientDetectedBeacon> getSingleDetectedMinewBeacon(BluetoothScanResult mockResult) {
        Map<String, BluetoothScanResult> mockScanResults = new LinkedHashMap<>();
        mockScanResults.put(mockResult.macAddress(), mockResult);

        beaconManager.registerProvider(MinewPatientBeaconProvider.class);

        TestSubscriber<List<MinewPatientDetectedBeacon>> testSubscriber = TestSubscriber.create();

        beaconManager.observeBeaconType(MinewPatientDetectedBeacon.class)
                .subscribe(testSubscriber);

        facade.handleCommand(new ProcessBluetoothScanResults.Builder()
                .scanResults(mockScanResults)
                .build());

        testSubscriber.assertNoErrors();
        testSubscriber.assertValueCount(1);

        // return first emitted result
        return testSubscriber.getOnNextEvents().get(0);
    }

    private void broadcastMockScanresults(List<BluetoothScanResult> scanResults) {
        Map<String, BluetoothScanResult> mockScanResults = new LinkedHashMap<>();

        for (BluetoothScanResult result : scanResults) {
            mockScanResults.put(result.macAddress(), result);
        }

        facade.handleCommand(new ProcessBluetoothScanResults.Builder()
                .scanResults(mockScanResults)
                .build());
    }
}
