package com.cookingfox.android.beacon_management.component;

import com.cookingfox.android.beacon_management.domain.facade.BeaconManagementFacade;
import com.cookingfox.android.beacon_management.domain.state.BeaconManagementStateObserver;
import com.cookingfox.android.core_component.api.component.manager.ComponentManager;
import com.cookingfox.android.core_component.testing.component.manager.TestComponentManager;
import com.cookingfox.chefling.api.CheflingContainer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertTrue;

public class BeaconManagementComponentTest {

    //----------------------------------------------------------------------------------------------
    // TEST SETUP
    //----------------------------------------------------------------------------------------------

    ComponentManager componentManager;
    CheflingContainer container;

    @Before
    public void setUp() throws Exception {
        container = TestComponentManager.buildContainer();
        componentManager = TestComponentManager.getComponentManager(container);
    }

    @After
    public void tearDown() throws Exception {
        componentManager.unloadAllComponents();
        container.disposeContainer();
    }

    //----------------------------------------------------------------------------------------------
    // TESTS: loadComponent
    //----------------------------------------------------------------------------------------------

    @Test
    public void load_component_should_not_throw() throws Exception {
        componentManager.loadComponent(BeaconManagementComponent.class);
    }

    @Test
    public void should_map_beacon_management_state_observer() throws Exception {
        componentManager.loadComponent(BeaconManagementComponent.class);

        BeaconManagementStateObserver stateObserver =
                container.getInstance(BeaconManagementStateObserver.class);

        assertTrue(stateObserver instanceof BeaconManagementFacade);
    }
}
