package com.cookingfox.android.beacon_management.infrastructure.beacon.blue_maestro_button.translator;

import org.junit.Before;
import org.junit.Test;

import static com.cookingfox.android.beacon_management.testing.beacon.TestAdPacketProvider.BLUE_MAESTRO_BUTTON_BATTERY_99;
import static com.cookingfox.android.beacon_management.testing.beacon.TestAdPacketProvider.BLUE_MAESTRO_BUTTON_IDLE;
import static com.cookingfox.android.beacon_management.testing.beacon.TestAdPacketProvider.BLUE_MAESTRO_BUTTON_PRESSED;
import static com.cookingfox.android.beacon_management.testing.beacon.TestAdPacketProvider.MINEW_PACKET_1;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for {@link BlueMaestroButtonBeaconTranslator}.
 */
public class BlueMaestroButtonBeaconTranslatorTest {

    //----------------------------------------------------------------------------------------------
    // TEST SETUP
    //----------------------------------------------------------------------------------------------

    private BlueMaestroButtonBeaconTranslator translator;

    @Before
    public void setUp() throws Exception {
        translator = new BlueMaestroButtonBeaconTranslator();
    }

    //----------------------------------------------------------------------------------------------
    // TESTS: isOfBeaconType
    //----------------------------------------------------------------------------------------------

    @Test(expected = NullPointerException.class)
    public void isOfBeaconType_should_throw_for_null() throws Exception {
        translator.isOfBeaconType(null);
    }

    @Test
    public void isOfBeaconType_should_return_false_for_empty() throws Exception {
        boolean result = translator.isOfBeaconType(new byte[0]);

        assertFalse(result);
    }

    @Test
    public void isOfBeaconType_should_return_false_for_other() throws Exception {
        boolean result = translator.isOfBeaconType(MINEW_PACKET_1);

        assertFalse(result);
    }

    @Test
    public void isOfBeaconType_should_return_true_for_idle() throws Exception {
        boolean result = translator.isOfBeaconType(BLUE_MAESTRO_BUTTON_IDLE);

        assertTrue(result);
    }

    @Test
    public void isOfBeaconType_should_return_true_for_pressed() throws Exception {
        boolean result = translator.isOfBeaconType(BLUE_MAESTRO_BUTTON_PRESSED);

        assertTrue(result);
    }

    //----------------------------------------------------------------------------------------------
    // TESTS: getBatteryLevel
    //----------------------------------------------------------------------------------------------

    @Test(expected = IllegalStateException.class)
    public void getBatteryLevel_should_throw_if_not_initialized() throws Exception {
        translator.getBatteryLevel(BLUE_MAESTRO_BUTTON_IDLE);
    }

    @Test
    public void getBatteryLevel_should_return_battery_level_100() throws Exception {
        translator.isOfBeaconType(BLUE_MAESTRO_BUTTON_IDLE); // first initialize

        int result = translator.getBatteryLevel(BLUE_MAESTRO_BUTTON_IDLE);

        assertEquals(100, result);
    }

    @Test
    public void getBatteryLevel_should_return_battery_level_99() throws Exception {
        translator.isOfBeaconType(BLUE_MAESTRO_BUTTON_BATTERY_99); // first initialize

        int result = translator.getBatteryLevel(BLUE_MAESTRO_BUTTON_BATTERY_99);

        assertEquals(99, result);
    }

    //----------------------------------------------------------------------------------------------
    // TESTS: getPressedState
    //----------------------------------------------------------------------------------------------

    @Test(expected = IllegalStateException.class)
    public void getPressedState_should_throw_if_not_initialized() throws Exception {
        translator.getPressedState(BLUE_MAESTRO_BUTTON_IDLE);
    }

    @Test
    public void getPressedState_should_return_false_for_idle() throws Exception {
        translator.isOfBeaconType(BLUE_MAESTRO_BUTTON_IDLE); // first initialize

        int result = translator.getPressedState(BLUE_MAESTRO_BUTTON_IDLE);

        assertEquals(0, result);
    }

    @Test
    public void getPressedState_should_return_true_for_pressed() throws Exception {
        translator.isOfBeaconType(BLUE_MAESTRO_BUTTON_PRESSED); // first initialize

        int result = translator.getPressedState(BLUE_MAESTRO_BUTTON_PRESSED);

        assertEquals(1, result);
    }

}
