package com.cookingfox.android.beacon_management.infrastructure.beacon.minew_patient.connected;

import com.cookingfox.android.beacon_management.infrastructure.beacon.minew_patient.vo.MinewPatientDetectedBeacon;
import com.cookingfox.android.bluetooth_management.domain.connection.BluetoothConnection;
import com.cookingfox.android.bluetooth_management.domain.manager.BluetoothDeviceConnectionManager;

import org.junit.Test;

import static org.mockito.Mockito.mock;

public class MinewPatientConnectedBeaconTest {

    @Test
    public void should_not_throw_exception() throws Exception {
        BluetoothConnection connection = mock(BluetoothConnection.class);
        BluetoothDeviceConnectionManager bluetoothDeviceConnectionManager = mock(BluetoothDeviceConnectionManager.class);
        MinewPatientDetectedBeacon minewPatientDetectedBeacon = mock(MinewPatientDetectedBeacon.class);

        MinewPatientConnectedBeacon connectedBeacon = new MinewPatientConnectedBeacon(connection, bluetoothDeviceConnectionManager, minewPatientDetectedBeacon);

        connectedBeacon.getConnection();
    }

}