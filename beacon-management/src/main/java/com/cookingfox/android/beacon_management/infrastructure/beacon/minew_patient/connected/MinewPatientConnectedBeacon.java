package com.cookingfox.android.beacon_management.infrastructure.beacon.minew_patient.connected;

import com.cookingfox.android.beacon_management.domain.beacon.ConnectedBeacon;
import com.cookingfox.android.beacon_management.infrastructure.beacon.minew_patient.values.MinewPatientControlState;
import com.cookingfox.android.beacon_management.infrastructure.beacon.minew_patient.vo.MinewPatientDetectedBeacon;
import com.cookingfox.android.beacon_management.infrastructure.beacon.translator.BaseBeaconTranslator;
import com.cookingfox.android.bluetooth_management.domain.connection.BluetoothConnection;
import com.cookingfox.android.bluetooth_management.domain.manager.BluetoothDeviceConnectionManager;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothConnectionResult;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothIOResult;

import java.util.UUID;

import rx.Observable;

public class MinewPatientConnectedBeacon implements ConnectedBeacon<MinewPatientDetectedBeacon> {

    private static final UUID UUID_SERVICE = UUID.fromString(
            "66400001-b5a3-f393-e0a9-e50e24dcca99");
    private static final UUID UUID_CONTROL_STATE = UUID.fromString(
            "66400011-b5a3-f393-e0a9-e50e24dcca99");
    private static final UUID UUID_CUSTOM_IDENTIFIER = UUID.fromString(
            "66400012-b5a3-f393-e0a9-e50e24dcca99");
    private static final UUID UUID_LIGHT_SENSOR_THRESHOLD = UUID.fromString(
            "66400013-b5a3-f393-e0a9-e50e24dcca99");
    private static final UUID UUID_PASSWORD = UUID.fromString(
            "66400014-b5a3-f393-e0a9-e50e24dcca99");

    private static final UUID UUID_MANUFACTURER_NAME = UUID.fromString(
            "66410793-b5a3-f393-e0a9-e50e24dcca99");

    private final BluetoothConnection connection;
    private final BluetoothDeviceConnectionManager connectionManager;
    private final MinewPatientDetectedBeacon detectedDevice;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public MinewPatientConnectedBeacon(
            BluetoothConnection connection,
            BluetoothDeviceConnectionManager connectionManager,
            MinewPatientDetectedBeacon detectedDevice) {
        this.connection = connection;
        this.connectionManager = connectionManager;
        this.detectedDevice = detectedDevice;
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    public Observable<BluetoothConnectionResult> disconnect() {
        return connection.disconnect();
    }

    @Override
    public MinewPatientDetectedBeacon getDetectedDevice() {
        return detectedDevice;
    }

    @Override
    public BluetoothConnection getConnection() {
        return connection;
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC READ METHODS
    //----------------------------------------------------------------------------------------------

    public Observable<MinewPatientControlState> readControlState() {
        return connectionManager.read(this, UUID_SERVICE, UUID_CONTROL_STATE)
                .map(result -> {
                    byte rawValue = result.rawValue()[0];

                    for (MinewPatientControlState state : MinewPatientControlState.values()) {
                        if (state.getState() == rawValue) {
                            return state;
                        }
                    }

                    return null;
                });
    }

    public Observable<Integer> readCustomIdentifier() {
        return connectionManager.read(this, UUID_SERVICE, UUID_CUSTOM_IDENTIFIER)
                .map(bluetoothIOResult -> Integer.valueOf(
                        BaseBeaconTranslator.bytesToHex(bluetoothIOResult.rawValue()), 16));
    }

    public Observable<Integer> readLightSensorThreshold() {
        return connectionManager.read(this, UUID_SERVICE, UUID_LIGHT_SENSOR_THRESHOLD)
                .map(bluetoothIOResult -> Integer.valueOf(
                        BaseBeaconTranslator.bytesToHex(bluetoothIOResult.rawValue()), 16));
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC WRITE METHODS
    //----------------------------------------------------------------------------------------------

    public Observable<BluetoothIOResult> writeControlState(MinewPatientControlState state) {
        byte[] bytes = new byte[]{state.getState()};

        return connectionManager.write(this, UUID_SERVICE, UUID_CONTROL_STATE, bytes);
    }

    public Observable<BluetoothIOResult> writeCustomIdentifier(int id) {
        byte[] identifier = new byte[2];
        identifier[0] = (byte) (id >> 8);
        identifier[1] = (byte) (id /*>> 0*/);

        return connectionManager.write(this, UUID_SERVICE, UUID_CUSTOM_IDENTIFIER, identifier);
    }

    public Observable<BluetoothIOResult> writeLightSensorThreshold(int threshold) {
        if (threshold < 0 || threshold > 255) {
            return Observable.error(new IllegalStateException(
                    "Light sensor threshold value must be between 0 and 255"));
        }

        byte[] bytes = new byte[]{(byte) threshold};

        return connectionManager.write(this, UUID_SERVICE, UUID_LIGHT_SENSOR_THRESHOLD, bytes);
    }

    public Observable<BluetoothIOResult> writePassword(String password) {
        byte[] bytes = password.getBytes();

        if (bytes.length > 8) {
            return Observable.error(
                    new IllegalStateException("Password can not be larger than 8 bytes"));
        }

        return connectionManager.write(this, UUID_SERVICE, UUID_PASSWORD, bytes);
    }

}
