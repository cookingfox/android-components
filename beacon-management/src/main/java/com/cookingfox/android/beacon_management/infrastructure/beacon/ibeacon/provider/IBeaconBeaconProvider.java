package com.cookingfox.android.beacon_management.infrastructure.beacon.ibeacon.provider;

import com.cookingfox.android.beacon_management.domain.factory.BeaconFactory;
import com.cookingfox.android.beacon_management.domain.provider.BeaconProvider;
import com.cookingfox.android.beacon_management.infrastructure.beacon.ibeacon.connected.ConnectedIBeaconBeacon;
import com.cookingfox.android.beacon_management.infrastructure.beacon.ibeacon.factory.IBeaconBeaconFactory;
import com.cookingfox.android.beacon_management.infrastructure.beacon.ibeacon.translator.IBeaconBeaconTranslator;
import com.cookingfox.android.beacon_management.infrastructure.beacon.ibeacon.vo.IBeaconDetectedBeacon;
import com.cookingfox.android.bluetooth_management.domain.connection.BluetoothConnection;
import com.cookingfox.android.bluetooth_management.domain.manager.BluetoothDeviceConnectionManager;

public class IBeaconBeaconProvider implements BeaconProvider<IBeaconBeaconTranslator,
        IBeaconDetectedBeacon, ConnectedIBeaconBeacon> {

    private final BluetoothDeviceConnectionManager connectionManager;
    private final IBeaconBeaconFactory factory;
    private final IBeaconBeaconTranslator translator;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public IBeaconBeaconProvider(BluetoothDeviceConnectionManager connectionManager,
                                 IBeaconBeaconFactory factory,
                                 IBeaconBeaconTranslator translator) {
        this.connectionManager = connectionManager;
        this.factory = factory;
        this.translator = translator;
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public ConnectedIBeaconBeacon createConnectedBeaconInstance(BluetoothConnection connection, IBeaconDetectedBeacon detectedDevice) {
        return new ConnectedIBeaconBeacon(connection, connectionManager, detectedDevice);
    }

    @Override
    public BeaconFactory<IBeaconDetectedBeacon> getFactory() {
        return factory;
    }

    @Override
    public IBeaconBeaconTranslator getTranslator() {
        return translator;
    }

    @Override
    public Class<IBeaconDetectedBeacon> getBeaconType() {
        return IBeaconDetectedBeacon.class;
    }
}
