package com.cookingfox.android.beacon_management.domain.beacon;

import com.cookingfox.android.bluetooth_management.domain.device.ConnectedBluetoothDevice;

public interface ConnectedBeacon<D extends DetectedBeacon> extends ConnectedBluetoothDevice<D> {

}
