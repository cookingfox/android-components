package com.cookingfox.android.beacon_management.infrastructure.beacon.blue_maestro_button.factory;

import com.cookingfox.android.beacon_management.domain.factory.BeaconFactory;
import com.cookingfox.android.beacon_management.infrastructure.beacon.blue_maestro_button.translator.BlueMaestroButtonBeaconTranslator;
import com.cookingfox.android.beacon_management.infrastructure.beacon.blue_maestro_button.vo.BlueMaestroButtonDetectedBeacon;
import com.cookingfox.android.bluetooth_management.domain.vo.scanning.BluetoothScanResult;

/**
 * Creates detected Blue Maestro button beacon VO using translator.
 */
public class BlueMaestroButtonBeaconFactory implements BeaconFactory<BlueMaestroButtonDetectedBeacon> {

    protected final BlueMaestroButtonBeaconTranslator translator;

    public BlueMaestroButtonBeaconFactory(BlueMaestroButtonBeaconTranslator translator) {
        this.translator = translator;
    }

    @Override
    public BlueMaestroButtonDetectedBeacon createDetectedBeacon(BluetoothScanResult scanResult) {
        byte[] adPacket = scanResult.scanRecord();

        return new BlueMaestroButtonDetectedBeacon.Builder()
                .scanResult(scanResult)
                .batteryLevel(translator.getBatteryLevel(adPacket))
                .pressedState(translator.getPressedState(adPacket))
                .build();
    }

}
