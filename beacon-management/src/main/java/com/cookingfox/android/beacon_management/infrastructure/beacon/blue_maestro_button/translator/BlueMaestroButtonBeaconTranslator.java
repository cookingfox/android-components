package com.cookingfox.android.beacon_management.infrastructure.beacon.blue_maestro_button.translator;

import com.cookingfox.android.beacon_management.domain.translator.BeaconTranslator;

import static com.cookingfox.guava_preconditions.Preconditions.checkState;

/**
 * Bluetooth scan record translator for Blue Maestro button beacon.
 * <p>
 * TODO: Refactor so implementation does not use while loop - gets rid of exception in getters!
 */
public class BlueMaestroButtonBeaconTranslator implements BeaconTranslator {

    //----------------------------------------------------------------------------------------------
    // CONSTANTS
    //----------------------------------------------------------------------------------------------

    public static final byte[] PRODUCT_IDENTIFIER = {0x33, 0x01};

    //----------------------------------------------------------------------------------------------
    // PROPERTIES
    //----------------------------------------------------------------------------------------------

    private int customDataOffset = 0;

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public boolean isOfBeaconType(byte[] adPacket) {
        boolean isTempoDevice = false;
        int pos = 0;

        // analyze advertising packet
        while (pos < adPacket.length) {
            // Pull length and treat as unsigned, although in current BT 4.1
            // len < 32
            int len = ((int) adPacket[pos++]) & 0xFF;

            // Early termination
            if (len == 0) {
                break;
            }

            // We are interested in the Custom Vendor
            // data, however, we may not know this is a Tempo yet...
            if (adPacket[pos] == (byte) 0xFF &&
                    adPacket[pos + 1] == PRODUCT_IDENTIFIER[0] &&
                    adPacket[pos + 2] == PRODUCT_IDENTIFIER[1]) {
                customDataOffset = pos;
                isTempoDevice = true; // let's assume it is
                break;
            }

            pos += len;
        }

        return isTempoDevice;
    }

    /**
     * @param adPacket The Bluetooth scan record.
     * @return The extracted battery level.
     */
    public int getBatteryLevel(byte[] adPacket) {
        checkState(customDataOffset > 0, "Call `isOfBeaconType` first");

        return byteToInt(adPacket[customDataOffset + 4], (byte) 0);
    }

    /**
     * @param adPacket The Bluetooth scan record.
     * @return The extracted button pressed state.
     */
    public int getPressedState(byte[] adPacket) {
        checkState(customDataOffset > 0, "Call `isOfBeaconType` first");

        return byteToInt(adPacket[customDataOffset + 5], (byte) 0);
    }

    //----------------------------------------------------------------------------------------------
    // PRIVATE METHODS
    //----------------------------------------------------------------------------------------------

    /**
     * @param lsb Least significant byte
     * @param msb Most significant byte
     * @return int value combined from lsb/msb
     */
    private int byteToInt(byte lsb, byte msb) {
        return (((int) lsb) & 0xFF) | (((int) msb) << 8);
    }

}
