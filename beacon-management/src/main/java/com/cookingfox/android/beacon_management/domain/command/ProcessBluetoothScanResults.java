package com.cookingfox.android.beacon_management.domain.command;

import com.cookingfox.android.bluetooth_management.domain.vo.scanning.BluetoothScanResult;
import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;
import com.cookingfox.lapasse.api.command.Command;

import org.immutables.value.Value;

import java.util.Map;

@DefaultValueStyle
@Value.Immutable
public interface ProcessBluetoothScanResults extends Command {

    Map<String, BluetoothScanResult> scanResults();

    class Builder extends ImmutableProcessBluetoothScanResults.Builder {
    }
}
