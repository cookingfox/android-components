package com.cookingfox.android.beacon_management.android.preferences;

import com.cookingfox.android.beacon_management.domain.preferences.BeaconManagementPreferences;
import com.cookingfox.android.prefer_rx.api.pref.RxPrefGroup;
import com.cookingfox.android.prefer_rx.impl.pref.AndroidRxPrefGroup;
import com.cookingfox.android.prefer_rx.impl.pref.typed.AndroidIntegerRxPref;
import com.cookingfox.android.prefer_rx.impl.prefer.AndroidRxPrefer;

/**
 * Preferences regarding beacon management.
 */
public class AndroidBeaconManagementPreferences implements BeaconManagementPreferences {

    private final AndroidRxPrefGroup<Key> prefGroup;
    private final AndroidIntegerRxPref<Key> beaconScanningBufferTimeMs;

    public AndroidBeaconManagementPreferences(AndroidRxPrefer prefer) {
        prefGroup = prefer.newGroup(Key.class);
        prefGroup.setTitle("Beacon Scanning Management");
        prefGroup.setSummary("Settings for Beacon Scanning");

        beaconScanningBufferTimeMs = prefGroup.addNewInteger(Key.BeaconScanningBufferTimeMs, 100);
        beaconScanningBufferTimeMs.setTitle("Beacon Scanning Buffer Time");
        beaconScanningBufferTimeMs.setSummary("Amount of Milliseconds before the buffer gets processed");
    }

    @Override
    public RxPrefGroup<Key> getPrefGroup() {
        return prefGroup;
    }
}
