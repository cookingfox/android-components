package com.cookingfox.android.beacon_management.domain.manager;

import com.cookingfox.android.beacon_management.domain.beacon.ConnectedBeacon;
import com.cookingfox.android.beacon_management.domain.beacon.DetectedBeacon;
import com.cookingfox.android.beacon_management.domain.command.ProcessBluetoothScanResults;
import com.cookingfox.android.beacon_management.domain.event.DetectedBeaconsUpdated;
import com.cookingfox.android.beacon_management.domain.exception.CouldNotFindProviderForBeaconException;
import com.cookingfox.android.beacon_management.domain.exception.NoProviderForBeaconTypeRegisteredException;
import com.cookingfox.android.beacon_management.domain.facade.BeaconManagementFacade;
import com.cookingfox.android.beacon_management.domain.provider.BeaconProvider;
import com.cookingfox.android.beacon_management.domain.state.BeaconManagementState;
import com.cookingfox.android.bluetooth_management.domain.event.BluetoothScanResultsUpdated;
import com.cookingfox.android.bluetooth_management.domain.manager.BluetoothDeviceConnectionManagerImpl;
import com.cookingfox.android.bluetooth_management.domain.state.BluetoothManagementState;
import com.cookingfox.android.bluetooth_management.domain.state.BluetoothManagementStateObserver;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothConnectionResult;
import com.cookingfox.android.bluetooth_management.domain.vo.scanning.BluetoothScanResult;
import com.cookingfox.chefling.api.CheflingContainer;
import com.cookingfox.chefling.api.CheflingLifecycle;
import com.cookingfox.lapasse.annotation.HandleCommand;
import com.cookingfox.lapasse.annotation.HandleEvent;
import com.cookingfox.lapasse.api.state.observer.StateChanged;
import com.cookingfox.lapasse.impl.helper.LaPasse;
import com.cookingfox.logging.Logger;
import com.google.common.collect.FluentIterable;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.functions.Action1;

import static com.cookingfox.guava_preconditions.Preconditions.checkNotNull;

/**
 * Main communications point for working with the Beacon Management Component.
 */
public class BeaconManagerImpl implements BeaconManager, CheflingLifecycle {

    private final BluetoothManagementStateObserver bluetoothManagementStateObserver;
    private final CheflingContainer container;
    private final BeaconManagementFacade facade;
    private final BluetoothDeviceConnectionManagerImpl connectionManager;

    protected final Map<Class<? extends BeaconProvider>, BeaconProvider> beaconProviders
            = new LinkedHashMap<>();

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public BeaconManagerImpl(BluetoothManagementStateObserver bluetoothManagementStateObserver,
                             CheflingContainer container,
                             BeaconManagementFacade facade,
                             BluetoothDeviceConnectionManagerImpl connectionManager) {
        this.bluetoothManagementStateObserver = bluetoothManagementStateObserver;
        this.container = container;
        this.facade = facade;
        this.connectionManager = connectionManager;
    }

    //----------------------------------------------------------------------------------------------
    // LIFECYCLE
    //----------------------------------------------------------------------------------------------

    /**
     * Registers the BluetoothAdapterManager and subscribes for scan results
     */
    @Override
    public void initialize() {
        LaPasse.mapHandlers(this, facade);

        bluetoothManagementStateObserver.observeStateChanges()
                .subscribe(onAdapterManagementStateChange, Logger::error);
    }

    @Override
    public void dispose() {
        beaconProviders.clear();
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public <T extends ConnectedBeacon> Observable<T> connectWithBeacon(Class<T> connectedBeaconType, DetectedBeacon beacon) {
        checkNotNull(beacon);

        final BeaconProvider provider = getProviderForBeacon(beacon);

        if (provider == null) {
            return Observable.error(new CouldNotFindProviderForBeaconException());
        }

        return connectWithBeaconExpectRaw(beacon)
                .map(connectionResult -> {
                    //noinspection unchecked
                    return (T) provider.createConnectedBeaconInstance(
                            connectionResult.connection(),
                            (DetectedBeacon) connectionResult.detectedDevice());
                });

    }

    @Override
    public Observable<BluetoothConnectionResult> connectWithBeaconExpectRaw(DetectedBeacon beacon) {
        checkNotNull(beacon);

        final BeaconProvider provider = getProviderForBeacon(beacon);

        if (provider == null) {
            return Observable.error(new CouldNotFindProviderForBeaconException());
        }

        return connectionManager.connectWithDevice(beacon);
    }

    @Override
    public <T extends DetectedBeacon> Observable<List<T>> observeBeaconType(Class<T> beaconType) {
        checkNotNull(beaconType);

        boolean hasProvider = false;
        for (BeaconProvider provider : beaconProviders.values()) {

            if (provider.getBeaconType().equals(beaconType)) {
                hasProvider = true;
                break;
            }
        }

        if (!hasProvider) {
            return Observable.error(new NoProviderForBeaconTypeRegisteredException(beaconType));
        }

        // observe state changes
        return facade.observeStateChanges()
                // grab detected beacons from state
                .map(stateChanged -> stateChanged.getState().detectedBeacons().values())
                // filter detected beacons by beacon type, by using Guava helper
                .map(detectedBeacons -> filterBeaconsByType(detectedBeacons, beaconType))
                // only continue when values changed from previous emit
                .distinctUntilChanged();
    }

    /**
     * Registers a beacon provider.
     *
     * @param provider BeaconProvider, e.g. IBeaconProvider/MinewPatientBeaconProvider.
     */
    @Override
    public void registerProvider(Class<? extends BeaconProvider> provider) {
        checkNotNull(provider, "Provider cannot be null!");

        if (beaconProviders.containsKey(provider)) {
            Logger.warn("Beacon provider already registered: %s", provider.getSimpleName());
            return;
        }

        BeaconProvider providerInstance = container.getInstance(provider);

        beaconProviders.put(provider, providerInstance);
    }

    /**
     * Unregisters the target provider, shows error in log if provider does not exist.
     *
     * @param provider BeaconProvider, e.g. IBeaconProvider/MinewPatientBeaconProvider.
     */
    @Override
    public void unregisterProvider(Class<? extends BeaconProvider> provider) throws IllegalStateException {
        checkNotNull(provider, "Provider cannot be null!");

        if (!beaconProviders.containsKey(provider)) {
            Logger.warn("Beacon provider not registered: %s", provider.getSimpleName());
            return;
        }

        beaconProviders.remove(provider);
    }

    //----------------------------------------------------------------------------------------------
    // COMMAND HANDLERS
    //----------------------------------------------------------------------------------------------

    /**
     * Command to update beacon detectedBeacons.
     * <p>
     * Uses the BeaconFactory to create a HashMap of Beacon objects mapped to corresponding macAddress
     * addresses.
     *
     * @param command Command to handle
     * @return BeaconDetectsUpdated containing macAddress/beacon map
     */
    @HandleCommand
    DetectedBeaconsUpdated handle(ProcessBluetoothScanResults command) {
        Map<String, BluetoothScanResult> rawScanResults = command.scanResults();
        Map<String, DetectedBeacon> detectedBeacons = filterDetectedBeacons(rawScanResults);

        // junk scan results is raw scan results minus detected beacons
        Map<String, BluetoothScanResult> junkScanResults = new LinkedHashMap<>(rawScanResults);
        junkScanResults.keySet().removeAll(detectedBeacons.keySet());

        return new DetectedBeaconsUpdated.Builder()
                .detectedBeacons(detectedBeacons)
                .junkScanResults(junkScanResults)
                .build();
    }

    //----------------------------------------------------------------------------------------------
    // EVENT HANDLERS
    //----------------------------------------------------------------------------------------------

    @HandleEvent
    BeaconManagementState handle(BeaconManagementState state, DetectedBeaconsUpdated event) {
        return new BeaconManagementState.Builder()
                .from(state)
                .detectedBeacons(event.detectedBeacons())
                .junkScanResults(event.junkScanResults())
                .build();
    }

    //----------------------------------------------------------------------------------------------
    // SUBSCRIBERS
    //----------------------------------------------------------------------------------------------

    /**
     * Subscriber for handling Bluetooth Adapter Management state changes.
     */
    protected final Action1<StateChanged<BluetoothManagementState>> onAdapterManagementStateChange =
            new Action1<StateChanged<BluetoothManagementState>>() {
                @Override
                public void call(StateChanged<BluetoothManagementState> stateChanged) {
                    if (stateChanged.getEvent() instanceof BluetoothScanResultsUpdated) {
                        facade.handleCommand(new ProcessBluetoothScanResults.Builder()
                                .scanResults(stateChanged.getState().scanResults())
                                .build());
                    }
                }
            };

    //----------------------------------------------------------------------------------------------
    // PRIVATE METHODS
    //----------------------------------------------------------------------------------------------

    /**
     * Filters detected beacons from incoming scan results.
     * <p>
     * For each registered provider the scan record is checked to determine of which beacon type
     * the scan record belongs to. If it does, a detected beacon object is generated an added to a
     * HashMap.
     *
     * @param scanResults BluetoothScanResult containing advertisement packet
     * @return HashMap of mac address to detected beacon objects.
     */
    protected Map<String, DetectedBeacon> filterDetectedBeacons(Map<String, BluetoothScanResult> scanResults) {
        Map<String, DetectedBeacon> detectedBeacons = new LinkedHashMap<>();

        if (beaconProviders.size() == 0) {
            return detectedBeacons;
        }

        for (BluetoothScanResult scanResult : scanResults.values()) {
            for (BeaconProvider provider : beaconProviders.values()) {
                byte[] scanRecord = scanResult.scanRecord();

                if (!provider.getTranslator().isOfBeaconType(scanRecord)) continue;

                DetectedBeacon detectedBeacon = provider.getFactory().createDetectedBeacon(scanResult);
                detectedBeacons.put(scanResult.macAddress(), detectedBeacon);
            }
        }

        return detectedBeacons;
    }

    /**
     * Filters the collection of detected beacons by the provided beacon type - concrete
     * {@link DetectedBeacon} implementation.
     *
     * @param detectedBeacons Collection of detected beacons.
     * @param beaconType      Concrete class of detected beacon.
     * @param <T>             Indicates the concrete beacon type.
     * @return A filtered list.
     */
    protected <T extends DetectedBeacon> List<T> filterBeaconsByType(Collection<DetectedBeacon> detectedBeacons, Class<T> beaconType) {
        // noinspection Guava
        return FluentIterable
                .from(detectedBeacons)
                .filter(beaconType)
                .toList();
    }

    /**
     * Finds a registered (if any) provider for a given detected beacon.
     *
     * @param beacon DetectedBeacon
     * @return BeaconProvider
     */
    private BeaconProvider getProviderForBeacon(DetectedBeacon beacon) {
        for (BeaconProvider provider : beaconProviders.values()) {
            if (provider.getBeaconType().isInstance(beacon)) {
                return provider;
            }
        }
        return null;
    }
}
