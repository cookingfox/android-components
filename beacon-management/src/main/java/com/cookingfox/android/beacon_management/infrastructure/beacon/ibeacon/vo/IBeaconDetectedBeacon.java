package com.cookingfox.android.beacon_management.infrastructure.beacon.ibeacon.vo;

import com.cookingfox.android.beacon_management.domain.beacon.DetectedBeacon;
import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;

import org.immutables.value.Value;

@DefaultValueStyle
@Value.Immutable
public interface IBeaconDetectedBeacon extends DetectedBeacon {

    int major();

    String manufacturerName();

    int minor();

    class Builder extends ImmutableIBeaconDetectedBeacon.Builder {
    }
}
