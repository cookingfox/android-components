package com.cookingfox.android.beacon_management.infrastructure.beacon.blue_maestro_button.connected;

import com.cookingfox.android.beacon_management.domain.beacon.ConnectedBeacon;
import com.cookingfox.android.beacon_management.infrastructure.beacon.blue_maestro_button.vo.BlueMaestroButtonDetectedBeacon;
import com.cookingfox.android.bluetooth_management.domain.connection.BluetoothConnection;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothConnectionResult;

import rx.Observable;

/**
 * Connected beacon implementation for Blue Maestro button.
 */
public class BlueMaestroButtonConnectedBeacon implements ConnectedBeacon<BlueMaestroButtonDetectedBeacon> {

    //----------------------------------------------------------------------------------------------
    // PROPERTIES
    //----------------------------------------------------------------------------------------------

    protected final BluetoothConnection connection;
    protected final BlueMaestroButtonDetectedBeacon detectedDevice;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public BlueMaestroButtonConnectedBeacon(BluetoothConnection connection, BlueMaestroButtonDetectedBeacon detectedDevice) {
        this.connection = connection;
        this.detectedDevice = detectedDevice;
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    public Observable<BluetoothConnectionResult> disconnect() {
        return connection.disconnect();
    }

    @Override
    public BlueMaestroButtonDetectedBeacon getDetectedDevice() {
        return detectedDevice;
    }

    @Override
    public BluetoothConnection getConnection() {
        return connection;
    }
}
