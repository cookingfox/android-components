package com.cookingfox.android.beacon_management.infrastructure.beacon.ibeacon.connected;

import com.cookingfox.android.beacon_management.domain.beacon.ConnectedBeacon;
import com.cookingfox.android.beacon_management.infrastructure.beacon.ibeacon.vo.IBeaconDetectedBeacon;
import com.cookingfox.android.bluetooth_management.domain.connection.BluetoothConnection;
import com.cookingfox.android.bluetooth_management.domain.manager.BluetoothDeviceConnectionManager;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothConnectionResult;

import rx.Observable;

public class ConnectedIBeaconBeacon implements ConnectedBeacon<IBeaconDetectedBeacon> {

    private BluetoothConnection connection;
    private BluetoothDeviceConnectionManager connectionManager;
    private IBeaconDetectedBeacon detectedDevice;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public ConnectedIBeaconBeacon(BluetoothConnection connection,
                                  BluetoothDeviceConnectionManager connectionManager,
                                  IBeaconDetectedBeacon detectedDevice) {
        this.connection = connection;
        this.connectionManager = connectionManager;
        this.detectedDevice = detectedDevice;
    }

    //----------------------------------------------------------------------------------------------
    // GETTER METHODS
    //----------------------------------------------------------------------------------------------

    public Observable<BluetoothConnectionResult> disconnect() {
        return connection.disconnect();
    }

    @Override
    public IBeaconDetectedBeacon getDetectedDevice() {
        return detectedDevice;
    }

    @Override
    public BluetoothConnection getConnection() {
        return connection;
    }
}
