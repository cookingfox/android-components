package com.cookingfox.android.beacon_management.infrastructure.beacon.blue_maestro_button.vo;

import com.cookingfox.android.beacon_management.domain.beacon.DetectedBeacon;

import org.immutables.value.Value;

/**
 * Value Object for a detected Blue Maestro button beacon.
 */
@Value.Immutable
public abstract class BlueMaestroButtonDetectedBeacon implements DetectedBeacon {

    /**
     * @return The beacon's battery level.
     */
    public abstract int batteryLevel();

    /**
     * @return The button's pressed state, typically 0 or 1.
     */
    public abstract int pressedState();

    /**
     * @return Whether the button is pressed.
     */
    public boolean isPressed() {
        return pressedState() == 1;
    }

    public static class Builder extends ImmutableBlueMaestroButtonDetectedBeacon.Builder {
    }

}
