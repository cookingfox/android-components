package com.cookingfox.android.beacon_management.infrastructure.beacon.blue_maestro_button.provider;

import com.cookingfox.android.beacon_management.domain.factory.BeaconFactory;
import com.cookingfox.android.beacon_management.domain.provider.BeaconProvider;
import com.cookingfox.android.beacon_management.infrastructure.beacon.blue_maestro_button.connected.BlueMaestroButtonConnectedBeacon;
import com.cookingfox.android.beacon_management.infrastructure.beacon.blue_maestro_button.factory.BlueMaestroButtonBeaconFactory;
import com.cookingfox.android.beacon_management.infrastructure.beacon.blue_maestro_button.translator.BlueMaestroButtonBeaconTranslator;
import com.cookingfox.android.beacon_management.infrastructure.beacon.blue_maestro_button.vo.BlueMaestroButtonDetectedBeacon;
import com.cookingfox.android.bluetooth_management.domain.connection.BluetoothConnection;

/**
 * Wraps the necessary objects to be able to detect, read and connect to a Blue Maestro button
 * beacon.
 */
public class BlueMaestroButtonBeaconProvider implements BeaconProvider<
        BlueMaestroButtonBeaconTranslator,
        BlueMaestroButtonDetectedBeacon,
        BlueMaestroButtonConnectedBeacon> {

    //----------------------------------------------------------------------------------------------
    // PROPERTIES
    //----------------------------------------------------------------------------------------------

    protected final BlueMaestroButtonBeaconFactory factory;
    protected final BlueMaestroButtonBeaconTranslator translator;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public BlueMaestroButtonBeaconProvider(BlueMaestroButtonBeaconFactory factory, BlueMaestroButtonBeaconTranslator translator) {
        this.factory = factory;
        this.translator = translator;
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public BlueMaestroButtonConnectedBeacon createConnectedBeaconInstance(BluetoothConnection connection, BlueMaestroButtonDetectedBeacon detectedDevice) {
        return new BlueMaestroButtonConnectedBeacon(connection, detectedDevice);
    }

    @Override
    public Class<BlueMaestroButtonDetectedBeacon> getBeaconType() {
        return BlueMaestroButtonDetectedBeacon.class;
    }

    @Override
    public BeaconFactory<BlueMaestroButtonDetectedBeacon> getFactory() {
        return factory;
    }

    @Override
    public BlueMaestroButtonBeaconTranslator getTranslator() {
        return translator;
    }

}
