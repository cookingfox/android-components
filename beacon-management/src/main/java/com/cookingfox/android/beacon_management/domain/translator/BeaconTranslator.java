package com.cookingfox.android.beacon_management.domain.translator;

public interface BeaconTranslator {

    /**
     * 'Asks' the corresponding advertisement implementation whether the data packet is originating
     * from a specific Beacon Implementation Type, e.g. iBeacon, altBeacon, EddyStone etc.
     *
     * @param adPacket Advertisement packet.
     * @return True if the packet is a positive match for type of advertisement.
     */
    boolean isOfBeaconType(byte[] adPacket);

}
