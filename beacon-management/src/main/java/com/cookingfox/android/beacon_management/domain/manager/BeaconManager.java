package com.cookingfox.android.beacon_management.domain.manager;

import com.cookingfox.android.beacon_management.domain.beacon.ConnectedBeacon;
import com.cookingfox.android.beacon_management.domain.beacon.DetectedBeacon;
import com.cookingfox.android.beacon_management.domain.provider.BeaconProvider;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothConnectionResult;

import java.util.List;

import rx.Observable;

/**
 * Acts as main accesspoint for beacon related actions. Provides functionality to register beacon
 * providers and respond to beacon connectivity changes.
 */
public interface BeaconManager {

    /**
     * Sugar method for 'connectWithBeaconExpectRaw', casting the connected beacon to the
     * provided type, returning an observable of specific connected beacon type.
     *
     * @param connectedBeaconType Type of connection class
     * @param beacon              The detected beacon object
     * @param <T>                 e.g. MinewPatientConnectedBeacon
     * @return An Observable of a connected beacon, e.g. MinewPatientConnectedBeacon
     */
    <T extends ConnectedBeacon> Observable<T> connectWithBeacon(Class<T> connectedBeaconType, DetectedBeacon beacon);

    /**
     * Establishes a connection of a given type with the provided detected beacon object.
     * Returns an Observable as soon as a beacon gets connected. From there on further actions
     * (read/write) can be performed.
     *
     * @param beacon The detected beacon object
     * @return An Observable of a connected beacon, e.g. MinewPatientConnectedBeacon
     */
    Observable<BluetoothConnectionResult> connectWithBeaconExpectRaw(DetectedBeacon beacon);

    /**
     * Observe for a specific type of beacon.
     * <p>
     * Observing a beacon of a certain type only passes through valid detects of the provided type.
     *
     * @param beaconType type of beacon, e.g. MinewPatientDetectedBeacon.class
     * @param <T>        provided type of beacon
     * @return Observable containing list of detected beacons
     */
    <T extends DetectedBeacon> Observable<List<T>> observeBeaconType(Class<T> beaconType);

    /**
     * Registers a provider for a certain type of beacon.
     * <p>
     * Before observing beacons of a certain type a provider should be registered.
     *
     * @param provider beacon provider, e.g. MinewPatientBeaconProvider
     */
    void registerProvider(Class<? extends BeaconProvider> provider);

    /**
     * Unregisters an existing provider.
     *
     * @param provider beacon provider, e.g. MinewPatientBeaconProvider
     */
    void unregisterProvider(Class<? extends BeaconProvider> provider);

}
