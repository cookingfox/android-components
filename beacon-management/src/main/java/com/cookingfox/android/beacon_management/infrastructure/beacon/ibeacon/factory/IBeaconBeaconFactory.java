package com.cookingfox.android.beacon_management.infrastructure.beacon.ibeacon.factory;

import com.cookingfox.android.beacon_management.domain.factory.BeaconFactory;
import com.cookingfox.android.beacon_management.infrastructure.beacon.ibeacon.translator.IBeaconBeaconTranslator;
import com.cookingfox.android.beacon_management.infrastructure.beacon.ibeacon.vo.IBeaconDetectedBeacon;
import com.cookingfox.android.bluetooth_management.domain.vo.scanning.BluetoothScanResult;

public class IBeaconBeaconFactory implements BeaconFactory<IBeaconDetectedBeacon> {

    private final IBeaconBeaconTranslator translator;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public IBeaconBeaconFactory(IBeaconBeaconTranslator translator) {
        this.translator = translator;
    }

    @Override
    public IBeaconDetectedBeacon createDetectedBeacon(BluetoothScanResult scanResult) {
        byte[] adPacket = scanResult.scanRecord();

        return new IBeaconDetectedBeacon.Builder()
                .scanResult(scanResult)
                .major(translator.getMajor(adPacket))
                .minor(translator.getMinor(adPacket))
                .manufacturerName(translator.getManufacturerName(adPacket))
                .build();
    }

}
