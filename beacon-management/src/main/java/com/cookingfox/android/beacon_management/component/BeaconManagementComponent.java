package com.cookingfox.android.beacon_management.component;

import com.cookingfox.android.beacon_management.component.config.BeaconManagementCheflingConfig;
import com.cookingfox.android.beacon_management.domain.facade.BeaconManagementFacade;
import com.cookingfox.android.beacon_management.domain.manager.BeaconManager;
import com.cookingfox.android.beacon_management.domain.preferences.BeaconManagementPreferences;
import com.cookingfox.android.bluetooth_management.component.BluetoothManagementComponent;
import com.cookingfox.android.core_component.api.component.Component;
import com.cookingfox.android.core_component.api.component.config.ComponentCheflingConfig;
import com.cookingfox.android.core_component.api.component.manager.ComponentManager;
import com.cookingfox.android.prefer.api.pref.Prefs;
import com.cookingfox.chefling.api.CheflingContainer;
import com.cookingfox.lapasse.api.facade.Facade;

/**
 * Component implementation for beacon management.
 */
public class BeaconManagementComponent implements Component {

    private CheflingContainer container;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public BeaconManagementComponent(CheflingContainer container) {
        this.container = container;
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public ComponentCheflingConfig getCheflingConfig() {
        return new BeaconManagementCheflingConfig();
    }

    @Override
    public Facade getLaPasseFacade() {
        return container.getInstance(BeaconManagementFacade.class);
    }

    @Override
    public Prefs getPrefs() {
        return container.getInstance(BeaconManagementPreferences.class);
    }

    @Override
    public void loadComponent(ComponentManager componentManager) {
        componentManager.loadComponent(BluetoothManagementComponent.class);
    }

    @Override
    public void registerComponent() {
        container.getInstance(BeaconManager.class);
    }

    @Override
    public void unregisterComponent() {
        container.removeInstanceAndMapping(BeaconManager.class);
    }
}
