package com.cookingfox.android.beacon_management.domain.event;

import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;
import com.cookingfox.lapasse.api.event.Event;

import org.immutables.value.Value;

@DefaultValueStyle
@Value.Immutable
public interface BeaconProviderRegistered extends Event {
    class Builder extends ImmutableBeaconProviderRegistered.Builder {
    }
}
