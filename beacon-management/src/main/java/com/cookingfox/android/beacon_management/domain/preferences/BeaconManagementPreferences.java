package com.cookingfox.android.beacon_management.domain.preferences;

import com.cookingfox.android.prefer_rx.api.pref.RxPrefs;

/**
 * Beacon Management Preferences
 */
public interface BeaconManagementPreferences
        extends RxPrefs<BeaconManagementPreferences.Key> {

    enum Key {
        BeaconScanningBufferTimeMs
    }

}
