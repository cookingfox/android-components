package com.cookingfox.android.beacon_management.domain.command;

import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;
import com.cookingfox.lapasse.api.command.Command;

import org.immutables.value.Value;

@DefaultValueStyle
@Value.Immutable
public interface RegisterBeaconProvider extends Command {
    class Builder extends ImmutableRegisterBeaconProvider.Builder {
    }
}
