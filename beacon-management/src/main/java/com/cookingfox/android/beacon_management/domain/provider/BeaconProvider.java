package com.cookingfox.android.beacon_management.domain.provider;

import com.cookingfox.android.beacon_management.domain.beacon.ConnectedBeacon;
import com.cookingfox.android.beacon_management.domain.beacon.DetectedBeacon;
import com.cookingfox.android.beacon_management.domain.factory.BeaconFactory;
import com.cookingfox.android.beacon_management.domain.translator.BeaconTranslator;
import com.cookingfox.android.bluetooth_management.domain.connection.BluetoothConnection;

/**
 * A Beacon Provider provides and bundles all the necessary tools needed to validate and connect
 * with specific beacon types.
 *
 * @param <T> BeaconTranslator - Used to translate advertising packets into human readable data.
 * @param <D> DetectedBeacon - Contains specific information regarding the detected beacon.
 * @param <C> ConnectedBeacon - Provides specific actions for the connected beacon type.
 */
public interface BeaconProvider<T extends BeaconTranslator, D extends DetectedBeacon, C extends ConnectedBeacon> {

    /**
     * After a beacon connection is established, the connection itself and the detected device for
     * which the connection was created is constructed into a Connected Beacon instance.
     *
     * @param connection     ConnectedBluetoothDevice containing connection hooks
     * @param detectedDevice DetectedBeacon
     * @return Object Extending ConnectedBeacon
     */
    C createConnectedBeaconInstance(BluetoothConnection connection, D detectedDevice);

    /**
     * Returns the specific beacon type the provider is using, e.g. MinewPatientDetectedBeacon.
     *
     * @return Specific Detected Beacon Type Extending DetectedBeacon
     */
    Class<D> getBeaconType();

    /**
     * Returns the factory for the specific Detected Beacon Type.
     *
     * @return Specific Detected Beacon Type Extending DetectedBeacon
     */
    BeaconFactory<D> getFactory();

    /**
     * Returns the specific translator for a specific beacon type.
     *
     * @return Translator For Specific Beacon Type Extending DetectedBeacon
     */
    T getTranslator();

}
