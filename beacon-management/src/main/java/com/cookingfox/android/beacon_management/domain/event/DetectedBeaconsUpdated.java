package com.cookingfox.android.beacon_management.domain.event;

import com.cookingfox.android.beacon_management.domain.beacon.DetectedBeacon;
import com.cookingfox.android.bluetooth_management.domain.vo.scanning.BluetoothScanResult;
import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;
import com.cookingfox.lapasse.api.event.Event;

import org.immutables.value.Value;

import java.util.Map;

@DefaultValueStyle
@Value.Immutable
public interface DetectedBeaconsUpdated extends Event {

    Map<String, DetectedBeacon> detectedBeacons();

    Map<String, BluetoothScanResult> junkScanResults();

    class Builder extends ImmutableDetectedBeaconsUpdated.Builder {
    }
}
