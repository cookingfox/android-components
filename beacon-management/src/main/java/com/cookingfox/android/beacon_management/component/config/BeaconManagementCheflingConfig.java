package com.cookingfox.android.beacon_management.component.config;

import com.cookingfox.android.beacon_management.android.preferences.AndroidBeaconManagementPreferences;
import com.cookingfox.android.beacon_management.domain.facade.BeaconManagementFacade;
import com.cookingfox.android.beacon_management.domain.manager.BeaconManager;
import com.cookingfox.android.beacon_management.domain.manager.BeaconManagerImpl;
import com.cookingfox.android.beacon_management.domain.preferences.BeaconManagementPreferences;
import com.cookingfox.android.beacon_management.domain.state.BeaconManagementState;
import com.cookingfox.android.beacon_management.domain.state.BeaconManagementStateObserver;
import com.cookingfox.android.core_component.api.component.config.ComponentCheflingConfig;
import com.cookingfox.android.core_component.impl.threading.ComponentThreadPoolProvider;
import com.cookingfox.chefling.api.CheflingBuilder;
import com.cookingfox.chefling.api.CheflingConfig;
import com.cookingfox.chefling.impl.Chefling;
import com.cookingfox.lapasse.impl.facade.LaPasseRxFacade;

import rx.schedulers.Schedulers;

public class BeaconManagementCheflingConfig implements ComponentCheflingConfig {

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public CheflingBuilder createCheflingBuilder() {
        return Chefling.createBuilder()
                .addConfig(domainConfig);
    }

    @Override
    public CheflingConfig createDeviceConfig() {
        return null;
    }

    @Override
    public CheflingConfig createEmulatorConfig() {
        return null;
    }

    @Override
    public CheflingConfig createTestConfig() {
        return null;
    }

    //----------------------------------------------------------------------------------------------
    // DOMAIN CONFIGURATION
    //----------------------------------------------------------------------------------------------

    public final CheflingConfig domainConfig = container -> {
        // Mapping factory for facade
        container.mapFactory(BeaconManagementFacade.class, c -> {
            BeaconManagementState initialState = BeaconManagementState.createInitialState();

            LaPasseRxFacade<BeaconManagementState> facade = new LaPasseRxFacade.Builder<>(initialState).build();

            // note: set scheduler for all commands that return an Rx Observable
            ComponentThreadPoolProvider threadPoolProvider = c.getInstance(ComponentThreadPoolProvider.class);
            facade.setCommandSubscribeScheduler(Schedulers.from(threadPoolProvider.getExecutor()));

            return new BeaconManagementFacade(facade);
        });

        container.mapType(BeaconManagementPreferences.class, AndroidBeaconManagementPreferences.class);

        container.mapType(BeaconManagementStateObserver.class, BeaconManagementFacade.class);

        //Map Managers
        container.mapType(BeaconManager.class, BeaconManagerImpl.class);
    };

}
