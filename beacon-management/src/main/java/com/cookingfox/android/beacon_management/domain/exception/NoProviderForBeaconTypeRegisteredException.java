package com.cookingfox.android.beacon_management.domain.exception;

public class NoProviderForBeaconTypeRegisteredException extends Exception {
    public NoProviderForBeaconTypeRegisteredException(Class<?> beaconType) {
        super(beaconType.getSimpleName());
    }
}
