package com.cookingfox.android.beacon_management.domain.factory;

import com.cookingfox.android.beacon_management.domain.beacon.DetectedBeacon;
import com.cookingfox.android.bluetooth_management.domain.vo.scanning.BluetoothScanResult;

public interface BeaconFactory<T extends DetectedBeacon> {

    T createDetectedBeacon(BluetoothScanResult scanResult);

}
