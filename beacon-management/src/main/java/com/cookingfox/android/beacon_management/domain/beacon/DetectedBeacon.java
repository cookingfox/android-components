package com.cookingfox.android.beacon_management.domain.beacon;

import com.cookingfox.android.bluetooth_management.domain.device.DetectedBluetoothDevice;

public interface DetectedBeacon extends DetectedBluetoothDevice {
}
