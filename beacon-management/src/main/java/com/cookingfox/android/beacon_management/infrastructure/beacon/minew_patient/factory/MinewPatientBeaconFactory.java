package com.cookingfox.android.beacon_management.infrastructure.beacon.minew_patient.factory;

import com.cookingfox.android.beacon_management.domain.factory.BeaconFactory;
import com.cookingfox.android.beacon_management.infrastructure.beacon.minew_patient.translator.MinewPatientBeaconTranslator;
import com.cookingfox.android.beacon_management.infrastructure.beacon.minew_patient.vo.MinewPatientDetectedBeacon;
import com.cookingfox.android.bluetooth_management.domain.vo.scanning.BluetoothScanResult;

public class MinewPatientBeaconFactory implements BeaconFactory<MinewPatientDetectedBeacon> {

    private final MinewPatientBeaconTranslator translator;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public MinewPatientBeaconFactory(MinewPatientBeaconTranslator translator) {
        this.translator = translator;
    }

    //----------------------------------------------------------------------------------------------
    // OVERRIDDEN REQUIRED METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public MinewPatientDetectedBeacon createDetectedBeacon(BluetoothScanResult scanResult) {
        byte[] adPacket = scanResult.scanRecord();

        return new MinewPatientDetectedBeacon.Builder()
                .batteryLevel(translator.getBatteryLevel(adPacket))
                .controlState(translator.getState(adPacket))
                .customIdentifier(translator.getCustomIdentifier(adPacket))
                .productCode(translator.getProductCode(adPacket))
                .scanResult(scanResult)
                .build();
    }
}
