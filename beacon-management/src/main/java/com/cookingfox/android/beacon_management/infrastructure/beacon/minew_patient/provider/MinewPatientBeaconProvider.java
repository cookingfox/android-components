package com.cookingfox.android.beacon_management.infrastructure.beacon.minew_patient.provider;

import com.cookingfox.android.beacon_management.domain.factory.BeaconFactory;
import com.cookingfox.android.beacon_management.domain.provider.BeaconProvider;
import com.cookingfox.android.beacon_management.infrastructure.beacon.minew_patient.connected.MinewPatientConnectedBeacon;
import com.cookingfox.android.beacon_management.infrastructure.beacon.minew_patient.factory.MinewPatientBeaconFactory;
import com.cookingfox.android.beacon_management.infrastructure.beacon.minew_patient.translator.MinewPatientBeaconTranslator;
import com.cookingfox.android.beacon_management.infrastructure.beacon.minew_patient.vo.MinewPatientDetectedBeacon;
import com.cookingfox.android.bluetooth_management.domain.connection.BluetoothConnection;
import com.cookingfox.android.bluetooth_management.domain.manager.BluetoothDeviceConnectionManager;

public class MinewPatientBeaconProvider implements BeaconProvider<MinewPatientBeaconTranslator,
        MinewPatientDetectedBeacon, MinewPatientConnectedBeacon> {

    private final BluetoothDeviceConnectionManager connectionManager;
    private final MinewPatientBeaconFactory factory;
    private final MinewPatientBeaconTranslator translator;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public MinewPatientBeaconProvider(BluetoothDeviceConnectionManager connectionManager,
                                      MinewPatientBeaconFactory factory,
                                      MinewPatientBeaconTranslator translator) {
        this.connectionManager = connectionManager;
        this.factory = factory;
        this.translator = translator;
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public MinewPatientConnectedBeacon createConnectedBeaconInstance(BluetoothConnection connection,
                                                                     MinewPatientDetectedBeacon detectedDevice) {
        return new MinewPatientConnectedBeacon(connection, connectionManager, detectedDevice);
    }

    @Override
    public Class<MinewPatientDetectedBeacon> getBeaconType() {
        return MinewPatientDetectedBeacon.class;
    }

    @Override
    public BeaconFactory<MinewPatientDetectedBeacon> getFactory() {
        return factory;
    }

    @Override
    public MinewPatientBeaconTranslator getTranslator() {
        return translator;
    }
}
