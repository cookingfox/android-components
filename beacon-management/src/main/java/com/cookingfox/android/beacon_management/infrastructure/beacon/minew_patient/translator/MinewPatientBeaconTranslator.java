package com.cookingfox.android.beacon_management.infrastructure.beacon.minew_patient.translator;

import com.cookingfox.android.beacon_management.domain.translator.BeaconTranslator;
import com.cookingfox.android.beacon_management.infrastructure.beacon.minew_patient.values.MinewPatientControlState;
import com.cookingfox.android.beacon_management.infrastructure.beacon.translator.BaseBeaconTranslator;

public class MinewPatientBeaconTranslator extends BaseBeaconTranslator implements BeaconTranslator {

    private static final String LOCAL_NAME = "MINEW Tag";

    //----------------------------------------------------------------------------------------------
    // AD0
    //----------------------------------------------------------------------------------------------

    private static final byte AD0_LENGTH = 0x02; // Total upcoming data length:  2 bytes
    private static final byte AD0_TYPE = 0x01;   // Data type:                   0/1 flags
    private static final byte PAYLOAD = 0x1A;    // LE and BR/EDR:               0/1 flags

    //----------------------------------------------------------------------------------------------
    // AD1
    //----------------------------------------------------------------------------------------------

    /**
     * LOCAL_NAME_INDICATOR - References start of name set by the manufacturer (no manufacturer name!!)
     * SERVICE_DATA_INDICATOR - References start of service data, e.g. battery level, control point etc.
     */
    private static final byte LOCAL_TAG_NAME_INDICATOR = 0x09;  // Name of the beacon (set in hardware)
    private static final byte SERVICE_DATA_INDICATOR = 0x16;    // Service data indicator
    private static final int PRODUCT_CODE_PATIENT_TAG = 25;     // Product code for the Minew Patient Tag (0x19)

    private static final int LOCAL_NAME_OFFSET = 20;            // Position of where local name starts
    private static final int SERVICE_DATA_OFFSET = 4;           // Position of where service data starts
    private static final int SERVICE_DATA_LENGTH = 14;

    private static final int PRODUCT_CODE_LOCATION = 3;         // Position of product code value within service data
    private static final int BATTERY_LEVEL_LOCATION = 4;        // Position of battery level value within service data
    private static final int STATE_LOCATION = 5;                // Position of state value within service data
    private static final int CUSTOM_IDENTIFIER_LOCATION = 12;    // Position of custom identifier value within service data

    //----------------------------------------------------------------------------------------------
    // SERVICE DATA
    //----------------------------------------------------------------------------------------------

    private static final byte SERVICE_UUID = 0x0001;
    private static final byte STATE_SLEEP = 0x01;
    private static final byte STATE_ARMED = 0x00;
    private static final byte STATE_ACTIVE = 0x02;

    //----------------------------------------------------------------------------------------------
    // REQUIRED OVERRIDDEN METHODS
    //----------------------------------------------------------------------------------------------

    /**
     * Checks if the provided advertisement package is of MinewPatientBeacon type.
     * <p>
     * The following steps determine if the beacon is of type Minew Patient:
     * - Checks if the advertisement packet has a valid length before processing
     * - Checks if service data length is of 14 bytes as defined in Minew Patient Protocol
     * - Confirms the product code identifier is equal to 0x19 (int 25), a Minew Patient Tag
     * - Validates if the local tag name is of MINEW Tag
     *
     * @param adPacket Advertisement packet.
     * @return True if of type iBeacon.
     */
    @Override
    public boolean isOfBeaconType(byte[] adPacket) {
        // Make sure we don't get ArrayOutOfBoundsException
        if (adPacket.length < SERVICE_DATA_OFFSET + SERVICE_DATA_LENGTH) return false;

        // The '- 1' is pointing to the 'length' value before the first service data byte
        if (((int) adPacket[SERVICE_DATA_OFFSET - 1] & 0xFF) != SERVICE_DATA_LENGTH) return false;

        if (getProductCode(adPacket) != PRODUCT_CODE_PATIENT_TAG) {
            return false;
        }

        byte[] localNameBytes = new byte[9];
        System.arraycopy(adPacket, LOCAL_NAME_OFFSET, localNameBytes, 0, 9);
        String localName = new String(localNameBytes);

        return localName.equals(LOCAL_NAME);
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC CUSTOM METHODS
    //----------------------------------------------------------------------------------------------

    /**
     * Returns the battery level in percentage.
     * <p>
     * 0 = empty / 100 = full
     *
     * @param adPacket Advertisement packet.
     * @return int Battery Level Percentage.
     */
    public int getBatteryLevel(byte[] adPacket) {
        return ((int) adPacket[SERVICE_DATA_OFFSET + BATTERY_LEVEL_LOCATION] & 0xFF);
    }

    /**
     * Returns the custom set identifier.
     *
     * @param adPacket Advertisement packet.
     * @return int Custom Identifier Value
     */
    public int getCustomIdentifier(byte[] adPacket) {
        return (adPacket[SERVICE_DATA_OFFSET + CUSTOM_IDENTIFIER_LOCATION] & 0xFF) * 0x100 +
                (adPacket[SERVICE_DATA_OFFSET + CUSTOM_IDENTIFIER_LOCATION + 1] & 0xFF);
    }

    /**
     * Extracts the current state, either sleep, armed or active.
     *
     * @param adPacket Advertisement packet.
     * @return MinewPatientControlState Control State (SLEEP, ARMED, ACTIVE)
     */
    public MinewPatientControlState getState(byte[] adPacket) throws IllegalStateException {
        switch (adPacket[SERVICE_DATA_OFFSET + STATE_LOCATION]) {
            case STATE_SLEEP:
                return MinewPatientControlState.SLEEP;
            case STATE_ARMED:
                return MinewPatientControlState.ARMED;
            case STATE_ACTIVE:
                return MinewPatientControlState.ACTIVE;
        }

        throw new IllegalStateException(
                String.format("Device's state value is of unknown type: %s",
                        (int) adPacket[SERVICE_DATA_OFFSET + STATE_LOCATION]));
    }

    /**
     * Extracts the product code which indicates whether it's a button/patient tag etc.
     *
     * @param adPacket Advertisement packet.
     * @return int Product code.
     */
    public int getProductCode(byte[] adPacket) {
        return ((int) adPacket[SERVICE_DATA_OFFSET + PRODUCT_CODE_LOCATION] & 0xFF);
    }
}
