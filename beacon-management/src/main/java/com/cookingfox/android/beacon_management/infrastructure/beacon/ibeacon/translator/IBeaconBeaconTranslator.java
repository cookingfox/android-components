package com.cookingfox.android.beacon_management.infrastructure.beacon.ibeacon.translator;

import com.cookingfox.android.beacon_management.domain.translator.BeaconTranslator;
import com.cookingfox.android.beacon_management.infrastructure.beacon.translator.BaseBeaconTranslator;
import com.cookingfox.logging.Logger;

public class IBeaconBeaconTranslator extends BaseBeaconTranslator implements BeaconTranslator {

    /**
     * Manufacturer name based on official valid Bluetooth company identifier names.
     * {https://www.bluetooth.com/speci fications/assigned-numbers/company-identifiers}
     */
    private static final String MANUFACTURER_NAME = "Apple, Inc.";

    /**
     * AD0 - First Advertisement Data Set
     * <p>
     * First part of the iBeacon advertisement packet, consisting out of 4 bytes.
     * Indicates the general beacon dataset configuration:
     * <p>
     * Bluetooth (...):
     * - BR: Basic Rate
     * - EDR: Enhanced Data Rate
     * <p>
     * Most of the times these packets are skipped for regular beacon scanning and identification.
     */

    private static final byte AD0_LENGTH = 0x02; // Total upcoming data length:  2 bytes
    private static final byte AD0_TYPE = 0x01;   // Data type:                   0/1 flags
    private static final byte PAYLOAD = 0x1A;    // LE and BR/EDR:               0/1 flags

    /**
     * AD1 - Second Advertisement Data Set
     * <p>
     * Second part of the iBeacon advertisement packet, consisting out of 27 bytes for iBeacon.
     */

    private static final byte AD1_LENGTH = 0x1A;             // Total data length:           26 bytes
    private static final byte AD1_TYPE = (byte) 0xFF;
    private static final byte COMPANY_ID_FIRST = 0x4C;
    private static final byte COMPANY_ID_SECOND = 0x00;
    private static final byte INDICATOR = 0x02;              // Apple specific indicator
    private static final byte PROXIMITY_UUID_LENGTH = 0x15;  // Length of upcoming UUID:     21 bytes

    private static final int COMPANY_ID_FIRST_OFFSET = 5;           // 5th byte = company id part 1
    private static final int COMPANY_ID_SECOND_OFFSET = 6;          // 6th byte = companu id part 2
    private static final int INDICATOR_OFFSET = 7;                  // 7th byte = indicator
    private static final int MAJOR_OFFSET = 25;                     // 25th & 26th byte = major
    private static final int MINOR_OFFSET = 27;                     // 27th & 28th byte = minor
    private static final int PROXIMITY_UUID_LENGTH_OFFSET = 8;      // 8th byte = UUID length
    private static final int UUID_OFFSET = 9;

    //----------------------------------------------------------------------------------------------
    // REQUIRED OVERRIDDEN METHODS
    //----------------------------------------------------------------------------------------------

    /**
     * Checks if the provided advertisement packet is of iBeacon type by confirming the following:
     * <p>
     * - position startByteAd1 + 2 (bitwise used to sign unsigned) - Is Apple Specific indicator
     * - position startByteAd1 + 3 - Is of correct iBeacon data length.
     *
     * @param adPacket Advertisement packet.
     * @return True if of type iBeacon.
     */
    @Override
    public boolean isOfBeaconType(byte[] adPacket) {
        if (adPacket.length < (int) AD1_LENGTH) return false;

        return (((int) adPacket[INDICATOR_OFFSET] & 0xFF) == INDICATOR &&
                ((int) adPacket[PROXIMITY_UUID_LENGTH_OFFSET] & 0xFF) == PROXIMITY_UUID_LENGTH);
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC CUSTOM METHODS
    //----------------------------------------------------------------------------------------------

    /**
     * Returns the beacons' major, which may contain up to 5 digits.
     *
     * @param adPacket Advertisement packet.
     * @return int major id
     */
    public int getMajor(byte[] adPacket) {
        if (!isOfBeaconType(adPacket)) return notifyInvalidBeaconInteger();

        return (adPacket[MAJOR_OFFSET] & 0xFF) * 0x100 + (adPacket[MAJOR_OFFSET + 1] & 0xFF);
    }

    /**
     * Returns the beacons' minor, which may contain up to 5 digits.
     *
     * @param adPacket Advertisement packet.
     * @return int minor id
     */
    public int getMinor(byte[] adPacket) {
        if (!isOfBeaconType(adPacket)) return notifyInvalidBeaconInteger();

        return ((adPacket[MINOR_OFFSET] & 0xFF) * 0x100 + (adPacket[MINOR_OFFSET + 1] & 0xFF));
    }

    /**
     * Presents the manufacturer name if the iBeacon contains the correct identifier.
     *
     * @param adPacket Advertisement packet.
     * @return String Manufacturer Name.
     */
    public String getManufacturerName(byte[] adPacket) {
        if (!isOfBeaconType(adPacket)) return notifyInvalidBeaconString();

        if (adPacket[COMPANY_ID_FIRST_OFFSET] == COMPANY_ID_FIRST &&
                adPacket[COMPANY_ID_SECOND_OFFSET] == COMPANY_ID_SECOND)
            return MANUFACTURER_NAME;

        return notifyInvalidBeaconString();
    }

    /**
     * Extracts the UUID from the iBeacon.
     * <p>
     * The iBeacon has a so called Proximity UUID with a length of 16 bytes.
     *
     * @param adPacket Advertisement packet.
     * @return String containing proximity id.
     */
    public String getProximityId(byte[] adPacket) {
        if (!isOfBeaconType(adPacket)) return notifyInvalidBeaconString();

        byte[] uuidBytes = new byte[16];
        System.arraycopy(adPacket, UUID_OFFSET, uuidBytes, 0, 16);
        return bytesToHex(uuidBytes);
    }

    //----------------------------------------------------------------------------------------------
    // PRIVATE METHODS
    //----------------------------------------------------------------------------------------------

    // TODO: 7/5/16 Throw/handle exceptions and remove logs

    /**
     * Shows an 'invalid beacon for String action' error in the console.
     *
     * @return Empty string
     */
    private String notifyInvalidBeaconString() {
        Logger.error("Beacon is not of type 'iBeacon'!");
        return "";
    }

    /**
     * Shows an 'invalid beacon for Integer action' error in the console.
     *
     * @return Empty string
     */
    private int notifyInvalidBeaconInteger() {
        Logger.error("Beacon is not of type 'iBeacon'!");
        return 0;
    }
}
