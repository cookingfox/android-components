package com.cookingfox.android.beacon_management.domain.facade;

import com.cookingfox.android.beacon_management.domain.state.BeaconManagementState;
import com.cookingfox.android.beacon_management.domain.state.BeaconManagementStateObserver;
import com.cookingfox.lapasse.api.facade.RxFacade;
import com.cookingfox.lapasse.impl.facade.LaPasseRxFacadeDelegate;

public class BeaconManagementFacade
        extends LaPasseRxFacadeDelegate<BeaconManagementState>
        implements BeaconManagementStateObserver {

    public BeaconManagementFacade(RxFacade<BeaconManagementState> facade) {
        super(facade);
    }

}
