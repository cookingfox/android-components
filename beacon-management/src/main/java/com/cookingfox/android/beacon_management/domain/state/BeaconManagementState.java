package com.cookingfox.android.beacon_management.domain.state;

import com.cookingfox.android.beacon_management.domain.beacon.DetectedBeacon;
import com.cookingfox.android.bluetooth_management.domain.vo.scanning.BluetoothScanResult;
import com.cookingfox.lapasse.api.state.State;

import org.immutables.value.Value;

import java.util.LinkedHashMap;
import java.util.Map;

@Value.Immutable
public abstract class BeaconManagementState implements State {

    //----------------------------------------------------------------------------------------------
    // IMMUTABLE PROPERTIES
    //----------------------------------------------------------------------------------------------

    /**
     * @return A map of detected beacons, as determined by the registered beacon providers. The key
     * of the map is the MAC address of the detected bluetooth beacon.
     */
    public abstract Map<String, DetectedBeacon> detectedBeacons();

    /**
     * @return A map of the leftover bluetooth scan results that could not be 'detected' by the
     * registered beacon providers. The key of the map is the MAC address of the bluetooth device.
     */
    public abstract Map<String, BluetoothScanResult> junkScanResults();

    //----------------------------------------------------------------------------------------------
    // STATIC METHODS
    //----------------------------------------------------------------------------------------------

    public static BeaconManagementState createInitialState() {
        return new Builder()
                .detectedBeacons(new LinkedHashMap<>())
                .junkScanResults(new LinkedHashMap<>())
                .build();
    }

    //----------------------------------------------------------------------------------------------
    // IMMUTABLE BUILDER
    //----------------------------------------------------------------------------------------------

    public static class Builder extends ImmutableBeaconManagementState.Builder {
    }
}
