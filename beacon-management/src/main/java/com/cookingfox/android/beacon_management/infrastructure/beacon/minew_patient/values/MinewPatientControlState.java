package com.cookingfox.android.beacon_management.infrastructure.beacon.minew_patient.values;

/**
 * Indicates in which state the beacon operates:
 * - SLEEP: beacon does transmit at a very low interval to provide connectivity information
 * needed to set a new state.
 * - ARMED: beacon does not transmit, it only regularly checks the light sensor value. If the
 * sensor value is within set threshold range, the beacon activates itself by going into
 * ACTIVE state.
 * - ACTIVE: beacon emits with the predefined interval, the state for example is active during
 * patient tracking.
 */
public enum MinewPatientControlState {
    SLEEP((byte) 0x01), ARMED((byte) 0x00), ACTIVE((byte) 0x02);

    private final byte state;

    MinewPatientControlState(byte state) {
        this.state = state;
    }

    public byte getState() {
        return state;
    }
}
