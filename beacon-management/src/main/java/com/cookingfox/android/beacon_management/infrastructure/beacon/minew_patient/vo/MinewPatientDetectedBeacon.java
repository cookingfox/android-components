package com.cookingfox.android.beacon_management.infrastructure.beacon.minew_patient.vo;

import com.cookingfox.android.beacon_management.domain.beacon.DetectedBeacon;
import com.cookingfox.android.beacon_management.infrastructure.beacon.minew_patient.values.MinewPatientControlState;
import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;

import org.immutables.value.Value;

@DefaultValueStyle
@Value.Immutable
public interface MinewPatientDetectedBeacon extends DetectedBeacon {

    int batteryLevel();

    MinewPatientControlState controlState();

    int customIdentifier();

    int productCode();

    class Builder extends ImmutableMinewPatientDetectedBeacon.Builder {
    }
}
