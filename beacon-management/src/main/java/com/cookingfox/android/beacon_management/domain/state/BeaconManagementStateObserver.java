package com.cookingfox.android.beacon_management.domain.state;

import com.cookingfox.lapasse.api.state.observer.RxStateObserver;

public interface BeaconManagementStateObserver
        extends RxStateObserver<BeaconManagementState> {
}
