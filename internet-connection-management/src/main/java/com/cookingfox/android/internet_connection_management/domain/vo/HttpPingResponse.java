package com.cookingfox.android.internet_connection_management.domain.vo;

import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;
import com.google.common.base.Optional;

import org.immutables.value.Value;

import java.util.Date;

/**
 * Represents an HTTP ping response.
 */
@DefaultValueStyle
@Value.Immutable
public abstract class HttpPingResponse {

    //----------------------------------------------------------------------------------------------
    // IMMUTABLE PROPERTIES
    //----------------------------------------------------------------------------------------------

    /**
     * Returns the error that occurred (optional).
     *
     * @return The error that occurred.
     */
    public abstract Optional<Throwable> error();

    /**
     * @return Whether the response returned "OK", e.g. status code 200.
     */
    public abstract boolean isOk();

    /**
     * @return The HTTP status code, e.g. 200.
     */
    public abstract int statusCode();

    /**
     * @return The UNIX timestamp of when the response was received.
     */
    public abstract long timestamp();

    /**
     * @return The URL that was requested.
     */
    public abstract String url();

    //----------------------------------------------------------------------------------------------
    // HELPER METHODS
    //----------------------------------------------------------------------------------------------

    public static HttpPingResponse forError(String url, Throwable error) {
        return newBuilder()
                .error(error)
                .isOk(false)
                .statusCode(0)
                .url(url)
                .build();
    }

    public static Builder newBuilder() {
        return new Builder()
                .timestamp(new Date().getTime());
    }

    //----------------------------------------------------------------------------------------------
    // IMMUTABLE BUILDER
    //----------------------------------------------------------------------------------------------

    public static class Builder extends ImmutableHttpPingResponse.Builder {
    }

}
