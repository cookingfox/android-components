package com.cookingfox.android.internet_connection_management.application.controller;

import com.cookingfox.android.internet_connection_management.domain.manager.InternetConnectionManager;
import com.cookingfox.chefling.api.CheflingLifecycle;

/**
 * Controls the elements of the internet connection management component.
 */
public class InternetConnectionManagementController implements CheflingLifecycle {

    private final InternetConnectionManager internetConnectionManager;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public InternetConnectionManagementController(InternetConnectionManager internetConnectionManager) {
        this.internetConnectionManager = internetConnectionManager;
    }

    //----------------------------------------------------------------------------------------------
    // LIFECYCLE
    //----------------------------------------------------------------------------------------------

    @Override
    public void initialize() {
        internetConnectionManager.register();
    }

    @Override
    public void dispose() {
        internetConnectionManager.unregister();
    }

}
