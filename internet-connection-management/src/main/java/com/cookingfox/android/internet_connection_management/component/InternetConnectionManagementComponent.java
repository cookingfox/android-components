package com.cookingfox.android.internet_connection_management.component;

import com.cookingfox.android.core_component.api.component.Component;
import com.cookingfox.android.core_component.api.component.config.ComponentCheflingConfig;
import com.cookingfox.android.core_component.api.component.manager.ComponentManager;
import com.cookingfox.android.internet_connection_management.application.controller.InternetConnectionManagementController;
import com.cookingfox.android.internet_connection_management.component.config.InternetConnectionManagementCheflingConfig;
import com.cookingfox.android.internet_connection_management.domain.facade.InternetConnectionManagementFacade;
import com.cookingfox.android.internet_connection_management.domain.preferences.InternetConnectionManagementPreferences;
import com.cookingfox.android.network_adapter_management.component.NetworkAdapterManagementComponent;
import com.cookingfox.android.prefer.api.pref.Prefs;
import com.cookingfox.chefling.api.CheflingContainer;
import com.cookingfox.lapasse.api.facade.Facade;

/**
 * Internet connection management component.
 */
public class InternetConnectionManagementComponent implements Component {

    private final CheflingContainer container;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public InternetConnectionManagementComponent(CheflingContainer container) {
        this.container = container;
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public ComponentCheflingConfig getCheflingConfig() {
        return new InternetConnectionManagementCheflingConfig();
    }

    @Override
    public Facade getLaPasseFacade() {
        return container.getInstance(InternetConnectionManagementFacade.class);
    }

    @Override
    public Prefs getPrefs() {
        return container.getInstance(InternetConnectionManagementPreferences.class);
    }

    @Override
    public void loadComponent(ComponentManager componentManager) {
        // component dependencies:
        componentManager.loadComponent(NetworkAdapterManagementComponent.class);
    }

    @Override
    public void registerComponent() {
        container.getInstance(InternetConnectionManagementController.class);
    }

    @Override
    public void unregisterComponent() {
        container.removeInstanceAndMapping(InternetConnectionManagementController.class);
    }

}
