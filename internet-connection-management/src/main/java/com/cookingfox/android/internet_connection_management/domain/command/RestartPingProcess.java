package com.cookingfox.android.internet_connection_management.domain.command;

import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;
import com.cookingfox.lapasse.api.command.Command;

import org.immutables.value.Value;

/**
 * Restart the timed ping process.
 */
@DefaultValueStyle
@Value.Immutable
public interface RestartPingProcess extends Command {

    class Builder extends ImmutableRestartPingProcess.Builder {
    }

}
