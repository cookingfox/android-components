package com.cookingfox.android.internet_connection_management.android.preferences;

import com.cookingfox.android.internet_connection_management.domain.preferences.InternetConnectionManagementPreferences;
import com.cookingfox.android.prefer.api.pref.PrefValidator;
import com.cookingfox.android.prefer_rx.api.pref.RxPrefGroup;
import com.cookingfox.android.prefer_rx.api.pref.typed.LongRxPref;
import com.cookingfox.android.prefer_rx.api.pref.typed.StringRxPref;
import com.cookingfox.android.prefer_rx.impl.pref.AndroidRxPrefGroup;
import com.cookingfox.android.prefer_rx.impl.pref.typed.AndroidLongRxPref;
import com.cookingfox.android.prefer_rx.impl.pref.typed.AndroidStringRxPref;
import com.cookingfox.android.prefer_rx.impl.prefer.AndroidRxPrefer;

/**
 * Implementation of {@link InternetConnectionManagementPreferences} using Android Prefer.
 */
public class AndroidInternetConnectionManagementPreferences
        implements InternetConnectionManagementPreferences {

    //----------------------------------------------------------------------------------------------
    // PROPERTIES
    //----------------------------------------------------------------------------------------------

    private final AndroidRxPrefGroup<Key> prefGroup;
    private final AndroidStringRxPref<Key> pingHttpAddress;
    private final AndroidStringRxPref<Key> pingHttpMethod;
    private final AndroidLongRxPref<Key> pingIntervalSeconds;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public AndroidInternetConnectionManagementPreferences(AndroidRxPrefer prefer) {
        prefGroup = prefer.newGroup(Key.class);
        prefGroup.setTitle("Internet Connection Management");
        prefGroup.setSummary("Manage Internet connection settings");

        pingHttpAddress = prefGroup.addNewString(Key.PingHttpAddress, "");
        pingHttpAddress.setTitle("Ping HTTP address");
        pingHttpAddress.setSummary("The URL to request to test the Internet connection");
        pingHttpAddress.setValidator(pingHttpAddressValidator);

        pingHttpMethod = prefGroup.addNewString(Key.PingHttpMethod, "HEAD");
        pingHttpMethod.setTitle("Ping HTTP method");
        pingHttpMethod.setSummary("The HTTP method to use for the ping requests");
        pingHttpMethod.setValidator(pingHttpMethodValidator);

        pingIntervalSeconds = prefGroup.addNewLong(Key.PingIntervalSeconds, 60);
        pingIntervalSeconds.setTitle("Ping interval (seconds)");
        pingIntervalSeconds.setSummary("How often to perform a ping request");
        pingIntervalSeconds.setValidator(pingIntervalSecondsValidator);
    }

    //----------------------------------------------------------------------------------------------
    // VALIDATORS
    //----------------------------------------------------------------------------------------------

    /**
     * Validates the ping HTTP address is a URL.
     */
    protected final PrefValidator<String> pingHttpAddressValidator = new PrefValidator<String>() {
        @Override
        public boolean validate(String value) throws Exception {
            // empty URL is allowed: this will disable the ping process
            if (value.isEmpty()) {
                return true;
            }

            // validate URL
            if (!value.matches("^(?i)(https?)://.*$")) {
                throw new Exception("Invalid URL: " + value);
            }

            return true;
        }
    };

    /**
     * Validates the HTTP method.
     */
    protected final PrefValidator<String> pingHttpMethodValidator = new PrefValidator<String>() {
        @Override
        public boolean validate(String value) throws Exception {
            // validate HTTP method
            if (!value.matches("(?i)(get|head)")) {
                throw new Exception("Invalid HTTP method. Expected: 'GET', 'HEAD'");
            }

            return true;
        }
    };

    /**
     * Validates the minimum value for the ping interval.
     */
    protected final PrefValidator<Long> pingIntervalSecondsValidator = new PrefValidator<Long>() {
        @Override
        public boolean validate(Long value) throws Exception {
            // must be minimum value
            if (value < MINIMUM_PING_INTERVAL_SECONDS) {
                throw new Exception(String.format("Ping interval must be at least %s seconds",
                        MINIMUM_PING_INTERVAL_SECONDS));
            }

            return true;
        }
    };

    //----------------------------------------------------------------------------------------------
    // GETTERS
    //----------------------------------------------------------------------------------------------

    @Override
    public StringRxPref<Key> pingHttpAddress() {
        return pingHttpAddress;
    }

    @Override
    public StringRxPref<Key> pingHttpMethod() {
        return pingHttpMethod;
    }

    @Override
    public LongRxPref<Key> pingIntervalSeconds() {
        return pingIntervalSeconds;
    }

    @Override
    public RxPrefGroup<Key> getPrefGroup() {
        return prefGroup;
    }

}
