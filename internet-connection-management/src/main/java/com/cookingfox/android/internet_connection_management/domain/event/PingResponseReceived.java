package com.cookingfox.android.internet_connection_management.domain.event;

import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;
import com.cookingfox.android.internet_connection_management.domain.vo.HttpPingResponse;
import com.cookingfox.lapasse.api.event.Event;

import org.immutables.value.Value;

/**
 * When a ping response is received.
 */
@DefaultValueStyle
@Value.Immutable
public interface PingResponseReceived extends Event {

    HttpPingResponse response();

    class Builder extends ImmutablePingResponseReceived.Builder {
    }

}
