package com.cookingfox.android.internet_connection_management.domain.facade;

import com.cookingfox.android.internet_connection_management.domain.state.InternetConnectionManagementState;
import com.cookingfox.android.internet_connection_management.domain.state.InternetConnectionManagementStateObserver;
import com.cookingfox.lapasse.api.facade.RxFacade;
import com.cookingfox.lapasse.impl.facade.LaPasseRxFacadeDelegate;

/**
 * Concrete LaPasse facade delegate which implements the state observer.
 */
public class InternetConnectionManagementFacade
        extends LaPasseRxFacadeDelegate<InternetConnectionManagementState>
        implements InternetConnectionManagementStateObserver {

    public InternetConnectionManagementFacade(RxFacade<InternetConnectionManagementState> facade) {
        super(facade);
    }

}
