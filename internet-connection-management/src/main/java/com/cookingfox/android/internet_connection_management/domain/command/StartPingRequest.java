package com.cookingfox.android.internet_connection_management.domain.command;

import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;
import com.cookingfox.lapasse.api.command.Command;

import org.immutables.value.Value;

/**
 * Trigger to execute a ping request.
 */
@DefaultValueStyle
@Value.Immutable
public interface StartPingRequest extends Command {

    class Builder extends ImmutableStartPingRequest.Builder {
    }

}
