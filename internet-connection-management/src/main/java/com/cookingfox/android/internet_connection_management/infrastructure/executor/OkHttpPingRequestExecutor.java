package com.cookingfox.android.internet_connection_management.infrastructure.executor;

import com.cookingfox.android.internet_connection_management.domain.executor.PingRequestExecutor;
import com.cookingfox.android.internet_connection_management.domain.vo.HttpPingResponse;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import rx.Observable;
import rx.functions.Func0;
import rx.functions.Func1;

/**
 * OkHttp implementation of ping request executor.
 */
public class OkHttpPingRequestExecutor implements PingRequestExecutor {

    private final OkHttpClient httpClient;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public OkHttpPingRequestExecutor(OkHttpClient httpClient) {
        this.httpClient = httpClient;
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public Observable<HttpPingResponse> executePingRequest(final String url, String requestMethod) {
        Request.Builder requestBuilder = new Request.Builder();
        requestBuilder.url(url);

        // set the HTTP request method - defaults to "GET"
        if (requestMethod.equalsIgnoreCase("HEAD")) {
            requestBuilder.head();
        }

        final Request request = requestBuilder.build();

        return Observable.defer(new Func0<Observable<Response>>() {
            @Override
            public Observable<Response> call() {
                try {
                    // execute the call
                    return Observable.just(httpClient.newCall(request).execute());
                } catch (IOException e) {
                    return Observable.error(e);
                }
            }
        }).map(new Func1<Response, HttpPingResponse>() {
            @Override
            public HttpPingResponse call(Response response) {
                // use the call response to generate the VO
                return HttpPingResponse.newBuilder()
                        .isOk(response.isSuccessful())
                        .statusCode(response.code())
                        .url(url)
                        .build();
            }
        });
    }

}
