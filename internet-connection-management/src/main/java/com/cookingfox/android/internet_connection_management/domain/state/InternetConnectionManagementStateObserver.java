package com.cookingfox.android.internet_connection_management.domain.state;

import com.cookingfox.lapasse.api.state.observer.RxStateObserver;

/**
 * Concrete state observer for internet connection management state.
 */
public interface InternetConnectionManagementStateObserver
        extends RxStateObserver<InternetConnectionManagementState> {
}
