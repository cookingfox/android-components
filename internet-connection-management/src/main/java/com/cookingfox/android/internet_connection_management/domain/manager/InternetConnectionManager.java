package com.cookingfox.android.internet_connection_management.domain.manager;

import com.cookingfox.android.internet_connection_management.domain.command.RestartPingProcess;
import com.cookingfox.android.internet_connection_management.domain.command.StartPingRequest;
import com.cookingfox.android.internet_connection_management.domain.event.PingResponseReceived;
import com.cookingfox.android.internet_connection_management.domain.executor.PingRequestExecutor;
import com.cookingfox.android.internet_connection_management.domain.facade.InternetConnectionManagementFacade;
import com.cookingfox.android.internet_connection_management.domain.preferences.InternetConnectionManagementPreferences;
import com.cookingfox.android.internet_connection_management.domain.state.InternetConnectionManagementState;
import com.cookingfox.android.internet_connection_management.domain.vo.HttpPingResponse;
import com.cookingfox.android.network_adapter_management.domain.state.NetworkAdapterManagementStateObserver;
import com.cookingfox.lapasse.annotation.HandleCommand;
import com.cookingfox.lapasse.annotation.HandleEvent;
import com.cookingfox.lapasse.impl.helper.LaPasse;
import com.cookingfox.logging.Logger;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;
import rx.subscriptions.Subscriptions;

/**
 * Manages the internet connection.
 */
public class InternetConnectionManager {

    private final InternetConnectionManagementFacade facade;
    private final PingRequestExecutor pingRequestExecutor;
    private Subscription pingIntervalSubscription;
    private final InternetConnectionManagementPreferences preferences;
    private final NetworkAdapterManagementStateObserver networkAdapterObserver;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public InternetConnectionManager(InternetConnectionManagementFacade facade,
                                     PingRequestExecutor pingRequestExecutor,
                                     InternetConnectionManagementPreferences preferences,
                                     NetworkAdapterManagementStateObserver networkAdapterObserver) {
        this.facade = facade;
        this.pingRequestExecutor = pingRequestExecutor;
        this.preferences = preferences;
        this.networkAdapterObserver = networkAdapterObserver;
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    /**
     * Register the manager and start the ping process.
     */
    public void register() {
        LaPasse.mapHandlers(this, facade);

        // immediately restart pinging process when network adapter state changes
        observeNetworkAdapterState();

        // start pinging
        restartPingProcess();
    }

    /**
     * Stop the ping process.
     */
    public void unregister() {
        stopPingProcess();
    }

    //----------------------------------------------------------------------------------------------
    // HANDLERS
    //----------------------------------------------------------------------------------------------

    /**
     * Handle (re-)starting the HTTP ping process.
     *
     * @param state   The current state object.
     * @param command The command to process.
     */
    @HandleCommand
    @SuppressWarnings("unused")
    void handle(InternetConnectionManagementState state, RestartPingProcess command) {
        // stop previous ping interval
        stopPingProcess();

        // grab interval from settings
        final long interval = preferences.pingIntervalSeconds().getValue();

        // start new ping interval
        pingIntervalSubscription = Observable.interval(0, interval, TimeUnit.SECONDS)
                .subscribe(tick -> facade.handleCommand(new StartPingRequest.Builder().build()));
    }

    /**
     * Perform the actual HTTP ping request.
     *
     * @param state   The current state object.
     * @param command The command to process.
     * @return An observable of the HTTP ping response.
     */
    @HandleCommand
    @SuppressWarnings("unused")
    Observable<PingResponseReceived> handle(InternetConnectionManagementState state, StartPingRequest command) {
        // get values from settings
        final String requestMethod = preferences.pingHttpMethod().getValue();
        final String url = preferences.pingHttpAddress().getValue();

        if (url.isEmpty()) {
            Logger.info("Ping URL is empty: skip request");
            return null;
        }

        // create observable for ping request
        return Observable.create((Observable.OnSubscribe<PingResponseReceived>) subscriber -> {
            // execute ping request
            Subscription subscription = pingRequestExecutor.executePingRequest(url, requestMethod)
                    .subscribe(httpPingResponse -> {
                        subscriber.onNext(new PingResponseReceived.Builder()
                                .response(httpPingResponse)
                                .build());
                    }, error -> {
                        subscriber.onNext(new PingResponseReceived.Builder()
                                .response(HttpPingResponse.forError(url, error))
                                .build());
                    });

            // cancel ping request on observable unsubscribe
            subscriber.add(Subscriptions.create(subscription::unsubscribe));
        });
    }

    /**
     * A ping response is received: apply the event to the state.
     *
     * @param state The current state object.
     * @param event The command to process.
     * @return The new state
     */
    @HandleEvent
    InternetConnectionManagementState handle(InternetConnectionManagementState state, PingResponseReceived event) {
        return new InternetConnectionManagementState.Builder()
                .from(state)
                .isConnectionOk(event.response().isOk())
                .lastPingResponse(event.response())
                .build();
    }

    //----------------------------------------------------------------------------------------------
    // PROTECTED METHODS
    //----------------------------------------------------------------------------------------------

    /**
     * Observes the network adapter, so that when the state changes, the ping process is
     * automatically restarted to get the most accurate result.
     */
    protected void observeNetworkAdapterState() {
        networkAdapterObserver.observeStateChanges()
                /**
                 * Reason for debounce: network adapter state changes might occur in rapid
                 * succession (e.g. connected -> disconnecting -> disconnected). We "debounce" here
                 * to prevent restarting the ping process many times in a row.
                 */
                .debounce(1, TimeUnit.SECONDS)
                .subscribe(stateChanged -> {
                    // restart ping process, whether we are connected or not
                    restartPingProcess();
                });
    }

    /**
     * Restart the ping process.
     */
    protected void restartPingProcess() {
        facade.handleCommand(new RestartPingProcess.Builder().build());
    }

    /**
     * Stop the current ping process.
     */
    protected void stopPingProcess() {
        if (pingIntervalSubscription != null) {
            pingIntervalSubscription.unsubscribe();
        }
    }

}
