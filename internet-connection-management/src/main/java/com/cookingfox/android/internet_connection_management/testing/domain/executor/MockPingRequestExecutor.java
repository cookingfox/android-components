package com.cookingfox.android.internet_connection_management.testing.domain.executor;

import com.cookingfox.android.internet_connection_management.domain.executor.PingRequestExecutor;
import com.cookingfox.android.internet_connection_management.domain.vo.HttpPingResponse;

import java.util.Date;

import rx.Observable;

/**
 * Mock implementation of ping request executor.
 */
public class MockPingRequestExecutor implements PingRequestExecutor {

    public int mock_statusCode = 200;

    @Override
    public Observable<HttpPingResponse> executePingRequest(String url, String requestMethod) {
        return Observable.just((HttpPingResponse) new HttpPingResponse.Builder()
                .statusCode(mock_statusCode)
                .timestamp(new Date().getTime())
                .url(url)
                .build());
    }

}
