package com.cookingfox.android.internet_connection_management.domain.preferences;

import com.cookingfox.android.prefer_rx.api.pref.RxPrefs;
import com.cookingfox.android.prefer_rx.api.pref.typed.LongRxPref;
import com.cookingfox.android.prefer_rx.api.pref.typed.StringRxPref;

/**
 * Preferences for internet connection management component.
 */
public interface InternetConnectionManagementPreferences
        extends RxPrefs<InternetConnectionManagementPreferences.Key> {

    /**
     * Preference keys.
     */
    enum Key {
        PingHttpAddress,
        PingHttpMethod,
        PingIntervalSeconds
    }

    /**
     * Minimum ping interval.
     */
    long MINIMUM_PING_INTERVAL_SECONDS = 3;

    /**
     * @return The URL that is used for ping requests.
     */
    StringRxPref<Key> pingHttpAddress();

    /**
     * @return The HTTP method that should be used for ping requests, e.g. "HEAD".
     */
    StringRxPref<Key> pingHttpMethod();

    /**
     * @return The interval with which the ping requests should be performed.
     */
    LongRxPref<Key> pingIntervalSeconds();

}
