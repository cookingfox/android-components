package com.cookingfox.android.internet_connection_management.domain.state;

import com.cookingfox.android.internet_connection_management.domain.vo.HttpPingResponse;
import com.cookingfox.lapasse.api.state.State;

import org.immutables.value.Value;

/**
 * Internet connection management immutable state object.
 */
@Value.Immutable
public abstract class InternetConnectionManagementState implements State {

    //----------------------------------------------------------------------------------------------
    // PROPERTIES
    //----------------------------------------------------------------------------------------------

    /**
     * @return Whether the internet connection is ok.
     */
    public abstract boolean isConnectionOk();

    /**
     * @return The last HTTP ping response.
     */
    public abstract HttpPingResponse lastPingResponse();

    //----------------------------------------------------------------------------------------------
    // STATIC METHODS
    //----------------------------------------------------------------------------------------------

    /**
     * @return The initial state object.
     */
    public static InternetConnectionManagementState createInitialState() {
        HttpPingResponse lastPingResponse = new HttpPingResponse.Builder()
                .isOk(false)
                .statusCode(0)
                .timestamp(0)
                .url("")
                .build();

        return new Builder()
                .isConnectionOk(false)
                .lastPingResponse(lastPingResponse)
                .build();
    }

    //----------------------------------------------------------------------------------------------
    // IMMUTABLE BUILDER
    //----------------------------------------------------------------------------------------------

    public static class Builder extends ImmutableInternetConnectionManagementState.Builder {
    }

}
