package com.cookingfox.android.internet_connection_management.component.config;

import com.cookingfox.android.core_component.api.component.config.ComponentCheflingConfig;
import com.cookingfox.android.internet_connection_management.android.preferences.AndroidInternetConnectionManagementPreferences;
import com.cookingfox.android.internet_connection_management.domain.executor.PingRequestExecutor;
import com.cookingfox.android.internet_connection_management.domain.facade.InternetConnectionManagementFacade;
import com.cookingfox.android.internet_connection_management.domain.preferences.InternetConnectionManagementPreferences;
import com.cookingfox.android.internet_connection_management.domain.state.InternetConnectionManagementState;
import com.cookingfox.android.internet_connection_management.domain.state.InternetConnectionManagementStateObserver;
import com.cookingfox.android.internet_connection_management.infrastructure.executor.OkHttpPingRequestExecutor;
import com.cookingfox.android.internet_connection_management.testing.domain.executor.MockPingRequestExecutor;
import com.cookingfox.chefling.api.CheflingBuilder;
import com.cookingfox.chefling.api.CheflingConfig;
import com.cookingfox.chefling.impl.Chefling;
import com.cookingfox.lapasse.api.facade.RxFacade;
import com.cookingfox.lapasse.impl.facade.LaPasseRxFacade;

import rx.schedulers.Schedulers;

/**
 * Chefling configuration for internet connection management component.
 */
public class InternetConnectionManagementCheflingConfig implements ComponentCheflingConfig {

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public CheflingBuilder createCheflingBuilder() {
        return Chefling.createBuilder()
                .addConfig(domainConfig);
    }

    @Override
    public CheflingConfig createDeviceConfig() {
        return deviceConfig;
    }

    @Override
    public CheflingConfig createEmulatorConfig() {
        // use same as for device
        return deviceConfig;
    }

    @Override
    public CheflingConfig createTestConfig() {
        return testConfig;
    }

    //----------------------------------------------------------------------------------------------
    // DEVICE CONFIGURATION
    //----------------------------------------------------------------------------------------------

    public final CheflingConfig deviceConfig = container ->
            container.mapType(PingRequestExecutor.class, OkHttpPingRequestExecutor.class);

    //----------------------------------------------------------------------------------------------
    // DOMAIN CONFIGURATION
    //----------------------------------------------------------------------------------------------

    public final CheflingConfig domainConfig = container -> {
        // use a factory to create the facade
        container.mapFactory(InternetConnectionManagementFacade.class, c -> {
            InternetConnectionManagementState initialState = InternetConnectionManagementState.createInitialState();

            RxFacade<InternetConnectionManagementState> facade =
                    new LaPasseRxFacade.Builder<>(initialState).build();

            // set command bus schedulers
            facade.setCommandSubscribeScheduler(Schedulers.io());

            return new InternetConnectionManagementFacade(facade);
        });

        // map state observer interface to facade
        container.mapType(InternetConnectionManagementStateObserver.class, InternetConnectionManagementFacade.class);

        // map preferences interface to implementation
        container.mapType(InternetConnectionManagementPreferences.class, AndroidInternetConnectionManagementPreferences.class);
    };

    //----------------------------------------------------------------------------------------------
    // TEST CONFIGURATION
    //----------------------------------------------------------------------------------------------

    public final CheflingConfig testConfig = container ->
            container.mapType(PingRequestExecutor.class, MockPingRequestExecutor.class);

}
