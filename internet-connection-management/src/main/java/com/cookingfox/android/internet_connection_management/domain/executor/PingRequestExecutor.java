package com.cookingfox.android.internet_connection_management.domain.executor;

import com.cookingfox.android.internet_connection_management.domain.vo.HttpPingResponse;

import rx.Observable;

/**
 * Interface for an abstract ping request executor.
 */
public interface PingRequestExecutor {

    /**
     * Execute the HTTP ping request.
     *
     * @param url           The URL to use for the request.
     * @param requestMethod The HTTP method to use for the request.
     * @return An observable of the HTTP response.
     */
    Observable<HttpPingResponse> executePingRequest(String url, String requestMethod);

}
