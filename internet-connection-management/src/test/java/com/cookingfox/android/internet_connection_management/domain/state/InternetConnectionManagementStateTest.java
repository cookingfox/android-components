package com.cookingfox.android.internet_connection_management.domain.state;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Unit tests for {@link InternetConnectionManagementState}.
 */
public class InternetConnectionManagementStateTest {

    //----------------------------------------------------------------------------------------------
    // TESTS: createInitialState
    //----------------------------------------------------------------------------------------------

    @Test
    public void createInitialState_should_not_throw() throws Exception {
        InternetConnectionManagementState.createInitialState();
    }

}
