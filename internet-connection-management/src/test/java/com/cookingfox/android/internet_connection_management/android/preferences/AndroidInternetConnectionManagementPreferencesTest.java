package com.cookingfox.android.internet_connection_management.android.preferences;

import android.content.SharedPreferences;

import com.cookingfox.android.prefer.api.exception.InvalidPrefValueException;
import com.cookingfox.android.prefer_rx.impl.prefer.AndroidRxPrefer;
import com.cookingfox.android.prefer_rx.impl.prefer.SharedPreferencesRxPrefer;
import com.cookingfox.android.prefer_testing.shared_preferences.InMemorySharedPreferences;

import org.junit.Before;
import org.junit.Test;

/**
 * Unit tests for {@link AndroidInternetConnectionManagementPreferences}.
 */
public class AndroidInternetConnectionManagementPreferencesTest {

    //----------------------------------------------------------------------------------------------
    // TEST SETUP
    //----------------------------------------------------------------------------------------------

    private AndroidInternetConnectionManagementPreferences preferences;

    @Before
    public void setUp() throws Exception {
        SharedPreferences sharedPreferences = new InMemorySharedPreferences();
        AndroidRxPrefer prefer = new SharedPreferencesRxPrefer(sharedPreferences);
        preferences = new AndroidInternetConnectionManagementPreferences(prefer);
    }

    //----------------------------------------------------------------------------------------------
    // TESTS: pingHttpAddress
    //----------------------------------------------------------------------------------------------

    @Test
    public void pingHttpAddress_should_not_throw_for_empty_url() throws Exception {
        preferences.pingHttpAddress().setValue("");
    }

    @Test(expected = InvalidPrefValueException.class)
    public void pingHttpAddress_should_throw_for_invalid_url() throws Exception {
        preferences.pingHttpAddress().setValue("foo");
    }

    @Test
    public void pingHttpAddress_should_accept_http_url() throws Exception {
        preferences.pingHttpAddress().setValue("http://cookingfox.com");
    }

    @Test
    public void pingHttpAddress_should_accept_https_url() throws Exception {
        preferences.pingHttpAddress().setValue("https://cookingfox.com");
    }

    @Test
    public void pingHttpAddress_should_accept_uppercase_url() throws Exception {
        preferences.pingHttpAddress().setValue("HTTP://COOKINGFOX.COM");
    }

    //----------------------------------------------------------------------------------------------
    // TESTS: pingHttpMethod
    //----------------------------------------------------------------------------------------------

    @Test(expected = InvalidPrefValueException.class)
    public void pingHttpMethod_should_throw_for_empty_value() throws Exception {
        preferences.pingHttpMethod().setValue("");
    }

    @Test(expected = InvalidPrefValueException.class)
    public void pingHttpMethod_should_throw_for_invalid_method() throws Exception {
        preferences.pingHttpMethod().setValue("POST");
    }

    @Test
    public void pingHttpMethod_should_accept_method_get() throws Exception {
        preferences.pingHttpMethod().setValue("GET");
    }

    @Test
    public void pingHttpMethod_should_accept_method_head() throws Exception {
        preferences.pingHttpMethod().setValue("HEAD");
    }

    @Test
    public void pingHttpMethod_should_accept_lowercase_method() throws Exception {
        preferences.pingHttpMethod().setValue("get");
    }

    //----------------------------------------------------------------------------------------------
    // TESTS: pingIntervalSeconds
    //----------------------------------------------------------------------------------------------

    @Test(expected = InvalidPrefValueException.class)
    public void pingIntervalSeconds_should_throw_for_negative_value() throws Exception {
        preferences.pingIntervalSeconds().setValue(-1L);
    }

    @Test(expected = InvalidPrefValueException.class)
    public void pingIntervalSeconds_should_throw_for_value_lower_than_minimum() throws Exception {
        long min = AndroidInternetConnectionManagementPreferences.MINIMUM_PING_INTERVAL_SECONDS;
        preferences.pingIntervalSeconds().setValue(min - 1);
    }

    @Test
    public void pingIntervalSeconds_should_accept_minimum() throws Exception {
        long min = AndroidInternetConnectionManagementPreferences.MINIMUM_PING_INTERVAL_SECONDS;
        preferences.pingIntervalSeconds().setValue(min);
    }

}
