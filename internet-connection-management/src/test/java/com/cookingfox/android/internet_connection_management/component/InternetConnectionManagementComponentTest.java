package com.cookingfox.android.internet_connection_management.component;

import com.cookingfox.android.core_component.api.component.manager.ComponentManager;
import com.cookingfox.android.core_component.testing.component.manager.TestComponentManager;
import com.cookingfox.android.internet_connection_management.domain.facade.InternetConnectionManagementFacade;
import com.cookingfox.android.internet_connection_management.domain.state.InternetConnectionManagementStateObserver;
import com.cookingfox.chefling.api.CheflingContainer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Unit tests for {@link InternetConnectionManagementComponent}.
 */
public class InternetConnectionManagementComponentTest {

    //----------------------------------------------------------------------------------------------
    // TEST SETUP
    //----------------------------------------------------------------------------------------------

    ComponentManager componentManager;
    CheflingContainer container;

    @Before
    public void setUp() throws Exception {
        container = TestComponentManager.buildContainer();
        componentManager = TestComponentManager.getComponentManager(container);
    }

    @After
    public void tearDown() throws Exception {
        componentManager.unloadAllComponents();
        container.disposeContainer();
    }

    //----------------------------------------------------------------------------------------------
    // TESTS: loadComponent
    //----------------------------------------------------------------------------------------------

    @Test
    public void loadComponent_should_not_throw() throws Exception {
        componentManager.loadComponent(InternetConnectionManagementComponent.class);
    }

    @Test
    public void loadComponent_should_map_state_observer_correctly() throws Exception {
        componentManager.loadComponent(InternetConnectionManagementComponent.class);

        InternetConnectionManagementStateObserver stateObserver =
                container.getInstance(InternetConnectionManagementStateObserver.class);

        assertTrue(stateObserver instanceof InternetConnectionManagementFacade);
    }

}
