package com.cookingfox.android.bluetooth_management.domain.manager;

import com.cookingfox.android.bluetooth_management.component.BluetoothManagementComponent;
import com.cookingfox.android.bluetooth_management.domain.command.UpdateBluetoothScanResults;
import com.cookingfox.android.bluetooth_management.domain.facade.BluetoothManagementFacade;
import com.cookingfox.android.bluetooth_management.domain.preferences.BluetoothManagementPreferences;
import com.cookingfox.android.bluetooth_management.domain.state.BluetoothManagementState;
import com.cookingfox.android.bluetooth_management.domain.vo.scanning.BluetoothScanResult;
import com.cookingfox.android.bluetooth_management.testing.driver.MockBluetoothAdapterDriver;
import com.cookingfox.android.bluetooth_management.testing.driver.MockBluetoothScannerDriver;
import com.cookingfox.android.core_component.testing.component.manager.TestComponentManager;
import com.cookingfox.chefling.api.CheflingContainer;
import com.cookingfox.lapasse.api.event.Event;
import com.cookingfox.lapasse.api.state.observer.OnStateChanged;
import com.cookingfox.logging.Logger;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import rx.observers.TestSubscriber;
import rx.schedulers.TestScheduler;

import static org.junit.Assert.assertEquals;

public class BluetoothScanningManagerTest {

    private MockBluetoothScannerDriver mockScannerDriver;
    private BluetoothScanningManagerImpl bluetoothScanningManager;
    private BluetoothManagementPreferences preferences;
    private CheflingContainer container;
    private BluetoothManagementFacade facade;
    private MockBluetoothAdapterDriver mockAdapterDriver;
    private Event stateChangeEvent;
    private TestScheduler testScheduler;

    static {
        Logger.initTest();
    }

    @Before
    public void setup() {
        // create component manager container
        container = TestComponentManager.buildComponent(BluetoothManagementComponent.class);

        testScheduler = container.getInstance(TestScheduler.class);

        preferences = container.getInstance(BluetoothManagementPreferences.class);

        mockAdapterDriver = container.getInstance(MockBluetoothAdapterDriver.class);
        mockScannerDriver = container.getInstance(MockBluetoothScannerDriver.class);

        facade = container.getInstance(BluetoothManagementFacade.class);
        facade.addStateChangedListener(onStateChanged);

        bluetoothScanningManager = container.getInstance(BluetoothScanningManagerImpl.class);
    }

    @After
    public void tearDown() {
        container.disposeContainer();
    }

    //----------------------------------------------------------------------------------------------
    // SUBSCRIPTION TESTS
    //----------------------------------------------------------------------------------------------

    @Test
    public void observe_ble_scan_results_should_succeed() throws Exception {
        mockAdapterDriver.mock_adapterIsEnabled = true;

        bluetoothScanningManager.startScanning();

        BluetoothScanResult scanResult = mockScannerDriver
                .generateMockScanResult("A1-B2-C3-D4-E5-F6", -30, new byte[0]);

        TestSubscriber<BluetoothScanResult> testSubscriber = TestSubscriber.create();
        mockScannerDriver.observeScanResults().subscribe(testSubscriber);

        mockScannerDriver.scanResults.onNext(scanResult);
        mockScannerDriver.scanResults.onNext(scanResult);
        mockScannerDriver.scanResults.onNext(scanResult);
        mockScannerDriver.scanResults.onNext(scanResult);
        mockScannerDriver.scanResults.onNext(scanResult);

        testSubscriber.assertValueCount(5);
        testSubscriber.assertNoErrors();
    }

    //----------------------------------------------------------------------------------------------
    // STATE TESTS
    //----------------------------------------------------------------------------------------------

    @Test
    public void bluetooth_management_state_should_contain_correct_amount_of_scan_results() throws Exception {
        mockAdapterDriver.mock_adapterIsEnabled = true;

        bluetoothScanningManager.startScanning();

        BluetoothScanResult scanResultA = mockScannerDriver
                .generateMockScanResult("A1-B2-C3-D4-E5-F6", -30, new byte[0]);

        BluetoothScanResult scanResultB = mockScannerDriver
                .generateMockScanResult("A1-B2-C3-D4-E5-F3", -30, new byte[0]);

        Map<String, BluetoothScanResult> results = new HashMap<>();
        results.put(scanResultA.macAddress(), scanResultA);
        results.put(scanResultA.macAddress(), scanResultA);
        results.put(scanResultB.macAddress(), scanResultB);
        results.put(scanResultA.macAddress(), scanResultA);
        results.put(scanResultB.macAddress(), scanResultB);

        facade.handleCommand(new UpdateBluetoothScanResults.Builder()
                .putAllResults(results)
                .build());

        Assert.assertEquals(2, facade.getCurrentState().scanResults().size());
    }

    //----------------------------------------------------------------------------------------------
    // TESTS: observeBufferedScanResultMap
    //----------------------------------------------------------------------------------------------

    @Test
    public void observeBufferedScanResultMap_should_create_sorted_map_1() throws Exception {
        TestSubscriber<Map<String, BluetoothScanResult>> testSubscriber = TestSubscriber.create();

        bluetoothScanningManager.observeBufferedScanResultMap()
                .subscribe(testSubscriber);

        BluetoothScanResult r1 = mockScannerDriver.generateMockScanResult("1", -1, new byte[0]);
        BluetoothScanResult r2 = mockScannerDriver.generateMockScanResult("2", -1, new byte[0]);
        BluetoothScanResult r3 = mockScannerDriver.generateMockScanResult("3", -1, new byte[0]);
        BluetoothScanResult r4 = mockScannerDriver.generateMockScanResult("4", -1, new byte[0]);

        // DISPATCH IN SPECIFIC ORDER
        mockScannerDriver.scanResults.onNext(r4);
        mockScannerDriver.scanResults.onNext(r3);
        mockScannerDriver.scanResults.onNext(r2);
        mockScannerDriver.scanResults.onNext(r1);

        testScheduler.advanceTimeBy(preferences.scanningBufferPeriodDurationMs().getValue(), TimeUnit.MILLISECONDS);

        testSubscriber.assertNoErrors();

        Set<String> expectedResultMapKeys = new LinkedHashSet<>(Arrays.asList("1", "2", "3", "4"));
        Set<String> actualResultMapKeys = testSubscriber.getOnNextEvents().get(0).keySet();

        assertEquals(expectedResultMapKeys, actualResultMapKeys);
    }

    @Test
    public void observeBufferedScanResultMap_should_create_sorted_map_2() throws Exception {
        TestSubscriber<Map<String, BluetoothScanResult>> testSubscriber = TestSubscriber.create();

        bluetoothScanningManager.observeBufferedScanResultMap()
                .subscribe(testSubscriber);

        BluetoothScanResult r1 = mockScannerDriver.generateMockScanResult("1", -1, new byte[0]);
        BluetoothScanResult r2 = mockScannerDriver.generateMockScanResult("2", -1, new byte[0]);
        BluetoothScanResult r3 = mockScannerDriver.generateMockScanResult("3", -1, new byte[0]);
        BluetoothScanResult r4 = mockScannerDriver.generateMockScanResult("4", -1, new byte[0]);

        // DISPATCH IN SPECIFIC ORDER
        mockScannerDriver.scanResults.onNext(r1);
        mockScannerDriver.scanResults.onNext(r4);
        mockScannerDriver.scanResults.onNext(r2);
        mockScannerDriver.scanResults.onNext(r3);

        testScheduler.advanceTimeBy(preferences.scanningBufferPeriodDurationMs().getValue(), TimeUnit.MILLISECONDS);

        testSubscriber.assertNoErrors();

        Set<String> expectedResultMapKeys = new LinkedHashSet<>(Arrays.asList("1", "2", "3", "4"));
        Set<String> actualResultMapKeys = testSubscriber.getOnNextEvents().get(0).keySet();

        assertEquals(expectedResultMapKeys, actualResultMapKeys);
    }

    //----------------------------------------------------------------------------------------------
    // HELPERS
    //----------------------------------------------------------------------------------------------

    final OnStateChanged<BluetoothManagementState> onStateChanged = new OnStateChanged<BluetoothManagementState>() {
        @Override
        public void onStateChanged(BluetoothManagementState state, Event event) {
            stateChangeEvent = event;
        }
    };
}
