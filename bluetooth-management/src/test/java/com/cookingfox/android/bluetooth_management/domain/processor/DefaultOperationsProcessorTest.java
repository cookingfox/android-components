package com.cookingfox.android.bluetooth_management.domain.processor;

import com.cookingfox.android.bluetooth_management.component.BluetoothManagementComponent;
import com.cookingfox.android.bluetooth_management.domain.command.AddPendingIoOperation;
import com.cookingfox.android.bluetooth_management.domain.connection.BluetoothConnection;
import com.cookingfox.android.bluetooth_management.domain.device.ConnectedBluetoothDevice;
import com.cookingfox.android.bluetooth_management.domain.device.DetectedBluetoothDevice;
import com.cookingfox.android.bluetooth_management.domain.facade.BluetoothManagementFacade;
import com.cookingfox.android.bluetooth_management.domain.operation.OperationIdentifier;
import com.cookingfox.android.bluetooth_management.domain.operation.OperationType;
import com.cookingfox.android.bluetooth_management.domain.operation.PendingRead;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothConnectionResult;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothConnectionState;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothIOResult;
import com.cookingfox.android.bluetooth_management.domain.vo.scanning.BluetoothScanResult;
import com.cookingfox.android.bluetooth_management.testing.connection.MockBluetoothGattConnection;
import com.cookingfox.android.bluetooth_management.testing.driver.MockBluetoothGattDriver;
import com.cookingfox.android.core_component.testing.component.manager.TestComponentManager;
import com.cookingfox.chefling.api.CheflingContainer;
import com.cookingfox.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

import rx.observers.TestSubscriber;
import rx.schedulers.TestScheduler;
import rx.subjects.PublishSubject;

import static org.junit.Assert.assertEquals;

public class DefaultOperationsProcessorTest {

    private boolean connected = false;
    private CheflingContainer container;
    private BluetoothManagementFacade facade;
    private MockBluetoothGattDriver gattDriver;
    private DefaultOperationsProcessor processor;
    private TestScheduler testScheduler;

    private static final UUID FAKE_CHAR_UUID = UUID.fromString("66400001-b5a3-f393-e0a9-e50e24dcca99");
    private static final UUID FAKE_SERV_UUID = UUID.fromString("66400000-b5a3-f393-e0a9-e50e24dcca99");

    //----------------------------------------------------------------------------------------------
    // SETUP
    //----------------------------------------------------------------------------------------------

    @Before
    public void setUp() throws Exception {
        Logger.initTest();

        // create component manager container
        container = TestComponentManager.buildComponent(BluetoothManagementComponent.class);

        facade = container.getInstance(BluetoothManagementFacade.class);
        gattDriver = container.getInstance(MockBluetoothGattDriver.class);
        testScheduler = container.getInstance(TestScheduler.class);
        processor = container.getInstance(DefaultOperationsProcessor.class);
    }

    //----------------------------------------------------------------------------------------------
    // TEAR DOWN
    //----------------------------------------------------------------------------------------------

    @After
    public void tearDown() {
        container.disposeContainer();
    }

    //----------------------------------------------------------------------------------------------
    // PENDING READ TESTS
    //----------------------------------------------------------------------------------------------

    @Test
    public void should_execute_and_remove_pending_read() throws Exception {
        TestSubscriber<BluetoothIOResult> testSubscriber = TestSubscriber.create();
        DetectedBluetoothDevice device = generateDetectedDevice("A1-B2-C3-D4-E5-F6");
        MockBluetoothGattConnection connection = new MockBluetoothGattConnection();

        processor.connectWithDevice(device)
                // take first incoming state
                .takeFirst(connectionResult -> connectionResult.state().equals(BluetoothConnectionState.SERVICES_DISCOVERED))
                // if first, emit connection result 'connected'
                .doOnNext(connectionResult -> {
                    // simulate state 'CONNECTED'
                    gattDriver.connectionPublisher.onNext(new BluetoothConnectionResult.Builder()
                            .connection(connection)
                            .detectedDevice(device)
                            .macAddress(device.scanResult().macAddress())
                            .state(BluetoothConnectionState.CONNECTED)
                            .build());
                })
                .subscribe(connectionResult -> {
                    MockConnectedDevice connectedDevice = new MockConnectedDevice(device, (MockBluetoothGattConnection) connection);

                    processor.read(connectedDevice, FAKE_SERV_UUID, FAKE_CHAR_UUID)
                            .subscribe(testSubscriber);

                    assertEquals(1, facade.getCurrentState().queuedIoOperations().size());

                    BluetoothIOResult result = generateResult(FAKE_CHAR_UUID, "A1-B2-C3-D4-E5-F6", FAKE_SERV_UUID, OperationType.READ);
                    connection.onReadResult.onNext(result);

                    assertEquals(0, facade.getCurrentState().queuedIoOperations().size());
                });

        MockBluetoothGattConnection mockConnection = new MockBluetoothGattConnection();

        gattDriver.connectionPublisher.onNext(new BluetoothConnectionResult.Builder()
                .connection(mockConnection)
                .detectedDevice(device)
                .macAddress(device.scanResult().macAddress())
                .state(BluetoothConnectionState.SERVICES_DISCOVERED)
                .build());

        testSubscriber.assertValueCount(1);
    }

    @Test
    public void processing_read_result_not_finding_pending_read_should_result_in_early_return() throws Exception {
        TestSubscriber<BluetoothIOResult> testSubscriber = TestSubscriber.create();
        DetectedBluetoothDevice device = generateDetectedDevice("A1-B2-C3-D4-E5-F6");

        processor.connectWithDevice(device).subscribe(connectionResult -> {

            MockConnectedDevice connectedDevice = new MockConnectedDevice(device, (MockBluetoothGattConnection) connectionResult.connection());

            processor.read(connectedDevice, FAKE_SERV_UUID, FAKE_CHAR_UUID)
                    .subscribe(testSubscriber);

            assertEquals(1, facade.getCurrentState().queuedIoOperations().size());

            BluetoothIOResult result = generateResult(UUID.fromString("00000000-0000-0000-0000-000000000000"), "A1-B2-C3-D4-E5-F6", FAKE_SERV_UUID, OperationType.READ);
            processor.processOperation(result);

            assertEquals(1, facade.getCurrentState().queuedIoOperations().size());
        });
    }

    @Test
    public void should_perform_first_pending_read_from_list() throws Exception {
        TestSubscriber<BluetoothIOResult> testSubscriber = TestSubscriber.create();
        DetectedBluetoothDevice device = generateDetectedDevice("A1-B2-C3-D4-E5-F6");

        UUID uuid1 = UUID.fromString("10000000-0000-0000-0000-000000000000");
        UUID uuid2 = UUID.fromString("20000000-0000-0000-0000-000000000000");

        processor.connectWithDevice(device).subscribe(connectionResult -> {

            MockConnectedDevice connectedDevice = new MockConnectedDevice(device, (MockBluetoothGattConnection) connectionResult.connection());

            processor.read(connectedDevice, FAKE_SERV_UUID, uuid1).subscribe(testSubscriber);
            assertEquals(1, facade.getCurrentState().queuedIoOperations().size());

            processor.read(connectedDevice, FAKE_SERV_UUID, uuid2);
            assertEquals(2, facade.getCurrentState().queuedIoOperations().size());

            BluetoothIOResult result2 = generateResult(uuid2, "A1-B2-C3-D4-E5-F6", FAKE_SERV_UUID, OperationType.READ);
            processor.processOperation(result2);
            assertEquals(1, facade.getCurrentState().queuedIoOperations().size());
        });
    }

    @Test
    public void connect_with_device_should_on_next_connected_device_on_observable() throws Exception {
        // Generate MOCK detected device..
        DetectedBluetoothDevice device = generateDetectedDevice("A1-B2-C3-D4-E5-F6");

        // Generate MOCK connection as a result of connecting to MOCK detected device..
        MockBluetoothGattConnection connection = new MockBluetoothGattConnection();

        TestSubscriber<BluetoothConnectionResult> testSubscriber = TestSubscriber.create();

        processor.connectWithDevice(device).subscribe(testSubscriber);

        gattDriver.connectionPublisher.onNext(new BluetoothConnectionResult.Builder()
                .connection(connection)
                .detectedDevice(device)
                .macAddress(device.scanResult().macAddress())
                .state(BluetoothConnectionState.CONNECTED)
                .build());

        testSubscriber.assertNoErrors();

        Logger.info(testSubscriber.getOnNextEvents());

        testSubscriber.assertValueCount(1);
    }

    @Test
    public void should_process_pending_reads_and_writes_in_correct_order() throws Exception {
        MockBluetoothGattConnection connection = new MockBluetoothGattConnection();
        MockConnectedDevice deviceConnection;

        deviceConnection = new MockConnectedDevice(generateDetectedDevice("A1-B2-C3-D4-E5-F6"), connection);
        processor.read(deviceConnection, FAKE_SERV_UUID, UUID.fromString("10000000-0000-0000-0000-000000000000"));

        deviceConnection = new MockConnectedDevice(generateDetectedDevice("A2-B2-C3-D4-E5-F6"), connection);
        processor.read(deviceConnection, FAKE_SERV_UUID, UUID.fromString("20000000-0000-0000-0000-000000000000"));

        deviceConnection = new MockConnectedDevice(generateDetectedDevice("A3-B2-C3-D4-E5-F6"), connection);
        processor.read(deviceConnection, FAKE_SERV_UUID, UUID.fromString("30000000-0000-0000-0000-000000000000"));

        deviceConnection = new MockConnectedDevice(generateDetectedDevice("A4-B2-C3-D4-E5-F6"), connection);
        processor.write(deviceConnection, FAKE_SERV_UUID, UUID.fromString("40000000-0000-0000-0000-000000000000"), new byte[0]);

        deviceConnection = new MockConnectedDevice(generateDetectedDevice("A5-B2-C3-D4-E5-F6"), connection);
        processor.read(deviceConnection, FAKE_SERV_UUID, UUID.fromString("50000000-0000-0000-0000-000000000000"));

        deviceConnection = new MockConnectedDevice(generateDetectedDevice("A6-B2-C3-D4-E5-F6"), connection);
        processor.read(deviceConnection, FAKE_SERV_UUID, UUID.fromString("60000000-0000-0000-0000-000000000000"));

        BluetoothIOResult result1 = generateResult(UUID.fromString("10000000-0000-0000-0000-000000000000"), "A1-B2-C3-D4-E5-F6", FAKE_SERV_UUID, OperationType.READ);
        BluetoothIOResult result2 = generateResult(UUID.fromString("20000000-0000-0000-0000-000000000000"), "A2-B2-C3-D4-E5-F6", FAKE_SERV_UUID, OperationType.READ);
        BluetoothIOResult result3 = generateResult(UUID.fromString("30000000-0000-0000-0000-000000000000"), "A3-B2-C3-D4-E5-F6", FAKE_SERV_UUID, OperationType.READ);
        BluetoothIOResult result4 = generateResult(UUID.fromString("40000000-0000-0000-0000-000000000000"), "A4-B2-C3-D4-E5-F6", FAKE_SERV_UUID, OperationType.WRITE);
        BluetoothIOResult result5 = generateResult(UUID.fromString("50000000-0000-0000-0000-000000000000"), "A5-B2-C3-D4-E5-F6", FAKE_SERV_UUID, OperationType.READ);
        BluetoothIOResult result6 = generateResult(UUID.fromString("60000000-0000-0000-0000-000000000000"), "A6-B2-C3-D4-E5-F6", FAKE_SERV_UUID, OperationType.READ);

        assertEquals(UUID.fromString("10000000-0000-0000-0000-000000000000"), facade.getCurrentState().queuedIoOperations().get(0).identifier().characteristicUuid());
        assertEquals(6, facade.getCurrentState().queuedIoOperations().size());
        processor.processOperation(result1);

        assertEquals(UUID.fromString("20000000-0000-0000-0000-000000000000"), facade.getCurrentState().queuedIoOperations().get(0).identifier().characteristicUuid());
        assertEquals(5, facade.getCurrentState().queuedIoOperations().size());
        processor.processOperation(result2);

        assertEquals(UUID.fromString("30000000-0000-0000-0000-000000000000"), facade.getCurrentState().queuedIoOperations().get(0).identifier().characteristicUuid());
        assertEquals(4, facade.getCurrentState().queuedIoOperations().size());
        processor.processOperation(result3);

        assertEquals(UUID.fromString("40000000-0000-0000-0000-000000000000"), facade.getCurrentState().queuedIoOperations().get(0).identifier().characteristicUuid());
        assertEquals(3, facade.getCurrentState().queuedIoOperations().size());
        processor.processOperation(result4);

        assertEquals(UUID.fromString("50000000-0000-0000-0000-000000000000"), facade.getCurrentState().queuedIoOperations().get(0).identifier().characteristicUuid());
        assertEquals(2, facade.getCurrentState().queuedIoOperations().size());
        processor.processOperation(result5);

        assertEquals(UUID.fromString("60000000-0000-0000-0000-000000000000"), facade.getCurrentState().queuedIoOperations().get(0).identifier().characteristicUuid());
        assertEquals(1, facade.getCurrentState().queuedIoOperations().size());
        processor.processOperation(result6);

        assertEquals(0, facade.getCurrentState().queuedIoOperations().size());
    }

    //----------------------------------------------------------------------------------------------
    // PENDING WRITE TESTS
    //----------------------------------------------------------------------------------------------

    @Test
    public void should_execute_pending_write() throws Exception {
        TestSubscriber<BluetoothIOResult> onWriteResultTestSubscriber = TestSubscriber.create();
        DetectedBluetoothDevice device = generateDetectedDevice("A1-B2-C3-D4-E5-F6");
        MockBluetoothGattConnection connection = new MockBluetoothGattConnection();

        processor.connectWithDevice(device)
                .takeFirst(connectionResult -> connectionResult.state().equals(BluetoothConnectionState.SERVICES_DISCOVERED))
                .doOnNext(connectionResult -> {
                    // simulate state 'CONNECTED'
                    gattDriver.connectionPublisher.onNext(new BluetoothConnectionResult.Builder()
                            .connection(connection)
                            .detectedDevice(device)
                            .macAddress(device.scanResult().macAddress())
                            .state(BluetoothConnectionState.CONNECTED)
                            .build());
                })
                .subscribe(connectionResult -> {
                    MockConnectedDevice connectedDevice = new MockConnectedDevice(device, (MockBluetoothGattConnection) connectionResult.connection());

                    processor.write(connectedDevice, FAKE_SERV_UUID, FAKE_CHAR_UUID, new byte[]{0x02})
                            .subscribe(onWriteResultTestSubscriber);

                    assertEquals(1, facade.getCurrentState().queuedIoOperations().size());

                    BluetoothIOResult result = generateResult(FAKE_CHAR_UUID, "A1-B2-C3-D4-E5-F6", FAKE_SERV_UUID, OperationType.WRITE);
                    connection.onWriteResult.onNext(result);

                    assertEquals(0, facade.getCurrentState().queuedIoOperations().size());
                    assertEquals(onWriteResultTestSubscriber.getOnNextEvents().get(0).identifier().characteristicUuid(),
                            FAKE_CHAR_UUID);

                    onWriteResultTestSubscriber.assertNoErrors();
                    onWriteResultTestSubscriber.assertCompleted();
                });

        // simulate state 'SERVICES_DISCOVERED' first, or we can't continue!!
        gattDriver.connectionPublisher.onNext(new BluetoothConnectionResult.Builder()
                .connection(connection)
                .detectedDevice(device)
                .macAddress(device.scanResult().macAddress())
                .state(BluetoothConnectionState.SERVICES_DISCOVERED)
                .build());

        onWriteResultTestSubscriber.assertValueCount(1);
    }

    //----------------------------------------------------------------------------------------------
    // OPERATION TESTS
    //----------------------------------------------------------------------------------------------

    @Test
    public void should_return_void_on_passing_null_operation() throws Exception {
        PublishSubject<BluetoothIOResult> mockPublisher = PublishSubject.create();
        DetectedBluetoothDevice device = generateDetectedDevice("A1-B2-C3-D4-E5-F6");
        MockBluetoothGattConnection connection = new MockBluetoothGattConnection();
        MockConnectedDevice connectedDevice = new MockConnectedDevice(device, (MockBluetoothGattConnection) connection);

        OperationIdentifier identifier = new OperationIdentifier.Builder()
                .characteristicUuid(FAKE_CHAR_UUID)
                .macAddress(device.scanResult().macAddress())
                .serviceUuid(FAKE_SERV_UUID)
                .type(OperationType.READ)
                .build();

        PendingRead pendingRead = new PendingRead.Builder()
                .connection(connectedDevice)
                .identifier(identifier)
                .publisher(mockPublisher)
                .build();

        facade.handleCommand(new AddPendingIoOperation.Builder()
                .operation(pendingRead)
                .build());

        assertEquals(1, facade.getCurrentState().queuedIoOperations().size());

        OperationIdentifier faultyIdentifier = new OperationIdentifier.Builder()
                .characteristicUuid(FAKE_CHAR_UUID)
                .macAddress(device.scanResult().macAddress())
                .serviceUuid(FAKE_SERV_UUID)
                .type(OperationType.WRITE)
                .build();

        BluetoothIOResult result = new BluetoothIOResult.Builder()
                .identifier(faultyIdentifier)
                .rawValue(new byte[0])
                .build();

        processor.processOperation(result);

        assertEquals(1, facade.getCurrentState().queuedIoOperations().size());
    }

    //----------------------------------------------------------------------------------------------
    // CONNECTION PROCESSING
    //----------------------------------------------------------------------------------------------

    @Test
    public void should_process_pending_connection() throws Exception {
        TestSubscriber<BluetoothConnectionResult> testSubscriber = TestSubscriber.create();
        DetectedBluetoothDevice device = generateDetectedDevice("A1-B2-C3-D4-E5-F6");
        MockBluetoothGattConnection connection = new MockBluetoothGattConnection();

        processor.connectWithDevice(device).subscribe(testSubscriber);

        assertEquals(1, facade.getCurrentState().pendingConnections().size());

        gattDriver.connectionPublisher.onNext(new BluetoothConnectionResult.Builder()
                .detectedDevice(device)
                .connection(connection)
                .macAddress(device.scanResult().macAddress())
                .state(BluetoothConnectionState.CONNECTED)
                .build());

        assertEquals(0, facade.getCurrentState().pendingConnections().size());

        testSubscriber.assertNoErrors();
    }

    @Test
    public void should_process_multiple_pending_connections() throws Exception {
        DetectedBluetoothDevice device1 = generateDetectedDevice("A1-B2-C3-D4-E5-F6");
        MockBluetoothGattConnection connection1 = new MockBluetoothGattConnection();
        DetectedBluetoothDevice device2 = generateDetectedDevice("A6-B5-C4-D3-E2-F1");
        MockBluetoothGattConnection connection2 = new MockBluetoothGattConnection();

        processor.connectWithDevice(device1)
                .takeFirst(connectionResult -> connectionResult.state().equals(BluetoothConnectionState.SERVICES_DISCOVERED))
                .doOnNext(connectionResult -> {
                    // simulate state 'CONNECTED'
                    gattDriver.connectionPublisher.onNext(new BluetoothConnectionResult.Builder()
                            .connection(connection1)
                            .detectedDevice(device1)
                            .macAddress(device1.scanResult().macAddress())
                            .state(BluetoothConnectionState.CONNECTED)
                            .build());
                })
                .subscribe();

        processor.connectWithDevice(device2)
                .takeFirst(connectionResult -> connectionResult.state().equals(BluetoothConnectionState.SERVICES_DISCOVERED))
                .doOnNext(connectionResult -> {
                    // simulate state 'CONNECTED'
                    gattDriver.connectionPublisher.onNext(new BluetoothConnectionResult.Builder()
                            .connection(connection2)
                            .detectedDevice(device2)
                            .macAddress(device2.scanResult().macAddress())
                            .state(BluetoothConnectionState.CONNECTED)
                            .build());
                })
                .subscribe();

        assertEquals(2, facade.getCurrentState().pendingConnections().size());

        gattDriver.connectionPublisher.onNext(new BluetoothConnectionResult.Builder()
                .detectedDevice(device1)
                .connection(connection1)
                .macAddress(device1.scanResult().macAddress())
                .state(BluetoothConnectionState.SERVICES_DISCOVERED)
                .build());

        assertEquals(1, facade.getCurrentState().pendingConnections().size());

        gattDriver.connectionPublisher.onNext(new BluetoothConnectionResult.Builder()
                .detectedDevice(device2)
                .connection(connection2)
                .macAddress(device2.scanResult().macAddress())
                .state(BluetoothConnectionState.SERVICES_DISCOVERED)
                .build());

        assertEquals(0, facade.getCurrentState().pendingConnections().size());
    }

    @Test
    public void should_remove_pending_connection() throws Exception {
        DetectedBluetoothDevice device = generateDetectedDevice("A1-B2-C3-D4-E5-F6");
        MockBluetoothGattConnection connection = new MockBluetoothGattConnection();

        processor.connectWithDevice(device)
                .takeFirst(connectionResult -> connectionResult.state().equals(BluetoothConnectionState.SERVICES_DISCOVERED))
                .doOnNext(connectionResult -> {
                    // simulate state 'CONNECTED'
                    gattDriver.connectionPublisher.onNext(new BluetoothConnectionResult.Builder()
                            .connection(connection)
                            .detectedDevice(device)
                            .macAddress(device.scanResult().macAddress())
                            .state(BluetoothConnectionState.CONNECTED)
                            .build());
                })
                .subscribe(connectionResult -> {
                    MockConnectedDevice connectedDevice = new MockConnectedDevice(device, (MockBluetoothGattConnection) connectionResult.connection());

                    assertEquals(1, facade.getCurrentState().establishedConnections().size());

                    processor.disconnectFromDevice(connectedDevice).subscribe(result -> {
                        assertEquals(0, facade.getCurrentState().establishedConnections().size());
                    });
                });

        gattDriver.connectionPublisher.onNext(new BluetoothConnectionResult.Builder()
                .connection(connection)
                .detectedDevice(device)
                .macAddress(device.scanResult().macAddress())
                .state(BluetoothConnectionState.SERVICES_DISCOVERED)
                .build());

        gattDriver.connectionPublisher.onNext(new BluetoothConnectionResult.Builder()
                .connection(connection)
                .detectedDevice(device)
                .macAddress(device.scanResult().macAddress())
                .state(BluetoothConnectionState.DISCONNECTED)
                .build());
    }

    //----------------------------------------------------------------------------------------------
    // MULTIPLE CONNECTION PROCESSING
    //----------------------------------------------------------------------------------------------

    @Test
    public void multiple_connections_should_be_processed_in_sequence() throws Exception {
        TestSubscriber<BluetoothConnectionResult> testSubscriber = TestSubscriber.create();

        DetectedBluetoothDevice device1 = generateDetectedDevice("A1-B2-C3-D4-E5-F6");
        DetectedBluetoothDevice device2 = generateDetectedDevice("B2-C3-D4-E5-F6-A7");
        DetectedBluetoothDevice device3 = generateDetectedDevice("C3-D4-E5-F6-A7-B8");

        MockBluetoothGattConnection connection1 = new MockBluetoothGattConnection();
        MockBluetoothGattConnection connection2 = new MockBluetoothGattConnection();
        MockBluetoothGattConnection connection3 = new MockBluetoothGattConnection();

        processor.connectWithDevice(device1).subscribe(testSubscriber);
        assertEquals(1, facade.getCurrentState().pendingConnections().size());

        processor.connectWithDevice(device2).subscribe(testSubscriber);
        assertEquals(2, facade.getCurrentState().pendingConnections().size());

        processor.connectWithDevice(device3).subscribe(testSubscriber);
        assertEquals(3, facade.getCurrentState().pendingConnections().size());

        assertEquals(3, facade.getCurrentState().pendingConnections().size());

        // Process connections..
        gattDriver.connectionPublisher.onNext(new BluetoothConnectionResult.Builder()
                .detectedDevice(device2)
                .connection(connection2)
                .macAddress(device2.scanResult().macAddress())
                .state(BluetoothConnectionState.CONNECTED)
                .build());
        testSubscriber.assertValueCount(1);

        assertEquals(2, facade.getCurrentState().pendingConnections().size());
        assertEquals(device1.scanResult().macAddress(), facade.getCurrentState()
                .pendingConnections().get(0).macAddress());

        gattDriver.connectionPublisher.onNext(new BluetoothConnectionResult.Builder()
                .detectedDevice(device1)
                .connection(connection1)
                .macAddress(device1.scanResult().macAddress())
                .state(BluetoothConnectionState.CONNECTED)
                .build());
        testSubscriber.assertValueCount(2);

        assertEquals(1, facade.getCurrentState().pendingConnections().size());

        gattDriver.connectionPublisher.onNext(new BluetoothConnectionResult.Builder()
                .detectedDevice(device3)
                .connection(connection3)
                .macAddress(device3.scanResult().macAddress())
                .state(BluetoothConnectionState.CONNECTED)
                .build());
        testSubscriber.assertValueCount(3);

        assertEquals(0, facade.getCurrentState().pendingConnections().size());
    }

    //----------------------------------------------------------------------------------------------
    // HELPER METHODS
    //----------------------------------------------------------------------------------------------

    private BluetoothScanResult generateBluetoothScanResult(String mac) {
        return new BluetoothScanResult() {
            @Override
            public String macAddress() {
                return mac;
            }

            @Override
            public Integer signalStrength() {
                return -50;
            }

            @Override
            public byte[] scanRecord() {
                return new byte[0];
            }
        };
    }

    private DetectedBluetoothDevice generateDetectedDevice(String mac) {
        return () -> generateBluetoothScanResult(mac);
    }

    private BluetoothIOResult generateResult(UUID characteristicUuid, String macAddress,
                                             UUID serviceUuid, OperationType type) {
        return new BluetoothIOResult.Builder()
                .identifier(new OperationIdentifier.Builder()
                        .characteristicUuid(characteristicUuid)
                        .macAddress(macAddress)
                        .serviceUuid(serviceUuid)
                        .type(type)
                        .build())
                .rawValue(new byte[0])
                .build();
    }
}

//----------------------------------------------------------------------------------------------
// HELPER CLASSES
//----------------------------------------------------------------------------------------------

class MockConnectedDevice implements ConnectedBluetoothDevice {

    private DetectedBluetoothDevice device;
    private MockBluetoothGattConnection connection;

    //------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //------------------------------------------------------------------------------------------

    public MockConnectedDevice(DetectedBluetoothDevice device,
                               MockBluetoothGattConnection connection) {
        this.device = device;
        this.connection = connection;
    }

    @Override
    public DetectedBluetoothDevice getDetectedDevice() {
        return device;
    }

    @Override
    public BluetoothConnection getConnection() {
        return connection;
    }
}
