package com.cookingfox.android.bluetooth_management.domain.manager;

import com.cookingfox.android.bluetooth_management.component.BluetoothManagementComponent;
import com.cookingfox.android.bluetooth_management.domain.facade.BluetoothManagementFacade;
import com.cookingfox.android.bluetooth_management.testing.driver.MockBluetoothAdapterDriver;
import com.cookingfox.android.core_component.testing.component.manager.TestComponentManager;
import com.cookingfox.chefling.api.CheflingContainer;
import com.cookingfox.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for {@link BluetoothAdapterManagerImpl}.
 */
public class BluetoothAdapterManagerImplTest {

    //----------------------------------------------------------------------------------------------
    // TEST SETUP
    //----------------------------------------------------------------------------------------------

    private BluetoothAdapterManager bluetoothAdapterManager;
    private CheflingContainer container;
    private BluetoothManagementFacade facade;
    private MockBluetoothAdapterDriver mockBluetoothAdapterDriver;

    static {
        Logger.initTest();
    }

    @Before
    public void setUp() throws Exception {
        // create component manager container
        container = TestComponentManager.buildComponent(BluetoothManagementComponent.class);

        mockBluetoothAdapterDriver = container.getInstance(MockBluetoothAdapterDriver.class);

        facade = container.getInstance(BluetoothManagementFacade.class);

        bluetoothAdapterManager = container.getInstance(BluetoothAdapterManagerImpl.class);
    }

    @After
    public void tearDown() throws Exception {
        container.disposeContainer();
    }

    //----------------------------------------------------------------------------------------------
    // TESTS: enable/disable BluetoothAdapter
    //----------------------------------------------------------------------------------------------

    @Test
    public void enableBluetoothAdapter_should_enable_adapter() throws Exception {
        mockBluetoothAdapterDriver.mock_adapterIsEnabled = false;

        bluetoothAdapterManager.enableBluetoothAdapter();

        assertTrue("Should be disabled", facade.getCurrentState().isAdapterEnabled());
    }

    @Test
    public void disableBluetoothAdapter_should_disable_adapter() throws Exception {
        mockBluetoothAdapterDriver.mock_adapterIsEnabled = true;

        bluetoothAdapterManager.disableBluetoothAdapter();

        assertFalse("Should be enabled", facade.getCurrentState().isAdapterEnabled());
    }

}
