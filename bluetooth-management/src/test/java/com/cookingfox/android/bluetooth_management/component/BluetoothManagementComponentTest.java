package com.cookingfox.android.bluetooth_management.component;

import com.cookingfox.android.bluetooth_management.domain.facade.BluetoothManagementFacade;
import com.cookingfox.android.bluetooth_management.domain.state.BluetoothManagementStateObserver;
import com.cookingfox.android.core_component.api.component.manager.ComponentManager;
import com.cookingfox.android.core_component.testing.component.manager.TestComponentManager;
import com.cookingfox.chefling.api.CheflingContainer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Unit test for {@link BluetoothManagementComponent}.
 */
public class BluetoothManagementComponentTest {

    //----------------------------------------------------------------------------------------------
    // TEST SETUP
    //----------------------------------------------------------------------------------------------

    ComponentManager componentManager;
    CheflingContainer container;

    @Before
    public void setUp() throws Exception {
        container = TestComponentManager.buildContainer();
        componentManager = TestComponentManager.getComponentManager(container);
    }

    @After
    public void tearDown() throws Exception {
        componentManager.unloadAllComponents();
        container.disposeContainer();
    }

    //----------------------------------------------------------------------------------------------
    // TESTS: loadComponent
    //----------------------------------------------------------------------------------------------

    @Test
    public void loadComponent_should_not_throw() throws Exception {
        componentManager.loadComponent(BluetoothManagementComponent.class);
    }

    @Test
    public void should_map_bluetooth_management_state_observer() throws Exception {
        componentManager.loadComponent(BluetoothManagementComponent.class);

        BluetoothManagementStateObserver stateObserver =
                container.getInstance(BluetoothManagementStateObserver.class);

        assertTrue(stateObserver instanceof BluetoothManagementFacade);
    }

}
