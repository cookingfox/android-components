package com.cookingfox.android.bluetooth_management.domain.manager;

import com.cookingfox.android.bluetooth_management.component.BluetoothManagementComponent;
import com.cookingfox.android.bluetooth_management.domain.connection.BluetoothConnection;
import com.cookingfox.android.bluetooth_management.domain.device.ConnectedBluetoothDevice;
import com.cookingfox.android.bluetooth_management.domain.device.DetectedBluetoothDevice;
import com.cookingfox.android.bluetooth_management.domain.exception.ConnectionAlreadyEstablishedOrPendingException;
import com.cookingfox.android.bluetooth_management.domain.exception.DiscoverServicesFailedException;
import com.cookingfox.android.bluetooth_management.domain.facade.BluetoothManagementFacade;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothConnectionResult;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothConnectionState;
import com.cookingfox.android.bluetooth_management.domain.vo.scanning.BluetoothScanResult;
import com.cookingfox.android.bluetooth_management.testing.connection.MockBluetoothGattConnection;
import com.cookingfox.android.bluetooth_management.testing.driver.MockBluetoothGattDriver;
import com.cookingfox.android.core_component.testing.component.manager.TestComponentManager;
import com.cookingfox.chefling.api.CheflingContainer;
import com.cookingfox.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import rx.functions.Action1;
import rx.observers.TestSubscriber;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BluetoothDeviceConnectionManagerImplTest {

    private BluetoothDeviceConnectionManagerImpl connectionManager;
    private CheflingContainer container;
    private MockBluetoothGattDriver gattDriver;
    private BluetoothManagementFacade facade;

    //----------------------------------------------------------------------------------------------
    // SETUP
    //----------------------------------------------------------------------------------------------

    @Before
    public void setUp() throws Exception {
        Logger.initTest();

        container = TestComponentManager.buildComponent(BluetoothManagementComponent.class);

        connectionManager = container.getInstance(BluetoothDeviceConnectionManagerImpl.class);
        gattDriver = container.getInstance(MockBluetoothGattDriver.class);
        facade = container.getInstance(BluetoothManagementFacade.class);
    }

    @After
    public void tearDown() throws Exception {
        container.disposeContainer();
    }

    //----------------------------------------------------------------------------------------------
    // CONNECTION TESTS
    //----------------------------------------------------------------------------------------------

    @Test
    public void calling_connect_with_device_should_not_return_null() throws Exception {
        DetectedBluetoothDevice device = generateDetectedDevice("A1-B2-C3-D4-E5-F6");

        assertTrue(connectionManager.connectWithDevice(device) != null);
    }

    @Test
    public void should_fail_on_trying_to_connect_the_same_device_twice() throws Exception {
        TestSubscriber<BluetoothConnectionResult> testSubscriber = TestSubscriber.create();
        DetectedBluetoothDevice device = generateDetectedDevice("A1-B2-C3-D4-E5-F6");
        MockBluetoothGattConnection connection = new MockBluetoothGattConnection();

        connectionManager.connectWithDevice(device).subscribe(testSubscriber);

        connectionManager.connectWithDevice(device).subscribe(testSubscriber);

        testSubscriber.assertError(ConnectionAlreadyEstablishedOrPendingException.class);
        assertEquals(1, facade.getCurrentState().pendingConnections().size());
    }

    @Test
    public void confirmed_connection_should_move_from_connecting_to_connected() throws Exception {
        TestSubscriber<BluetoothConnectionResult> testSubscriber = TestSubscriber.create();
        DetectedBluetoothDevice device = generateDetectedDevice("A1-B2-C3-D4-E5-F6");
        MockBluetoothGattConnection connection = new MockBluetoothGattConnection();

        // Simulate CONNECTING
        connectionManager.connectWithDevice(device).subscribe(testSubscriber);

        assertEquals(1, facade.getCurrentState().pendingConnections().size());

        // Simulate CONNECTED
        gattDriver.performConnect(device, connection);

        assertEquals(0, facade.getCurrentState().pendingConnections().size());

        assertEquals(1, facade.getCurrentState().establishedConnections().size());
    }

    @Test
    public void should_remove_connected_on_disconnect() throws Exception {
        TestSubscriber<BluetoothConnectionResult> testSubscriber = TestSubscriber.create();
        DetectedBluetoothDevice device = generateDetectedDevice("A1-B2-C3-D4-E5-F6");
        MockBluetoothGattConnection connection = new MockBluetoothGattConnection();

        /**
         * Connecting with device
         */
        connectionManager.connectWithDevice(device).subscribe(new Action1<BluetoothConnectionResult>() {
            @Override
            public void call(BluetoothConnectionResult result) {
                MockConnectedDevice connectedDevice = new MockConnectedDevice(result.detectedDevice(),
                        (MockBluetoothGattConnection) result.connection());

                connectionManager.disconnectFromDevice(connectedDevice)
                        .subscribe(new Action1<BluetoothConnectionResult>() {
                            @Override
                            public void call(BluetoothConnectionResult connectionResult) {
                                Logger.info("State: %s", connectionResult.state());
                            }
                        });

            }
        });

        BluetoothConnectionResult resultConnected = new BluetoothConnectionResult.Builder()
                .detectedDevice(device)
                .connection(connection)
                .macAddress(device.scanResult().macAddress())
                .state(BluetoothConnectionState.CONNECTED)
                .build();

        BluetoothConnectionResult resultServicesDiscovered = new BluetoothConnectionResult.Builder()
                .detectedDevice(device)
                .connection(connection)
                .macAddress(device.scanResult().macAddress())
                .state(BluetoothConnectionState.SERVICES_DISCOVERED)
                .build();

        // simulate connection state 'CONNECTED'
        gattDriver.connectionPublisher.onNext(resultConnected);

        // simulate connection state 'SERVICES_DISCOVERED'
        connection.onDiscoverServicesResult.onNext(resultServicesDiscovered);

        BluetoothConnectionResult resultDisconnected = new BluetoothConnectionResult.Builder()
                .detectedDevice(device)
                .connection(connection)
                .macAddress(device.scanResult().macAddress())
                .state(BluetoothConnectionState.DISCONNECTED)
                .build();

        // simulate connection state 'DISCONNECTED'
        gattDriver.connectionPublisher.onNext(resultDisconnected);
    }

    @Test
    public void should_disconnect_on_discover_services_error() throws Exception {
        DetectedBluetoothDevice device = generateDetectedDevice("A1-B2-C3-D4-E5-F6");
        String macAddress = device.scanResult().macAddress();

        MockBluetoothGattConnection connection = new MockBluetoothGattConnection();

        BluetoothConnectionResult resultConnected = new BluetoothConnectionResult.Builder()
                .detectedDevice(device)
                .connection(connection)
                .macAddress(macAddress)
                .state(BluetoothConnectionState.CONNECTED)
                .build();

        BluetoothConnectionResult resultDisconnected = new BluetoothConnectionResult.Builder()
                .from(resultConnected)
                .state(BluetoothConnectionState.DISCONNECTED)
                .build();

        TestSubscriber<BluetoothConnectionResult> subscriber = TestSubscriber.create();

        connectionManager.connectWithDevice(device).subscribe(subscriber);

        gattDriver.performConnect(device, connection);

        connection.onDiscoverServicesResult.onError(new DiscoverServicesFailedException(macAddress));

        connection.onConnectionResult.onNext(resultDisconnected);

        subscriber.assertError(RuntimeException.class);
    }

    //----------------------------------------------------------------------------------------------
    // HELPER METHODS
    //----------------------------------------------------------------------------------------------

    private BluetoothScanResult generateBluetoothScanResult(String mac) {
        return new BluetoothScanResult() {
            @Override
            public String macAddress() {
                return mac;
            }

            @Override
            public Integer signalStrength() {
                return -50;
            }

            @Override
            public byte[] scanRecord() {
                return new byte[0];
            }
        };
    }

    private DetectedBluetoothDevice generateDetectedDevice(String mac) {
        return () -> generateBluetoothScanResult(mac);
    }

    //----------------------------------------------------------------------------------------------
    // HELPER CLASSES
    //----------------------------------------------------------------------------------------------

    class MockConnectedDevice implements ConnectedBluetoothDevice {

        private DetectedBluetoothDevice device;
        private MockBluetoothGattConnection connection;

        //------------------------------------------------------------------------------------------
        // CONSTRUCTOR
        //------------------------------------------------------------------------------------------

        public MockConnectedDevice(DetectedBluetoothDevice device,
                                   MockBluetoothGattConnection connection) {
            this.device = device;
            this.connection = connection;
        }

        @Override
        public DetectedBluetoothDevice getDetectedDevice() {
            return device;
        }

        @Override
        public BluetoothConnection getConnection() {
            return connection;
        }
    }
}
