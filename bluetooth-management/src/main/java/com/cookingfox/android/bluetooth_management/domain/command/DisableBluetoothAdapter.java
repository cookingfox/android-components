package com.cookingfox.android.bluetooth_management.domain.command;

import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;
import com.cookingfox.lapasse.api.command.Command;

import org.immutables.value.Value;

/**
 * Command to disable the bluetooth adapter.
 */
@DefaultValueStyle
@Value.Immutable
public interface DisableBluetoothAdapter extends Command {

    class Builder extends ImmutableDisableBluetoothAdapter.Builder {
    }

}
