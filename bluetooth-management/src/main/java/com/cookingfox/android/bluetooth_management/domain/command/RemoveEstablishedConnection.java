package com.cookingfox.android.bluetooth_management.domain.command;

import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothConnectionResult;
import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;
import com.cookingfox.lapasse.api.command.Command;

import org.immutables.value.Value;

@DefaultValueStyle
@Value.Immutable
public interface RemoveEstablishedConnection extends Command {

    BluetoothConnectionResult result();

    class Builder extends ImmutableRemoveEstablishedConnection.Builder {
    }
}