package com.cookingfox.android.bluetooth_management.component.config;

import android.os.Build;

import com.cookingfox.android.bluetooth_management.android.driver.AndroidBluetoothAdapterDriver;
import com.cookingfox.android.bluetooth_management.android.driver.AndroidBluetoothGattDriver;
import com.cookingfox.android.bluetooth_management.android.driver.CookingFoxBluetoothScannerDriver;
import com.cookingfox.android.bluetooth_management.android.preferences.AndroidBluetoothManagementPreferences;
import com.cookingfox.android.bluetooth_management.android.scanner.BluetoothScanner;
import com.cookingfox.android.bluetooth_management.android.scanner.KitKatBluetoothScanner;
import com.cookingfox.android.bluetooth_management.android.scanner.LollipopBluetoothScanner;
import com.cookingfox.android.bluetooth_management.domain.driver.BluetoothAdapterDriver;
import com.cookingfox.android.bluetooth_management.domain.driver.BluetoothGattDriver;
import com.cookingfox.android.bluetooth_management.domain.driver.BluetoothScannerDriver;
import com.cookingfox.android.bluetooth_management.domain.facade.BluetoothManagementFacade;
import com.cookingfox.android.bluetooth_management.domain.manager.BluetoothAdapterManager;
import com.cookingfox.android.bluetooth_management.domain.manager.BluetoothAdapterManagerImpl;
import com.cookingfox.android.bluetooth_management.domain.manager.BluetoothDeviceConnectionManager;
import com.cookingfox.android.bluetooth_management.domain.manager.BluetoothDeviceConnectionManagerImpl;
import com.cookingfox.android.bluetooth_management.domain.manager.BluetoothScanningManager;
import com.cookingfox.android.bluetooth_management.domain.manager.BluetoothScanningManagerImpl;
import com.cookingfox.android.bluetooth_management.domain.preferences.BluetoothManagementPreferences;
import com.cookingfox.android.bluetooth_management.domain.processor.DefaultOperationsProcessor;
import com.cookingfox.android.bluetooth_management.domain.processor.OperationsProcessor;
import com.cookingfox.android.bluetooth_management.domain.state.BluetoothManagementState;
import com.cookingfox.android.bluetooth_management.domain.state.BluetoothManagementStateObserver;
import com.cookingfox.android.bluetooth_management.testing.driver.MockBluetoothAdapterDriver;
import com.cookingfox.android.bluetooth_management.testing.driver.MockBluetoothGattDriver;
import com.cookingfox.android.bluetooth_management.testing.driver.MockBluetoothScannerDriver;
import com.cookingfox.android.core_component.api.component.config.ComponentCheflingConfig;
import com.cookingfox.android.core_component.impl.threading.ComponentThreadPoolProvider;
import com.cookingfox.chefling.api.CheflingBuilder;
import com.cookingfox.chefling.api.CheflingConfig;
import com.cookingfox.chefling.impl.Chefling;
import com.cookingfox.lapasse.api.message.store.MessageStore;
import com.cookingfox.lapasse.impl.facade.LaPasseRxFacade;

import rx.schedulers.Schedulers;

/**
 * Chefling configuration for bluetooth management component.
 */
public class BluetoothManagementCheflingConfig implements ComponentCheflingConfig {

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public CheflingBuilder createCheflingBuilder() {
        return Chefling.createBuilder()
                .addConfig(domainConfig);
    }

    @Override
    public CheflingConfig createDeviceConfig() {
        return deviceConfig;
    }

    @Override
    public CheflingConfig createEmulatorConfig() {
        // NOTE: uses test config with mock implementations
        return testConfig;
    }

    @Override
    public CheflingConfig createTestConfig() {
        return testConfig;
    }

    //----------------------------------------------------------------------------------------------
    // DEVICE CONFIGURATION
    //----------------------------------------------------------------------------------------------

    public final CheflingConfig deviceConfig = container -> {
        // Map Bluetooth Scanner we want to use, bas on SDK version
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            container.mapType(BluetoothScanner.class, KitKatBluetoothScanner.class);
        } else {
            container.mapType(BluetoothScanner.class, LollipopBluetoothScanner.class);
        }

        // Map Bluetooth Drivers
        container.mapType(BluetoothAdapterDriver.class, AndroidBluetoothAdapterDriver.class);
        container.mapType(BluetoothScannerDriver.class, CookingFoxBluetoothScannerDriver.class);
        container.mapType(BluetoothGattDriver.class, AndroidBluetoothGattDriver.class);
    };

    //----------------------------------------------------------------------------------------------
    // DOMAIN CONFIGURATION
    //----------------------------------------------------------------------------------------------

    public final CheflingConfig domainConfig = container -> {
        container.mapFactory(BluetoothManagementFacade.class, c -> {
            BluetoothManagementState initialState = BluetoothManagementState.createInitialState();

            LaPasseRxFacade<BluetoothManagementState> facade = new LaPasseRxFacade.Builder<>(initialState)
                    .setMessageStore(c.getInstance(MessageStore.class))
                    .build();

            // Tell facade which thread should handle the Subscribers
            ComponentThreadPoolProvider threadPoolProvider = c.getInstance(ComponentThreadPoolProvider.class);
            facade.setCommandSubscribeScheduler(Schedulers.from(threadPoolProvider.getExecutor()));

            return new BluetoothManagementFacade(facade);
        });

        // map state observer interface to facade
        container.mapType(BluetoothManagementStateObserver.class, BluetoothManagementFacade.class);

        // map preferences
        container.mapType(BluetoothManagementPreferences.class, AndroidBluetoothManagementPreferences.class);

        // map Bluetooth device connection manager
        container.mapType(BluetoothDeviceConnectionManager.class, BluetoothDeviceConnectionManagerImpl.class);

        container.mapType(BluetoothAdapterManager.class, BluetoothAdapterManagerImpl.class);
        container.mapType(BluetoothScanningManager.class, BluetoothScanningManagerImpl.class);

        // map connections processor
        container.mapType(OperationsProcessor.class, DefaultOperationsProcessor.class);
    };

    //----------------------------------------------------------------------------------------------
    // TEST CONFIG
    //----------------------------------------------------------------------------------------------

    public final CheflingConfig testConfig = container -> {
        container.mapType(BluetoothAdapterDriver.class, MockBluetoothAdapterDriver.class);
        container.mapType(BluetoothScannerDriver.class, MockBluetoothScannerDriver.class);
        container.mapType(BluetoothGattDriver.class, MockBluetoothGattDriver.class);
    };

}
