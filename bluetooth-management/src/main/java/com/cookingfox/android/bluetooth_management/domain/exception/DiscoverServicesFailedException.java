package com.cookingfox.android.bluetooth_management.domain.exception;

/**
 * Thrown when the discover services operation failed.
 */
public class DiscoverServicesFailedException extends Exception {

    public DiscoverServicesFailedException(String macAddress) {
        super("Discover services failed for bluetooth device: " + macAddress);
    }

}
