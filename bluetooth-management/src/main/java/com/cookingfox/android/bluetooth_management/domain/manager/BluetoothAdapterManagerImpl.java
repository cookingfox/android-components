package com.cookingfox.android.bluetooth_management.domain.manager;

import com.cookingfox.android.bluetooth_management.domain.command.DisableBluetoothAdapter;
import com.cookingfox.android.bluetooth_management.domain.command.EnableBluetoothAdapter;
import com.cookingfox.android.bluetooth_management.domain.command.RequestBluetoothAdapterInfo;
import com.cookingfox.android.bluetooth_management.domain.driver.BluetoothAdapterDriver;
import com.cookingfox.android.bluetooth_management.domain.event.BluetoothAdapterInfoUpdated;
import com.cookingfox.android.bluetooth_management.domain.facade.BluetoothManagementFacade;
import com.cookingfox.android.bluetooth_management.domain.preferences.BluetoothManagementPreferences;
import com.cookingfox.android.bluetooth_management.domain.state.BluetoothManagementState;
import com.cookingfox.android.bluetooth_management.domain.vo.BluetoothAdapterInfo;
import com.cookingfox.chefling.api.CheflingLifecycle;
import com.cookingfox.lapasse.annotation.HandleCommand;
import com.cookingfox.lapasse.annotation.HandleEvent;
import com.cookingfox.lapasse.impl.helper.LaPasse;
import com.cookingfox.logging.Logger;

import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

/**
 * Default implementation of {@link BluetoothAdapterManager}.
 */
public class BluetoothAdapterManagerImpl implements CheflingLifecycle, BluetoothAdapterManager {

    private final BluetoothAdapterDriver bluetoothAdapterDriver;
    private final BluetoothManagementFacade facade;
    private final BluetoothManagementPreferences preferences;
    private final CompositeSubscription subs = new CompositeSubscription();

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public BluetoothAdapterManagerImpl(BluetoothAdapterDriver bluetoothAdapterDriver,
                                       BluetoothManagementFacade facade,
                                       BluetoothManagementPreferences preferences) {
        this.bluetoothAdapterDriver = bluetoothAdapterDriver;
        this.facade = facade;
        this.preferences = preferences;
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public void disableBluetoothAdapter() {
        facade.handleCommand(new DisableBluetoothAdapter.Builder().build());
    }

    @Override
    public void enableBluetoothAdapter() {
        facade.handleCommand(new EnableBluetoothAdapter.Builder().build());
    }

    //----------------------------------------------------------------------------------------------
    // LIFECYCLE
    //----------------------------------------------------------------------------------------------

    /**
     * Register the manager:
     * - Registers the Bluetooth adapter driver
     * - Subscribes to the Bluetooth adapter driver's adapter state changes
     * - Enables the Bluetooth adapter if set to true in preferences
     */
    @Override
    public void initialize() {
        LaPasse.mapHandlers(this, facade);

        // Observe changes of the Bluetooth adapter
        subs.add(bluetoothAdapterDriver.observeBluetoothStateChanges()
                .subscribe(onAdapterStateChange, Logger::error));

        // Enable the Bluetooth adapter if necessary
        if (preferences.autoEnableBluetoothAdapter().getValue()) {
            enableBluetoothAdapter();
        }
    }

    @Override
    public void dispose() {
        if (!subs.isUnsubscribed()) subs.unsubscribe();
    }

    //----------------------------------------------------------------------------------------------
    // COMMAND HANDLERS
    //----------------------------------------------------------------------------------------------

    /**
     * @param state   The current state.
     * @param command The command to process.
     * @return Bluetooth adapter info updated event.
     */
    @HandleCommand
    BluetoothAdapterInfoUpdated handle(BluetoothManagementState state, EnableBluetoothAdapter command) {
        if (state.isAdapterEnabled()) {
            return null;
        }

        bluetoothAdapterDriver.enableAdapter();

        return new BluetoothAdapterInfoUpdated.Builder()
                .info(bluetoothAdapterDriver.getBluetoothAdapterInfo())
                .build();
    }

    /**
     * @param state   The current state.
     * @param command The command to process.
     * @return Bluetooth adapter info updated event.
     */
    @HandleCommand
    BluetoothAdapterInfoUpdated handle(BluetoothManagementState state, DisableBluetoothAdapter command) {
        if (!state.isAdapterEnabled()) {
            return null;
        }

        bluetoothAdapterDriver.disableAdapter();

        return new BluetoothAdapterInfoUpdated.Builder()
                .info(bluetoothAdapterDriver.getBluetoothAdapterInfo())
                .build();
    }

    /**
     * @param state   The current state.
     * @param command The command to process.
     * @return Bluetooth adapter info updated event.
     */
    @HandleCommand
    BluetoothAdapterInfoUpdated handle(BluetoothManagementState state, RequestBluetoothAdapterInfo command) {
        return new BluetoothAdapterInfoUpdated.Builder()
                .info(bluetoothAdapterDriver.getBluetoothAdapterInfo())
                .build();
    }

    //----------------------------------------------------------------------------------------------
    // EVENT HANDLERS
    //----------------------------------------------------------------------------------------------

    /**
     * @param state The current state.
     * @param event The command to process.
     * @return Updated Bluetooth adapter status info.
     */
    @HandleEvent
    BluetoothManagementState handle(BluetoothManagementState state, BluetoothAdapterInfoUpdated event) {
        return new BluetoothManagementState.Builder()
                .from(state)
                .info(event.info())
                .build();
    }

    //----------------------------------------------------------------------------------------------
    // SUBSCRIBERS
    //----------------------------------------------------------------------------------------------

    /**
     * Subscriber for adapter state & behavior changes.
     */
    protected final Action1<BluetoothAdapterInfo> onAdapterStateChange = new Action1<BluetoothAdapterInfo>() {
        @Override
        public void call(BluetoothAdapterInfo bluetoothAdapterStateChange) {
            facade.handleCommand(new RequestBluetoothAdapterInfo.Builder().build());
        }
    };
}
