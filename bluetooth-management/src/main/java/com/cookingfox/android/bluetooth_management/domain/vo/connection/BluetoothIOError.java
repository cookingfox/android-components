package com.cookingfox.android.bluetooth_management.domain.vo.connection;

import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;

import org.immutables.value.Value;

@DefaultValueStyle
@Value.Immutable
public interface BluetoothIOError {

    String message();

    class Builder extends ImmutableBluetoothIOError.Builder {
    }
}