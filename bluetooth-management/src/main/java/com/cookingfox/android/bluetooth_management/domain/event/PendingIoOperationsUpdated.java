package com.cookingfox.android.bluetooth_management.domain.event;

import com.cookingfox.android.bluetooth_management.domain.operation.PendingOperation;
import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;
import com.cookingfox.lapasse.api.event.Event;

import org.immutables.value.Value;

import java.util.List;

@DefaultValueStyle
@Value.Immutable
public interface PendingIoOperationsUpdated extends Event {

    List<PendingOperation> operations();

    class Builder extends ImmutablePendingIoOperationsUpdated.Builder {
    }
}
