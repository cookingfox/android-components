package com.cookingfox.android.bluetooth_management.android.scanner;

import android.bluetooth.BluetoothAdapter;
import android.support.annotation.CallSuper;

import com.cookingfox.android.bluetooth_management.domain.vo.scanning.BluetoothScanError;
import com.cookingfox.android.bluetooth_management.domain.vo.scanning.BluetoothScanResult;
import com.cookingfox.chefling.api.CheflingLifecycle;

import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Abstract implementation of {@link BluetoothScanner}, containing default functionality and helper
 * methods.
 */
public abstract class AbstractBluetoothScanner implements BluetoothScanner, CheflingLifecycle {

    /**
     * Reference to bluetooth adapter.
     */
    protected BluetoothAdapter bluetoothAdapter;

    /**
     * Publishes scan errors.
     */
    protected PublishSubject<BluetoothScanError> scanErrorsPublisher;

    /**
     * Publishes scan results.
     */
    protected PublishSubject<BluetoothScanResult> scanResultsPublisher;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public AbstractBluetoothScanner(BluetoothAdapter bluetoothAdapter) {
        this.bluetoothAdapter = bluetoothAdapter;
    }

    //----------------------------------------------------------------------------------------------
    // LIFECYCLE
    //----------------------------------------------------------------------------------------------

    @CallSuper
    @Override
    public void initialize() {
        scanErrorsPublisher = PublishSubject.create();
        scanResultsPublisher = PublishSubject.create();
    }

    @CallSuper
    @Override
    public void dispose() {
        scanErrorsPublisher.onCompleted();
        scanResultsPublisher.onCompleted();
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public final Observable<BluetoothScanError> observeScanErrors() {
        return scanErrorsPublisher;
    }

    @Override
    public final Observable<BluetoothScanResult> observeScanResults() {
        return scanResultsPublisher;
    }

    @Override
    public final void startScanning() {
        if (bluetoothAdapter != null && bluetoothAdapter.isEnabled()) {
            safeStartScanning();
        }
    }

    @Override
    public final void stopScanning() {
        if (bluetoothAdapter != null && bluetoothAdapter.isEnabled()) {
            safeStopScanning();
        }
    }

    //----------------------------------------------------------------------------------------------
    // PROTECTED METHODS
    //----------------------------------------------------------------------------------------------

    /**
     * Publish a scan error.
     *
     * @param errorCode The error code, if available.
     */
    protected void publishScanError(int errorCode) {
        scanErrorsPublisher.onNext(new BluetoothScanError.Builder()
                .errorCode(errorCode)
                .build());
    }

    /**
     * Publish a scan result.
     *
     * @param macAddress The scanned bluetooth device's MAC address.
     * @param rssi       The detected signal strength of the scanned bluetooth device.
     * @param scanRecord The broadcasted advertisement packet of the bluetooth device.
     */
    protected void publishScanResult(String macAddress, int rssi, byte[] scanRecord) {
        scanResultsPublisher.onNext(new BluetoothScanResult.Builder()
                .macAddress(macAddress)
                .signalStrength(rssi)
                .scanRecord(scanRecord)
                .build());
    }

    /**
     * The bluetooth adapter can safely be used to start scanning for devices.
     */
    protected abstract void safeStartScanning();

    /**
     * The bluetooth adapter can safely be used to stop scanning for devices.
     */
    protected abstract void safeStopScanning();

}
