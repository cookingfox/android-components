package com.cookingfox.android.bluetooth_management.domain.operation;

import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;

import org.immutables.value.Value;

@DefaultValueStyle
@Value.Immutable
public interface PendingRead extends PendingOperation {
    class Builder extends ImmutablePendingRead.Builder {
    }
}