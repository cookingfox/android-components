package com.cookingfox.android.bluetooth_management.domain.command;

import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;
import com.cookingfox.lapasse.api.command.Command;

import org.immutables.value.Value;

/**
 * Command to enable bluetooth adapter.
 */
@DefaultValueStyle
@Value.Immutable
public interface EnableBluetoothAdapter extends Command {

    class Builder extends ImmutableEnableBluetoothAdapter.Builder {
    }

}
