package com.cookingfox.android.bluetooth_management.domain.event;

import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;
import com.cookingfox.lapasse.api.event.Event;

import org.immutables.value.Value;

/**
 * Event for when the bluetooth adapter state changed.
 */
@DefaultValueStyle
@Value.Immutable
public interface BluetoothAdapterStateChanged extends Event {
    class Builder extends ImmutableBluetoothAdapterStateChanged.Builder {

    }
}
