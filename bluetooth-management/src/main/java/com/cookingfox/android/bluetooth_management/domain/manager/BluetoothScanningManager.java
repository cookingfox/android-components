package com.cookingfox.android.bluetooth_management.domain.manager;

import com.cookingfox.android.bluetooth_management.domain.api.BluetoothScanningAware;

/**
 * Interface for bluetooth scanning manager.
 */
public interface BluetoothScanningManager extends BluetoothScanningAware {
}
