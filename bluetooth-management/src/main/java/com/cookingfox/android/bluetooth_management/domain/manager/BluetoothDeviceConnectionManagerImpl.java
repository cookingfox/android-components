package com.cookingfox.android.bluetooth_management.domain.manager;

import com.cookingfox.android.bluetooth_management.domain.connection.BluetoothConnection;
import com.cookingfox.android.bluetooth_management.domain.device.ConnectedBluetoothDevice;
import com.cookingfox.android.bluetooth_management.domain.device.DetectedBluetoothDevice;
import com.cookingfox.android.bluetooth_management.domain.processor.OperationsProcessor;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothConnectionResult;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothConnectionState;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothIOResult;

import java.util.UUID;

import rx.Observable;

public class BluetoothDeviceConnectionManagerImpl implements BluetoothDeviceConnectionManager {

    private final OperationsProcessor processor;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public BluetoothDeviceConnectionManagerImpl(OperationsProcessor processor) {
        this.processor = processor;
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public Observable<BluetoothConnectionResult> connectWithDevice(DetectedBluetoothDevice device) {
        ProgressVo progressVo = new ProgressVo();

        return processor.connectWithDevice(device)
                // first time we receive CONNECTED
                .takeFirst(result -> result.state() == BluetoothConnectionState.CONNECTED)
                // store reference to connection
                .doOnNext(result -> progressVo.connection = result.connection())
                // discover services
                .flatMap(result -> result.connection().discoverServices())
                // disconnect on error
                .onErrorResumeNext(error -> {
                    // no stored connection? error
                    if (progressVo.connection == null) {
                        return Observable.error(error);
                    }

                    // get stored reference to connection object
                    return progressVo.connection
                            // disconnect
                            .disconnect()
                            // throw when disconnected: could not establish connection
                            .doOnNext(result -> {
                                throw new RuntimeException("Could not establish connection", error);
                            });
                });
    }

    @Override
    public Observable<BluetoothConnectionResult> disconnectFromDevice(ConnectedBluetoothDevice connectedDevice) {
        return processor.disconnectFromDevice(connectedDevice);
    }

    @Override
    public Observable<BluetoothIOResult> read(ConnectedBluetoothDevice connection, UUID serviceUuid, UUID characteristicUuid) {
        return processor.read(connection, serviceUuid, characteristicUuid);
    }

    @Override
    public Observable<BluetoothIOResult> write(ConnectedBluetoothDevice connection, UUID serviceUuid, UUID characteristicUuid, byte[] value) {
        return processor.write(connection, serviceUuid, characteristicUuid, value);
    }

    //----------------------------------------------------------------------------------------------
    // MEMBER CLASS: CONNECT PROGRESS VO
    //----------------------------------------------------------------------------------------------

    /**
     * Value Object for tracking connection progress.
     */
    static class ProgressVo {

        /**
         * Connection reference.
         */
        BluetoothConnection connection;

    }

}
