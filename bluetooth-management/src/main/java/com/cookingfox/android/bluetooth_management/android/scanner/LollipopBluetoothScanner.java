package com.cookingfox.android.bluetooth_management.android.scanner;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.os.Build;

import com.cookingfox.logging.Logger;

import java.util.LinkedList;
import java.util.List;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class LollipopBluetoothScanner extends AbstractBluetoothScanner {

    /**
     * List of scan filters.
     */
    protected final List<ScanFilter> scanFilters = new LinkedList<>();

    /**
     * Bluetooth scanner settings.
     */
    protected ScanSettings scanSettings;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public LollipopBluetoothScanner(BluetoothAdapter bluetoothAdapter) {
        super(bluetoothAdapter);
    }

    //----------------------------------------------------------------------------------------------
    // LIFECYCLE
    //----------------------------------------------------------------------------------------------

    @Override
    public void initialize() {
        super.initialize();

        scanSettings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                .build();
    }

    //----------------------------------------------------------------------------------------------
    // PROTECTED METHODS
    //----------------------------------------------------------------------------------------------

    /**
     * Publish the Lollipop scan result.
     *
     * @param result The Lollipop scan result.
     */
    protected void publishLollipopScanResult(ScanResult result) {
        ScanRecord scanRecord = result.getScanRecord();

        if (scanRecord == null) {
            Logger.error("`ScanResult.getScanRecord()` returned null - ignoring");
            return;
        }

        publishScanResult(result.getDevice().getAddress(), result.getRssi(), scanRecord.getBytes());
    }

    @Override
    protected void safeStartScanning() {
        BluetoothLeScanner bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner();

        if (bluetoothLeScanner == null) {
            Logger.error("`BluetoothAdapter.getBluetoothLeScanner()` returned null");
            return;
        }

        bluetoothLeScanner.startScan(scanFilters, scanSettings, scanCallback);
    }

    @Override
    protected void safeStopScanning() {
        BluetoothLeScanner bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner();

        if (bluetoothLeScanner == null) {
            Logger.error("`BluetoothAdapter.getBluetoothLeScanner()` returned null");
            return;
        }

        bluetoothLeScanner.stopScan(scanCallback);
    }

    //----------------------------------------------------------------------------------------------
    // SCAN CALLBACK
    //----------------------------------------------------------------------------------------------

    /**
     * Bluetooth Low Energy scan callback implementation.
     */
    protected final ScanCallback scanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            publishLollipopScanResult(result);
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            for (ScanResult result : results) {
                publishLollipopScanResult(result);
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            publishScanError(errorCode);
        }
    };

}
