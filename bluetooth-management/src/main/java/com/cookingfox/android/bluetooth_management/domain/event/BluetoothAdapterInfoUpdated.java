package com.cookingfox.android.bluetooth_management.domain.event;

import com.cookingfox.android.bluetooth_management.domain.vo.BluetoothAdapterInfo;
import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;
import com.cookingfox.lapasse.api.event.Event;

import org.immutables.value.Value;

/**
 * Event for when the bluetooth adapter info is updated.
 */
@DefaultValueStyle
@Value.Immutable
public interface BluetoothAdapterInfoUpdated extends Event {

    BluetoothAdapterInfo info();

    class Builder extends ImmutableBluetoothAdapterInfoUpdated.Builder {
    }

}
