package com.cookingfox.android.bluetooth_management.android.driver;

import android.bluetooth.BluetoothAdapter;

import com.cookingfox.android.bluetooth_management.android.connection.AndroidBluetoothGattConnection;
import com.cookingfox.android.bluetooth_management.domain.device.ConnectedBluetoothDevice;
import com.cookingfox.android.bluetooth_management.domain.device.DetectedBluetoothDevice;
import com.cookingfox.android.bluetooth_management.domain.driver.BluetoothGattDriver;
import com.cookingfox.android.bluetooth_management.domain.operation.PendingRead;
import com.cookingfox.android.bluetooth_management.domain.operation.PendingWrite;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothConnectionResult;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothIOResult;
import com.cookingfox.android.core_component.api.android.context.AppContext;

import rx.Observable;

public class AndroidBluetoothGattDriver implements BluetoothGattDriver {

    private final BluetoothAdapter bluetoothAdapter;
    private final AppContext context;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public AndroidBluetoothGattDriver(AppContext context, BluetoothAdapter bluetoothAdapter) {
        this.context = context;
        this.bluetoothAdapter = bluetoothAdapter;
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public <T extends DetectedBluetoothDevice> Observable<BluetoothConnectionResult> connectWithDevice(T device) {
        // We provide the onDeviceConnected observable so we can call 'onNext' only if the device is connected
        AndroidBluetoothGattConnection connection = new AndroidBluetoothGattConnection(bluetoothAdapter, context, device);

        return connection.connect();
    }

    @Override
    public Observable<BluetoothConnectionResult> discoverServices(ConnectedBluetoothDevice device) {
        return device.getConnection().discoverServices();
    }

    @Override
    public Observable<BluetoothConnectionResult> disconnectFromDevice(ConnectedBluetoothDevice connectedDevice) {
        AndroidBluetoothGattConnection connection = (AndroidBluetoothGattConnection) connectedDevice.getConnection();

        return connection.disconnect();
    }

    @Override
    public Observable<BluetoothIOResult> read(PendingRead pendingRead) {
        AndroidBluetoothGattConnection gattConnection = (AndroidBluetoothGattConnection) pendingRead.connection().getConnection();

        return gattConnection.read(pendingRead.identifier().serviceUuid(), pendingRead.identifier().characteristicUuid());
    }

    @Override
    public Observable<BluetoothIOResult> write(PendingWrite pendingWrite) {
        AndroidBluetoothGattConnection gattConnection = (AndroidBluetoothGattConnection) pendingWrite.connection().getConnection();

        return gattConnection.write(pendingWrite.identifier().serviceUuid(), pendingWrite.identifier().characteristicUuid(), pendingWrite.value());
    }
}
