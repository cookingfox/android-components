package com.cookingfox.android.bluetooth_management.android.scanner;

import com.cookingfox.android.bluetooth_management.domain.api.BluetoothScanningAware;
import com.cookingfox.android.bluetooth_management.domain.vo.scanning.BluetoothScanError;
import com.cookingfox.android.bluetooth_management.domain.vo.scanning.BluetoothScanResult;

import rx.Observable;

/**
 * Interface for a Bluetooth scanner, which produces scan results and errors.
 */
public interface BluetoothScanner extends BluetoothScanningAware {

    /**
     * @return Observable for scan errors.
     */
    Observable<BluetoothScanError> observeScanErrors();

    /**
     * @return Observable for scan results.
     */
    Observable<BluetoothScanResult> observeScanResults();

}
