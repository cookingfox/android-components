package com.cookingfox.android.bluetooth_management.domain.api;

/**
 * Base interface for bluetooth scanning aware implementations.
 */
public interface BluetoothScanningAware {

    /**
     * Start the bluetooth scanning process.
     */
    void startScanning();

    /**
     * Stop the bluetooth scanning process.
     */
    void stopScanning();

}
