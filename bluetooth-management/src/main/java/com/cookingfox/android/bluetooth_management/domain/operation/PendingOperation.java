package com.cookingfox.android.bluetooth_management.domain.operation;

import com.cookingfox.android.bluetooth_management.domain.device.ConnectedBluetoothDevice;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothIOResult;

import rx.subjects.PublishSubject;

/**
 * Represents a READ or WRITE operation.
 * <p>
 * IO operations are uniquely stored in order to keep track of the order in which they need to be
 * executed.
 * <p>
 * The identifier is used to match pending operations against incoming finished operations.
 */
public interface PendingOperation {

    /**
     * @return OperationIdentifier unique for every operation.
     */
    OperationIdentifier identifier();

    /**
     * @return ConnectedBluetoothDevice of which the operation belongs to.
     */
    ConnectedBluetoothDevice connection();

    /**
     * @return PublishSubject<BluetoothIOResult> for publishing the IO result.
     */
    PublishSubject<BluetoothIOResult> publisher();
}
