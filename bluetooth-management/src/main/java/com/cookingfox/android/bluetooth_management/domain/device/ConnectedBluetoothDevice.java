package com.cookingfox.android.bluetooth_management.domain.device;

import com.cookingfox.android.bluetooth_management.domain.connection.BluetoothConnection;

/**
 * A ConnectedBluetoothDevice could practically be any kind of BLE peripheral.
 * <p>
 * ConnectedBluetoothDevice contains a type of detected device, e.g. MinewDetectedBeacon or
 * DetectedAppleKeyboard.
 *
 * @param <D> extending DetectedBluetoothDevice
 */
public interface ConnectedBluetoothDevice<D extends DetectedBluetoothDevice> {

    /**
     * Returns the detected device which is used to establish a connection.
     *
     * @return D extending DetectedBluetoothDevice
     */
    D getDetectedDevice();

    /**
     * Returns the connection of the connected device.
     *
     * @return BluetoothConnection
     */
    BluetoothConnection getConnection();
}
