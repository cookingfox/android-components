package com.cookingfox.android.bluetooth_management.domain.processor;

import com.cookingfox.android.bluetooth_management.domain.device.ConnectedBluetoothDevice;
import com.cookingfox.android.bluetooth_management.domain.device.DetectedBluetoothDevice;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothConnectionResult;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothIOResult;

import java.util.UUID;

import rx.Observable;

/**
 * Default Operations Processor
 * <p>
 * The processor implementation decides for example whether queueing is applied or not, and the way
 * connectivity and read/write operations should be performed. By default connections and IO operations are
 * queued. This means that only one READ or WRITE operation at the time can be processed.
 * <p>
 * For each incoming connection-to-establish a queued connection item is generated, the same goes
 * for read and writes. As long as there are queued items, the connection/read/write cycle will
 * continue until all queued items are processed.
 */
public interface OperationsProcessor {

    /**
     * Passes connectWithDevice with provided DetectedBluetoothDevice.
     *
     * @param device DetectedBluetoothDevice
     * @return Observable<BluetoothConnectionResult>
     */
    Observable<BluetoothConnectionResult> connectWithDevice(DetectedBluetoothDevice device);

    /**
     * Passes discoverServices on the provided ConnectedBluetoothDevice.
     *
     * @param device ConnectedBluetoothDevice
     * @return Observable<BluetoothConnectionResult>
     */
    Observable<BluetoothConnectionResult> discoverServices(ConnectedBluetoothDevice device);

    /**
     * Passes the disconnectFromDevice for the ConnectedBluetoothDevice.
     *
     * @param connectedDevice ConnectedBluetoothDevice
     * @return Observable<BluetoothConnectionResult>
     */
    Observable<BluetoothConnectionResult> disconnectFromDevice(ConnectedBluetoothDevice connectedDevice);

    /**
     * Passes perform read.
     *
     * @param connection         ConnectedBluetoothDevice
     * @param serviceUuid        UUID serviceUuid
     * @param characteristicUuid UUID characteristicUuid
     * @return Observable<BluetoothIOResult>
     */
    Observable<BluetoothIOResult> read(ConnectedBluetoothDevice connection, UUID serviceUuid, UUID characteristicUuid);

    /**
     * Passes perform write.
     *
     * @param connection         ConnectedBluetoothDevice
     * @param serviceUuid        UUID serviceUuid
     * @param characteristicUuid UUID characteristicUuid
     * @param value              byte[] value to update
     * @return Observable<BluetoothIOResult>
     */
    Observable<BluetoothIOResult> write(ConnectedBluetoothDevice connection, UUID serviceUuid, UUID characteristicUuid, byte[] value);
}
