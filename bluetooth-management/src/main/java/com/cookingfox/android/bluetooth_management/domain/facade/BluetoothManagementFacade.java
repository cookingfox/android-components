package com.cookingfox.android.bluetooth_management.domain.facade;

import com.cookingfox.android.bluetooth_management.domain.state.BluetoothManagementState;
import com.cookingfox.android.bluetooth_management.domain.state.BluetoothManagementStateObserver;
import com.cookingfox.lapasse.api.facade.RxFacade;
import com.cookingfox.lapasse.impl.facade.LaPasseRxFacadeDelegate;

/**
 * LaPasse facade delegate for bluetooth management state.
 */
public class BluetoothManagementFacade
        extends LaPasseRxFacadeDelegate<BluetoothManagementState>
        implements BluetoothManagementStateObserver {

    public BluetoothManagementFacade(RxFacade<BluetoothManagementState> facade) {
        super(facade);
    }

}
