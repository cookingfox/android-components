package com.cookingfox.android.bluetooth_management.domain.preferences;

import com.cookingfox.android.prefer_rx.api.pref.RxPrefs;
import com.cookingfox.android.prefer_rx.api.pref.typed.BooleanRxPref;
import com.cookingfox.android.prefer_rx.api.pref.typed.IntegerRxPref;

/**
 * Preferences for the bluetooth management component.
 */
public interface BluetoothManagementPreferences
        extends RxPrefs<BluetoothManagementPreferences.Key> {

    enum Key {
        AutoEnableBluetoothAdapter,
        ScanningBufferPeriodDurationMs,
        ScanningPeriodDurationMs,
        ScanningTimeoutDurationMs
    }

    /**
     * @return Whether to automatically enable the bluetooth adapter when the component is loaded.
     */
    BooleanRxPref<Key> autoEnableBluetoothAdapter();

    /**
     * @return The amount of milliseconds for which scan results should be buffered.
     */
    IntegerRxPref<Key> scanningBufferPeriodDurationMs();

    /**
     * @return The amount of milliseconds the scanner should be active during the scan process.
     */
    IntegerRxPref<Key> scanningPeriodDurationMs();

    /**
     * @return The amount of milliseconds the scanner should be inactive during the scan process.
     */
    IntegerRxPref<Key> scanningTimeoutDurationMs();

}
