package com.cookingfox.android.bluetooth_management.domain.vo.connection;

import com.cookingfox.android.bluetooth_management.domain.connection.BluetoothConnection;
import com.cookingfox.android.bluetooth_management.domain.device.DetectedBluetoothDevice;
import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;

import org.immutables.value.Value;

@DefaultValueStyle
@Value.Immutable
public interface BluetoothConnectionResult {

    BluetoothConnection connection();

    DetectedBluetoothDevice detectedDevice();

    String macAddress();

    BluetoothConnectionState state();

    class Builder extends ImmutableBluetoothConnectionResult.Builder {
    }
}