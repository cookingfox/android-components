package com.cookingfox.android.bluetooth_management.domain.driver;

import com.cookingfox.android.bluetooth_management.domain.device.ConnectedBluetoothDevice;
import com.cookingfox.android.bluetooth_management.domain.device.DetectedBluetoothDevice;
import com.cookingfox.android.bluetooth_management.domain.operation.PendingRead;
import com.cookingfox.android.bluetooth_management.domain.operation.PendingWrite;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothConnectionResult;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothIOResult;

import rx.Observable;

/**
 * The Bluetooth Gatt Driver acts as a bridge to the native Android parts.
 */
public interface BluetoothGattDriver {

    /**
     * Connects with a provided detected device of type T.
     *
     * @param detectedDevice Explicit type of detected device
     * @param <T>            Explicit type extending DetectedBluetoothDevice
     * @return Explicit type extending DetectedBluetoothDevice
     */
    <T extends DetectedBluetoothDevice> Observable<BluetoothConnectionResult> connectWithDevice(T detectedDevice);

    /**
     * Discovers services for the provided connected target device.
     * <p>
     * If successful, result contains 'SERVICES_DISCOVERED'
     *
     * @param device ConnectedBluetoothDevice
     * @return Observable<BluetoothConnectionResult>
     */
    Observable<BluetoothConnectionResult> discoverServices(ConnectedBluetoothDevice device);

    /**
     * Disconnects the provided connected target device.
     * <p>
     * If successful, result contains state 'DISCONNECTED'
     *
     * @param connectedDevice ConnectedBluetoothDevice
     * @return Observable<BluetoothConnectionResult>
     */
    Observable<BluetoothConnectionResult> disconnectFromDevice(ConnectedBluetoothDevice connectedDevice);

    /**
     * Performs a read from a PendingRead object.
     * <p>
     * The driver always gets pending reads, for they're not executed at this moment.
     *
     * @param read PendingRead
     * @return Observable<BluetoothIOResult>
     */
    Observable<BluetoothIOResult> read(PendingRead read);

    /**
     * Performs a write from a PendingWrite object.
     * <p>
     * The driver always gets pending writes, for they're not executed at this moment.
     *
     * @param write PendingWrite
     * @return Observable<BluetoothIOResult>
     */
    Observable<BluetoothIOResult> write(PendingWrite write);
}
