package com.cookingfox.android.bluetooth_management.domain.vo.connection;

import com.cookingfox.android.bluetooth_management.domain.operation.OperationIdentifier;
import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;

import org.immutables.value.Value;

@DefaultValueStyle
@Value.Immutable
public interface BluetoothIOResult {

    OperationIdentifier identifier();

    byte[] rawValue();

    class Builder extends ImmutableBluetoothIOResult.Builder {
    }
}