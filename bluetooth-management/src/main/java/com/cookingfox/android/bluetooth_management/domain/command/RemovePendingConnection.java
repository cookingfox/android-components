package com.cookingfox.android.bluetooth_management.domain.command;

import com.cookingfox.android.bluetooth_management.domain.operation.PendingConnection;
import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;
import com.cookingfox.lapasse.api.command.Command;

import org.immutables.value.Value;

@DefaultValueStyle
@Value.Immutable
public interface RemovePendingConnection extends Command {

    PendingConnection pendingConnection();

    class Builder extends ImmutableRemovePendingConnection.Builder {
    }
}