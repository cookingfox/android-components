package com.cookingfox.android.bluetooth_management.domain.event;

import com.cookingfox.android.bluetooth_management.domain.connection.BluetoothConnection;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothConnectionResult;
import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;
import com.cookingfox.lapasse.api.event.Event;

import org.immutables.value.Value;

import java.util.List;
import java.util.Map;

import rx.subscriptions.CompositeSubscription;

/**
 * Event containing information about Established connection(s)
 */
@DefaultValueStyle
@Value.Immutable
public interface EstablishedConnectionsUpdated extends Event {

    /**
     * Updated list of connections.
     *
     * @return List of BluetoothConnection
     */
    List<BluetoothConnection> connections();

    /**
     * Contains information about target device.
     *
     * @return BluetoothConnectionResult of connected/disconnected target device.
     */
    BluetoothConnectionResult connectionResult();

    // TODO: 8/25/16 Joep: comment
    Map<BluetoothConnection, CompositeSubscription> connectionSubscriptions();

    class Builder extends ImmutableEstablishedConnectionsUpdated.Builder {
    }
}
