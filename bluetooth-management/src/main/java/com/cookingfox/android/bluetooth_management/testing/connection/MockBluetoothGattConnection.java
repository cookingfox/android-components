package com.cookingfox.android.bluetooth_management.testing.connection;

import com.cookingfox.android.bluetooth_management.android.connection.GattConnection;
import com.cookingfox.android.bluetooth_management.domain.connection.BluetoothConnection;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothConnectionResult;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothIOResult;
import com.cookingfox.logging.Logger;

import java.util.UUID;

import rx.Observable;
import rx.subjects.PublishSubject;

public class MockBluetoothGattConnection implements GattConnection, BluetoothConnection {

    //    public PublishSubject<BluetoothConnectionResult> onConnectionStateChange = PublishSubject.create();
    public PublishSubject<BluetoothConnectionResult> onConnectionResult = PublishSubject.create();
    public PublishSubject<BluetoothIOResult> onReadResult = PublishSubject.create();
    public PublishSubject<BluetoothIOResult> onWriteResult = PublishSubject.create();
    public PublishSubject<BluetoothConnectionResult> onDiscoverServicesResult = PublishSubject.create();

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public Observable<BluetoothConnectionResult> disconnect() {
        Logger.info("Mock Connection Disconnecting...");
        return onConnectionResult;
    }

    @Override
    public Observable<BluetoothConnectionResult> observeConnectionStateChanges() {
        Logger.info("Mock Observing Connection State.");
        return onConnectionResult;
    }

    @Override
    public Observable<BluetoothConnectionResult> observeServicesDiscovered() {
        return null;
    }

    @Override
    public Observable<BluetoothIOResult> observeReadResults() {
        return onReadResult;
    }

    @Override
    public Observable<BluetoothIOResult> observeWriteResults() {
        return onWriteResult;
    }

    @Override
    public PublishSubject<BluetoothConnectionResult> connect() {
        return onConnectionResult;
    }

    @Override
    public Observable<BluetoothConnectionResult> discoverServices() {
        return onDiscoverServicesResult;
    }

    @Override
    public PublishSubject<BluetoothIOResult> read(UUID serviceUuid, UUID characteristicUuid) {
        return onReadResult;
    }

    @Override
    public PublishSubject<BluetoothIOResult> write(UUID serviceUuid, UUID characteristicUuid, byte[] value) {
        return onWriteResult;
    }
}
