package com.cookingfox.android.bluetooth_management.domain.command;

import com.cookingfox.android.bluetooth_management.domain.operation.PendingOperation;
import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;
import com.cookingfox.lapasse.api.command.Command;

import org.immutables.value.Value;

@DefaultValueStyle
@Value.Immutable
public interface AddPendingIoOperation extends Command {

    PendingOperation operation();

    class Builder extends ImmutableAddPendingIoOperation.Builder {
    }
}