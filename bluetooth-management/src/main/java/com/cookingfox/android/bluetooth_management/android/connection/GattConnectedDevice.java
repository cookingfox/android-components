package com.cookingfox.android.bluetooth_management.android.connection;

import com.cookingfox.android.bluetooth_management.domain.connection.BluetoothConnection;
import com.cookingfox.android.bluetooth_management.domain.device.ConnectedBluetoothDevice;
import com.cookingfox.android.bluetooth_management.domain.device.DetectedBluetoothDevice;

public class GattConnectedDevice implements ConnectedBluetoothDevice {

    private DetectedBluetoothDevice detectedDevice;
    private BluetoothConnection gattConnection;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public GattConnectedDevice(DetectedBluetoothDevice detectedDevice,
                               BluetoothConnection gattConnection) {
        this.detectedDevice = detectedDevice;
        this.gattConnection = gattConnection;
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public DetectedBluetoothDevice getDetectedDevice() {
        return detectedDevice;
    }

    @Override
    public BluetoothConnection getConnection() {
        return gattConnection;
    }

}
