package com.cookingfox.android.bluetooth_management.domain.command;

import com.cookingfox.android.bluetooth_management.domain.vo.scanning.BluetoothScanResult;
import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;
import com.cookingfox.lapasse.api.command.Command;

import org.immutables.value.Value;

import java.util.Map;

/**
 * Command to update bluetooth scan results.
 */
@DefaultValueStyle
@Value.Immutable
public interface UpdateBluetoothScanResults extends Command {

    Map<String, BluetoothScanResult> results();

    class Builder extends ImmutableUpdateBluetoothScanResults.Builder {
    }
}
