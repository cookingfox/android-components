package com.cookingfox.android.bluetooth_management.domain.vo.scanning;

import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;

import org.immutables.value.Value;

/**
 * Value Object for a bluetooth scan error.
 */
@DefaultValueStyle
@Value.Immutable
public interface BluetoothScanError {

    int errorCode();

    class Builder extends ImmutableBluetoothScanError.Builder {
    }
}
