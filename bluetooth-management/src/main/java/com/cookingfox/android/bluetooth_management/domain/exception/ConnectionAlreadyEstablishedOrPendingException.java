package com.cookingfox.android.bluetooth_management.domain.exception;

/**
 * Is thrown whenever a connection tries to be established with a device that already has a pending
 * connection or is already connected. Concurrent connections to a single device are not allowed.
 */
public class ConnectionAlreadyEstablishedOrPendingException extends RuntimeException {
}
