package com.cookingfox.android.bluetooth_management.domain.event;

import com.cookingfox.android.bluetooth_management.domain.vo.scanning.BluetoothScanResult;
import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;
import com.cookingfox.lapasse.api.event.Event;

import org.immutables.value.Value;

import java.util.Map;

/**
 * Event for when the bluetooth scan results have been updated.
 */
@DefaultValueStyle
@Value.Immutable
public interface BluetoothScanResultsUpdated extends Event {

    /**
     * @return Map of bluetooth scan results, where the key is MAC address of the detected device.
     */
    Map<String, BluetoothScanResult> results();

    class Builder extends ImmutableBluetoothScanResultsUpdated.Builder {
    }
}
