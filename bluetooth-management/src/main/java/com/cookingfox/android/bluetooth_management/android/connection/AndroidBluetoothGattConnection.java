package com.cookingfox.android.bluetooth_management.android.connection;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;

import com.cookingfox.android.bluetooth_management.domain.connection.BluetoothConnection;
import com.cookingfox.android.bluetooth_management.domain.device.DetectedBluetoothDevice;
import com.cookingfox.android.bluetooth_management.domain.exception.DiscoverServicesFailedException;
import com.cookingfox.android.bluetooth_management.domain.operation.OperationIdentifier;
import com.cookingfox.android.bluetooth_management.domain.operation.OperationType;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothConnectionResult;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothConnectionState;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothIOResult;
import com.cookingfox.android.core_component.api.android.context.AppContext;
import com.cookingfox.logging.Logger;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import rx.Observable;
import rx.subjects.PublishSubject;

import static com.cookingfox.guava_preconditions.Preconditions.checkNotNull;

public class AndroidBluetoothGattConnection extends BluetoothGattCallback implements GattConnection, BluetoothConnection {

    private final AppContext appContext;
    private final BluetoothAdapter bluetoothAdapter;
    private final PublishSubject<BluetoothConnectionResult> connectionResultPublisher = PublishSubject.create();
    private boolean connectionShouldClose = false;
    private final DetectedBluetoothDevice detectedDevice;
    private BluetoothGatt gatt;
    private final PublishSubject<BluetoothIOResult> readResultPublisher = PublishSubject.create();
    private List<BluetoothGattService> services = new LinkedList<>();
    private PublishSubject<BluetoothConnectionResult> servicesDiscoveredPublisher;
    private final PublishSubject<BluetoothIOResult> writeResultPublisher = PublishSubject.create();

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public AndroidBluetoothGattConnection(BluetoothAdapter bluetoothAdapter,
                                          AppContext appContext,
                                          DetectedBluetoothDevice detectedDevice) {
        this.bluetoothAdapter = bluetoothAdapter;
        this.appContext = appContext;
        this.detectedDevice = detectedDevice;
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public Observable<BluetoothConnectionResult> connect() {
        return Observable.defer(() -> {
            AndroidBluetoothGattConnection connection = this;
            String macAddress = detectedDevice.scanResult().macAddress();
            BluetoothDevice device = bluetoothAdapter.getRemoteDevice(macAddress);

            Logger.info("Connecting device %s", macAddress);

            gatt = device.connectGatt(appContext.getApplicationContext(), true, connection);

            return connectionResultPublisher;
        });
    }

    @Override
    public Observable<BluetoothConnectionResult> discoverServices() {
        return Observable.defer(() -> {
            Logger.info("Discovering Services...");

            gatt.discoverServices();

            return getServicesDiscoveredPublisher();
        });
    }

    /**
     * This disconnect method is never called by the component itself or Android. If the user calls
     * this method, the gatt connection will be closed on receiving the 'disconnected' callback.
     * <p>
     * If the device loses connection to the BLE peripheral Android automatically tries to
     * reconnect. The connection in that case may not be closed on receiving 'disconnected'
     * callback.
     *
     * @return BluetoothConnectionResult Observable
     */
    @Override
    public Observable<BluetoothConnectionResult> disconnect() {
        return Observable.defer(() -> {
            if (gatt == null) {
                Logger.warn("Gatt connection does not exist, device might never been connected!");
                return connectionResultPublisher;
            }

            Logger.info("Disconnecting device...");

            // Connection should close on disconnected callback
            connectionShouldClose = true;

            gatt.disconnect();

            return connectionResultPublisher;
        });
    }

    @Override
    public Observable<BluetoothIOResult> read(UUID serviceUuid, UUID characteristicUuid) {
        return Observable.defer(() -> {
            Logger.info("DO READ: %s", characteristicUuid);

            BluetoothGattCharacteristic characteristic = findCharacteristic(serviceUuid, characteristicUuid);
            checkNotNull(characteristic);

            gatt.readCharacteristic(characteristic);

            return readResultPublisher;
        });
    }

    @Override
    public Observable<BluetoothIOResult> write(UUID serviceUuid, UUID characteristicUuid, byte[] value) {
        return Observable.defer(() -> {
            Logger.info("DO WRITE: %s", characteristicUuid);

            BluetoothGattCharacteristic characteristic = findCharacteristic(serviceUuid, characteristicUuid);
            checkNotNull(characteristic);

            gatt.setCharacteristicNotification(characteristic, true);
            characteristic.setValue(value);
            gatt.writeCharacteristic(characteristic);

            return writeResultPublisher;
        });
    }

    //----------------------------------------------------------------------------------------------
    // PRIVATE METHODS
    //----------------------------------------------------------------------------------------------

    /**
     * Completes Publish Subjects
     */
    private void dispose() {
        getServicesDiscoveredPublisher().onCompleted();
        connectionResultPublisher.onCompleted();
        readResultPublisher.onCompleted();
        writeResultPublisher.onCompleted();
    }

    /**
     * Searches the found characteristics for any matches. If found, a characteristic is returned.
     * <p>
     * Always call this method after services are discovered, otherwise it will return 'null'.
     *
     * @param serviceUuid        target service UUID
     * @param characteristicUuid target characteristic UUID
     * @return BluetoothGattCharacteristic
     */
    private BluetoothGattCharacteristic findCharacteristic(UUID serviceUuid, UUID characteristicUuid) {
        for (BluetoothGattService service : services) {
            if (!service.getUuid().equals(serviceUuid)) continue;
            for (BluetoothGattCharacteristic characteristic : service.getCharacteristics()) {
                if (!characteristic.getUuid().equals(characteristicUuid)) continue;
                return characteristic;
            }
        }
        return null;
    }

    /**
     * @return Returns the services discovered publisher, and creates it if it doesn't exist or has
     * terminated.
     */
    private PublishSubject<BluetoothConnectionResult> getServicesDiscoveredPublisher() {
        if (servicesDiscoveredPublisher == null ||
                servicesDiscoveredPublisher.hasCompleted() ||
                servicesDiscoveredPublisher.hasThrowable()) {
            servicesDiscoveredPublisher = PublishSubject.create();
        }

        return servicesDiscoveredPublisher;
    }

    /**
     * Returns whether the provided status indicates a successful GATT operation.
     *
     * @param status The GATT operation status.
     * @return Whether the provided status indicates a successful GATT operation.
     */
    private boolean isGattSuccess(int status) {
        return status == BluetoothGatt.GATT_SUCCESS;
    }

    //----------------------------------------------------------------------------------------------
    // GETTER METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public Observable<BluetoothConnectionResult> observeConnectionStateChanges() {
        return Observable.defer(() -> connectionResultPublisher);
    }

    @Override
    public Observable<BluetoothConnectionResult> observeServicesDiscovered() {
        return Observable.defer(this::getServicesDiscoveredPublisher);
    }

    @Override
    public Observable<BluetoothIOResult> observeReadResults() {
        return readResultPublisher;
    }

    @Override
    public Observable<BluetoothIOResult> observeWriteResults() {
        return writeResultPublisher;
    }

    //----------------------------------------------------------------------------------------------
    // CALLBACK METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
        super.onConnectionStateChange(gatt, status, newState);

        BluetoothConnectionState connectionState = null;

        switch (newState) {
            case BluetoothProfile.STATE_CONNECTING:
                connectionState = BluetoothConnectionState.CONNECTING;
                Logger.info("Connection State Changed: CONNECTING | Status: %s", status);
                break;
            case BluetoothProfile.STATE_CONNECTED:
                connectionState = BluetoothConnectionState.CONNECTED;
                Logger.info("Connection State Changed: CONNECTED | Status: %s", status);
                break;
            case BluetoothProfile.STATE_DISCONNECTING:
                connectionState = BluetoothConnectionState.DISCONNECTING;
                Logger.info("Connection State Changed: DISCONNECTING | Status: %s", status);
                break;
            case BluetoothProfile.STATE_DISCONNECTED:
                connectionState = BluetoothConnectionState.DISCONNECTED;
                Logger.info("Connection State Changed: DISCONNECTED | Status: %s", status);

                // TODO: 8/25/16 This is a temporary workaround related to an issue where on Lollipop you can't call 'close()' right after 'disconnect()'.
                // Issue reported to Google by various devs. http://stackoverflow.com/a/33760271
                if (connectionShouldClose) {
                    this.gatt.close();
                    connectionShouldClose = false;
                }

                break;
        }

        if (connectionState == null) {
            Logger.warn("Connection state is unknown: %s", newState);
            return;
        }

        connectionResultPublisher.onNext(new BluetoothConnectionResult.Builder()
                .connection(this)
                .detectedDevice(detectedDevice)
                .macAddress(gatt.getDevice().getAddress())
                .state(connectionState)
                .build());

        if (connectionState.equals(BluetoothConnectionState.DISCONNECTED)) {
            dispose();
        }
    }

    @Override
    public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
        Logger.info("ON CHARACTERISTIC READ");

        // Sometimes we see a Nullpointer Exception on 'characteristic.getValue()'.
        if (characteristic.getValue() == null) {
            Logger.error("'characteristic.getValue() is NULL!'");
            return;
        }

        OperationIdentifier readIdentifier = new OperationIdentifier.Builder()
                .serviceUuid(characteristic.getService().getUuid())
                .macAddress(gatt.getDevice().getAddress())
                .characteristicUuid(characteristic.getUuid())
                .type(OperationType.READ)
                .build();

        readResultPublisher.onNext(new BluetoothIOResult.Builder()
                .identifier(readIdentifier)
                .rawValue(characteristic.getValue())
                .build());
    }

    @Override
    public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
        Logger.info("ON CHARACTERISTIC WRITE");

        OperationIdentifier writeIdentifier = new OperationIdentifier.Builder()
                .serviceUuid(characteristic.getService().getUuid())
                .macAddress(gatt.getDevice().getAddress())
                .characteristicUuid(characteristic.getUuid())
                .type(OperationType.WRITE)
                .build();

        writeResultPublisher.onNext(new BluetoothIOResult.Builder()
                .identifier(writeIdentifier)
                .rawValue(characteristic.getValue())
                .build());
    }

    @Override
    public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
    }

    @Override
    public void onServicesDiscovered(BluetoothGatt gatt, int status) {
        int numServices = gatt.getServices().size();
        String macAddress = detectedDevice.scanResult().macAddress();

        Logger.info("Services Discovered: %s services were found.", numServices);

        if (!isGattSuccess(status) || numServices == 0) {
            getServicesDiscoveredPublisher()
                    .onError(new DiscoverServicesFailedException(macAddress));

            return;
        }

        services = gatt.getServices();

        getServicesDiscoveredPublisher().onNext(new BluetoothConnectionResult.Builder()
                .connection(this)
                .detectedDevice(detectedDevice)
                .macAddress(macAddress)
                .state(BluetoothConnectionState.SERVICES_DISCOVERED)
                .build());
    }

}
