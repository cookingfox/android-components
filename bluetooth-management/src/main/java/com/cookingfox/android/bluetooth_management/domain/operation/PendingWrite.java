package com.cookingfox.android.bluetooth_management.domain.operation;

import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;

import org.immutables.value.Value;

@DefaultValueStyle
@Value.Immutable
public interface PendingWrite extends PendingOperation {

    byte[] value();

    class Builder extends ImmutablePendingWrite.Builder {
    }
}