package com.cookingfox.android.bluetooth_management.testing.driver;

import com.cookingfox.android.bluetooth_management.domain.device.ConnectedBluetoothDevice;
import com.cookingfox.android.bluetooth_management.domain.device.DetectedBluetoothDevice;
import com.cookingfox.android.bluetooth_management.domain.driver.BluetoothGattDriver;
import com.cookingfox.android.bluetooth_management.domain.operation.PendingRead;
import com.cookingfox.android.bluetooth_management.domain.operation.PendingWrite;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothConnectionResult;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothConnectionState;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothIOResult;
import com.cookingfox.android.bluetooth_management.testing.connection.MockBluetoothGattConnection;
import com.cookingfox.logging.Logger;

import rx.Observable;
import rx.subjects.PublishSubject;

public class MockBluetoothGattDriver implements BluetoothGattDriver {

    public PublishSubject<BluetoothConnectionResult> connectionPublisher = PublishSubject.create();
    public PublishSubject<BluetoothConnectionResult> discoverServicesPublisher = PublishSubject.create();
    public PublishSubject<BluetoothIOResult> readPublisher = PublishSubject.create();
    public PublishSubject<BluetoothIOResult> writePublisher = PublishSubject.create();

    @Override
    public <T extends DetectedBluetoothDevice> Observable<BluetoothConnectionResult> connectWithDevice(T detectedDevice) {
        Logger.info("Mock Driver Connecting...");
        return connectionPublisher;
    }

    @Override
    public Observable<BluetoothConnectionResult> discoverServices(ConnectedBluetoothDevice device) {
        return discoverServicesPublisher;
    }

    @Override
    public Observable<BluetoothConnectionResult> disconnectFromDevice(ConnectedBluetoothDevice connectedDevice) {
        Logger.info("Mock Driver Disconnecting...");
        return connectionPublisher;
    }

    @Override
    public Observable<BluetoothIOResult> read(PendingRead read) {
        return readPublisher;
    }

    @Override
    public Observable<BluetoothIOResult> write(PendingWrite write) {
        return writePublisher;
    }

    public void performConnect(DetectedBluetoothDevice device, MockBluetoothGattConnection connection) {
        connectionPublisher.onNext(new BluetoothConnectionResult.Builder()
                .detectedDevice(device)
                .connection(connection)
                .macAddress(device.scanResult().macAddress())
                .state(BluetoothConnectionState.CONNECTED)
                .build());
    }

    public void performDisconnect(DetectedBluetoothDevice device, MockBluetoothGattConnection connection) {
        connectionPublisher.onNext(new BluetoothConnectionResult.Builder()
                .detectedDevice(device)
                .connection(connection)
                .macAddress(device.scanResult().macAddress())
                .state(BluetoothConnectionState.DISCONNECTED)
                .build());
    }

    public void performDiscoverServices(DetectedBluetoothDevice device, MockBluetoothGattConnection connection) {
        discoverServicesPublisher.onNext(new BluetoothConnectionResult.Builder()
                .detectedDevice(device)
                .connection(connection)
                .macAddress(device.scanResult().macAddress())
                .state(BluetoothConnectionState.SERVICES_DISCOVERED)
                .build());
    }
}
