package com.cookingfox.android.bluetooth_management.domain.operation;

import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;

import org.immutables.value.Value;

import java.util.UUID;

/**
 * Identifies a read/write operation for a specific device.
 * <p>
 * In order to process stored pending operations, the identifier is used to find corresponding
 * operations for incoming BluetoothIoResults. A single device (read: peripheral) might have
 * multiple pending read/write operations, each with specific instructions on what to do.
 */
@DefaultValueStyle
@Value.Immutable
public interface OperationIdentifier {

    /**
     * @return Characteristic UUID of the target device.
     */
    UUID characteristicUuid();

    /**
     * @return String MAC address of the device where the operation is for.
     */
    String macAddress();

    /**
     * @return Service UUID of the target device.
     */
    UUID serviceUuid();

    /**
     * An operation can be of type READ or WRITE.
     *
     * @return OperationType
     */
    OperationType type();

    class Builder extends ImmutableOperationIdentifier.Builder {
    }
}