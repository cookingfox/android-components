package com.cookingfox.android.bluetooth_management.domain.driver;

import com.cookingfox.android.bluetooth_management.domain.api.BluetoothScanningAware;
import com.cookingfox.android.bluetooth_management.domain.vo.scanning.BluetoothScanError;
import com.cookingfox.android.bluetooth_management.domain.vo.scanning.BluetoothScanResult;

import rx.Observable;

/**
 * Interface for Bluetooth scanner driver.
 */
public interface BluetoothScannerDriver extends BluetoothScanningAware {

    /**
     * @return Whether the scanner is currently active.
     */
    boolean isScanning();

    /**
     * @return Observable for scan errors.
     */
    Observable<BluetoothScanError> observeScanErrors();

    /**
     * @return Observable for scan results.
     */
    Observable<BluetoothScanResult> observeScanResults();

    /**
     * Sets the amount of milliseconds the scanner should be active during the scan process.
     *
     * @param scanningPeriodDurationMs The amount of milliseconds the scanner should be active
     *                                 during the scan process.
     */
    void setScanningPeriodDurationMs(int scanningPeriodDurationMs);

    /**
     * Sets the amount of milliseconds the scanner should be inactive during the scan process.
     *
     * @param scanningTimeoutDurationMs The amount of milliseconds the scanner should be inactive
     *                                  during the scan process.
     */
    void setScanningTimeoutDurationMs(int scanningTimeoutDurationMs);

}
