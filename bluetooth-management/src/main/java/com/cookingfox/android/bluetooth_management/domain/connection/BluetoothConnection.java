package com.cookingfox.android.bluetooth_management.domain.connection;

import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothConnectionResult;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothIOResult;

import rx.Observable;

public interface BluetoothConnection {

    /**
     * Disconnects the connection.
     * <p>
     * After calling this method, the connection object will be disposed.
     *
     * @return Observable<BluetoothConnectionResult> containing state DISCONNECTED
     */
    Observable<BluetoothConnectionResult> disconnect();

    /**
     * Discovers the BLE peripheral's services.
     * <p>
     * Only call this method after a connection is successfully established.
     *
     * @return Observable<BluetoothConnectionResult> containing state SERVICES_DISCOVERED
     */
    Observable<BluetoothConnectionResult> discoverServices();

    /**
     * Provides an observable for monitoring a connection's state changes.
     *
     * @return Observable<BluetoothConnectionResult>
     */
    Observable<BluetoothConnectionResult> observeConnectionStateChanges();

    /**
     * Provides an observable for monitoring when services are discovered.
     *
     * @return Observable<BluetoothConnectionResult>
     */
    Observable<BluetoothConnectionResult> observeServicesDiscovered();

    /**
     * Observe connection's read results.
     *
     * @return Observable<BluetoothIOResult>
     */
    Observable<BluetoothIOResult> observeReadResults();

    /**
     * Observe connection's write results.
     *
     * @return Observable<BluetoothIOResult>
     */
    Observable<BluetoothIOResult> observeWriteResults();

}
