package com.cookingfox.android.bluetooth_management.domain.vo;

import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;

import org.immutables.value.Value;

/**
 * Value Object with information about the bluetooth adapter.
 */
@DefaultValueStyle
@Value.Immutable
public interface BluetoothAdapterInfo {

    boolean isEnabled();

    boolean isTurningOn();

    boolean isOn();

    boolean isTurningOff();

    boolean isOff();

    class Builder extends ImmutableBluetoothAdapterInfo.Builder {
    }

}
