package com.cookingfox.android.bluetooth_management.testing.driver;

import com.cookingfox.android.bluetooth_management.domain.driver.BluetoothScannerDriver;
import com.cookingfox.android.bluetooth_management.domain.vo.scanning.BluetoothScanError;
import com.cookingfox.android.bluetooth_management.domain.vo.scanning.BluetoothScanResult;

import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Mock implementation of {@link BluetoothScannerDriver}.
 */
public class MockBluetoothScannerDriver implements BluetoothScannerDriver {

    public boolean isScanning = false;
    public final PublishSubject<BluetoothScanError> scanErrors = PublishSubject.create();
    public final PublishSubject<BluetoothScanResult> scanResults = PublishSubject.create();

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public boolean isScanning() {
        return isScanning;
    }

    @Override
    public Observable<BluetoothScanError> observeScanErrors() {
        return scanErrors;
    }

    @Override
    public Observable<BluetoothScanResult> observeScanResults() {
        return scanResults;
    }

    @Override
    public void setScanningPeriodDurationMs(int scanningPeriodDurationMs) {
        //Stub
    }

    @Override
    public void setScanningTimeoutDurationMs(int scanningTimeoutDurationMs) {
        //Stub
    }

    @Override
    public void startScanning() {
        isScanning = true;
    }

    @Override
    public void stopScanning() {
        isScanning = false;
    }

    //----------------------------------------------------------------------------------------------
    // MOCK ACTIONS
    //----------------------------------------------------------------------------------------------

    /**
     * Generates a mock scan result.
     *
     * @param mac  Mac address.
     * @param rssi Rssi value.
     * @return BluetoothScanResult
     */
    public BluetoothScanResult generateMockScanResult(String mac, final Integer rssi, byte[] scanRecord) {
        return new BluetoothScanResult() {
            @Override
            public String macAddress() {
                return mac;
            }

            @Override
            public Integer signalStrength() {
                return rssi;
            }

            @Override
            public byte[] scanRecord() {
                return scanRecord;
            }
        };
    }
}
