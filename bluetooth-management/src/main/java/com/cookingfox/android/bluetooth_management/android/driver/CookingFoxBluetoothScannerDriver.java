package com.cookingfox.android.bluetooth_management.android.driver;

import android.os.Handler;
import android.os.HandlerThread;

import com.cookingfox.android.bluetooth_management.android.scanner.BluetoothScanner;
import com.cookingfox.android.bluetooth_management.domain.driver.BluetoothScannerDriver;
import com.cookingfox.android.bluetooth_management.domain.vo.scanning.BluetoothScanError;
import com.cookingfox.android.bluetooth_management.domain.vo.scanning.BluetoothScanResult;
import com.cookingfox.chefling.api.CheflingLifecycle;

import rx.Observable;

/**
 * Implementation of bluetooth scanner driver which uses a {@link Handler} to control the start /
 * stop scanning process.
 */
public class CookingFoxBluetoothScannerDriver implements BluetoothScannerDriver, CheflingLifecycle {

    protected final BluetoothScanner bluetoothScanner;
    protected final HandlerThread handlerThread = new HandlerThread(getClass().getSimpleName());
    protected boolean isScannerRunning = false;
    protected Handler scanHandler;
    protected int scanningPeriodDurationMs;
    protected int scanningTimeoutDurationMs;
    protected boolean startStopIsScanning = false;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public CookingFoxBluetoothScannerDriver(BluetoothScanner bluetoothScanner) {
        this.bluetoothScanner = bluetoothScanner;
    }

    //----------------------------------------------------------------------------------------------
    // LIFECYCLE
    //----------------------------------------------------------------------------------------------

    @Override
    public void initialize() {
        // no-op
    }

    @Override
    public void dispose() {
        stopScanning();

        handlerThread.quit();
        scanHandler = null;
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public boolean isScanning() {
        return isScannerRunning;
    }

    @Override
    public Observable<BluetoothScanError> observeScanErrors() {
        return bluetoothScanner.observeScanErrors();
    }

    @Override
    public Observable<BluetoothScanResult> observeScanResults() {
        return bluetoothScanner.observeScanResults();
    }

    @Override
    public void setScanningPeriodDurationMs(int scanningPeriodDurationMs) {
        this.scanningPeriodDurationMs = scanningPeriodDurationMs;
    }

    @Override
    public void setScanningTimeoutDurationMs(int scanningTimeoutDurationMs) {
        this.scanningTimeoutDurationMs = scanningTimeoutDurationMs;
    }

    @Override
    public void startScanning() {
        // already running: skip
        if (isScanning()) {
            return;
        }

        isScannerRunning = true;
        startStopIsScanning = false;

        if (scanHandler == null) {
            handlerThread.start();

            scanHandler = new Handler(handlerThread.getLooper());
        }

        scanHandler.post(scanHandlerRunnable);
    }

    @Override
    public void stopScanning() {
        // not running: skip
        if (!isScanning()) {
            return;
        }

        // first set to false, so runnable can respond immediately
        isScannerRunning = false;

        // set to true, so runnable will stop IF it reaches the check
        startStopIsScanning = true;

        // cancel runnable
        scanHandler.removeCallbacks(scanHandlerRunnable);

        // actually stop scanning
        bluetoothScanner.stopScanning();

        // set to false after stop
        startStopIsScanning = true;
    }

    //----------------------------------------------------------------------------------------------
    // RUNNABLE IMPLEMENTATION
    //----------------------------------------------------------------------------------------------

    /**
     * Scan handler runnable, which controls the actual starting and stopping of the scan process.
     */
    protected final Runnable scanHandlerRunnable = new Runnable() {
        @Override
        public void run() {
            // request to stop scanning
            if (!isScanning()) {
                return;
            }

            if (startStopIsScanning) {
                bluetoothScanner.stopScanning();

                // re-execute this runnable after a delay
                scanHandler.postDelayed(this, scanningTimeoutDurationMs);
            } else {
                bluetoothScanner.startScanning();

                // re-execute this runnable after a delay
                scanHandler.postDelayed(this, scanningPeriodDurationMs);
            }

            startStopIsScanning = !startStopIsScanning;
        }
    };

}
