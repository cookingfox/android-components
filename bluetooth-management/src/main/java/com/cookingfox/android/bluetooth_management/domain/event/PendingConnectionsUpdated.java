package com.cookingfox.android.bluetooth_management.domain.event;

import com.cookingfox.android.bluetooth_management.domain.operation.PendingConnection;
import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;
import com.cookingfox.lapasse.api.event.Event;

import org.immutables.value.Value;

import java.util.List;

@DefaultValueStyle
@Value.Immutable
public interface PendingConnectionsUpdated extends Event {

    List<PendingConnection> connections();

    class Builder extends ImmutablePendingConnectionsUpdated.Builder {
    }
}
