package com.cookingfox.android.bluetooth_management.android.scanner;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.os.Build;

@TargetApi(Build.VERSION_CODES.KITKAT)
public class KitKatBluetoothScanner extends AbstractBluetoothScanner {

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public KitKatBluetoothScanner(BluetoothAdapter bluetoothAdapter) {
        super(bluetoothAdapter);
    }

    //----------------------------------------------------------------------------------------------
    // PROTECTED METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    protected void safeStartScanning() {
        if (!bluetoothAdapter.startLeScan(scanCallback)) {
            publishScanError(0);
        }
    }

    @Override
    protected void safeStopScanning() {
        bluetoothAdapter.stopLeScan(scanCallback);
    }

    //----------------------------------------------------------------------------------------------
    // SCAN CALLBACK
    //----------------------------------------------------------------------------------------------

    /**
     * Bluetooth Low Energy scan callback implementation.
     */
    protected final BluetoothAdapter.LeScanCallback scanCallback = (device, rssi, scanRecord) ->
            publishScanResult(device.getAddress(), rssi, scanRecord);

}
