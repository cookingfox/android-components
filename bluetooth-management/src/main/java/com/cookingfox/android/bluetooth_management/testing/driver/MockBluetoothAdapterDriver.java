package com.cookingfox.android.bluetooth_management.testing.driver;

import com.cookingfox.android.bluetooth_management.domain.driver.BluetoothAdapterDriver;
import com.cookingfox.android.bluetooth_management.domain.vo.BluetoothAdapterInfo;

import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Mock implementation of bluetooth adapter driver, which can be used in unit tests.
 */
public class MockBluetoothAdapterDriver implements BluetoothAdapterDriver {

    public boolean mock_adapterIsEnabled = false;
    public boolean mock_adapterIsDisabled = false;
    public boolean mock_adapterIsTurningOn = false;
    public boolean mock_adapterIsTurningOff = false;

    private final PublishSubject<BluetoothAdapterInfo> bluetoothStateChangeSubject = PublishSubject.create();

    //----------------------------------------------------------------------------------------------
    // OVERIDDEN METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public void disableAdapter() {
        mock_adapterIsEnabled = false;
    }

    @Override
    public void enableAdapter() {
        mock_adapterIsEnabled = true;
    }

    @Override
    public BluetoothAdapterInfo getBluetoothAdapterInfo() {
        return new BluetoothAdapterInfo.Builder()
                .isEnabled(mock_adapterIsEnabled)
                .isOn(isAdapterOn())
                .isOff(isAdapterOff())
                .isTurningOn(isAdapterTurningOn())
                .isTurningOff(isAdapterTurningOff())
                .build();
    }

    @Override
    public boolean isAdapterEnabled() {
        return mock_adapterIsEnabled;
    }

    @Override
    public boolean isAdapterOn() {
        return mock_adapterIsEnabled;
    }

    @Override
    public boolean isAdapterOff() {
        return mock_adapterIsDisabled;
    }

    @Override
    public boolean isAdapterTurningOn() {
        return mock_adapterIsTurningOn;
    }

    @Override
    public boolean isAdapterTurningOff() {
        return mock_adapterIsTurningOff;
    }

    @Override
    public Observable<BluetoothAdapterInfo> observeBluetoothStateChanges() {
        return bluetoothStateChangeSubject;
    }
}
