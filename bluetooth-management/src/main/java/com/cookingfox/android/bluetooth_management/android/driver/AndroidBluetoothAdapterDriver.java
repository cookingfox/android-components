package com.cookingfox.android.bluetooth_management.android.driver;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.cookingfox.android.bluetooth_management.domain.driver.BluetoothAdapterDriver;
import com.cookingfox.android.bluetooth_management.domain.vo.BluetoothAdapterInfo;
import com.cookingfox.android.core_component.api.android.context.AppContext;
import com.cookingfox.chefling.api.CheflingLifecycle;
import com.cookingfox.logging.Logger;

import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Android implementation of bluetooth adapter driver.
 */
public class AndroidBluetoothAdapterDriver implements BluetoothAdapterDriver, CheflingLifecycle {

    //----------------------------------------------------------------------------------------------
    // CONSTANTS
    //----------------------------------------------------------------------------------------------

    public static final String INTENT_ACTION = BluetoothAdapter.ACTION_STATE_CHANGED;

    //----------------------------------------------------------------------------------------------
    // PROPERTIES
    //----------------------------------------------------------------------------------------------

    private final AppContext appContext;
    private final BluetoothAdapter bluetoothAdapter;
    private final PublishSubject<BluetoothAdapterInfo> bluetoothStateChangeSubject = PublishSubject.create();

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public AndroidBluetoothAdapterDriver(AppContext appContext,
                                         BluetoothAdapter bluetoothAdapter) {
        this.appContext = appContext;
        this.bluetoothAdapter = bluetoothAdapter;
    }

    //----------------------------------------------------------------------------------------------
    // LIFECYCLE
    //----------------------------------------------------------------------------------------------

    @Override
    public void initialize() {
        appContext.registerReceiver(bluetoothAdapterBroadcastReceiver, new IntentFilter(INTENT_ACTION));
    }

    @Override
    public void dispose() {
        appContext.unregisterReceiver(bluetoothAdapterBroadcastReceiver);
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    /**
     * Disables the adapter.
     */
    @Override
    public void disableAdapter() {
        bluetoothAdapter.disable();
    }

    /**
     * Enables the adapter.
     */
    @Override
    public void enableAdapter() {
        bluetoothAdapter.enable();
    }

    /**
     * Constructs the BluetoothAdapterInfo from accurate device state.
     *
     * @return BluetoothAdapterInfo containing adapter state information
     */
    @Override
    public BluetoothAdapterInfo getBluetoothAdapterInfo() {
        return new BluetoothAdapterInfo.Builder()
                .isEnabled(isAdapterEnabled())
                .isOn(isAdapterOn())
                .isOff(isAdapterOff())
                .isTurningOn(isAdapterTurningOn())
                .isTurningOff(isAdapterTurningOff())
                .build();
    }

    @Override
    public boolean isAdapterEnabled() {
        return bluetoothAdapter.isEnabled();
    }

    @Override
    public boolean isAdapterOn() {
        return (bluetoothAdapter.getState() == BluetoothAdapter.STATE_ON);
    }

    @Override
    public boolean isAdapterOff() {
        return (bluetoothAdapter.getState() == BluetoothAdapter.STATE_OFF);
    }

    @Override
    public boolean isAdapterTurningOn() {
        return (bluetoothAdapter.getState() == BluetoothAdapter.STATE_TURNING_ON);
    }

    @Override
    public boolean isAdapterTurningOff() {
        return (bluetoothAdapter.getState() == BluetoothAdapter.STATE_TURNING_OFF);
    }

    @Override
    public Observable<BluetoothAdapterInfo> observeBluetoothStateChanges() {
        return bluetoothStateChangeSubject;
    }

    //----------------------------------------------------------------------------------------------
    // PROTECTED METHODS
    //----------------------------------------------------------------------------------------------

    /**
     * Monitors changes to the bluetooth adapter.
     */
    protected final BroadcastReceiver bluetoothAdapterBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent == null) {
                Logger.error("Received null intent");
            } else if (!INTENT_ACTION.equalsIgnoreCase(intent.getAction())) {
                Logger.error("Received incorrect intent action: %s", intent.getAction());
            } else {
                handleBluetoothStateChange(intent);
            }
        }
    };

    //----------------------------------------------------------------------------------------------
    // PRIVATE METHODS
    //----------------------------------------------------------------------------------------------

    /**
     * Emits current Bluetooth adapter info on the bluetoothStateChangeSubject.
     *
     * @param intent Intent can be used for further processing.
     */
    private void handleBluetoothStateChange(Intent intent) {
        bluetoothStateChangeSubject.onNext(getBluetoothAdapterInfo());
    }
}
