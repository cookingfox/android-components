package com.cookingfox.android.bluetooth_management.android.connection;

import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothConnectionResult;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothIOResult;

import java.util.UUID;

import rx.Observable;

/**
 * Represents a GATT connection, complying to Bluetooth standards.
 */
public interface GattConnection {

    /**
     * Connects a new Connection instance.
     *
     * @return Observable<BluetoothConnectionResult>
     */
    Observable<BluetoothConnectionResult> connect();

    /**
     * Reads value from device using provided UUID's.
     *
     * @param serviceUuid        service UUID
     * @param characteristicUuid characteristic UUID
     * @return Observable<BluetoothIOResult> containing identifier type 'READ'
     */
    Observable<BluetoothIOResult> read(UUID serviceUuid, UUID characteristicUuid);

    /**
     * Writes value to device using provided UUID's.
     *
     * @param serviceUuid        service UUID
     * @param characteristicUuid character UUID
     * @param value              value to be written to device
     * @return Observable<BluetoothIOResult> containing identifier type 'WRITE'
     */
    Observable<BluetoothIOResult> write(UUID serviceUuid, UUID characteristicUuid, byte[] value);

}
