package com.cookingfox.android.bluetooth_management.component;

import com.cookingfox.android.bluetooth_management.component.config.BluetoothManagementCheflingConfig;
import com.cookingfox.android.bluetooth_management.domain.facade.BluetoothManagementFacade;
import com.cookingfox.android.bluetooth_management.domain.manager.BluetoothAdapterManager;
import com.cookingfox.android.bluetooth_management.domain.manager.BluetoothScanningManager;
import com.cookingfox.android.bluetooth_management.domain.preferences.BluetoothManagementPreferences;
import com.cookingfox.android.core_component.api.component.Component;
import com.cookingfox.android.core_component.api.component.config.ComponentCheflingConfig;
import com.cookingfox.android.core_component.api.component.manager.ComponentManager;
import com.cookingfox.android.prefer.api.pref.Prefs;
import com.cookingfox.chefling.api.CheflingContainer;
import com.cookingfox.lapasse.api.facade.Facade;

/**
 * Implementation of Android component for bluetooth management.
 */
public class BluetoothManagementComponent implements Component {

    private final CheflingContainer container;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public BluetoothManagementComponent(CheflingContainer container) {
        this.container = container;
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public ComponentCheflingConfig getCheflingConfig() {
        return new BluetoothManagementCheflingConfig();
    }

    @Override
    public Facade getLaPasseFacade() {
        return container.getInstance(BluetoothManagementFacade.class);
    }

    @Override
    public Prefs getPrefs() {
        return container.getInstance(BluetoothManagementPreferences.class);
    }

    @Override
    public void loadComponent(ComponentManager componentManager) {
        // no component dependencies
    }

    @Override
    public void registerComponent() {
        container.getInstance(BluetoothAdapterManager.class);
        container.getInstance(BluetoothScanningManager.class);
    }

    @Override
    public void unregisterComponent() {
        // implicit deregister
        container.removeInstanceAndMapping(BluetoothAdapterManager.class);
        container.removeInstanceAndMapping(BluetoothScanningManager.class);
    }

}
