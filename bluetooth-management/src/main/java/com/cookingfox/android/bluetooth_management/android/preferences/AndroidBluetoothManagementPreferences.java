package com.cookingfox.android.bluetooth_management.android.preferences;

import com.cookingfox.android.bluetooth_management.domain.preferences.BluetoothManagementPreferences;
import com.cookingfox.android.prefer_rx.api.pref.RxPrefGroup;
import com.cookingfox.android.prefer_rx.api.pref.typed.BooleanRxPref;
import com.cookingfox.android.prefer_rx.api.pref.typed.IntegerRxPref;
import com.cookingfox.android.prefer_rx.impl.pref.AndroidRxPrefGroup;
import com.cookingfox.android.prefer_rx.impl.pref.typed.AndroidBooleanRxPref;
import com.cookingfox.android.prefer_rx.impl.pref.typed.AndroidIntegerRxPref;
import com.cookingfox.android.prefer_rx.impl.prefer.AndroidRxPrefer;

/**
 * Android Prefer implementation of bluetooth management preferences.
 */
public class AndroidBluetoothManagementPreferences
        implements BluetoothManagementPreferences {

    //----------------------------------------------------------------------------------------------
    // PREFERENCES
    //----------------------------------------------------------------------------------------------

    private final AndroidRxPrefGroup<Key> prefGroup;
    private final AndroidBooleanRxPref<Key> autoEnableBluetoothAdapter;
    private final AndroidIntegerRxPref<Key> scanningBufferPeriodDurationMs;
    private final AndroidIntegerRxPref<Key> scanningPeriodDurationMs;
    private final AndroidIntegerRxPref<Key> scanningTimeoutDurationMs;

    //----------------------------------------------------------------------------------------------
    // STATIC PROPERTIES
    //----------------------------------------------------------------------------------------------

    private static final int DEFAULT_SCANNING_BUFFER_PERIOD_DURATION = 1000;
    private static final int DEFAULT_SCANNING_PERIOD_DURATION_MS = 10000;
    private static final int DEFAULT_SCANNING_TIMEOUT_DURATION_MS = 250;
    private static final boolean DEFAULT_AUTO_ENABLE_BLUETOOTH_ADAPTER = true;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    /**
     * Constructs all the desired preferences and assigns default values to them.
     *
     * @param prefer AndroidRxPrefer
     */
    public AndroidBluetoothManagementPreferences(AndroidRxPrefer prefer) {
        prefGroup = prefer.addNewGroup(Key.class);
        prefGroup.setTitle("Bluetooth Adapter Management");
        prefGroup.setSummary("Settings for bluetooth adapter");

        autoEnableBluetoothAdapter = prefGroup.addNewBoolean(Key.AutoEnableBluetoothAdapter,
                DEFAULT_AUTO_ENABLE_BLUETOOTH_ADAPTER);
        autoEnableBluetoothAdapter.setTitle("Auto-enable Bluetooth Adapter");
        autoEnableBluetoothAdapter.setSummary("Whether to enable the bluetooth adapter on start");

        scanningBufferPeriodDurationMs = prefGroup.addNewInteger(Key.ScanningBufferPeriodDurationMs,
                DEFAULT_SCANNING_BUFFER_PERIOD_DURATION);
        scanningBufferPeriodDurationMs.setTitle("Scanning Buffer Period Duration Ms");
        scanningBufferPeriodDurationMs.setSummary("Duration of buffering period");

        scanningPeriodDurationMs = prefGroup.addNewInteger(Key.ScanningPeriodDurationMs,
                DEFAULT_SCANNING_PERIOD_DURATION_MS);
        scanningPeriodDurationMs.setTitle("Scanning Period Duration Ms");
        scanningPeriodDurationMs.setSummary("Duration of scanning period");

        scanningTimeoutDurationMs = prefGroup.addNewInteger(Key.ScanningTimeoutDurationMs,
                DEFAULT_SCANNING_TIMEOUT_DURATION_MS);
        scanningTimeoutDurationMs.setTitle("Scan Timeout Duration Ms");
        scanningTimeoutDurationMs.setSummary("Duration of timeout between scanning period");
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    /**
     * Whether to enable the bluetooth adapter on start.
     *
     * @return BooleanRxPref Auto enabled
     */
    @Override
    public BooleanRxPref<Key> autoEnableBluetoothAdapter() {
        return autoEnableBluetoothAdapter;
    }

    /**
     * Amount of time the buffer should collect scanresults
     *
     * @return IntegerRxPref Buffer period duration in Ms
     */
    @Override
    public IntegerRxPref<Key> scanningBufferPeriodDurationMs() {
        return scanningBufferPeriodDurationMs;
    }

    /**
     * Duration of active scanning from a startScan until a stopScan.
     *
     * @return IntegerRxPref Period duration Ms
     */
    @Override
    public IntegerRxPref<Key> scanningPeriodDurationMs() {
        return scanningPeriodDurationMs;
    }

    /**
     * Duration between active scan processes.
     * <p>
     * The timout duration points to the time between a stopScan and startScan moment.
     *
     * @return IntegerRxPref Timeout in Ms
     */
    @Override
    public IntegerRxPref<Key> scanningTimeoutDurationMs() {
        return scanningTimeoutDurationMs;
    }

    /**
     * Preferences bundled in a group.
     *
     * @return PrefGroup Group containing preferences
     */
    @Override
    public RxPrefGroup<Key> getPrefGroup() {
        return prefGroup;
    }

}
