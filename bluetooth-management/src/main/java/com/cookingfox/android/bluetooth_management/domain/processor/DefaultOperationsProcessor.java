package com.cookingfox.android.bluetooth_management.domain.processor;

import com.cookingfox.android.bluetooth_management.domain.command.AddEstablishedConnection;
import com.cookingfox.android.bluetooth_management.domain.command.AddPendingConnection;
import com.cookingfox.android.bluetooth_management.domain.command.AddPendingIoOperation;
import com.cookingfox.android.bluetooth_management.domain.command.RemoveEstablishedConnection;
import com.cookingfox.android.bluetooth_management.domain.command.RemovePendingConnection;
import com.cookingfox.android.bluetooth_management.domain.command.RemovePendingIoOperation;
import com.cookingfox.android.bluetooth_management.domain.connection.BluetoothConnection;
import com.cookingfox.android.bluetooth_management.domain.device.ConnectedBluetoothDevice;
import com.cookingfox.android.bluetooth_management.domain.device.DetectedBluetoothDevice;
import com.cookingfox.android.bluetooth_management.domain.driver.BluetoothGattDriver;
import com.cookingfox.android.bluetooth_management.domain.event.EstablishedConnectionsUpdated;
import com.cookingfox.android.bluetooth_management.domain.event.PendingConnectionsUpdated;
import com.cookingfox.android.bluetooth_management.domain.event.PendingIoOperationsUpdated;
import com.cookingfox.android.bluetooth_management.domain.exception.ConnectionAlreadyEstablishedOrPendingException;
import com.cookingfox.android.bluetooth_management.domain.facade.BluetoothManagementFacade;
import com.cookingfox.android.bluetooth_management.domain.operation.OperationIdentifier;
import com.cookingfox.android.bluetooth_management.domain.operation.OperationType;
import com.cookingfox.android.bluetooth_management.domain.operation.PendingConnection;
import com.cookingfox.android.bluetooth_management.domain.operation.PendingOperation;
import com.cookingfox.android.bluetooth_management.domain.operation.PendingRead;
import com.cookingfox.android.bluetooth_management.domain.operation.PendingWrite;
import com.cookingfox.android.bluetooth_management.domain.state.BluetoothManagementState;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothConnectionResult;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothIOResult;
import com.cookingfox.chefling.api.CheflingLifecycle;
import com.cookingfox.lapasse.annotation.HandleCommand;
import com.cookingfox.lapasse.annotation.HandleEvent;
import com.cookingfox.lapasse.impl.helper.LaPasse;
import com.cookingfox.logging.Logger;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import rx.Observable;
import rx.subjects.PublishSubject;
import rx.subscriptions.CompositeSubscription;

public class DefaultOperationsProcessor implements OperationsProcessor, CheflingLifecycle {

    private final BluetoothGattDriver bluetoothGattDriver;
    private final BluetoothManagementFacade facade;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public DefaultOperationsProcessor(BluetoothManagementFacade facade,
                                      BluetoothGattDriver bluetoothGattDriver) {
        this.facade = facade;
        this.bluetoothGattDriver = bluetoothGattDriver;
    }

    //----------------------------------------------------------------------------------------------
    // LIFECYCLE
    //----------------------------------------------------------------------------------------------

    @Override
    public void initialize() {
        LaPasse.mapHandlers(this, facade);
    }

    @Override
    public void dispose() {
        // No-op
    }

    //----------------------------------------------------------------------------------------------
    // OVERRIDDEN METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public Observable<BluetoothConnectionResult> connectWithDevice(DetectedBluetoothDevice device)
            throws ConnectionAlreadyEstablishedOrPendingException {

        PublishSubject<BluetoothConnectionResult> connectionPublisher = PublishSubject.create();

        PendingConnection pendingConnection = new PendingConnection.Builder()
                .device(device)
                .macAddress(device.scanResult().macAddress())
                .publisher(connectionPublisher)
                .build();

        if (findPendingConnection(pendingConnection) != null) {
            return Observable.error(new ConnectionAlreadyEstablishedOrPendingException());
        }

        if (facade.getCurrentState().pendingConnections().isEmpty()) {
            facade.handleCommand(new AddPendingConnection.Builder()
                    .pendingConnection(pendingConnection)
                    .build());

            bluetoothGattDriver.connectWithDevice(device)
                    // connected: subscribe to read and write results
                    .doOnNext(connectionResult -> {
                        switch (connectionResult.state()) {
                            case CONNECTED:
                                CompositeSubscription ioSubs = new CompositeSubscription();

                                ioSubs.add(connectionResult.connection().observeReadResults()
                                        .subscribe(this::processOperation));

                                ioSubs.add(connectionResult.connection().observeWriteResults()
                                        .subscribe(this::processOperation));

                                facade.handleCommand(new AddEstablishedConnection.Builder()
                                        .ioSubscriptions(ioSubs)
                                        .result(connectionResult)
                                        .build());

                                break;
                            case DISCONNECTED:
                                facade.handleCommand(new RemoveEstablishedConnection.Builder()
                                        .result(connectionResult)
                                        .build());
                                break;
                        }
                    })
                    .subscribe(this::processConnectionResult);
        } else {
            facade.handleCommand(new AddPendingConnection.Builder()
                    .pendingConnection(pendingConnection)
                    .build());
        }

        // required for back pressure support
        return connectionPublisher.onBackpressureBuffer();
    }

    @Override
    public Observable<BluetoothConnectionResult> discoverServices(ConnectedBluetoothDevice device) {
        return bluetoothGattDriver.discoverServices(device);
    }

    @Override
    public Observable<BluetoothConnectionResult> disconnectFromDevice(ConnectedBluetoothDevice connectedDevice) {
        return bluetoothGattDriver.disconnectFromDevice(connectedDevice);
    }

    @Override
    public Observable<BluetoothIOResult> read(ConnectedBluetoothDevice connection,
                                              UUID serviceUuid,
                                              UUID characteristicUuid) {
        PublishSubject<BluetoothIOResult> readPublisher = PublishSubject.create();

        OperationIdentifier identifier = new OperationIdentifier.Builder()
                .characteristicUuid(characteristicUuid)
                .macAddress(connection.getDetectedDevice().scanResult().macAddress())
                .serviceUuid(serviceUuid)
                .type(OperationType.READ)
                .build();

        PendingRead pendingRead = new PendingRead.Builder()
                .connection(connection)
                .identifier(identifier)
                .publisher(readPublisher)
                .build();

        if (facade.getCurrentState().queuedIoOperations().isEmpty()) {
            facade.handleCommand(new AddPendingIoOperation.Builder()
                    .operation(pendingRead)
                    .build());

            bluetoothGattDriver.read(pendingRead).subscribe(readPublisher);
        } else {
            facade.handleCommand(new AddPendingIoOperation.Builder()
                    .operation(pendingRead)
                    .build());
        }

        return readPublisher;
    }

    @Override
    public Observable<BluetoothIOResult> write(ConnectedBluetoothDevice connection,
                                               UUID serviceUuid,
                                               UUID characteristicUuid,
                                               byte[] value) {
        PublishSubject<BluetoothIOResult> writePublisher = PublishSubject.create();

        OperationIdentifier identifier = new OperationIdentifier.Builder()
                .characteristicUuid(characteristicUuid)
                .macAddress(connection.getDetectedDevice().scanResult().macAddress())
                .serviceUuid(serviceUuid)
                .type(OperationType.WRITE)
                .build();

        PendingWrite pendingWrite = new PendingWrite.Builder()
                .connection(connection)
                .identifier(identifier)
                .publisher(writePublisher)
                .value(value)
                .build();

        if (facade.getCurrentState().queuedIoOperations().isEmpty()) {
            facade.handleCommand(new AddPendingIoOperation.Builder()
                    .operation(pendingWrite)
                    .build());

            bluetoothGattDriver.write(pendingWrite).subscribe(writePublisher);
        } else {
            facade.handleCommand(new AddPendingIoOperation.Builder()
                    .operation(pendingWrite)
                    .build());
        }

        return writePublisher;
    }

    //----------------------------------------------------------------------------------------------
    // COMMAND HANDLERS
    //----------------------------------------------------------------------------------------------

    @HandleCommand
    public PendingConnectionsUpdated handle(BluetoothManagementState state,
                                            AddPendingConnection command) {
        List<PendingConnection> pendingConnections = new LinkedList<>(state.pendingConnections());
        pendingConnections.add(command.pendingConnection());

        return new PendingConnectionsUpdated.Builder()
                .connections(pendingConnections)
                .build();
    }

    @HandleCommand
    public PendingConnectionsUpdated handle(BluetoothManagementState state,
                                            RemovePendingConnection command) {
        List<PendingConnection> pendingConnections = new LinkedList<>(state.pendingConnections());
        pendingConnections.remove(command.pendingConnection());

        return new PendingConnectionsUpdated.Builder()
                .connections(pendingConnections)
                .build();
    }

    @HandleCommand
    public PendingIoOperationsUpdated handle(BluetoothManagementState state,
                                             AddPendingIoOperation command) {
        List<PendingOperation> pendingOperations = new LinkedList<>(state.queuedIoOperations());
        pendingOperations.add(command.operation());

        return new PendingIoOperationsUpdated.Builder()
                .operations(pendingOperations)
                .build();
    }

    @HandleCommand
    public PendingIoOperationsUpdated handle(BluetoothManagementState state,
                                             RemovePendingIoOperation command) {
        List<PendingOperation> pendingOperations = new LinkedList<>(state.queuedIoOperations());
        pendingOperations.remove(command.operation());

        return new PendingIoOperationsUpdated.Builder()
                .operations(pendingOperations)
                .build();
    }

    /**
     * Checks if a connection is already established, if not it gets the pending connection to fire
     * the connection publisher's onNext.
     *
     * @param state   BluetoothManagementState
     * @param command AddEstablishedConnection
     * @return EstablishedConnectionsUpdated event containing target result & current connections
     */
    @HandleCommand
    public EstablishedConnectionsUpdated handle(BluetoothManagementState state,
                                                AddEstablishedConnection command) {
        List<BluetoothConnection> establishedConnections = new LinkedList<>(state.establishedConnections());
        BluetoothConnection connection = command.result().connection();

        if (establishedConnections.contains(connection)) {
            Logger.warn("Established Connection Already Exists!");
            return null;
        }

        // add composite subscription to state
        Map<BluetoothConnection, CompositeSubscription> connectionSubscriptions = new LinkedHashMap<>(
                state.connectionIoSubscriptions());
        connectionSubscriptions.put(connection, command.ioSubscriptions());

        establishedConnections.add(connection);

        return new EstablishedConnectionsUpdated.Builder()
                .connections(establishedConnections)
                .connectionResult(command.result())
                .connectionSubscriptions(connectionSubscriptions)
                .build();
    }

    /**
     * Removes an established connection from the state, checks for co-existing IO (Read/Write)
     * subscriptions, unsubscribes and removes them from the state.
     *
     * @param state   BluetoothManagementState
     * @param command RemoveEstablishedConnection
     * @return EstablishedConnectionsUpdated
     */
    @HandleCommand
    public EstablishedConnectionsUpdated handle(BluetoothManagementState state,
                                                RemoveEstablishedConnection command) {
        List<BluetoothConnection> establishedConnections = new LinkedList<>(state.establishedConnections());
        BluetoothConnection connection = command.result().connection();
        Map<BluetoothConnection, CompositeSubscription> connectionSubscriptions = new LinkedHashMap<>(
                state.connectionIoSubscriptions());

        // if existing, remove connection
        if (establishedConnections.contains(connection)) {
            establishedConnections.remove(connection);
        }

        // unsubscribe and remove connection subscriptions
        if (connectionSubscriptions.containsKey(connection)) {
            CompositeSubscription subs = connectionSubscriptions.get(connection);
            subs.unsubscribe();
            connectionSubscriptions.remove(connection);
        }

        return new EstablishedConnectionsUpdated.Builder()
                .connections(establishedConnections)
                .connectionResult(command.result())
                .connectionSubscriptions(connectionSubscriptions)
                .build();
    }

    //----------------------------------------------------------------------------------------------
    // EVENT HANDLERS
    //----------------------------------------------------------------------------------------------

    @HandleEvent
    public BluetoothManagementState handle(BluetoothManagementState state,
                                           PendingConnectionsUpdated event) {
        return new BluetoothManagementState.Builder()
                .from(state)
                .pendingConnections(event.connections())
                .build();
    }

    /**
     * After that the state is updated the onNext is fired on the pending connections' publisher
     * and an 'EstablishedConnectionsUpdated' event is returned.
     *
     * @param state BluetoothManagementState
     * @param event EstablishedConnectionsUpdated
     * @return BluetoothManagementState
     */
    @HandleEvent
    public BluetoothManagementState handle(BluetoothManagementState state,
                                           EstablishedConnectionsUpdated event) {
        PendingConnection pendingConnection = findPendingConnection(event.connectionResult());

        if (pendingConnection == null) {
            return new BluetoothManagementState.Builder()
                    .from(state)
                    .establishedConnections(event.connections())
                    .build();
        }

        // Fire the connection result on connection established
        pendingConnection.publisher().onNext(event.connectionResult());

        List<PendingConnection> pendingConnections = new LinkedList<>(state.pendingConnections());

        if (pendingConnections.contains(pendingConnection)) {
            pendingConnections.remove(pendingConnection);
        }

        return new BluetoothManagementState.Builder()
                .from(state)
                .establishedConnections(event.connections())
                .pendingConnections(pendingConnections)
                .build();
    }

    @HandleEvent
    public BluetoothManagementState handle(BluetoothManagementState state,
                                           PendingIoOperationsUpdated event) {
        return new BluetoothManagementState.Builder()
                .from(state)
                .queuedIoOperations(event.operations())
                .build();
    }

    //----------------------------------------------------------------------------------------------
    // PRIVATE METHODS
    //----------------------------------------------------------------------------------------------

    /**
     * Each time a BluetoothConnectionResult is coming in, we do the following:
     * <p>
     * - Find pending operation belonging to the incoming result
     * - Emit the result on the connection
     * - Pending connection is no longer pending, remove it from state
     * - If there are pending connections left, take one from stack and execute 'connectWithDevice'
     *
     * @param connectionResult BluetoothConnectionResult containing connection state
     */
    protected void processConnectionResult(BluetoothConnectionResult connectionResult) {
        PendingConnection pendingConnection = findPendingConnection(connectionResult);

        if (pendingConnection == null) {
            return;
        }

        pendingConnection.publisher().onNext(connectionResult);

        facade.handleCommand(new RemovePendingConnection.Builder()
                .pendingConnection(pendingConnection)
                .build());

        if (facade.getCurrentState().pendingConnections().isEmpty()) {
            return;
        }

        PendingConnection newConnection = facade.getCurrentState().pendingConnections().get(0);

        // TODO: 8/25/16 Joep: come up with solution for centralizing subscriptions
        bluetoothGattDriver.connectWithDevice(newConnection.device())
                // connected: subscribe to read and write results
                .doOnNext(result -> {
                    result.connection().observeReadResults().subscribe(this::processOperation);
                    result.connection().observeWriteResults().subscribe(this::processOperation);
                })
                .subscribe(this::processConnectionResult);
    }

    /**
     * Each time a BluetoothIOResult is coming in, we do the following:
     * <p>
     * - Find the pending operation belonging to the incoming result
     * - Emit the result on the operation
     * - Pending operation is no longer pending, remove it from state
     * - If there are pending operations left, take one from stack and execute read/write
     *
     * @param ioResult BluetoothIOResult containing info about a performed READ/WRITE
     */
    protected void processOperation(BluetoothIOResult ioResult) {
        PendingOperation operation = findPendingOperation(ioResult);

        if (operation == null) {
            return;
        }

        Logger.info("Calling onNext and onCompleted on publisher..");
        operation.publisher().onNext(ioResult);
        operation.publisher().onCompleted();

        facade.handleCommand(new RemovePendingIoOperation.Builder()
                .operation(operation)
                .build());

        if (facade.getCurrentState().queuedIoOperations().isEmpty()) {
            return;
        }

        PendingOperation newOperation = facade.getCurrentState().queuedIoOperations().get(0);
        switch (newOperation.identifier().type()) {
            case READ:
                PendingRead read = (PendingRead) newOperation;
                bluetoothGattDriver.read(read).subscribe();
                break;
            case WRITE:
                PendingWrite write = (PendingWrite) newOperation;
                bluetoothGattDriver.write(write).subscribe();
                break;
        }
    }

    /**
     * Searches for a pending connection in the state, based on a provided
     * BluetoothConnectionResult.
     *
     * @param connectionResult BluetoothConnectionResult
     * @return PendingConnection
     */
    protected PendingConnection findPendingConnection(BluetoothConnectionResult connectionResult) {
        for (PendingConnection connection : facade.getCurrentState().pendingConnections()) {
            if (connection.macAddress().equals(connectionResult.macAddress())) {
                return connection;
            }
        }
        return null;
    }

    /**
     * Searches for a pending connection in the state, based on a provided PendingConnection.
     *
     * @param pendingConnection PendingConnection
     * @return PendingConnection
     */
    protected PendingConnection findPendingConnection(PendingConnection pendingConnection) {
        for (PendingConnection connection : facade.getCurrentState().pendingConnections()) {
            if (connection.macAddress().equals(pendingConnection.macAddress())) {
                return connection;
            }
        }
        return null;
    }

    /**
     * Searches for a pending operation based on the provided BluetoothIOResult.
     *
     * @param ioResult BluetoothIOResult
     * @return PendingOperation
     */
    private PendingOperation findPendingOperation(BluetoothIOResult ioResult) {
        for (PendingOperation operation : facade.getCurrentState().queuedIoOperations()) {
            if (operation.identifier().equals(ioResult.identifier())) {
                return operation;
            }
        }
        return null;
    }
}
