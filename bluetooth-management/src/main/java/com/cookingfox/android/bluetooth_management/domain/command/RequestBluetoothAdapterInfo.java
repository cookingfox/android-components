package com.cookingfox.android.bluetooth_management.domain.command;

import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;
import com.cookingfox.lapasse.api.command.Command;

import org.immutables.value.Value;

/**
 * Command for requesting the current bluetooth adapter info.
 */
@DefaultValueStyle
@Value.Immutable
public interface RequestBluetoothAdapterInfo extends Command {

    class Builder extends ImmutableRequestBluetoothAdapterInfo.Builder {
    }

}
