package com.cookingfox.android.bluetooth_management.domain.manager;

import com.cookingfox.android.bluetooth_management.domain.command.UpdateBluetoothScanResults;
import com.cookingfox.android.bluetooth_management.domain.driver.BluetoothScannerDriver;
import com.cookingfox.android.bluetooth_management.domain.event.BluetoothScanResultsUpdated;
import com.cookingfox.android.bluetooth_management.domain.facade.BluetoothManagementFacade;
import com.cookingfox.android.bluetooth_management.domain.preferences.BluetoothManagementPreferences;
import com.cookingfox.android.bluetooth_management.domain.preferences.BluetoothManagementPreferences.Key;
import com.cookingfox.android.bluetooth_management.domain.state.BluetoothManagementState;
import com.cookingfox.android.bluetooth_management.domain.vo.scanning.BluetoothScanResult;
import com.cookingfox.android.core_component.compatibility.guava.collect.Maps2;
import com.cookingfox.android.core_component.compatibility.rx.scheduler.RxSchedulerProvider;
import com.cookingfox.android.prefer_rx.api.pref.typed.IntegerRxPref;
import com.cookingfox.chefling.api.CheflingLifecycle;
import com.cookingfox.lapasse.annotation.HandleCommand;
import com.cookingfox.lapasse.annotation.HandleEvent;
import com.cookingfox.lapasse.impl.helper.LaPasse;
import com.cookingfox.logging.Logger;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Default implementation of {@link BluetoothScanningManager}.
 */
public class BluetoothScanningManagerImpl implements BluetoothScanningManager, CheflingLifecycle {

    protected final BluetoothScannerDriver bluetoothScannerDriver;
    protected final BluetoothManagementFacade facade;
    protected final BluetoothManagementPreferences preferences;
    protected Subscription scanResultBufferSubscription;
    protected CompositeSubscription subscriptions;
    protected final RxSchedulerProvider rxSchedulerProvider;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public BluetoothScanningManagerImpl(BluetoothScannerDriver bluetoothScannerDriver,
                                        BluetoothManagementFacade facade,
                                        BluetoothManagementPreferences preferences,
                                        RxSchedulerProvider rxSchedulerProvider) {
        this.bluetoothScannerDriver = bluetoothScannerDriver;
        this.facade = facade;
        this.preferences = preferences;
        this.rxSchedulerProvider = rxSchedulerProvider;
    }

    //----------------------------------------------------------------------------------------------
    // LIFECYCLE
    //----------------------------------------------------------------------------------------------

    @Override
    public void initialize() {
        LaPasse.mapHandlers(this, facade);

        subscriptions = new CompositeSubscription();

        configureDriver();

        // restart scan result buffering when preference value changes
        subscriptions.add(preferences.scanningBufferPeriodDurationMs()
                .observeValueChanges()
                .subscribe(integer -> {
                    stopObservingScanResults();
                    startObservingScanResults();
                }));
    }

    @Override
    public void dispose() {
        stopObservingScanResults();

        subscriptions.clear();
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public void startScanning() {
        startObservingScanResults();

        bluetoothScannerDriver.startScanning();
    }

    @Override
    public void stopScanning() {
        stopObservingScanResults();

        bluetoothScannerDriver.stopScanning();
    }

    //----------------------------------------------------------------------------------------------
    // COMMAND HANDLERS
    //----------------------------------------------------------------------------------------------

    /**
     * @param command The command to process.
     * @return BLE Scan Result updated event.
     */
    @HandleCommand
    BluetoothScanResultsUpdated handle(UpdateBluetoothScanResults command) {
        return new BluetoothScanResultsUpdated.Builder()
                .results(command.results())
                .build();
    }

    //----------------------------------------------------------------------------------------------
    // EVENT HANDLERS
    //----------------------------------------------------------------------------------------------

    @HandleEvent
    BluetoothManagementState handle(BluetoothManagementState state, BluetoothScanResultsUpdated event) {
        return new BluetoothManagementState.Builder()
                .from(state)
                .scanResults(event.results())
                .build();
    }

    //----------------------------------------------------------------------------------------------
    // PROTECTED METHODS
    //----------------------------------------------------------------------------------------------

    /**
     * Configure the driver with values from the preferences.
     */
    protected void configureDriver() {
        IntegerRxPref<Key> scanningPeriodDurationMs = preferences.scanningPeriodDurationMs();
        IntegerRxPref<Key> scanningTimeoutDurationMs = preferences.scanningTimeoutDurationMs();

        bluetoothScannerDriver.setScanningPeriodDurationMs(scanningPeriodDurationMs.getValue());
        bluetoothScannerDriver.setScanningTimeoutDurationMs(scanningTimeoutDurationMs.getValue());

        subscriptions.add(scanningPeriodDurationMs.observeValueChanges()
                .subscribe(bluetoothScannerDriver::setScanningPeriodDurationMs, Logger::error));

        subscriptions.add(scanningTimeoutDurationMs.observeValueChanges()
                .subscribe(bluetoothScannerDriver::setScanningTimeoutDurationMs, Logger::error));
    }

    /**
     * Start observing scan results.
     */
    protected void startObservingScanResults() {
        // observe scan results
        scanResultBufferSubscription = observeBufferedScanResultMap()
                .subscribe(bluetoothScanResults -> {
                    facade.handleCommand(new UpdateBluetoothScanResults.Builder()
                            .results(bluetoothScanResults)
                            .build());
                }, Logger::error);
    }

    /**
     * @return Observable for bluetooth scan results, collected by a buffer and converted to a map.
     */
    protected Observable<Map<String, BluetoothScanResult>> observeBufferedScanResultMap() {
        int bufferPeriodMs = preferences.scanningBufferPeriodDurationMs().getValue();

        return bluetoothScannerDriver.observeScanResults()
                // buffer scan results
                .buffer(bufferPeriodMs, TimeUnit.MILLISECONDS, rxSchedulerProvider.timingScheduler())
                // filter out duplicate scan results by mac address
                .map(scanResults -> Maps2.indexTree(scanResults, BluetoothScanResult::macAddress))
                // only continue when value has changed - necessary because of buffer
                .distinctUntilChanged();
    }

    /**
     * Stop observing scan results.
     */
    protected void stopObservingScanResults() {
        if (scanResultBufferSubscription != null) {
            scanResultBufferSubscription.unsubscribe();
        }
    }

}
