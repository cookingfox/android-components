package com.cookingfox.android.bluetooth_management.domain.vo.connection;

/**
 * Connection state of a connected Bluetooth device.
 */
public enum BluetoothConnectionState {
    CONNECTING, CONNECTED, DISCONNECTING, DISCONNECTED, SERVICES_DISCOVERING, SERVICES_DISCOVERED
}
