package com.cookingfox.android.bluetooth_management.domain.state;

import com.cookingfox.android.bluetooth_management.domain.connection.BluetoothConnection;
import com.cookingfox.android.bluetooth_management.domain.operation.PendingConnection;
import com.cookingfox.android.bluetooth_management.domain.operation.PendingOperation;
import com.cookingfox.android.bluetooth_management.domain.vo.BluetoothAdapterInfo;
import com.cookingfox.android.bluetooth_management.domain.vo.scanning.BluetoothScanResult;
import com.cookingfox.lapasse.api.state.State;

import org.immutables.value.Value;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import rx.subscriptions.CompositeSubscription;

/**
 * LaPasse state implementation for bluetooth management.
 */
@Value.Immutable
public abstract class BluetoothManagementState implements State {

    //----------------------------------------------------------------------------------------------
    // IMMUTABLE PROPERTIES
    //----------------------------------------------------------------------------------------------

    public abstract Map<BluetoothConnection, CompositeSubscription> connectionIoSubscriptions();

    public abstract List<BluetoothConnection> establishedConnections();

    public abstract BluetoothAdapterInfo info();

    public abstract List<PendingConnection> pendingConnections();

    public abstract List<PendingOperation> queuedIoOperations();

    public abstract Map<String, BluetoothScanResult> scanResults();

    //----------------------------------------------------------------------------------------------
    // HELPER METHODS
    //----------------------------------------------------------------------------------------------

    public boolean isAdapterEnabled() {
        return info().isEnabled();
    }

    //----------------------------------------------------------------------------------------------
    // STATIC METHODS
    //----------------------------------------------------------------------------------------------

    public static BluetoothManagementState createInitialState() {
        // Initial adapter state
        BluetoothAdapterInfo adapterInfo = new BluetoothAdapterInfo.Builder()
                .isEnabled(false)
                .isOn(false)
                .isOff(false)
                .isTurningOn(false)
                .isTurningOff(false)
                .build();

        return new Builder()
                .connectionIoSubscriptions(new LinkedHashMap<>())
                .establishedConnections(new LinkedList<>())
                .info(adapterInfo)
                .pendingConnections(new LinkedList<>())
                .queuedIoOperations(new LinkedList<>())
                .scanResults(new LinkedHashMap<>())
                .build();
    }

    //----------------------------------------------------------------------------------------------
    // IMMUTABLE BUILDER
    //----------------------------------------------------------------------------------------------

    public static class Builder extends ImmutableBluetoothManagementState.Builder {
    }

}
