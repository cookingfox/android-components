package com.cookingfox.android.bluetooth_management.domain.operation;

/**
 * Represents type of pendingConnection (to be) executed:
 * - READ
 * - WRITE
 */
public enum OperationType {
    READ, WRITE
}
