package com.cookingfox.android.bluetooth_management.domain.state;

import com.cookingfox.lapasse.api.state.observer.RxStateObserver;

public interface BluetoothManagementStateObserver
        extends RxStateObserver<BluetoothManagementState> {
}
