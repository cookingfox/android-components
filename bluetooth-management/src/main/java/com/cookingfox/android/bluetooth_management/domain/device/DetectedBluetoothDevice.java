package com.cookingfox.android.bluetooth_management.domain.device;

import com.cookingfox.android.bluetooth_management.domain.vo.scanning.BluetoothScanResult;

public interface DetectedBluetoothDevice {
    /**
     * Returns the Bluetooth scan result retrieved from BluetoothManagement state.
     *
     * @return BluetoothScanResult
     */
    BluetoothScanResult scanResult();
}
