package com.cookingfox.android.bluetooth_management.domain.manager;

import com.cookingfox.android.bluetooth_management.domain.device.ConnectedBluetoothDevice;
import com.cookingfox.android.bluetooth_management.domain.device.DetectedBluetoothDevice;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothConnectionResult;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothIOResult;

import java.util.UUID;

import rx.Observable;

public interface BluetoothDeviceConnectionManager {

    /**
     * Connects with provided detected Bluetooth device, emits 'BluetoothConnectionResult'
     * containing BluetoothConnectionState.CONNECTED on success.
     *
     * @param device The detected target device.
     * @return Observable<BluetoothConnectionResult>
     */
    Observable<BluetoothConnectionResult> connectWithDevice(DetectedBluetoothDevice device);

    /**
     * Disconnects provided connected device.
     *
     * @param connectedDevice The connected target device.
     * @return Observable<BluetoothConnectionResult>
     */
    Observable<BluetoothConnectionResult> disconnectFromDevice(ConnectedBluetoothDevice connectedDevice);

    /**
     * Performs a 'read' on the provided connection for the properties matching service UUID and
     * characteristic UUID.
     *
     * @param connection         The target connection.
     * @param serviceUuid        Property's service UUID.
     * @param characteristicUuid Property's characteristic UUID.
     * @return Observable<BluetoothIOResult>
     */
    Observable<BluetoothIOResult> read(ConnectedBluetoothDevice connection, UUID serviceUuid, UUID characteristicUuid);

    /**
     * Performs a 'write' on the provided connection for the properties matching service UUID and
     * characteristic UUID.
     *
     * @param connection         The target connection.
     * @param serviceUuid        Property's service UUID.
     * @param characteristicUuid Property's characteristic UUID.
     * @param value              The value to be written.
     * @return Observable<BluetoothIOResult>
     */
    Observable<BluetoothIOResult> write(ConnectedBluetoothDevice connection, UUID serviceUuid, UUID characteristicUuid, byte[] value);

}
