package com.cookingfox.android.bluetooth_management.domain.operation;

import com.cookingfox.android.bluetooth_management.domain.device.DetectedBluetoothDevice;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothConnectionResult;
import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;

import org.immutables.value.Value;

import rx.subjects.PublishSubject;

@DefaultValueStyle
@Value.Immutable
public interface PendingConnection {

    DetectedBluetoothDevice device();

    String macAddress();

    PublishSubject<BluetoothConnectionResult> publisher();

    class Builder extends ImmutablePendingConnection.Builder {
    }
}