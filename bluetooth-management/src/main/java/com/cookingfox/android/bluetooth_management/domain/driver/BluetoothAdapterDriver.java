package com.cookingfox.android.bluetooth_management.domain.driver;

import com.cookingfox.android.bluetooth_management.domain.vo.BluetoothAdapterInfo;

import rx.Observable;

/**
 * Interface for bluetooth adapter driver. Contains methods to control and monitor the behavior of
 * the bluetooth adapter.
 */
public interface BluetoothAdapterDriver {

    void disableAdapter();

    void enableAdapter();

    BluetoothAdapterInfo getBluetoothAdapterInfo();

    boolean isAdapterEnabled();

    boolean isAdapterOn();

    boolean isAdapterOff();

    boolean isAdapterTurningOn();

    boolean isAdapterTurningOff();

    Observable<BluetoothAdapterInfo> observeBluetoothStateChanges();

}
