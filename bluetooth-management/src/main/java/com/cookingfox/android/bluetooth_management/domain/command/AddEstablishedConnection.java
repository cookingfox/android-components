package com.cookingfox.android.bluetooth_management.domain.command;

import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothConnectionResult;
import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;
import com.cookingfox.lapasse.api.command.Command;

import org.immutables.value.Value;

import rx.subscriptions.CompositeSubscription;

@DefaultValueStyle
@Value.Immutable
public interface AddEstablishedConnection extends Command {

    CompositeSubscription ioSubscriptions();

    BluetoothConnectionResult result();

    class Builder extends ImmutableAddEstablishedConnection.Builder {
    }
}