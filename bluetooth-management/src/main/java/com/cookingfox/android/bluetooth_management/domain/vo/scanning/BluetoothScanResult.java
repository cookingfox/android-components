package com.cookingfox.android.bluetooth_management.domain.vo.scanning;

import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.MoreObjects;

import org.immutables.value.Value;
import org.threeten.bp.ZonedDateTime;

/**
 * Value Object for a Bluetooth Low Energy scan result.
 */
@DefaultValueStyle
@JsonDeserialize(as = ImmutableBluetoothScanResult.class)
@Value.Immutable
public abstract class BluetoothScanResult {

    //----------------------------------------------------------------------------------------------
    // PROPERTIES
    //----------------------------------------------------------------------------------------------

    /**
     * MAC address od the detected device.
     *
     * @return String mac address
     */
    public abstract String macAddress();

    /**
     * Signal strength in RSSI (Received Signal Strength Indication) format.
     *
     * @return Negative number indicating strength (-1 = strong, -100 = weak)
     */
    public abstract Integer signalStrength();

    /**
     * Contains any data sent by the remote BluetoothDevice.
     * <p>
     * This can be advertisement data, such as manufacturer name, battery level, basically
     * anything that is published by the detected BluetoothDevice.
     * <p>
     * Note: this data represents only public data, to get more information from the device, a
     * GATT connect is likely necessary.
     *
     * @return Array of bytes containing public device information.
     */
    public abstract byte[] scanRecord();

    /**
     * The timestamp of when the bluetooth device scan result was recorded.
     *
     * @return The full date and time (and timezone) information of the detection instant.
     */
    @Value.Default
    public ZonedDateTime timestamp() {
        return ZonedDateTime.now();
    }

    //----------------------------------------------------------------------------------------------
    // OVERRIDDEN OBJECT METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public String toString() {
        // inspired by immutable `toString()`
        return MoreObjects.toStringHelper(BluetoothScanResult.class.getSimpleName())
                .omitNullValues()
                .add("macAddress", macAddress())
                .add("signalStrength", signalStrength())
                .add("timestamp", timestamp())
                .toString();
    }

    //----------------------------------------------------------------------------------------------
    // BUILDER
    //----------------------------------------------------------------------------------------------

    public static class Builder extends ImmutableBluetoothScanResult.Builder {
    }

}
