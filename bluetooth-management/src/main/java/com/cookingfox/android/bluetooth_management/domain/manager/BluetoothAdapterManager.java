package com.cookingfox.android.bluetooth_management.domain.manager;

/**
 * Provides methods for managing the bluetooth adapter.
 */
public interface BluetoothAdapterManager {

    /**
     * Disables the Bluetooth adapter, if not already disabled.
     */
    void disableBluetoothAdapter();

    /**
     * Enables the Bluetooth adapter, if not already enabled.
     */
    void enableBluetoothAdapter();

}
