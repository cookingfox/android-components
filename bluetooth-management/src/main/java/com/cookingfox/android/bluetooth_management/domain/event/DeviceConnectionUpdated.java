package com.cookingfox.android.bluetooth_management.domain.event;

import com.cookingfox.android.bluetooth_management.domain.device.DetectedBluetoothDevice;
import com.cookingfox.android.core_component.compatibility.immutables.DefaultValueStyle;
import com.cookingfox.lapasse.api.event.Event;

import org.immutables.value.Value;

import java.util.List;

@DefaultValueStyle
@Value.Immutable
public interface DeviceConnectionUpdated extends Event {

    List<DetectedBluetoothDevice> connectingDevices();

    List<DetectedBluetoothDevice> connectedDevices();

    class Builder extends ImmutableDeviceConnectionUpdated.Builder {
    }
}
