package com.cookingfox.android.bluetooth_management.domain.exception;

/**
 * Thrown when disconnecting a connection that does not exist.
 */
public class NoSuchConnectionExistingException extends Exception {
}
