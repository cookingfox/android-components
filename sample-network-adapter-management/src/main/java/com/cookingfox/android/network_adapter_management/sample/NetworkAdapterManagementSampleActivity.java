package com.cookingfox.android.network_adapter_management.sample;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.cookingfox.android.core_component.impl.android.activity.ComponentsAwareAppCompatActivity;
import com.cookingfox.android.network_adapter_management.domain.state.NetworkAdapterManagementState;
import com.cookingfox.android.network_adapter_management.domain.state.NetworkAdapterManagementStateObserver;
import com.cookingfox.chefling.api.CheflingContainer;
import com.cookingfox.lapasse.api.state.observer.StateChanged;
import com.cookingfox.logging.Logger;

import rx.android.schedulers.AndroidSchedulers;

public class NetworkAdapterManagementSampleActivity extends ComponentsAwareAppCompatActivity {

    private TextView wifiEnabled;
    private TextView wifiConnected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(view -> goToSettings());

        wifiEnabled = (TextView) findViewById(R.id.wifi_enabled);
        wifiConnected = (TextView) findViewById(R.id.wifi_connected);
    }

    @Override
    public void onComponentsLoaded(CheflingContainer container) {
        Logger.info();

        NetworkAdapterManagementStateObserver stateObserver =
                container.getInstance(NetworkAdapterManagementStateObserver.class);

        NetworkAdapterManagementState initialState = stateObserver.getCurrentState();

        setWifiEnabledText(initialState.isWifiAdapterEnabled());
        setWifiConnectedText(initialState.isWifiConnected());

        stateObserver.observeStateChanges()
                .map(StateChanged::getState)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(state -> {
                    setWifiEnabledText(state.isWifiAdapterEnabled());
                    setWifiConnectedText(state.isWifiConnected());
                });
    }

    private void setWifiConnectedText(boolean isWifiConnected) {
        wifiConnected.setText(String.format(getString(R.string.wifi_connected), isWifiConnected));
    }

    private void setWifiEnabledText(boolean isWifiAdapterEnabled) {
        wifiEnabled.setText(String.format(getString(R.string.wifi_enabled), isWifiAdapterEnabled));
    }

    private void goToSettings() {
        startActivity(new Intent(this, NetworkAdapterManagementSamplePreferActivity.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            goToSettings();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
