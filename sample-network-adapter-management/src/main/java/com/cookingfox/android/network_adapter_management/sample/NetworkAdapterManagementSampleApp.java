package com.cookingfox.android.network_adapter_management.sample;

import com.cookingfox.android.core_component.impl.android.application.AndroidComponentApplication;
import com.cookingfox.android.network_adapter_management.component.NetworkAdapterManagementComponent;

public class NetworkAdapterManagementSampleApp extends AndroidComponentApplication {

    @Override
    protected void onApplicationInitialized() {
        getComponentManager().loadComponent(NetworkAdapterManagementComponent.class);
    }

}
