package com.cookingfox.android.beacon_management.sample;

import android.os.Bundle;
import android.preference.PreferenceActivity;

import com.cookingfox.android.prefer.impl.prefer.AndroidPreferProvider;
import com.cookingfox.android.prefer_fragment.impl.PreferFragment;

public class BeaconManagementSamplePreferActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PreferFragment fragment = PreferFragment.create(AndroidPreferProvider.getDefault(this));

        getFragmentManager()
                .beginTransaction()
                .replace(android.R.id.content, fragment)
                .commit();
    }

}
