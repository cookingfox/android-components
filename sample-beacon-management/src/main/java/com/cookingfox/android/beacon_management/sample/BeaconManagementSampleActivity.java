package com.cookingfox.android.beacon_management.sample;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import com.cookingfox.android.beacon_management.domain.manager.BeaconManager;
import com.cookingfox.android.beacon_management.infrastructure.beacon.ibeacon.provider.IBeaconBeaconProvider;
import com.cookingfox.android.beacon_management.infrastructure.beacon.minew_patient.connected.MinewPatientConnectedBeacon;
import com.cookingfox.android.beacon_management.infrastructure.beacon.minew_patient.provider.MinewPatientBeaconProvider;
import com.cookingfox.android.beacon_management.infrastructure.beacon.minew_patient.vo.MinewPatientDetectedBeacon;
import com.cookingfox.android.bluetooth_management.domain.facade.BluetoothManagementFacade;
import com.cookingfox.android.bluetooth_management.domain.manager.BluetoothScanningManager;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothConnectionResult;
import com.cookingfox.android.bluetooth_management.domain.vo.connection.BluetoothIOResult;
import com.cookingfox.android.core_component.compatibility.lapasse.logger.CookingFoxLapasseLogger;
import com.cookingfox.android.core_component.impl.android.activity.ComponentsAwareAppCompatActivity;
import com.cookingfox.chefling.api.CheflingContainer;
import com.cookingfox.logging.Logger;

import rx.Observer;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

import static com.cookingfox.guava_preconditions.Preconditions.checkNotNull;

public class BeaconManagementSampleActivity extends ComponentsAwareAppCompatActivity {

    private boolean beaconsConnecting = false;
    private static final String MINEW_PATIENT_BEACON_1_MAC = "EE:12:34:00:00:44";
    private static final String MINEW_PATIENT_BEACON_2_MAC = "EE:12:34:00:00:45";

    private BeaconManager beaconManager;
    private MinewPatientDetectedBeacon minewPatientBeacon = null;
    private MinewPatientConnectedBeacon connectedMinewBeacon = null;

    private Button performBeaconConfigurationBtn = null;
    private Button startScanningBtn;
    private Button stopScanningBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(view -> showSettings());

        // Get button views, and check if they exist, otherwise we throw an exception!
        startScanningBtn = (Button) findViewById(R.id.btn_start_scanning);
        stopScanningBtn = (Button) findViewById(R.id.btn_stop_scanning);
        performBeaconConfigurationBtn = (Button) findViewById(R.id.btn_configure_beacon);
        performBeaconConfigurationBtn.setEnabled(false);
        Button connectBeaconBtn = (Button) findViewById(R.id.btn_connect);
        Button disconnectBeaconBtn = (Button) findViewById(R.id.btn_disconnect);
        checkNotNull(startScanningBtn, stopScanningBtn);

        // Buttons for start/stop scanning interaction
        performBeaconConfigurationBtn.setOnClickListener(v -> this.performBeaconConfiguration());
        connectBeaconBtn.setOnClickListener(v -> this.connectWithBeacon());
        disconnectBeaconBtn.setOnClickListener(v -> this.disconnectWithBeacon());
    }

    @Override
    public void onComponentsLoaded(CheflingContainer container) {
        BluetoothManagementFacade facade = container.getInstance(BluetoothManagementFacade.class);
        facade.addLogger(new CookingFoxLapasseLogger<>());

        // Get the BeaconManager, our main access point for beacon-related actions
        beaconManager = container.getInstance(BeaconManager.class);

        // Register IBeacon provider
        beaconManager.registerProvider(IBeaconBeaconProvider.class);

        // Register MinewPatient provider
        beaconManager.registerProvider(MinewPatientBeaconProvider.class);

        // Get the BluetoothScanningManager for start/stop scan control
        final BluetoothScanningManager bluetoothScanningManager = container.getInstance(BluetoothScanningManager.class);

        // Listen for Minew Tech Patient Beacons
        // For convenience we only take one beacon to connect with.
        beaconManager.observeBeaconType(MinewPatientDetectedBeacon.class)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(detectedBeacons -> {

                    if (beaconsConnecting || detectedBeacons.size() == 0 || minewPatientBeacon != null) {
                        return;
                    }

                    for (MinewPatientDetectedBeacon beacon : detectedBeacons) {
                        if (beacon.scanResult().macAddress().equals(MINEW_PATIENT_BEACON_2_MAC)) {
                            minewPatientBeacon = beacon;
                        }
                    }

                    performBeaconConfigurationBtn.setEnabled(true);
                }, Throwable::printStackTrace);

        startScanningBtn.setOnClickListener(v -> bluetoothScanningManager.startScanning());
        stopScanningBtn.setOnClickListener(v -> bluetoothScanningManager.stopScanning());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            showSettings();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //----------------------------------------------------------------------------------------------
    // PRIVATE METHODS
    //----------------------------------------------------------------------------------------------

    private void connectWithBeacon() {
        if (minewPatientBeacon == null) {
            Logger.error("Beacon not found yet, please scan for beacons first.");
            return;
        }

        beaconManager.connectWithBeacon(MinewPatientConnectedBeacon.class, minewPatientBeacon)
                .subscribe(new Subscriber<MinewPatientConnectedBeacon>() {
                    @Override
                    public void onCompleted() {
                        Logger.info();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.error();
                    }

                    @Override
                    public void onNext(MinewPatientConnectedBeacon connectedBeacon) {
                        if (connectedMinewBeacon == null) {
                            connectedMinewBeacon = connectedBeacon;
                        }

                        Logger.info("Connected with beacon: %s", connectedBeacon.getDetectedDevice().scanResult().macAddress());
                    }
                });
    }

    private void disconnectWithBeacon() {
        connectedMinewBeacon.disconnect().subscribe(new Observer<BluetoothConnectionResult>() {
            @Override
            public void onCompleted() {
                Logger.info();
            }

            @Override
            public void onError(Throwable e) {
                Logger.error();
            }

            @Override
            public void onNext(BluetoothConnectionResult bluetoothConnectionResult) {
                Logger.info("Disconnected from beacon: %s", bluetoothConnectionResult.macAddress());
                connectedMinewBeacon = null;
            }
        });
    }

    private void performBeaconConfiguration() {
        if (minewPatientBeacon != null) {
            performBeaconConfigurationBtn.setEnabled(false);

            beaconsConnecting = true;

            Logger.info("Beacon 2 custom identifier from ad packet: %s", minewPatientBeacon.customIdentifier());

            Logger.info("Connecting with beacon 2: %s", minewPatientBeacon.scanResult().macAddress());
            beaconManager.connectWithBeacon(MinewPatientConnectedBeacon.class, minewPatientBeacon)
                    .subscribe(connectedBeacon -> {
                        connectedMinewBeacon = connectedBeacon;

                        Logger.info("BEACON 2 CONNECTED");

                        Logger.info("BEACON 2 - READING Custom Identifier Value");
                        connectedMinewBeacon.readCustomIdentifier().subscribe(new Observer<Integer>() {
                            @Override
                            public void onCompleted() {
                                Logger.info();
                            }

                            @Override
                            public void onError(Throwable e) {
                                Logger.info();
                            }

                            @Override
                            public void onNext(Integer integer) {
                                Logger.info("BEACON 2 - READ Custom Identifier Value: %s", integer);
                                Logger.info("===============================================");
                            }
                        });

                        Logger.info("BEACON 2 - WRITING Custom Identifier Value: 1");
                        connectedMinewBeacon.writeCustomIdentifier(1).subscribe(new Observer<BluetoothIOResult>() {
                            @Override
                            public void onCompleted() {
                                Logger.info();
                            }

                            @Override
                            public void onError(Throwable e) {
                                Logger.info();
                            }

                            @Override
                            public void onNext(BluetoothIOResult bluetoothIOResult) {
                                Logger.info("BEACON 2 - WROTE Custom Identifier Value: %s", bluetoothIOResult.identifier());
                                Logger.info("===============================================");
                            }
                        });

                        Logger.info("BEACON 2 - READING Custom Identifier Value");
                        connectedMinewBeacon.readCustomIdentifier().subscribe(integer -> {
                            Logger.info("BEACON 2 - READ Custom Identifier Value: %s", integer);
                            Logger.info("===============================================");
                        });

                        Logger.info("BEACON 2 - WRITING Custom Identifier Value: 2");
                        connectedMinewBeacon.writeCustomIdentifier(2).subscribe(integer -> {
                            Logger.info("BEACON 2 - WROTE Custom Identifier Value: %s", integer);
                            Logger.info("===============================================");
                        });

                        Logger.info("BEACON 2 - READING Custom Identifier Value");
                        connectedMinewBeacon.readCustomIdentifier().subscribe(integer -> {
                            Logger.info("BEACON 2 - READ Custom Identifier Value: %s", integer);
                            Logger.info("===============================================");

                            Logger.info("Disconnecting...");
                            connectedMinewBeacon.disconnect()
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(bluetoothConnectionResult -> {
                                        Logger.info("Disconnected: %s", bluetoothConnectionResult.connection());
                                        performBeaconConfigurationBtn.setEnabled(true);
                                    }, throwable -> {
                                        throwable.printStackTrace();
                                        performBeaconConfigurationBtn.setEnabled(true);
                                    });
                        }, Throwable::printStackTrace);
                    }, Throwable::printStackTrace);
        }
    }

    private void showSettings() {
        startActivity(new Intent(this, BeaconManagementSamplePreferActivity.class));
    }
}
