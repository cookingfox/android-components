package com.cookingfox.android.beacon_management.sample;

import com.cookingfox.android.beacon_management.component.BeaconManagementComponent;
import com.cookingfox.android.core_component.impl.android.application.AndroidComponentApplication;

public class BeaconManagementSampleApp extends AndroidComponentApplication {

    @Override
    protected void onApplicationInitialized() {
        getComponentManager().loadComponent(BeaconManagementComponent.class);
    }

}
