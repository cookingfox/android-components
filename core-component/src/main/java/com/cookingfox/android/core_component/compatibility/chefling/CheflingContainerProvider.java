package com.cookingfox.android.core_component.compatibility.chefling;

import com.cookingfox.chefling.api.CheflingContainer;

import static com.cookingfox.guava_preconditions.Preconditions.checkNotNull;
import static com.cookingfox.guava_preconditions.Preconditions.checkState;

/**
 * Provides static access to a singleton Chefling container, so that it can be accessed from classes
 * which cannot add a constructor dependency on the container.
 */
public final class CheflingContainerProvider {

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    /**
     * Singleton class: construction prohibited.
     */
    private CheflingContainerProvider() {
        throw new AssertionError("Construction prohibited");
    }

    //----------------------------------------------------------------------------------------------
    // PROPERTIES
    //----------------------------------------------------------------------------------------------

    /**
     * Managed container instance.
     */
    protected static CheflingContainer container;

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    /**
     * Disposes the managed container instance.
     */
    public static void disposeContainer() {
        getContainer().disposeContainer();

        container = null;
    }

    /**
     * @return The managed container instance.
     */
    public static CheflingContainer getContainer() {
        return checkNotNull(container, "Container is not set");
    }

    /**
     * Sets the managed container instance.
     *
     * @param container The container instance to provide static access to.
     */
    public static void setContainer(CheflingContainer container) {
        checkState(CheflingContainerProvider.container == null, "Container is already set");

        CheflingContainerProvider.container = checkNotNull(container, "Container is null");
    }

}
