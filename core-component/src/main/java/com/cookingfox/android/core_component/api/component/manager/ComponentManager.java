package com.cookingfox.android.core_component.api.component.manager;

import com.cookingfox.android.core_component.api.component.Component;

/**
 * Load and manage components.
 */
public interface ComponentManager {

    /**
     * Returns whether the component is loaded.
     *
     * @param componentClass Class of the component.
     * @return Whether the component is loaded.
     */
    boolean isComponentLoaded(Class<? extends Component> componentClass);

    /**
     * Load a component. Calls {@link Component#loadComponent(ComponentManager)} and
     * {@link Component#registerComponent()}.
     *
     * @param componentClass Class of the component.
     */
    void loadComponent(Class<? extends Component> componentClass);

    /**
     * Unload all loaded components. Calls {@link Component#unregisterComponent()} for each.
     */
    void unloadAllComponents();

}
