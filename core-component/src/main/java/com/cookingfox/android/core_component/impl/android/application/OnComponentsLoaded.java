package com.cookingfox.android.core_component.impl.android.application;

import com.cookingfox.chefling.api.CheflingContainer;

/**
 * Listener interface for when the application's components have been loaded.
 */
public interface OnComponentsLoaded {

    /**
     * The application's components have been loaded.
     *
     * @param container The managed Chefling dependency injection container.
     */
    void onComponentsLoaded(CheflingContainer container);

}
