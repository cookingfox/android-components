package com.cookingfox.android.core_component.impl.component.manager;

import com.cookingfox.android.core_component.api.android.context.Environment;
import com.cookingfox.android.core_component.api.component.Component;
import com.cookingfox.android.core_component.api.component.config.ComponentCheflingConfig;
import com.cookingfox.android.core_component.api.component.manager.ComponentManager;
import com.cookingfox.android.core_component.api.component.manager.ComponentManagerException;
import com.cookingfox.android.prefer.api.pref.PrefGroup;
import com.cookingfox.android.prefer.api.pref.Prefs;
import com.cookingfox.android.prefer.api.prefer.Prefer;
import com.cookingfox.chefling.api.CheflingBuilder;
import com.cookingfox.chefling.api.CheflingContainer;
import com.cookingfox.lapasse.api.facade.Facade;

import java.util.LinkedHashSet;
import java.util.Set;

import static com.cookingfox.android.core_component.impl.component.manager.ComponentManagerHelper.createCheflingBuilderForEnvironment;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Implementation of {@link ComponentManager} using Android.
 */
public class AndroidComponentManager implements ComponentManager {

    /**
     * Dependency injection container.
     */
    protected final CheflingContainer container;

    /**
     * Current environment information.
     */
    protected Environment environment;

    /**
     * Components that have been loaded.
     */
    protected final Set<Class<? extends Component>> loadedComponents = new LinkedHashSet<>();

    /**
     * Prefer instance used for checking Pref groups.
     */
    protected Prefer prefer;

    /**
     * Components that are currently in the process of being loaded, tracked so that we can detect
     * circular component dependencies.
     */
    protected final Set<Class<? extends Component>> processingComponents = new LinkedHashSet<>();

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public AndroidComponentManager(CheflingContainer container) {
        this.container = checkNotNull(container, "Container can not be null");
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public boolean isComponentLoaded(Class<? extends Component> componentClass) {
        return loadedComponents.contains(checkNotNull(componentClass,
                "Component class can not be null"));
    }

    @Override
    public void loadComponent(Class<? extends Component> componentClass) {
        checkNotNull(componentClass, "Component class can not be null");

        if (isComponentLoaded(componentClass)) {
            // already loaded
            return;
        } else if (processingComponents.contains(componentClass)) {
            // detected circular dependency
            throw new ComponentManagerException("Circular component dependency on " +
                    componentClass.getName());
        }

        // start processing
        processingComponents.add(componentClass);

        // create, load, process component
        Component component = createAndLoadComponent(componentClass);

        try {
            processComponentInstance(component);
        } catch (Exception error) {
            throw new ComponentManagerException("Error during loading of component: " + component, error);
        }

        // loaded: store class reference
        loadedComponents.add(componentClass);

        // everything resolved: clear temp processing items
        processingComponents.clear();
    }

    @Override
    public void unloadAllComponents() {
        // unregister components
        for (Class<? extends Component> componentClass : loadedComponents) {
            Component component = container.getInstance(componentClass);

            // allow component to perform clean up operations
            component.unregisterComponent();

            // dispose the component's LaPasse facade
            component.getLaPasseFacade().dispose();
        }

        // clear loaded components
        loadedComponents.clear();
    }

    //----------------------------------------------------------------------------------------------
    // PROTECTED METHODS
    //----------------------------------------------------------------------------------------------

    /**
     * Fetches the Chefling config from the component and applies it to the Chefling container.
     *
     * @param component The created component.
     */
    protected void applyComponentCheflingConfig(Component component) {
        ComponentCheflingConfig componentConfig = component.getCheflingConfig();

        checkNotNull(componentConfig, "`Component#getCheflingConfig()` returned null");

        CheflingBuilder builder = createCheflingBuilderForEnvironment(componentConfig, getEnvironment());

        // apply component chefling config to current container
        builder.applyToContainer(container);
    }

    /**
     * Use the Chefling container to request an instance of the component class and then load it.
     *
     * @param componentClass The component class.
     * @return The created and loaded component.
     */
    protected Component createAndLoadComponent(Class<? extends Component> componentClass) {
        Component component;

        try {
            // use container to instantiate component
            component = container.getInstance(componentClass);
        } catch (Exception error) {
            throw new ComponentManagerException("Error during instantiation of component: " +
                    componentClass.getName(), error);
        }

        try {
            // load component
            component.loadComponent(this);
        } catch (Exception error) {
            throw new ComponentManagerException("Error during `loadComponent()` method of component: " +
                    componentClass.getName(), error);
        }

        return component;
    }

    /**
     * @return Returns current environment.
     */
    protected Environment getEnvironment() {
        return environment != null ? environment : container.getInstance(Environment.class);
    }

    /**
     * @return Returns the saved Prefer instance or requests it using the Chefling container.
     */
    protected Prefer getPrefer() {
        if (prefer == null) {
            prefer = container.getInstance(Prefer.class);
        }

        return prefer;
    }

    /**
     * Process a created and loaded component instance.
     *
     * @param component The component instance.
     * @throws RuntimeException when an error occurs.
     */
    protected void processComponentInstance(Component component) {
        applyComponentCheflingConfig(component);
        validateComponentLaPasseFacade(component);
        validateAndAddComponentPrefs(component);
        registerComponent(component);
    }

    /**
     * Allow the component to register with the application.
     *
     * @param component The component instance.
     */
    protected void registerComponent(Component component) {
        component.registerComponent();
    }

    /**
     * Validate the component returns a valid LaPasse {@link Facade} instance.
     *
     * @param component The component instance.
     */
    protected void validateComponentLaPasseFacade(Component component) {
        checkNotNull(component.getLaPasseFacade(), "`Component#getLaPasseFacade()` returned null");
    }

    /**
     * Make sure the component returns a valid Prefer {@link Prefs} instance and add it to Prefer
     * if necessary.
     *
     * @param component The component instance.
     */
    protected void validateAndAddComponentPrefs(Component component) {
        Prefs prefs = checkNotNull(component.getPrefs(), "`Component#getPrefs()` returned null");
        PrefGroup prefGroup = prefs.getPrefGroup();
        Prefer prefer = getPrefer();

        checkNotNull(prefGroup, "`Prefs#getPrefGroup()` returned null");

        // pref group not added to prefer yet? add it now
        if (prefer.findGroup(prefGroup.getKeyClass()) == null) {
            prefer.addGroup(prefGroup);
        }
    }

}
