package com.cookingfox.android.core_component.compatibility.guava.collect;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimaps;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Static utility methods pertaining to {@link Map} instances, inspired by Google Guava's
 * {@link Maps}.
 */
public final class Maps2 {

    /**
     * Similar to {@link Maps#uniqueIndex(Iterable, Function)}, but does not throw if
     * {@code keyFunction} produces the same key for more than one value in the input collection.
     * Instead, the value is overwritten.
     * <p>
     * Returns a map with the given {@code values}, indexed by keys derived from
     * those values. In other words, each input value produces an entry in the map
     * whose key is the result of applying {@code keyFunction} to that value.
     * These entries appear in the same order as the input values. Example usage:
     * <pre>   {@code
     *
     *   Color red = new Color("red", 255, 0, 0);
     *   ...
     *   ImmutableSet<Color> allColors = ImmutableSet.of(red, green, blue);
     *
     *   Map<String, Color> colorForName =
     *       index(allColors, toStringFunction());
     *   assertThat(colorForName).containsEntry("red", red);}</pre>
     * <p>
     * <p>If your index may associate multiple values with each key, use {@link
     * Multimaps#index(Iterable, Function) Multimaps.index}.
     *
     * @param values      the values to use when constructing the {@code Map}
     * @param keyFunction the function used to produce the key for each value
     * @return a map mapping the result of evaluating the function {@code
     * keyFunction} on each value in the input collection to that value
     * @throws NullPointerException if any elements of {@code values} is null, or
     *                              if {@code keyFunction} produces {@code null} for any value
     */
    public static <K, V> Map<K, V> index(
            Iterable<V> values, Function<? super V, K> keyFunction) {
        return index(values.iterator(), keyFunction);
    }

    /**
     * Similar to {@link Maps#uniqueIndex(Iterable, Function)}, but does not throw if
     * {@code keyFunction} produces the same key for more than one value in the input collection.
     * Instead, the value is overwritten.
     * <p>
     * Returns a map with the given {@code values}, indexed by keys derived from
     * those values. In other words, each input value produces an entry in the map
     * whose key is the result of applying {@code keyFunction} to that value.
     * These entries appear in the same order as the input values. Example usage:
     * <pre>   {@code
     *
     *   Color red = new Color("red", 255, 0, 0);
     *   ...
     *   Iterator<Color> allColors = ImmutableSet.of(red, green, blue).iterator();
     *
     *   Map<String, Color> colorForName =
     *       index(allColors, toStringFunction());
     *   assertThat(colorForName).containsEntry("red", red);}</pre>
     * <p>
     * <p>If your index may associate multiple values with each key, use {@link
     * Multimaps#index(Iterator, Function) Multimaps.index}.
     *
     * @param values      the values to use when constructing the {@code Map}
     * @param keyFunction the function used to produce the key for each value
     * @return a map mapping the result of evaluating the function {@code
     * keyFunction} on each value in the input collection to that value
     * @throws NullPointerException if any elements of {@code values} is null, or
     *                              if {@code keyFunction} produces {@code null} for any value
     * @since 10.0
     */
    public static <K, V> Map<K, V> index(Iterator<V> values, Function<? super V, K> keyFunction) {
        checkNotNull(keyFunction);
        Map<K, V> map = new LinkedHashMap<>();
        while (values.hasNext()) {
            V value = values.next();
            map.put(keyFunction.apply(value), value);
        }
        return ImmutableMap.<K, V>builder().putAll(map).build();
    }

    /**
     * Creates a sorted map ({@link TreeMap}) from the provided values.
     *
     * @param values      the values to use when constructing the {@code Map}
     * @param keyFunction the function used to produce the key for each value
     * @return a map mapping the result of evaluating the function {@code
     * keyFunction} on each value in the input collection to that value
     * @throws NullPointerException if any elements of {@code values} is null, or
     *                              if {@code keyFunction} produces {@code null} for any value
     */
    public static <K, V> Map<K, V> indexTree(Iterable<V> values, Function<? super V, K> keyFunction) {
        return indexTree(values.iterator(), keyFunction);
    }

    /**
     * Creates a sorted map ({@link TreeMap}) from the provided values.
     *
     * @param values      the values to use when constructing the {@code Map}
     * @param keyFunction the function used to produce the key for each value
     * @return a map mapping the result of evaluating the function {@code
     * keyFunction} on each value in the input collection to that value
     * @throws NullPointerException if any elements of {@code values} is null, or
     *                              if {@code keyFunction} produces {@code null} for any value
     */
    public static <K, V> Map<K, V> indexTree(Iterator<V> values, Function<? super V, K> keyFunction) {
        checkNotNull(keyFunction);
        Map<K, V> map = new TreeMap<>();
        while (values.hasNext()) {
            V value = values.next();
            map.put(keyFunction.apply(value), value);
        }
        return ImmutableMap.<K, V>builder().putAll(map).build();
    }

}
