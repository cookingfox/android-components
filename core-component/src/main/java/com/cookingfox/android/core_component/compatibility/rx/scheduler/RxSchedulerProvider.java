package com.cookingfox.android.core_component.compatibility.rx.scheduler;

import java.util.concurrent.Executor;

import rx.Observable;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func0;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.schedulers.TestScheduler;

/**
 * Provides access to Rx Schedulers. This interface allows us to mock or change the implementation,
 * for example by using a {@link TestScheduler}.
 */
public interface RxSchedulerProvider {

    /**
     * @return Scheduler for a dedicated {@link Executor}, defined by the implementation.
     * @see Schedulers#from(Executor)
     */
    Scheduler dedicatedExecutorScheduler();

    /**
     * @return Scheduler for I/O operations.
     * @see Schedulers#io()
     */
    Scheduler ioScheduler();

    /**
     * @return Scheduler for the Android main thread.
     * @see AndroidSchedulers#mainThread()
     */
    Scheduler mainThreadScheduler();

    /**
     * @return Scheduler for timing operations, such as {@link Observable#delay(Func0, Func1)}
     * @see Schedulers#computation()
     */
    Scheduler timingScheduler();

}
