package com.cookingfox.android.core_component.api.component;

import com.cookingfox.android.core_component.api.component.config.ComponentCheflingConfig;
import com.cookingfox.android.core_component.api.component.manager.ComponentManager;
import com.cookingfox.android.prefer.api.pref.Prefs;
import com.cookingfox.lapasse.api.facade.Facade;

/**
 * Represents a component which contains methods for configuration and initialization.
 */
public interface Component {

    /**
     * @return The Chefling configuration for this component.
     */
    ComponentCheflingConfig getCheflingConfig();

    /**
     * @return The LaPasse facade for this component.
     */
    Facade getLaPasseFacade();

    /**
     * @return The Prefer Prefs for this component.
     */
    Prefs getPrefs();

    /**
     * Load the component and use the component manager to loadComponent other components.
     *
     * @param componentManager The manager that loaded this component.
     */
    void loadComponent(ComponentManager componentManager);

    /**
     * Register the component with the application.
     */
    void registerComponent();

    /**
     * Unregister the component with the application.
     */
    void unregisterComponent();

}
