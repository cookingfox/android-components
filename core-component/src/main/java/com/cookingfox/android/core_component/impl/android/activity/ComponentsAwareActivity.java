package com.cookingfox.android.core_component.impl.android.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import com.cookingfox.android.core_component.impl.android.application.ComponentsLoadedReceiver;
import com.cookingfox.android.core_component.impl.android.application.OnComponentsLoaded;
import com.cookingfox.chefling.api.CheflingContainer;

/**
 * Base activity class that uses a {@link BroadcastReceiver} to monitor the components lifecycle.
 */
public class ComponentsAwareActivity extends Activity implements OnComponentsLoaded {

    //----------------------------------------------------------------------------------------------
    // PROPERTIES
    //----------------------------------------------------------------------------------------------

    /**
     * Broadcast receiver for when the components have been loaded.
     */
    final ComponentsLoadedReceiver componentsLoadedReceiver = new ComponentsLoadedReceiver(this);

    //----------------------------------------------------------------------------------------------
    // ACTIVITY LIFECYCLE
    //----------------------------------------------------------------------------------------------

    @CallSuper
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(componentsLoadedReceiver, ComponentsLoadedReceiver.INTENT_FILTER);

        // must be after `registerReceiver()`, to avoid race condition (broadcast sent before
        // receiver is registered)
        super.onCreate(savedInstanceState);
    }

    @CallSuper
    @Override
    protected void onStop() {
        super.onStop();

        if (isFinishing()) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(componentsLoadedReceiver);
        }
    }

    //----------------------------------------------------------------------------------------------
    // COMPONENTS LIFECYCLE
    //----------------------------------------------------------------------------------------------

    @Override
    public void onComponentsLoaded(CheflingContainer container) {
        // override in subclass
    }

}
