package com.cookingfox.android.core_component.compatibility.immutables;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import org.immutables.value.Value;

/**
 * Default {@link Value} style settings. This style is only suitable for interface-based Immutables,
 * for abstract class Immutables a custom style should be applied.
 */
@JsonSerialize
@Value.Style(
        // necessary for builder class in interface
        builderVisibility = Value.Style.BuilderVisibility.PACKAGE,
        visibility = Value.Style.ImplementationVisibility.PACKAGE
)
public @interface DefaultValueStyle {
}
