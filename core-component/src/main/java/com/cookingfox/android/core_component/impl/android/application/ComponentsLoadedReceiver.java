package com.cookingfox.android.core_component.impl.android.application;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.cookingfox.android.core_component.compatibility.chefling.CheflingContainerProvider;
import com.cookingfox.chefling.api.CheflingContainer;

import static com.cookingfox.android.core_component.impl.android.application.AndroidComponentApplication.ACTION_COMPONENTS_LOADED;
import static com.cookingfox.guava_preconditions.Preconditions.checkNotNull;

/**
 * Helper class for receiving the components loaded broadcast.
 */
public class ComponentsLoadedReceiver extends BroadcastReceiver implements OnComponentsLoaded {

    //----------------------------------------------------------------------------------------------
    // CONSTANTS
    //----------------------------------------------------------------------------------------------

    /**
     * Add this intent filter when registering this receiver.
     */
    public static final IntentFilter INTENT_FILTER =
            new IntentFilter(AndroidComponentApplication.ACTION_COMPONENTS_LOADED);

    //----------------------------------------------------------------------------------------------
    // PROPERTIES
    //----------------------------------------------------------------------------------------------

    /**
     * Optional listener that will be called in {@link #onComponentsLoaded(CheflingContainer)} if
     * provided.
     */
    private final OnComponentsLoaded listener;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTORS
    //----------------------------------------------------------------------------------------------

    public ComponentsLoadedReceiver() {
        listener = null;
    }

    /**
     * Create the receiver with a specific listener implementation.
     *
     * @param listener The components loaded listener.
     */
    public ComponentsLoadedReceiver(OnComponentsLoaded listener) {
        this.listener = checkNotNull(listener, "Listener cannot be null");
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public void onComponentsLoaded(CheflingContainer container) {
        // has listener? trigger now
        if (listener != null) {
            listener.onComponentsLoaded(container);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // incorrect intent? skip
        if (intent == null || !ACTION_COMPONENTS_LOADED.equals(intent.getAction())) {
            return;
        }

        onComponentsLoaded(CheflingContainerProvider.getContainer());
    }

}
