package com.cookingfox.android.core_component.testing.android.context;

import android.app.AlarmManager;
import android.app.admin.DevicePolicyManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;

import com.cookingfox.android.core_component.api.android.context.AppContext;

/**
 * No-operation implementation of {@link AppContext} which can be used for testing.
 */
public class TestAppContext implements AppContext {

    @Override
    public boolean bindService(Intent service, ServiceConnection conn, int flags) {
        return false;
    }

    @Override
    public AlarmManager getAlarmManager() {
        return null;
    }

    @Override
    public Context getApplicationContext() {
        return null;
    }

    @Override
    public AssetManager getAssetManager() {
        return null;
    }

    @Override
    public AudioManager getAudioManager() {
        return null;
    }

    @Override
    public BluetoothAdapter getBluetoothAdapter() {
        return null;
    }

    @Override
    public BluetoothManager getBluetoothManager() {
        return null;
    }

    @Override
    public ConnectivityManager getConnectivityManager() {
        return null;
    }

    @Override
    public DevicePolicyManager getDevicePolicyManager() {
        return null;
    }

    @Override
    public PackageManager getPackageManager() {
        return null;
    }

    @Override
    public SharedPreferences getSharedPreferences() {
        return null;
    }

    @Override
    public WifiManager getWifiManager() {
        return null;
    }

    @Override
    public Intent registerReceiver(BroadcastReceiver receiver, IntentFilter filter) {
        return null;
    }

    @Override
    public void sendBroadcast(Intent intent) {
    }

    @Override
    public ComponentName startService(Intent service) {
        return null;
    }

    @Override
    public boolean stopService(Intent service) {
        return false;
    }

    @Override
    public void unbindService(ServiceConnection conn) {
    }

    @Override
    public void unregisterReceiver(BroadcastReceiver receiver) {
    }

}
