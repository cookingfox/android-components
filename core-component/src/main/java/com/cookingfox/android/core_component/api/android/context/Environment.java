package com.cookingfox.android.core_component.api.android.context;

/**
 * Provides information about the current environment.
 */
public interface Environment {

    /**
     * Environment identifier.
     */
    enum Id {
        Device,
        Emulator,
        Test,
    }

    /**
     * @return Environment identifier.
     */
    Id getId();

    /**
     * @return Whether the current environment is an actual device.
     */
    boolean isDevice();

    /**
     * @return Whether the current environment is an emulator.
     */
    boolean isEmulator();

    /**
     * @return Whether the current environment is a unit test.
     */
    boolean isTest();

}
