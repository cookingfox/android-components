package com.cookingfox.android.core_component.impl.android.context;

import android.os.Build;

import com.cookingfox.android.core_component.api.android.context.Environment;

/**
 * Singleton Android implementation of {@link Environment}.
 */
public enum AndroidEnvironment implements Environment {

    INSTANCE;

    final Id id;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    AndroidEnvironment() {
        if (Build.FINGERPRINT == null) {
            id = Id.Test;
        } else if (buildIsEmulator()) {
            id = Id.Emulator;
        } else {
            id = Id.Device;
        }
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public Id getId() {
        return id;
    }

    @Override
    public boolean isDevice() {
        return id == Id.Device;
    }

    @Override
    public boolean isEmulator() {
        return id == Id.Emulator;
    }

    @Override
    public boolean isTest() {
        return id == Id.Test;
    }

    //----------------------------------------------------------------------------------------------
    // PRIVATE METHODS
    //----------------------------------------------------------------------------------------------

    /**
     * @return Whether the Android build is for an emulator.
     */
    static boolean buildIsEmulator() {
        return Build.FINGERPRINT.startsWith("generic")
                || Build.FINGERPRINT.startsWith("unknown")
                || Build.MODEL.contains("google_sdk")
                || Build.MODEL.contains("Emulator")
                || Build.MODEL.contains("Android SDK built for x86")
                || Build.MANUFACTURER.contains("Genymotion")
                || (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"))
                || "google_sdk".equals(Build.PRODUCT);
    }

    //----------------------------------------------------------------------------------------------
    // OBJECT OVERRIDES
    //----------------------------------------------------------------------------------------------

    @Override
    public String toString() {
        return "AndroidEnvironment{" +
                "id=" + id +
                '}';
    }

}
