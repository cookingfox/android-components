package com.cookingfox.android.core_component.testing.component.manager;

import com.cookingfox.android.core_component.api.component.Component;
import com.cookingfox.android.core_component.api.component.manager.ComponentManager;
import com.cookingfox.android.core_component.impl.component.config.CoreComponentCheflingConfig;
import com.cookingfox.android.core_component.impl.component.manager.AndroidComponentManager;
import com.cookingfox.chefling.api.CheflingContainer;

/**
 * Component manager implementation which contains extra functionality that can be used for testing.
 * Important: the {@link #registerComponent(Component)} method is overridden, so that the
 * {@link Component#registerComponent()} is not called. This makes sure that we can test the
 * registration process separately.
 */
public class TestComponentManager extends AndroidComponentManager {

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public TestComponentManager(CheflingContainer container) {
        super(container);
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC STATIC METHODS
    //----------------------------------------------------------------------------------------------

    /**
     * Build a component for the provided class and return the container which built it. Note that
     * this does NOT call {@link Component#registerComponent()}, so you will have to do that
     * yourself in the unit test.
     *
     * @param componentClass The class for the component which should be built.
     * @return The Chefling container which built the component.
     */
    public static CheflingContainer buildComponent(Class<? extends Component> componentClass) {
        CheflingContainer container = buildContainer();

        getComponentManager(container).loadComponent(componentClass);

        return container;
    }

    /**
     * Builds a new Chefling container which has the necessary mappings for the core component.
     *
     * @return The created Chefling container.
     */
    public static CheflingContainer buildContainer() {
        CoreComponentCheflingConfig config = new CoreComponentCheflingConfig();

        // create container builder using config
        return config.createCheflingBuilder()
                // add test environment config
                .addConfig(config.createTestConfig())
                // override existing mappings for testing purpose
                .addConfig(c -> {
                    // replace component manager mapping with custom implementation (this class)
                    c.removeInstanceAndMapping(ComponentManager.class);
                    c.mapType(ComponentManager.class, TestComponentManager.class);
                })
                // build the container
                .buildContainer();
    }

    /**
     * Get the component manager instance from the provided container.
     *
     * @param container The container to get the instance from.
     * @return The component manager.
     */
    public static ComponentManager getComponentManager(CheflingContainer container) {
        return container.getInstance(ComponentManager.class);
    }

    //----------------------------------------------------------------------------------------------
    // PROTECTED METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    protected void registerComponent(Component component) {
        // IMPORTANT: disables automatic component registration, so that it can be invoked separately
    }

}
