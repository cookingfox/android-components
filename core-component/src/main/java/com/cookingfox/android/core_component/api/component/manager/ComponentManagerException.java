package com.cookingfox.android.core_component.api.component.manager;

/**
 * Exception that is thrown by {@link ComponentManager}.
 */
public class ComponentManagerException extends RuntimeException {

    public ComponentManagerException(String detailMessage) {
        super(detailMessage);
    }

    public ComponentManagerException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

}
