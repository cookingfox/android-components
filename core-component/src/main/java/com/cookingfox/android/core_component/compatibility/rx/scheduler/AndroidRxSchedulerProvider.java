package com.cookingfox.android.core_component.compatibility.rx.scheduler;

import com.cookingfox.android.core_component.impl.threading.ComponentThreadPoolProvider;

import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Default implementation of scheduler provider, which wraps RxAndroid's {@link AndroidSchedulers}.
 */
public class AndroidRxSchedulerProvider implements RxSchedulerProvider {

    private final ComponentThreadPoolProvider threadPoolProvider;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public AndroidRxSchedulerProvider(ComponentThreadPoolProvider threadPoolProvider) {
        this.threadPoolProvider = threadPoolProvider;
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public Scheduler dedicatedExecutorScheduler() {
        return Schedulers.from(threadPoolProvider.getExecutor());
    }

    @Override
    public Scheduler ioScheduler() {
        return Schedulers.io();
    }

    @Override
    public Scheduler mainThreadScheduler() {
        return AndroidSchedulers.mainThread();
    }

    @Override
    public Scheduler timingScheduler() {
        return Schedulers.computation();
    }

}
