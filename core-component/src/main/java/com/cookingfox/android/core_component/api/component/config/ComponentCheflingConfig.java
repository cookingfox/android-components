package com.cookingfox.android.core_component.api.component.config;

import com.cookingfox.android.core_component.api.component.manager.ComponentManager;
import com.cookingfox.chefling.api.CheflingBuilder;
import com.cookingfox.chefling.api.CheflingConfig;

/**
 * Interface for a component's Chefling container configuration. The dedicated methods allow the
 * {@link ComponentManager} to decide which methods to call, depending on the current environment.
 */
public interface ComponentCheflingConfig {

    /**
     * @return A new Chefling container builder, which contains the necessary mappings for this
     * component.
     */
    CheflingBuilder createCheflingBuilder();

    /**
     * @return The Chefling configuration that is used on an actual Android device.
     */
    CheflingConfig createDeviceConfig();

    /**
     * @return The Chefling configuration that is used on an Android emulator.
     */
    CheflingConfig createEmulatorConfig();

    /**
     * @return The Chefling configuration that is used for unit tests.
     */
    CheflingConfig createTestConfig();

}
