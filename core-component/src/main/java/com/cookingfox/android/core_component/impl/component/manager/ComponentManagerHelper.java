package com.cookingfox.android.core_component.impl.component.manager;

import com.cookingfox.android.core_component.api.android.context.Environment;
import com.cookingfox.android.core_component.api.component.config.ComponentCheflingConfig;
import com.cookingfox.android.core_component.api.component.manager.ComponentManager;
import com.cookingfox.android.core_component.impl.android.context.AndroidEnvironment;
import com.cookingfox.chefling.api.CheflingBuilder;
import com.cookingfox.chefling.api.CheflingConfig;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Static helper methods for the {@link ComponentManager}.
 */
public final class ComponentManagerHelper {

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    private ComponentManagerHelper() {
        throw new UnsupportedOperationException();
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC STATIC METHODS
    //----------------------------------------------------------------------------------------------

    /**
     * Creates a new Chefling builder using the provided component config and applies the
     * configuration as determined by the provided environment.
     *
     * @param config      The component Chefling configuration.
     * @param environment The environment for which to configure the builder.
     * @return The created container builder.
     */
    public static CheflingBuilder createCheflingBuilderForEnvironment(ComponentCheflingConfig config, Environment environment) {
        CheflingBuilder builder = config.createCheflingBuilder();

        checkNotNull(builder, "`ComponentCheflingConfig#createCheflingBuilder()` returned null");

        CheflingConfig environmentConfig = null;

        // get the configuration for this environment
        switch (environment.getId()) {
            case Device:
                environmentConfig = config.createDeviceConfig();
                break;
            case Emulator:
                environmentConfig = config.createEmulatorConfig();
                break;
            case Test:
                environmentConfig = config.createTestConfig();
                break;
        }

        // add the configuration to the builder
        if (environmentConfig != null) {
            builder.addConfig(environmentConfig);
        }

        return builder;
    }

    /**
     * @return An instance of the default environment implementation.
     */
    public static Environment getDefaultEnvironment() {
        return AndroidEnvironment.INSTANCE;
    }

}
