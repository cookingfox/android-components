package com.cookingfox.android.core_component.testing.compatibility.rx.scheduler;

import com.cookingfox.android.core_component.compatibility.rx.scheduler.RxSchedulerProvider;

import rx.Scheduler;
import rx.schedulers.TestScheduler;

/**
 * Implementation of scheduler provider which uses a single {@link TestScheduler}.
 */
public class TestRxSchedulerProvider implements RxSchedulerProvider {

    private final TestScheduler testScheduler;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public TestRxSchedulerProvider(TestScheduler testScheduler) {
        this.testScheduler = testScheduler;
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public Scheduler dedicatedExecutorScheduler() {
        return testScheduler;
    }

    @Override
    public Scheduler ioScheduler() {
        return testScheduler;
    }

    @Override
    public Scheduler mainThreadScheduler() {
        return testScheduler;
    }

    @Override
    public Scheduler timingScheduler() {
        return testScheduler;
    }

}
