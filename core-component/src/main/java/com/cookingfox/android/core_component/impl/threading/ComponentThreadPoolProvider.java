package com.cookingfox.android.core_component.impl.threading;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Provides components with access to a managed thread pool, through an {@link ExecutorService}.
 */
public class ComponentThreadPoolProvider {

    /**
     * The provided executor service.
     */
    protected final ExecutorService executorService;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public ComponentThreadPoolProvider(ExecutorService executorService) {
        this.executorService = checkNotNull(executorService, "Executor service can not be null");
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    /**
     * @return The managed executor service.
     */
    public Executor getExecutor() {
        return executorService;
    }

    /**
     * Shuts down the executor service (if it hasn't already).
     */
    public void shutdown() {
        if (!executorService.isShutdown()) {
            executorService.shutdown();
        }
    }

}
