package com.cookingfox.android.core_component.api.android.context;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.admin.DevicePolicyManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.preference.PreferenceManager;

/**
 * Wrapper for common Android context operations.
 */
public interface AppContext {

    /**
     * @see Context#bindService(Intent, ServiceConnection, int)
     */
    boolean bindService(Intent service, ServiceConnection conn, int flags);

    /**
     * @see Context#getSystemService(String)
     * @see Context#ALARM_SERVICE
     */
    AlarmManager getAlarmManager();

    /**
     * @see Context#getApplicationContext()
     */
    Context getApplicationContext();

    /**
     * @see Context#getAssets()
     */
    AssetManager getAssetManager();

    /**
     * @see Context#getSystemService(String)
     * @see Context#AUDIO_SERVICE
     */
    AudioManager getAudioManager();

    /**
     * @see BluetoothAdapter#getDefaultAdapter()
     */
    BluetoothAdapter getBluetoothAdapter();

    /**
     * @see Context#getSystemService(String)
     * @see Context#BLUETOOTH_SERVICE
     */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    BluetoothManager getBluetoothManager();

    /**
     * @see Context#getSystemService(String)
     * @see Context#CONNECTIVITY_SERVICE
     */
    ConnectivityManager getConnectivityManager();

    /**
     * @see Context#getSystemService(String)
     * @see Context#DEVICE_POLICY_SERVICE
     */
    DevicePolicyManager getDevicePolicyManager();

    /**
     * @see Context#getPackageManager()
     */
    PackageManager getPackageManager();

    /**
     * @see PreferenceManager#getDefaultSharedPreferences(Context)
     */
    SharedPreferences getSharedPreferences();

    /**
     * @see Context#getSystemService(String)
     * @see Context#WIFI_SERVICE
     */
    WifiManager getWifiManager();

    /**
     * @see Context#registerReceiver(BroadcastReceiver, IntentFilter)
     */
    Intent registerReceiver(BroadcastReceiver receiver, IntentFilter filter);

    /**
     * @see Context#sendBroadcast(Intent)
     */
    void sendBroadcast(Intent intent);

    /**
     * @see Context#startService(Intent)
     */
    ComponentName startService(Intent service);

    /**
     * @see Context#stopService(Intent)
     */
    boolean stopService(Intent service);

    /**
     * @see Context#unbindService(ServiceConnection)
     */
    void unbindService(ServiceConnection conn);

    /**
     * @see Context#unregisterReceiver(BroadcastReceiver)
     */
    void unregisterReceiver(BroadcastReceiver receiver);

}
