package com.cookingfox.android.core_component.compatibility.lapasse.logger;

import com.cookingfox.lapasse.api.command.Command;
import com.cookingfox.lapasse.api.event.Event;
import com.cookingfox.lapasse.api.logging.CombinedLogger;
import com.cookingfox.lapasse.api.state.State;
import com.cookingfox.lapasse.impl.logging.DefaultLogger;
import com.cookingfox.logging.Logger;

import java.util.Collection;

/**
 * Implementation of {@link CombinedLogger} that uses the Cooking Fox {@link Logger}.
 *
 * @param <S> The concrete type of the state object.
 */
public class CookingFoxLapasseLogger<S extends State> extends DefaultLogger<S> {

    @Override
    public void onCommandHandlerError(Throwable error, Command command) {
        Logger.error("error: %s | command: %s", error, command);
        error.printStackTrace();
    }

    @Override
    public void onCommandHandlerResult(Command command, Collection<Event> events) {
        Logger.debug("command: %s | events: %s", command, events);
    }

    @Override
    public void onEventHandlerError(Throwable error, Event event) {
        Logger.error("error: %s | event: %s", error, event);
        error.printStackTrace();
    }

    @Override
    public void onEventHandlerResult(Event event, S newState) {
        Logger.debug("event: %s | new state: %s", event, newState);
    }

}
