package com.cookingfox.android.core_component.impl.android.application;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.support.annotation.CallSuper;
import android.support.v4.content.LocalBroadcastManager;

import com.cookingfox.android.app_lifecycle.api.listener.AppLifecycleListenable;
import com.cookingfox.android.app_lifecycle.api.listener.AppLifecycleListener;
import com.cookingfox.android.app_lifecycle.impl.AppLifecycleProvider;
import com.cookingfox.android.app_lifecycle.impl.listener.PersistentAppLifecycleListener;
import com.cookingfox.android.core_component.api.component.manager.ComponentManager;
import com.cookingfox.android.core_component.compatibility.chefling.CheflingContainerProvider;
import com.cookingfox.android.core_component.impl.component.config.CoreComponentCheflingConfig;
import com.cookingfox.chefling.api.CheflingBuilder;
import com.cookingfox.chefling.api.CheflingConfig;
import com.cookingfox.chefling.api.CheflingContainer;
import com.cookingfox.logging.Logger;
import com.cookingfox.logging.adapter.AndroidLoggerAdapter;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.cookingfox.android.core_component.impl.component.manager.ComponentManagerHelper.createCheflingBuilderForEnvironment;
import static com.cookingfox.android.core_component.impl.component.manager.ComponentManagerHelper.getDefaultEnvironment;
import static com.cookingfox.guava_preconditions.Preconditions.checkNotNull;
import static com.cookingfox.guava_preconditions.Preconditions.checkState;

/**
 * Implementation of Android {@link Application} class with support for the component-based
 * architecture and libraries.
 */
public class AndroidComponentApplication extends Application {

    private static final String TAG = AndroidComponentApplication.class.getName();

    /**
     * Register a {@link BroadcastReceiver} (e.g. in activity create) for this action to be notified
     * of when the components are loaded and its parts (e.g. container) are available.
     */
    public static final String ACTION_COMPONENTS_LOADED = TAG + ".ACTION_COMPONENTS_LOADED";

    //----------------------------------------------------------------------------------------------
    // PROPERTIES
    //----------------------------------------------------------------------------------------------

    /**
     * Android application reference.
     */
    protected Application app;

    /**
     * App lifecycle reference, so it can be mapped to the container.
     */
    protected AppLifecycleListenable appLifecycle;

    /**
     * The component app's Chefling container.
     */
    protected CheflingContainer container;

    /**
     * Executor service to perform component initialization and disposal off the main thread.
     */
    protected ExecutorService executorService;

    //----------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------------

    public AndroidComponentApplication() {
        super();
    }

    public AndroidComponentApplication(Application app) {
        this.app = checkNotNull(app, "Android `Application` can not be null");
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    @CallSuper
    @Override
    public void onCreate() {
        super.onCreate();

        // set Android `Application` reference to self
        app = this;

        // initialize
        create(this);
    }

    //----------------------------------------------------------------------------------------------
    // PUBLIC STATIC METHODS
    //----------------------------------------------------------------------------------------------

    /**
     * Create and initialize a new Android component application.
     *
     * @param app Reference to the real Android application instance.
     * @return The initialized component application.
     */
    public static AndroidComponentApplication create(Application app) {
        checkNotNull(app, "Application instance can not be null");

        AndroidComponentApplication componentApp;

        if (app instanceof AndroidComponentApplication) {
            componentApp = (AndroidComponentApplication) app;

            checkState(componentApp.app != null, "`AndroidComponentApplication` must be provided " +
                    "with a valid Android `Application` instance");
        } else {
            componentApp = new AndroidComponentApplication(app);
        }

        componentApp.initializeCore();

        return componentApp;
    }

    //----------------------------------------------------------------------------------------------
    // PROTECTED METHODS
    //----------------------------------------------------------------------------------------------

    /**
     * @return Chefling container builder, configured for component manager.
     */
    protected CheflingBuilder createCheflingContainerBuilder() {
        CoreComponentCheflingConfig config = new CoreComponentCheflingConfig();
        config.setContext(app);

        return createCheflingBuilderForEnvironment(config, getDefaultEnvironment())
                .addConfig(appCheflingConfig);
    }

    /**
     * Dispose the component application.
     */
    protected void disposeComponentApplication() {
        // dispose static dependencies
        disposeStaticDependencies();

        // unload all components
        getComponentManager().unloadAllComponents();

        // allow sub class to hook into destruction process
        onApplicationDisposed();
    }

    /**
     * Hook for disposing static dependencies.
     */
    protected void disposeStaticDependencies() {
        // dependency injection container
        CheflingContainerProvider.disposeContainer();
    }

    /**
     * @return The Chefling-provided component manager.
     */
    protected ComponentManager getComponentManager() {
        return getCheflingContainer().getInstance(ComponentManager.class);
    }

    /**
     * @return The Chefling container.
     */
    protected CheflingContainer getCheflingContainer() {
        return container;
    }

    /**
     * Initialize the core requirements to get started.
     */
    protected void initializeCore() {
        // initialize CF logger
        initializeLogger();

        // initialize app lifecycle manager and add listener
        appLifecycle = AppLifecycleProvider.initialize(app).addListener(appLifecycleListener);
    }

    /**
     * Initialize the component application.
     */
    protected void initializeComponentApplication() {
        // create dependency injection container
        container = createCheflingContainerBuilder().buildContainer();

        // initialize static dependencies
        initializeStaticDependencies();

        // allow sub class to hook into initialization process
        onApplicationInitialized();

        // notify interested parties that components are loaded
        LocalBroadcastManager.getInstance(app).sendBroadcast(new Intent(ACTION_COMPONENTS_LOADED));
    }

    /**
     * Initializes the Cooking Fox Logger.
     */
    protected void initializeLogger() {
        Logger.init()
                .callerAddLineNumber(true)
                .callerAddMethodName(true)
                .callerUseSimpleName(true)
                .setEnabled(true)
                .setLoggerAdapter(new AndroidLoggerAdapter());
    }

    /**
     * Hook for initializing static dependencies.
     */
    protected void initializeStaticDependencies() {
        // java8 date time backport
        AndroidThreeTen.init(app);

        // dependency injection container
        CheflingContainerProvider.setContainer(getCheflingContainer());
    }

    /**
     * Hook for when the application has been disposed.
     *
     * @see #appLifecycleListener
     */
    protected void onApplicationDisposed() {
        // override in subclass
    }

    /**
     * Hook for when the Android application has been initialized. This is where the components
     * should be loaded.
     *
     * @see #appLifecycleListener
     */
    protected void onApplicationInitialized() {
        // override in subclass
    }

    //----------------------------------------------------------------------------------------------
    // MEMBER CLASSES
    //----------------------------------------------------------------------------------------------

    /**
     * Additional configuration of the Chefling container that is necessary for the Android
     * application lifecycle.
     */
    protected final CheflingConfig appCheflingConfig = new CheflingConfig() {

        @Override
        public void apply(CheflingContainer container) {
            container.mapInstance(AppLifecycleListenable.class, appLifecycle);
        }

    };

    /**
     * Implementation of {@link AppLifecycleListener} that performs operations when the app is
     * created and finished.
     */
    @SuppressWarnings({"Anonymous2MethodRef", "Convert2Lambda"})
    protected final AppLifecycleListener appLifecycleListener = new PersistentAppLifecycleListener() {

        @Override
        public void onAppCreated(Class<?> origin) {
            // shuts down executor service immediately if still active
            if (executorService != null && !executorService.isShutdown()) {
                executorService.shutdownNow();
            }

            // create executor service
            executorService = Executors.newSingleThreadExecutor();

            // perform initialize on dedicated thread
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    initializeComponentApplication();
                }
            });
        }

        @Override
        public void onAppFinished(Class<?> origin) {
            // perform dispose on dedicated thread
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    disposeComponentApplication();
                }
            });

            // shut down executor service
            executorService.shutdown();
        }

    };

}
