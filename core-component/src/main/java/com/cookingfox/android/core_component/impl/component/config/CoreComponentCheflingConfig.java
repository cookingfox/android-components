package com.cookingfox.android.core_component.impl.component.config;

import android.app.AlarmManager;
import android.app.admin.DevicePolicyManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;

import com.cookingfox.android.core_component.api.android.context.AppContext;
import com.cookingfox.android.core_component.api.android.context.Environment;
import com.cookingfox.android.core_component.api.component.config.ComponentCheflingConfig;
import com.cookingfox.android.core_component.api.component.manager.ComponentManager;
import com.cookingfox.android.core_component.compatibility.rx.scheduler.AndroidRxSchedulerProvider;
import com.cookingfox.android.core_component.compatibility.rx.scheduler.RxSchedulerProvider;
import com.cookingfox.android.core_component.impl.android.context.AndroidAppContext;
import com.cookingfox.android.core_component.impl.android.context.AndroidEnvironment;
import com.cookingfox.android.core_component.impl.component.manager.AndroidComponentManager;
import com.cookingfox.android.core_component.impl.threading.ComponentThreadPoolProvider;
import com.cookingfox.android.core_component.testing.android.context.TestAppContext;
import com.cookingfox.android.core_component.testing.compatibility.rx.scheduler.TestRxSchedulerProvider;
import com.cookingfox.android.prefer.api.prefer.Prefer;
import com.cookingfox.android.prefer.impl.prefer.AndroidPrefer;
import com.cookingfox.android.prefer.impl.prefer.AndroidPreferProvider;
import com.cookingfox.android.prefer_rx.impl.prefer.AndroidRxPrefer;
import com.cookingfox.android.prefer_rx.impl.prefer.SharedPreferencesRxPrefer;
import com.cookingfox.android.prefer_testing.shared_preferences.InMemorySharedPreferences;
import com.cookingfox.chefling.api.CheflingBuilder;
import com.cookingfox.chefling.api.CheflingConfig;
import com.cookingfox.chefling.api.CheflingContainer;
import com.cookingfox.chefling.api.CheflingContainerListener;
import com.cookingfox.chefling.impl.Chefling;
import com.cookingfox.chefling.impl.helper.DefaultCheflingContainerListener;
import com.cookingfox.lapasse.api.message.store.MessageStore;
import com.cookingfox.lapasse.impl.message.store.NoStorageMessageStore;

import java.util.concurrent.Executors;

import static com.cookingfox.guava_preconditions.Preconditions.checkNotNull;
import static com.cookingfox.guava_preconditions.Preconditions.checkState;

/**
 * Chefling configuration for {@link AndroidComponentManager}.
 */
public class CoreComponentCheflingConfig implements ComponentCheflingConfig {

    /**
     * Reference to actual Android application context.
     */
    protected Context context;

    //----------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    //----------------------------------------------------------------------------------------------

    @Override
    public CheflingBuilder createCheflingBuilder() {
        return Chefling.createBuilder()
                .addConfig(libraryConfig)
                .addContainerListener(containerListener);
    }

    @Override
    public CheflingConfig createDeviceConfig() {
        return deviceConfig;
    }

    @Override
    public CheflingConfig createEmulatorConfig() {
        return deviceConfig;
    }

    @Override
    public CheflingConfig createTestConfig() {
        return testConfig;
    }

    /**
     * Set the Android context to use. Should be the application context.
     *
     * @param context The context to use.
     * @return The current instance, for method chaining.
     */
    public CoreComponentCheflingConfig setContext(Context context) {
        this.context = checkNotNull(context, "Context can not be null");
        return this;
    }

    //----------------------------------------------------------------------------------------------
    // CHEFLING CONTAINER EVENT LISTENER
    //----------------------------------------------------------------------------------------------

    public final CheflingContainerListener containerListener = new DefaultCheflingContainerListener() {

        @Override
        public void postBuilderApply(CheflingContainer container) {
            // initialize prefer
            AndroidPreferProvider.setDefault(container.getInstance(AndroidPrefer.class));
        }

        @Override
        public void preContainerDispose(CheflingContainer container) {
            // dispose prefer
            AndroidPreferProvider.disposeDefault();
        }

        @Override
        public void postContainerDispose(CheflingContainer container) {
            // has a mapping for thread pool provider? shut it down!
            if (container.hasInstanceOrMapping(ComponentThreadPoolProvider.class)) {
                container.getInstance(ComponentThreadPoolProvider.class).shutdown();
            }
        }

    };

    //----------------------------------------------------------------------------------------------
    // DEVICE CONFIGURATION
    //----------------------------------------------------------------------------------------------

    public final CheflingConfig deviceConfig = new CheflingConfig() {
        @Override
        public void apply(CheflingContainer container) {
            // throw if no android context
            checkState(context != null, "The Android application context must be provided");

            container.mapInstance(AppContext.class, new AndroidAppContext(context));

            container.mapFactory(AlarmManager.class,
                    c -> c.getInstance(AppContext.class).getAlarmManager());

            container.mapFactory(AssetManager.class,
                    c -> c.getInstance(AppContext.class).getAssetManager());

            container.mapFactory(AudioManager.class,
                    c -> c.getInstance(AppContext.class).getAudioManager());

            container.mapFactory(BluetoothAdapter.class,
                    c -> c.getInstance(AppContext.class).getBluetoothAdapter());

            container.mapFactory(BluetoothManager.class,
                    c -> c.getInstance(AppContext.class).getBluetoothManager());

            container.mapFactory(ConnectivityManager.class,
                    c -> c.getInstance(AppContext.class).getConnectivityManager());

            container.mapFactory(DevicePolicyManager.class,
                    c -> c.getInstance(AppContext.class).getDevicePolicyManager());

            container.mapFactory(PackageManager.class,
                    c -> c.getInstance(AppContext.class).getPackageManager());

            container.mapFactory(SharedPreferences.class,
                    c -> c.getInstance(AppContext.class).getSharedPreferences());

            container.mapFactory(WifiManager.class,
                    c -> c.getInstance(AppContext.class).getWifiManager());
        }
    };

    //----------------------------------------------------------------------------------------------
    // LIBRARY CONFIGURATION
    //----------------------------------------------------------------------------------------------

    public final CheflingConfig libraryConfig = container -> {
        // Core component
        container.mapType(ComponentManager.class, AndroidComponentManager.class);
        container.mapInstance(Environment.class, AndroidEnvironment.INSTANCE);
        container.mapType(RxSchedulerProvider.class, AndroidRxSchedulerProvider.class);
        container.mapFactory(ComponentThreadPoolProvider.class,
                c -> new ComponentThreadPoolProvider(Executors.newCachedThreadPool()));

        // LaPasse
        container.mapType(MessageStore.class, NoStorageMessageStore.class);

        // Prefer
        container.mapType(AndroidRxPrefer.class, SharedPreferencesRxPrefer.class);
        container.mapType(AndroidPrefer.class, AndroidRxPrefer.class);
        container.mapType(Prefer.class, AndroidPrefer.class);
    };

    //----------------------------------------------------------------------------------------------
    // TEST CONFIGURATION
    //----------------------------------------------------------------------------------------------

    public final CheflingConfig testConfig = container -> {
        // Core component
        container.mapType(AppContext.class, TestAppContext.class);

        // Prefer
        container.mapType(SharedPreferences.class, InMemorySharedPreferences.class);

        // Re-map Rx scheduler provider
        container.removeInstanceAndMapping(RxSchedulerProvider.class);
        container.mapType(RxSchedulerProvider.class, TestRxSchedulerProvider.class);
    };

}
