package fixtures.component;

import com.cookingfox.android.core_component.api.component.manager.ComponentManager;

/**
 * Contains three components that have a circular dependency on each other.
 */
public final class CircularDependencyThreeComponents {

    public static final class First extends AbstractComponent {
        @Override
        public void loadComponent(ComponentManager componentManager) {
            componentManager.loadComponent(Second.class);
        }
    }

    public static final class Second extends AbstractComponent {
        @Override
        public void loadComponent(ComponentManager componentManager) {
            componentManager.loadComponent(Third.class);
        }
    }

    public static final class Third extends AbstractComponent {
        @Override
        public void loadComponent(ComponentManager componentManager) {
            componentManager.loadComponent(First.class);
        }
    }

}
