package fixtures.component;

import com.cookingfox.android.core_component.api.component.manager.ComponentManager;

/**
 * Contains multiple components with dependencies.
 */
public final class MultipleDependencyComponents {

    public static final class FirstDependencyOnSecondAndFourth extends ValidComponent {
        @Override
        public void loadComponent(ComponentManager componentManager) {
            componentManager.loadComponent(SecondDependencyOnThird.class);
            componentManager.loadComponent(FourthDependencyOnSecondAndFifth.class);
        }
    }

    public static final class SecondDependencyOnThird extends ValidComponent {
        @Override
        public void loadComponent(ComponentManager componentManager) {
            componentManager.loadComponent(ThirdNoDependency.class);
        }
    }

    public static final class ThirdNoDependency extends ValidComponent {
        @Override
        public void loadComponent(ComponentManager componentManager) {
            // no dependency
        }
    }

    public static final class FourthDependencyOnSecondAndFifth extends ValidComponent {
        @Override
        public void loadComponent(ComponentManager componentManager) {
            componentManager.loadComponent(SecondDependencyOnThird.class);
            componentManager.loadComponent(FifthNoDependency.class);
        }
    }

    public static final class FifthNoDependency extends ValidComponent {
        @Override
        public void loadComponent(ComponentManager componentManager) {
            // no dependency
        }
    }

}
