package fixtures.component;

import com.cookingfox.lapasse.api.facade.Facade;
import com.cookingfox.lapasse.impl.facade.LaPasseFacade;

/**
 * Component implementation that tracks {@link LaPasseFacade#dispose()} call.
 */
public class DisposableLapasseFacadeComponent extends ValidComponent {

    public boolean disposeCalled = false;
    public LaPasseFacade<EmptyState> facade;

    public DisposableLapasseFacadeComponent() {
        LaPasseFacade.Builder<EmptyState> builder = new LaPasseFacade.Builder<>(new EmptyState());

        facade = new LaPasseFacade<EmptyState>(builder.getCommandBus(), builder.getEventBus(),
                builder.getLoggersHelper(), builder.getMessageStore(), builder.getStateManager()) {
            @Override
            public void dispose() {
                super.dispose();

                disposeCalled = true;
            }
        };
    }

    @Override
    public Facade getLaPasseFacade() {
        return facade;
    }

}
