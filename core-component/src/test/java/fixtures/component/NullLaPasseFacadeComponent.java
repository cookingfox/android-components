package fixtures.component;

import com.cookingfox.lapasse.api.facade.Facade;

/**
 * Component which returns `null` in {@link #getLaPasseFacade()}.
 */
public class NullLaPasseFacadeComponent extends ValidComponent {

    @Override
    public Facade getLaPasseFacade() {
        return null;
    }

}
