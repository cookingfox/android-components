package fixtures.component;

import com.cookingfox.android.core_component.api.component.manager.ComponentManager;

/**
 * Fixture component that throws an exception in its loadComponent method.
 */
public class ThrowingLoadMethodComponent extends AbstractComponent {

    @Override
    public void loadComponent(ComponentManager componentManager) {
        throw new RuntimeException("Example error during loadComponent method");
    }

}
