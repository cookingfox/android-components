package fixtures.component;

import com.cookingfox.android.core_component.api.component.Component;
import com.cookingfox.android.core_component.api.component.config.ComponentCheflingConfig;
import com.cookingfox.android.core_component.api.component.manager.ComponentManager;
import com.cookingfox.android.prefer.api.pref.PrefGroup;
import com.cookingfox.android.prefer.api.pref.Prefs;
import com.cookingfox.android.prefer_testing.fixtures.Key;
import com.cookingfox.lapasse.api.facade.Facade;
import com.cookingfox.lapasse.api.state.State;
import com.cookingfox.lapasse.impl.facade.LaPasseFacade;

import fixtures.prefer.MinimalPrefGroup;

/**
 * Component with correct behavior.
 */
public class ValidComponent implements Component {

    private boolean registered = false;

    @Override
    public ComponentCheflingConfig getCheflingConfig() {
        return new NoopComponentCheflingConfig();
    }

    @Override
    public Facade getLaPasseFacade() {
        // facade for empty state object
        return new LaPasseFacade.Builder<>(new EmptyState()).build();
    }

    @Override
    public Prefs getPrefs() {
        // test implementation
        return new Prefs<Key>() {
            @Override
            public PrefGroup<Key> getPrefGroup() {
                return new MinimalPrefGroup<>(Key.class);
            }
        };
    }

    @Override
    public void loadComponent(ComponentManager componentManager) {
        // no component dependencies
    }

    @Override
    public void registerComponent() {
        registered = true;
    }

    @Override
    public void unregisterComponent() {
        registered = false;
    }

    public boolean isRegistered() {
        return registered;
    }

    public static final class EmptyState implements State {
    }

}
