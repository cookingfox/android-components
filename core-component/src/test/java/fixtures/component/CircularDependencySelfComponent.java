package fixtures.component;

import com.cookingfox.android.core_component.api.component.manager.ComponentManager;

/**
 * A component that has a circular dependency on itself.
 */
public class CircularDependencySelfComponent extends AbstractComponent {

    @Override
    public void loadComponent(ComponentManager componentManager) {
        componentManager.loadComponent(getClass());
    }

}
