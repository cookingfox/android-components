package fixtures.component;

import com.cookingfox.android.core_component.api.component.config.ComponentCheflingConfig;

/**
 * A component that returns `null` in {@link #getCheflingConfig()}.
 */
public class NullCheflingConfigComponent extends ValidComponent {

    @Override
    public ComponentCheflingConfig getCheflingConfig() {
        return null;
    }

}
