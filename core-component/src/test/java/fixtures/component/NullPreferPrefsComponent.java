package fixtures.component;

import com.cookingfox.android.prefer.api.pref.Prefs;

/**
 * Component which returns `null` in {@link #getPrefs()}.
 */
public class NullPreferPrefsComponent extends ValidComponent {

    @Override
    public Prefs getPrefs() {
        return null;
    }

}
