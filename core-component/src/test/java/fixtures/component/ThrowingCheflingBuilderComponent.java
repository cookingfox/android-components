package fixtures.component;

import com.cookingfox.android.core_component.api.component.config.ComponentCheflingConfig;
import com.cookingfox.chefling.api.CheflingBuilder;
import com.cookingfox.chefling.impl.Chefling;

/**
 * A component whose {@link #getCheflingConfig()} will throw during container build.
 */
public class ThrowingCheflingBuilderComponent extends ValidComponent {

    @Override
    public ComponentCheflingConfig getCheflingConfig() {
        return new NoopComponentCheflingConfig() {
            @Override
            public CheflingBuilder createCheflingBuilder() {
                return Chefling.createBuilder().addConfig(container -> {
                    throw new RuntimeException("Example exception");
                });
            }
        };
    }

}
