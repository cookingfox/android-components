package fixtures.component;

import com.cookingfox.android.prefer.api.pref.PrefGroup;
import com.cookingfox.android.prefer.api.pref.Prefs;
import com.cookingfox.android.prefer.impl.prefer.AndroidPrefer;
import com.cookingfox.android.prefer_testing.fixtures.Key;

/**
 * Component that does not add its Pref group to Prefer.
 */
public class PrefGroupNotAddedComponent extends ValidComponent implements Prefs<Key> {

    final AndroidPrefer prefer;

    public PrefGroupNotAddedComponent(AndroidPrefer prefer) {
        this.prefer = prefer;
    }

    @Override
    public Prefs getPrefs() {
        return this;
    }

    @Override
    public PrefGroup<Key> getPrefGroup() {
        return prefer.newGroup(Key.class);
    }

}
