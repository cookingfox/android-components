package fixtures.component;

import com.cookingfox.android.core_component.api.component.config.ComponentCheflingConfig;
import com.cookingfox.chefling.api.CheflingBuilder;
import com.cookingfox.chefling.api.CheflingConfig;
import com.cookingfox.chefling.impl.Chefling;

/**
 * No-operation implementation of {@link ComponentCheflingConfig}.
 */
public class NoopComponentCheflingConfig implements ComponentCheflingConfig {

    @Override
    public CheflingBuilder createCheflingBuilder() {
        return Chefling.createBuilder();
    }

    @Override
    public CheflingConfig createDeviceConfig() {
        return null;
    }

    @Override
    public CheflingConfig createEmulatorConfig() {
        return null;
    }

    @Override
    public CheflingConfig createTestConfig() {
        return null;
    }

}
