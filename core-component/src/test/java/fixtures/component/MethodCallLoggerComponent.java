package fixtures.component;

import com.cookingfox.android.core_component.api.component.manager.ComponentManager;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Component implementation that logs the method calls.
 */
public class MethodCallLoggerComponent extends ValidComponent {

    public final AtomicInteger loadCalls = new AtomicInteger(0);

    @Override
    public void loadComponent(ComponentManager componentManager) {
        loadCalls.incrementAndGet();
    }

}
