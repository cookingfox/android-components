package fixtures.component;

import com.cookingfox.android.core_component.api.component.Component;
import com.cookingfox.android.core_component.api.component.config.ComponentCheflingConfig;
import com.cookingfox.android.core_component.api.component.manager.ComponentManager;
import com.cookingfox.android.prefer.api.pref.Prefs;
import com.cookingfox.lapasse.api.facade.Facade;

/**
 * Abstract implementation of Component with empty methods or methods returning null.
 */
abstract class AbstractComponent implements Component {

    @Override
    public ComponentCheflingConfig getCheflingConfig() {
        return null;
    }

    @Override
    public Facade getLaPasseFacade() {
        return null;
    }

    @Override
    public Prefs getPrefs() {
        return null;
    }

    @Override
    public void loadComponent(ComponentManager componentManager) {
    }

    @Override
    public void registerComponent() {
    }

    @Override
    public void unregisterComponent() {
    }

}
