package fixtures.prefer;

import com.cookingfox.android.prefer.api.pref.OnGroupValueChanged;
import com.cookingfox.android.prefer.api.pref.Pref;
import com.cookingfox.android.prefer.api.pref.PrefGroup;

import java.util.Iterator;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Minimal {@link PrefGroup} implementation.
 */
public class MinimalPrefGroup<K extends Enum<K>> implements PrefGroup<K> {

    final Class<K> keyClass;

    public MinimalPrefGroup(Class<K> keyClass) {
        this.keyClass = checkNotNull(keyClass);
    }

    @Override
    public void addGroupValueChangedListener(OnGroupValueChanged<K> listener) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void addPref(Pref<K, ?> pref) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Pref<K, ?> findPref(K k) {
        throw new UnsupportedOperationException();
    }

    @Override
    public <V> Pref<K, V> findPref(K k, Class<V> aClass) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Class<K> getKeyClass() {
        return keyClass;
    }

    @Override
    public void removeGroupValueChangedListener(OnGroupValueChanged<K> listener) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Iterator<Pref<K, ?>> iterator() {
        throw new UnsupportedOperationException();
    }

}
