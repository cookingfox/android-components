package com.cookingfox.android.core_component.impl.component.manager;

import com.cookingfox.android.core_component.api.component.Component;
import com.cookingfox.android.core_component.api.component.manager.ComponentManagerException;
import com.cookingfox.android.core_component.testing.component.manager.TestComponentManager;
import com.cookingfox.android.prefer.api.prefer.Prefer;
import com.cookingfox.chefling.api.CheflingContainer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fixtures.component.CircularDependencySelfComponent;
import fixtures.component.CircularDependencyThreeComponents;
import fixtures.component.DisposableLapasseFacadeComponent;
import fixtures.component.MethodCallLoggerComponent;
import fixtures.component.MultipleDependencyComponents.FirstDependencyOnSecondAndFourth;
import fixtures.component.NullCheflingConfigComponent;
import fixtures.component.NullLaPasseFacadeComponent;
import fixtures.component.NullPreferPrefsComponent;
import fixtures.component.PrefGroupNotAddedComponent;
import fixtures.component.ThrowingCheflingBuilderComponent;
import fixtures.component.ThrowingLoadMethodComponent;
import fixtures.component.ValidComponent;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for {@link AndroidComponentManager}.
 */
public class AndroidComponentManagerTest {

    //----------------------------------------------------------------------------------------------
    // TEST SETUP & TEARDOWN
    //----------------------------------------------------------------------------------------------

    AndroidComponentManager componentManager;
    CheflingContainer container;

    @Before
    public void setUp() throws Exception {
        container = TestComponentManager.buildContainer();

        componentManager = new AndroidComponentManager(container);
    }

    @After
    public void tearDown() throws Exception {
        componentManager.unloadAllComponents();
        container.disposeContainer();
    }

    //----------------------------------------------------------------------------------------------
    // TESTS: constructor
    //----------------------------------------------------------------------------------------------

    @Test(expected = NullPointerException.class)
    public void constructor_should_throw_if_container_null() throws Exception {
        new AndroidComponentManager(null);
    }

    //----------------------------------------------------------------------------------------------
    // TESTS: isComponentLoaded
    //----------------------------------------------------------------------------------------------

    @Test(expected = NullPointerException.class)
    public void isComponentLoaded_should_throw_if_class_null() throws Exception {
        componentManager.isComponentLoaded(null);
    }

    @Test
    public void isComponentLoaded_should_be_false_for_not_loaded() throws Exception {
        assertFalse(componentManager.isComponentLoaded(MethodCallLoggerComponent.class));

        componentManager.loadComponent(MethodCallLoggerComponent.class);

        assertTrue(componentManager.isComponentLoaded(MethodCallLoggerComponent.class));
    }

    //----------------------------------------------------------------------------------------------
    // TESTS: loadComponent
    //----------------------------------------------------------------------------------------------

    @Test(expected = NullPointerException.class)
    public void loadComponent_should_throw_if_class_null() throws Exception {
        componentManager.loadComponent(null);
    }

    @Test
    public void loadComponent_should_call_the_load_method_of_the_component() throws Exception {
        componentManager.loadComponent(MethodCallLoggerComponent.class);

        MethodCallLoggerComponent component = container.getInstance(MethodCallLoggerComponent.class);

        assertTrue(component.loadCalls.get() == 1);
    }

    @Test
    public void loadComponent_should_not_ignore_already_loaded_components() throws Exception {
        componentManager.loadComponent(MethodCallLoggerComponent.class);
        componentManager.loadComponent(MethodCallLoggerComponent.class);
        componentManager.loadComponent(MethodCallLoggerComponent.class);

        MethodCallLoggerComponent component = container.getInstance(MethodCallLoggerComponent.class);

        assertTrue(component.loadCalls.get() == 1);
    }

    @Test(expected = ComponentManagerException.class)
    public void loadComponent_should_detect_a_circular_dependency_on_component_self() throws Exception {
        componentManager.loadComponent(CircularDependencySelfComponent.class);
    }

    @Test(expected = ComponentManagerException.class)
    public void loadComponent_should_detect_a_circular_dependency_on_multiple_components() throws Exception {
        componentManager.loadComponent(CircularDependencyThreeComponents.First.class);
    }

    @Test(expected = ComponentManagerException.class)
    public void loadComponent_should_throw_for_component_construction_error() throws Exception {
        componentManager.loadComponent(Component.class);
    }

    @Test(expected = ComponentManagerException.class)
    public void loadComponent_should_throw_for_component_load_error() throws Exception {
        componentManager.loadComponent(ThrowingLoadMethodComponent.class);
    }

    @Test(expected = ComponentManagerException.class)
    public void loadComponent_should_throw_for_null_chefling_builder() throws Exception {
        componentManager.loadComponent(NullCheflingConfigComponent.class);
    }

    @Test(expected = ComponentManagerException.class)
    public void loadComponent_should_throw_for_throwing_chefling_builder() throws Exception {
        componentManager.loadComponent(ThrowingCheflingBuilderComponent.class);
    }

    @Test(expected = ComponentManagerException.class)
    public void loadComponent_should_throw_for_null_lapasse_facade() throws Exception {
        componentManager.loadComponent(NullLaPasseFacadeComponent.class);
    }

    @Test(expected = ComponentManagerException.class)
    public void loadComponent_should_throw_for_null_prefs() throws Exception {
        componentManager.loadComponent(NullPreferPrefsComponent.class);
    }

    @Test
    public void loadComponent_should_add_pref_group_if_needed() throws Exception {
        Prefer prefer = container.getInstance(Prefer.class);
        PrefGroupNotAddedComponent component = container.getInstance(PrefGroupNotAddedComponent.class);

        assertNull(prefer.findGroup(component.getPrefGroup().getKeyClass()));

        componentManager.loadComponent(PrefGroupNotAddedComponent.class);

        assertNotNull(prefer.findGroup(component.getPrefGroup().getKeyClass()));
    }

    @Test
    public void loadComponent_should_call_registerComponent() throws Exception {
        ValidComponent component = container.getInstance(ValidComponent.class);

        assertFalse(component.isRegistered());

        componentManager.loadComponent(ValidComponent.class);

        assertTrue(component.isRegistered());
    }

    //----------------------------------------------------------------------------------------------
    // TESTS: unloadAllComponents
    //----------------------------------------------------------------------------------------------

    @Test
    public void unloadAllComponents_should_not_throw_if_no_components_loaded() throws Exception {
        componentManager.unloadAllComponents();
    }

    @Test
    public void unloadAllComponents_should_clear_loaded_components() throws Exception {
        // load component with dependencies
        componentManager.loadComponent(FirstDependencyOnSecondAndFourth.class);

        assertTrue(componentManager.loadedComponents.size() == 5);

        componentManager.unloadAllComponents();

        assertTrue(componentManager.loadedComponents.size() == 0);
    }

    @Test
    public void unloadAllComponents_should_call_unregisterComponent() throws Exception {
        ValidComponent component = container.getInstance(ValidComponent.class);

        assertFalse(component.isRegistered());

        componentManager.loadComponent(ValidComponent.class);

        assertTrue(component.isRegistered());

        componentManager.unloadAllComponents();

        assertFalse(component.isRegistered());
    }

    @Test
    public void unloadAllComponents_should_dispose_lapasse_facade() throws Exception {
        componentManager.loadComponent(DisposableLapasseFacadeComponent.class);

        DisposableLapasseFacadeComponent component = container.getInstance(DisposableLapasseFacadeComponent.class);

        assertFalse(component.disposeCalled);

        componentManager.unloadAllComponents();

        assertTrue(component.disposeCalled);
    }

}
