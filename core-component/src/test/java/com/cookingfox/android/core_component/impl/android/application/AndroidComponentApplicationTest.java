package com.cookingfox.android.core_component.impl.android.application;

import android.app.Application;

import org.junit.Test;

import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

/**
 * Unit tests for {@link AndroidComponentApplication}.
 */
public class AndroidComponentApplicationTest {

    //----------------------------------------------------------------------------------------------
    // TESTS: create
    //----------------------------------------------------------------------------------------------

    @Test(expected = NullPointerException.class)
    public void create_should_throw_if_null() throws Exception {
        AndroidComponentApplication.create(null);
    }

    @Test
    public void create_should_create_component_app_with_mock() throws Exception {
        Application mockApp = mockApp();

        AndroidComponentApplication result = AndroidComponentApplication.create(mockApp);

        assertSame(mockApp, result.app);
    }

    @Test
    public void create_should_call_initialize() throws Exception {
        AtomicBoolean initializeCalled = new AtomicBoolean(false);

        AndroidComponentApplication delegate = new AndroidComponentApplication(mockApp()) {
            @Override
            protected void initializeCore() {
                initializeCalled.set(true);
            }
        };

        AndroidComponentApplication.create(delegate);

        assertTrue(initializeCalled.get());
    }

    @Test(expected = IllegalStateException.class)
    public void create_should_throw_if_delegate_app_not_provided() throws Exception {
        // delegate `Application` is not provided
        AndroidComponentApplication.create(new AndroidComponentApplication());
    }

    //----------------------------------------------------------------------------------------------
    // HELPER METHODS
    //----------------------------------------------------------------------------------------------

    private Application mockApp() {
        return mock(Application.class);
    }

}
