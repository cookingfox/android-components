package com.cookingfox.android.core_component.impl.android.context;

import com.cookingfox.android.core_component.api.android.context.Environment;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for {@link AndroidEnvironment}.
 */
public class AndroidEnvironmentTest {

    AndroidEnvironment environment;

    @Before
    public void setUp() throws Exception {
        environment = AndroidEnvironment.INSTANCE;
    }

    @Test
    public void getId_should_return_unit_test() throws Exception {
        Environment.Id result = environment.getId();

        assertEquals(Environment.Id.Test, result);
    }

    @Test
    public void isDevice_should_return_false() throws Exception {
        boolean result = environment.isDevice();

        assertFalse(result);
    }

    @Test
    public void isEmulator_should_return_false() throws Exception {
        boolean result = environment.isEmulator();

        assertFalse(result);
    }

    @Test
    public void isTest_should_return_true() throws Exception {
        boolean result = environment.isTest();

        assertTrue(result);
    }

}
