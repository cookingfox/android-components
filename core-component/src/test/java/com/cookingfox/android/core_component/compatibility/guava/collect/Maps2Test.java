package com.cookingfox.android.core_component.compatibility.guava.collect;

import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Unit tests for {@link Maps2}.
 */
public class Maps2Test {

    //----------------------------------------------------------------------------------------------
    // TESTS: index
    //----------------------------------------------------------------------------------------------

    @Test
    public void index_should_create_immutable_map_with_overwritable_keys() throws Exception {
        Person a = new Person(1, "a");
        Person b_1 = new Person(2, "b_1");
        Person b_2 = new Person(2, "b_2");
        Person b_3 = new Person(2, "b_3");
        Person c = new Person(3, "c");

        List<Person> personList = new ArrayList<>();
        personList.add(a);
        personList.add(b_1);
        personList.add(b_2);
        personList.add(b_3);
        personList.add(c);

        Map<Integer, Person> actual = Maps2.index(personList, input -> input.age);

        Map<Integer, Person> expected = new LinkedHashMap<>();
        expected.put(1, a);
        expected.put(2, b_3);
        expected.put(3, c);

        assertEquals(expected, actual);
    }

    @Test
    public void index_should_accept_iterator() throws Exception {
        Person a = new Person(1, "a");
        Person b_1 = new Person(2, "b_1");
        Person b_2 = new Person(2, "b_2");
        Person b_3 = new Person(2, "b_3");
        Person c = new Person(3, "c");

        List<Person> personList = new ArrayList<>();
        personList.add(a);
        personList.add(b_1);
        personList.add(b_2);
        personList.add(b_3);
        personList.add(c);

        Map<Integer, Person> actual = Maps2.index(personList.iterator(), input -> input.age);

        Map<Integer, Person> expected = new LinkedHashMap<>();
        expected.put(1, a);
        expected.put(2, b_3);
        expected.put(3, c);

        assertEquals(expected, actual);
    }

    @Test(expected = NullPointerException.class)
    public void index_should_throw_if_value_null() throws Exception {
        List<Person> personList = new ArrayList<>();
        personList.add(null);

        Maps2.index(personList, input -> input.age);
    }

    @Test(expected = NullPointerException.class)
    public void index_should_throw_if_key_function_produces_null() throws Exception {
        List<Person> personList = new ArrayList<>();
        personList.add(new Person(1, "a"));

        Maps2.index(personList, input -> null);
    }

    //----------------------------------------------------------------------------------------------
    // HELPER CLASSES
    //----------------------------------------------------------------------------------------------

    public static class Person {
        public final int age;
        public final String name;

        public Person(int age, String name) {
            this.age = age;
            this.name = name;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Person person = (Person) o;

            if (age != person.age) return false;
            return name.equals(person.name);

        }

        @Override
        public int hashCode() {
            int result = age;
            result = 31 * result + name.hashCode();
            return result;
        }
    }

}
