package com.cookingfox.android.core_component.compatibility.chefling;

import com.cookingfox.chefling.api.CheflingContainer;
import com.cookingfox.chefling.impl.Chefling;

import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * Unit tests for {@link CheflingContainerProvider}.
 */
public class CheflingContainerProviderTest {

    //----------------------------------------------------------------------------------------------
    // TEST SETUP
    //----------------------------------------------------------------------------------------------

    @After
    public void tearDown() throws Exception {
        CheflingContainerProvider.container = null;
    }

    //----------------------------------------------------------------------------------------------
    // TESTS: disposeContainer
    //----------------------------------------------------------------------------------------------

    @Test(expected = NullPointerException.class)
    public void disposeContainer_should_throw_if_no_instance_set() throws Exception {
        CheflingContainerProvider.disposeContainer();
    }

    @Test
    public void disposeContainer_should_dispose_container() throws Exception {
        CheflingContainer mockContainer = mock(CheflingContainer.class);
        CheflingContainerProvider.container = mockContainer;
        CheflingContainerProvider.disposeContainer();

        verify(mockContainer).disposeContainer();
    }

    //----------------------------------------------------------------------------------------------
    // TESTS: getContainer
    //----------------------------------------------------------------------------------------------

    @Test(expected = NullPointerException.class)
    public void getContainer_should_throw_if_no_instance_set() throws Exception {
        CheflingContainerProvider.getContainer();
    }

    @Test
    public void getContainer_should_return_instance() throws Exception {
        CheflingContainer container = Chefling.createContainer();
        CheflingContainerProvider.container = container;

        assertSame(container, CheflingContainerProvider.getContainer());
    }

    //----------------------------------------------------------------------------------------------
    // TESTS: setContainer
    //----------------------------------------------------------------------------------------------

    @Test(expected = NullPointerException.class)
    public void setContainer_should_throw_if_null() throws Exception {
        CheflingContainerProvider.setContainer(null);
    }

    @Test(expected = IllegalStateException.class)
    public void setContainer_should_throw_if_already_set() throws Exception {
        CheflingContainerProvider.container = Chefling.createContainer();

        CheflingContainerProvider.setContainer(Chefling.createContainer());
    }

    @Test
    public void setContainer_should_set_instance() throws Exception {
        CheflingContainer container = Chefling.createContainer();

        CheflingContainerProvider.setContainer(container);

        assertSame(container, CheflingContainerProvider.container);
    }

}
