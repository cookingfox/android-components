package com.cookingfox.android.bluetooth_management.sample;

import com.cookingfox.android.bluetooth_management.component.BluetoothManagementComponent;
import com.cookingfox.android.core_component.impl.android.application.AndroidComponentApplication;

public class BluetoothManagementSampleApp extends AndroidComponentApplication {

    @Override
    protected void onApplicationInitialized() {
        getComponentManager().loadComponent(BluetoothManagementComponent.class);
    }

}
