package com.cookingfox.android.bluetooth_management.sample;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.cookingfox.android.bluetooth_management.domain.event.BluetoothScanResultsUpdated;
import com.cookingfox.android.bluetooth_management.domain.manager.BluetoothAdapterManager;
import com.cookingfox.android.bluetooth_management.domain.manager.BluetoothScanningManager;
import com.cookingfox.android.bluetooth_management.domain.state.BluetoothManagementStateObserver;
import com.cookingfox.android.bluetooth_management.domain.vo.BluetoothAdapterInfo;
import com.cookingfox.android.core_component.impl.android.activity.ComponentsAwareAppCompatActivity;
import com.cookingfox.chefling.api.CheflingContainer;
import com.cookingfox.logging.Logger;

import rx.android.schedulers.AndroidSchedulers;

public class BluetoothManagementSampleActivity extends ComponentsAwareAppCompatActivity {

    private Button enableAdapterBtn;
    private Button disableAdapterBtn;
    private Button startScanningBtn;
    private Button stopScanningBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(view -> showSettings());

        enableAdapterBtn = (Button) findViewById(R.id.btn_enable_adapter);
        disableAdapterBtn = (Button) findViewById(R.id.btn_disable_adapter);
        startScanningBtn = (Button) findViewById(R.id.btn_start_scanning);
        stopScanningBtn = (Button) findViewById(R.id.btn_stop_scanning);
    }

    @Override
    public void onComponentsLoaded(CheflingContainer container) {
        final BluetoothAdapterManager bluetoothAdapterManager = container.getInstance(BluetoothAdapterManager.class);
        final BluetoothScanningManager bluetoothScanningManager = container.getInstance(BluetoothScanningManager.class);

        // Button for enabling Bluetooth Adapter
        enableAdapterBtn.setOnClickListener(v -> bluetoothAdapterManager.enableBluetoothAdapter());

        // Button for disabling Bluetooth Adapter
        disableAdapterBtn.setOnClickListener(v -> bluetoothAdapterManager.disableBluetoothAdapter());

        // Button for start scanning.
        startScanningBtn.setOnClickListener(v -> bluetoothScanningManager.startScanning());

        // Button for stop scanning.
        stopScanningBtn.setOnClickListener(v -> bluetoothScanningManager.stopScanning());

        BluetoothManagementStateObserver stateObserver = container.getInstance(BluetoothManagementStateObserver.class);

        stateObserver.observeStateChanges()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(stateChanged -> {
                    updateAdapterStateConsole(stateChanged.getState().info());

                    if (stateChanged.getEvent() instanceof BluetoothScanResultsUpdated) {
                        Logger.info("Detection Scan Result: %s", stateChanged.getEvent());
                    }
                });

        updateAdapterStateConsole(stateObserver.getCurrentState().info());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            showSettings();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //----------------------------------------------------------------------------------------------
    // PRIVATE METHODS
    //----------------------------------------------------------------------------------------------

    private void showSettings() {
        startActivity(new Intent(this, BluetoothManagementSamplePreferActivity.class));
    }

    /**
     * Is fired every time the Bluetooth Adapter State changes.
     *
     * @param adapterInfo Updated adapter state.
     */
    private void updateAdapterStateConsole(BluetoothAdapterInfo adapterInfo) {
        final TextView adapterStatusConsole = (TextView) findViewById(R.id.adapterStateConsole);
        if (adapterStatusConsole == null) return;

        if (adapterInfo.isOn())
            adapterStatusConsole.setText(R.string.adapter_on_status_text);
        if (adapterInfo.isOff())
            adapterStatusConsole.setText(R.string.adapter_off_status_text);
        if (adapterInfo.isTurningOn())
            adapterStatusConsole.setText(R.string.adapter_turning_on_status_text);
        if (adapterInfo.isTurningOff())
            adapterStatusConsole.setText(R.string.adapter_turning_off_status_text);
    }
}
